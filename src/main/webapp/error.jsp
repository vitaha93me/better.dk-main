<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Server Error</title>
    </head>

    <body>
        <h1>Ups, der skete en fejl!</h1>
        <p>Vi beklager - denne side kan desværre ikke vises lige nu. Prøv venligst igen senere, eller kontakt os hvis problemet fortsætter.</p>
    </body>
</html>