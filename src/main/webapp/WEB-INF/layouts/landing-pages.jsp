<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="tilesx" uri="http://tiles.apache.org/tags-tiles-extras" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<security:authorize access="isAuthenticated()" var="isLoggedIn" />
<fmt:setLocale value="da_DK" scope="session" /> <%-- improve: Make dynamic and perhaps configure this in AppConfig? --%>

<tiles:importAttribute name="scripts" />
<tiles:importAttribute name="stylesheets" />
<tiles:importAttribute name="trackPageView" />
<tiles:importAttribute name="principal" />

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <link rel="shortcut icon" href="/favicon.ico" />
        <title><c:out value="${title}" /></title>

        <script type="text/javascript">
            var URL_ROOT_DOMAIN = "<custom:domainNameUri />";
            var IS_LOGGED_IN = ${isLoggedIn};
        </script>

        <tiles:insertAttribute name="principal" />

        <script src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/js/jquery.min.js"></script>
        <script src="/js/better.js"></script>
        <script src="/js/main.js"></script>

        <script type="text/javascript">
            !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","group","track","ready","alias","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="3.0.1";
                analytics.load("<c:out value="${segmentWriteKey}" />");

                <c:if test="${trackPageView}">
                    analytics.page();
                </c:if>
            }}();

            <c:if test="${isLoggedIn}">
                if (document.cookie.indexOf('analytics-identified') == -1) {
                    <security:authentication property="principal.uuid" var="uuid" />
                    analytics.identify('<c:out value="${uuid}" />');

                    BETTER.setCookie('analytics-identified', '1', null, BETTER.getRootDomain());
                }
            </c:if>
        </script>

        <c:if test="${!empty viewport}">
            <meta name="viewport" content="<c:out value="${viewport}" />" />
        </c:if>

        <c:if test="${!empty metas}">
            <c:forEach var="entry" items="${metas}">
                <meta name="${entry.key}" content="<c:out value="${entry.value}" />" />
            </c:forEach>
        </c:if>

        <c:if test="${!empty metaProperties}">
            <c:forEach var="entry" items="${metaProperties}">
                <meta property="${entry.key}" content="<c:out value="${entry.value}" />" />
            </c:forEach>
        </c:if>

        <c:forEach var="stylesheet" items="${stylesheets}">
            <link rel="stylesheet" type="text/css" href="${stylesheet}" />
        </c:forEach>
    </head>

    <body>
        <tiles:insertAttribute name="content" />

        <c:forEach var="script" items="${scripts}">
            <script src="${script}"></script>
        </c:forEach>
    </body>
</html>