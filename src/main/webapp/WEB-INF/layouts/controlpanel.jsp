<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<security:authorize access="isAuthenticated()" var="isLoggedIn"/>
<fmt:setLocale value="da_DK" scope="session"/> <%-- improve: Make dynamic and perhaps configure this in AppConfig? --%>

<tiles:importAttribute name="scripts"/>
<tiles:importAttribute name="stylesheets"/>
<tiles:importAttribute name="choose-password-modal"/>
<tiles:importAttribute name="principal"/>

<%-- Generate URL postfix --%>
<c:if test="${!empty param.token}">
    <c:set var="urlPostfix" value="token=${param.token}"/>
</c:if>

<c:if test="${!empty param.checksum}">
    <c:set var="urlPostfix"
           value="${!empty urlPostfix ? urlPostfix += '&checksum=' += param.checksum : 'checksum' += param.checksum}"/>
</c:if>

<c:set var="urlPostfix" scope="request" value="${!empty urlPostfix ? '?' += urlPostfix : ''}"/>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <link rel="shortcut icon" href="<custom:domainNameUri />/favicon.ico">

    <title><c:out value="${title}"/></title>

    <script type="text/javascript">
        var URL_ROOT_DOMAIN = "<custom:domainNameUri />";
        var IS_LOGGED_IN = ${isLoggedIn};
        var IS_CLAIMED = ${!empty company.accountCompanies};
        var COMPANY_ID = <c:out value="${company.id}" />;
        var PUSHER_KEY = "<c:out value="${pusherKey}" />";

        <c:if test="${isLoggedIn}">
        var OWNED_COMPANIES = [
            <security:authentication property="principal.accountCompanies" var="accountCompanies" />

            <c:forEach var="ac" items="${accountCompanies}" varStatus="loop">
            {
                id: <c:out value="${ac.company.id}" />
            }

            <c:if test="${!loop.last}">, </c:if>
            </c:forEach>
        ];
        </c:if>
    </script>

    <tiles:insertAttribute name="principal"/>

    <script src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/js/jquery.min.js"></script>
    <script src="/js/pusher.min.js"></script>
    <script src="/js/better.js"></script>
    <script src="/js/messaging.js"></script>

    <script type="text/javascript">
        !function () {
            var analytics = window.analytics = window.analytics || [];
            if (!analytics.initialize)if (analytics.invoked)window.console && console.error && console.error("Segment snippet included twice."); else {
                analytics.invoked = !0;
                analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "group", "track", "ready", "alias", "page", "once", "off", "on"];
                analytics.factory = function (t) {
                    return function () {
                        var e = Array.prototype.slice.call(arguments);
                        e.unshift(t);
                        analytics.push(e);
                        return analytics
                    }
                };
                for (var t = 0; t < analytics.methods.length; t++) {
                    var e = analytics.methods[t];
                    analytics[e] = analytics.factory(e)
                }
                analytics.load = function (t) {
                    var e = document.createElement("script");
                    e.type = "text/javascript";
                    e.async = !0;
                    e.src = ("https:" === document.location.protocol ? "https://" : "http://") + "cdn.segment.com/analytics.js/v1/" + t + "/analytics.min.js";
                    var n = document.getElementsByTagName("script")[0];
                    n.parentNode.insertBefore(e, n)
                };
                analytics.SNIPPET_VERSION = "3.0.1";
                analytics.load("<c:out value="${segmentWriteKey}" />");
                analytics.page();
            }
        }();

        <c:if test="${isLoggedIn}">
        if (document.cookie.indexOf('analytics-identified') == -1) {
            <security:authentication property="principal.uuid" var="uuid" />
            analytics.identify('<c:out value="${uuid}" />');

            BETTER.setCookie('analytics-identified', '1', null, BETTER.getRootDomain());
        }
        </c:if>
    </script>

    <c:forEach var="stylesheet" items="${stylesheets}">
        <link rel="stylesheet" type="text/css" href="${stylesheet}"/>
    </c:forEach>
</head>

<body>
<tiles:insertAttribute name="topbar"/>
<div class="spacer-20"></div>

<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xs-1" style="padding: 0;">
                <tiles:insertAttribute name="menu"/>
            </div>

            <tiles:insertAttribute name="content"/>
        </div>
    </div>
</div>

<tiles:insertAttribute name="footer"/>
<div class="spacer-20"></div>

<tiles:insertAttribute name="choose-password-modal"/>

<c:forEach var="script" items="${scripts}">
    <script src="${script}"></script>
</c:forEach>
</body>
</html>