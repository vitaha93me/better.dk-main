<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<security:authorize access="isAuthenticated()" var="isLoggedIn" />

<div class="spacer-30"></div>

<div class="container" style="text-align: center;">
    <div class="row">
        <div class="col-xs-12" id="login-wrapper">
            <c:if test="${error == true}">
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Ups!</strong> Du kunne ikke logges ind. Enten er de indtastede informationer ikke korrekte, eller også er brugeren ikke blevet aktiveret.
                </div>
            </c:if>

            <c:choose>
                <c:when test="${success == true || isLoggedIn == true}">
                    <div class="alert alert-success" role="alert">
                        Du er nu logget ind.
                    </div>
                </c:when>

                <c:otherwise>
                    <h4 style="font-size: 24px; color: #4C4C4C; margin-top: 0; font-weight: bold;">Log ind på Better.dk</h4>

                    <p>Log ind med Facebook eller med adgangskode</p>

                    <form class="facebook-login" action="<custom:domainNameUri />/signin/facebook" method="POST">
                        <input type="hidden" name="hash" value="" />

                        <div class="btn fb-login flex flex-row justify-center align-center" style="width: 65%; height: 46px; padding: 11px; margin: 15px auto 15px auto;">
                            <span class="icon"></span><span>Log ind med Facebook</span>
                        </div>
                    </form>

                    <div style="text-align: center; margin-bottom: 10px;">
                        <hr style="width: 30%; display: inline-block; vertical-align: middle;" />
                        <span>eller</span>
                        <hr style="width: 30%; display: inline-block; vertical-align: middle;" />
                    </div>

                    <div id="login-with-email-cta" style="position: relative;">
                        <button class="btn btn-primary" style="width: 65%; height: 44px; margin-bottom: 5px;" id="login-with-email">Log ind med e-mail</button>

                        <p style="font-size: 14px; font-family: 'Roboto-Light';">Har du ikke en bruger? <a href="<custom:domainNameUri />/account/create">Så opret en gratis bruger her</a>.</p>
                    </div>

                    <div id="login-with-email-form-wrapper" style="opacity: 0; position: absolute; width: 92%;">
                        <form class="login form-horizontal" action="<custom:domainNameUri />/login/process" method="POST">
                            <input type="hidden" name="destination" value="/login?success" />

                            <div class="form-group">
                                <label for="login-dialog-login-email" class="col-xs-4 control-label">E-mail</label>

                                <div class="col-xs-8">
                                    <input name="username" id="login-dialog-login-email" class="form-control" style="width: 77%; height: 40px;" type="email" placeholder="E-mail" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="login-dialog-login-password" class="col-xs-4 control-label">Adgangskode</label>

                                <div class="col-xs-8">
                                    <input name="password" id="login-dialog-login-password" class="form-control" style="width: 77%; height: 40px;" type="password" placeholder="Adgangskode" />
                                </div>
                            </div>

                            <input type="submit" class="btn btn-primary" style="width: 67%; height: 44px; position: relative; left: 14px;" id="login-with-email-submit" value="Log ind" />
                        </form>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>

<div class="spacer-30"></div>