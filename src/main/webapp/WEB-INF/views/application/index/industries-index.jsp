<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<div class="spacer-30"></div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 style="margin-bottom: 10px;">Branche Oversigt</h1>

            <ul style="-webkit-column-count: 2; -moz-column-count: 2; column-count: 2;">
                <c:forEach var="industry" items="${industries}">
                    <c:set var="slug" value="${custom:slug(industry.name)}" />

                    <li>
                        <a href="/brancher/<c:out value="${industry.id}" />/<c:out value="${custom:encodeURIComponent(slug)}" />" title="<c:out value="${industry.name}" />">
                            <c:out value="${industry.name}" />
                        </a>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </div>
</div>

<div class="spacer-30"></div>