<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<div class="container-fluid" id="cover-photo">
    <div class="flex flex-column align-center justify-center top">
        <span class="small">Vi vil være</span>
        <span id="cover-photo-headline">Danmarks førende lokale søgemaskine</span>
        <span class="search-teaser light">Søg, find og bedøm alle <span class="bold">750.000+</span> danske virksomheder</span>

        <form action="" method="get" class="form-inline company-search-form" id="company-search-form">
            <div class="form-group">
                <span class="form-wrapper form-control search-terms-wrapper">
                    <label class="bold" for="search-terms">Søg</label>
                    <input type="text" name="q1" id="search-terms" class="light what" placeholder="fx efter tømrer, tandlæger, advokat" />
                </span>

                <span class="form-wrapper form-control search-location-wrapper">
                    <label class="bold" for="search-location">Nær</label>
                    <input type="text" name="q2" id="search-location" class="light where" placeholder="by, adresse, post nr." />
                </span>

                <button type="submit" class="btn btn-primary search-button flex flex-column align-center justify-center">
                    <span class="icon magnify" style="margin: 0;"></span>
                </button>
            </div>
        </form>
    </div>

    <div class="flex flex-row align-center" id="latest-reviews">
        <c:if test="${latestReviews != null}">
            <c:forEach var="review" items="${latestReviews}">
                <div class="review">
                    <div class="inner">
                        <span class="reviewer-company ellipsis-overflow">
                            <c:if test="${review.account != null && review.account.firstName != null}">
                                <c:out value="${review.account.firstName} ${review.account.lastName}" /> bedømte
                            </c:if>

                            <a href="<c:out value="${review.company.profileUrl}/bedoemmelser" />" title="<c:out value="${review.company.name}" />"><c:out value="${review.company.name}" /></a>
                        </span>
                        <span class="review-time light">Skrevet for <span class="review-time-relative" data-timestamp="<fmt:formatNumber type="number" value="${review.created.time}" groupingUsed="false" maxFractionDigits="0"  />"></span></span>
                        <div class="stars" data-stars="<c:out value="${review.rating}" />"></div>

                        <c:if test="${review.title != null}">
                            <span class="title bold ellipsis-overflow"><c:out value="${review.title}" /></span>
                        </c:if>

                        <p><c:out value="${review.text}" /></p>
                    </div>
                </div>
            </c:forEach>
        </c:if>
    </div>
</div>

<div class="container-fluid" id="most-popular-categories">
    <h2>Mest søgte kategorier</h2>

    <div class="flex flex-row justify-center light">
        <ul style="-webkit-column-count: 5; -moz-column-count: 5; column-count: 5;">
            <li><a href="/brancher/520/advokater" title="Advokater">Advokater</a></li>
            <li><a href="/brancher/277/bygge-og-anl%C3%A6gsentrepren%C3%B8rer" title="Anlægsentreprenører">Anlægsentreprenører</a></li>
            <li><a href="/brancher/526/arkitekter" title="Arkitekter">Arkitekter</a></li>
            <li><a href="/brancher/300/karosseriv%C3%A6rksteder-og-autolakering" title="Autolakering">Autolakering</a></li>
            <li><a href="/brancher/299/autov%C3%A6rksteder" title="Autoværksteder">Autoværksteder</a></li>
            <li><a href="/brancher/670/bedemand" title="Bedemænd">Bedemænd</a></li>
            <li><a href="/brancher/296/bilforhandlere" title="Bilforhandlere">Bilforhandlere</a></li>
            <li><a href="/brancher/518/boligadministration" title="Boligadministration">Boligadministration</a></li>
            <li><a href="/brancher/514/boligudlejning" title="Boligudlejning">Boligudlejning</a></li>
            <li><a href="/brancher/460/catering-og-diner-transportable" title="Catering">Catering</a></li>
            <li><a href="/brancher/478/computerprogrammering" title="Computerprogrammering">Computerprogrammering</a></li>
            <li><a href="/brancher/548/dyrl%C3%A6ge" title="Dyrlæge">Dyrlæge</a></li>
            <li><a href="/brancher/302/d%C3%A6kservice" title="Dækservice">Dækservice</a></li>
            <li><a href="/brancher/516/ejendomsm%C3%A6glere" title="Ejendomsmæglere">Ejendomsmæglere</a></li>
            <li><a href="/brancher/284/elektrikere" title="Elektrikere">Elektrikere</a></li>
            <li><a href="/brancher/430/flytteforretninger" title="Flytteforretninger">Flytteforretninger</a></li>
            <li><a href="/brancher/544/fotografer-og-fotografiske-virksomheder" title="Fotografer">Fotografer</a></li>
            <li><a href="/brancher/668/fris%C3%B8r" title="Frisører">Frisører</a></li>
            <li><a href="/brancher/609/fysioterapeuter-og-ergoterapeuter" title="Fysioterapeuter">Fysioterapeuter</a></li>
            <li><a href="/brancher/574/anl%C3%A6gsgartner-og-haveservice" title="Gartnere">Gartnere</a></li>
            <li><a href="/brancher/291/glarmestere" title="Glarmestere">Glarmestere</a></li>
            <li><a href="/brancher/447/godstransport-og-transportfirmaer" title="Godstransport">Godstransport</a></li>
            <li><a href="/brancher/611/kiropraktorer" title="Kiropraktorer">Kiropraktorer</a></li>
            <li><a href="/brancher/601/k%C3%B8reskole" title="Køreskoler">Køreskoler</a></li>
            <li><a href="/brancher/290/malere" title="Malere">Malere</a></li>
            <li><a href="/brancher/294/murere" title="Murere">Murere</a></li>
            <li><a href="/brancher/282/nedrivning" title="Nedrivning">Nedrivning</a></li>
            <li><a href="/brancher/412/optikere" title="Optikere">Optikere</a></li>
            <li><a href="/brancher/459/pizzeriaer-grillbarer-isbarer-mv" title="Pizzeriaer">Pizzeriaer</a></li>
            <li><a href="/brancher/610/psykologer-og-psykoterapeuter" title="Psykologer og psykoterapeuter">Psykologer og psykoterapeuter</a></li>
            <li><a href="/brancher/538/reklamebureauer" title="Reklamebureauer">Reklamebureauer</a></li>
            <li><a href="/brancher/570/reng%C3%B8ring" title="Rengøring">Rengøring</a></li>
            <li><a href="/brancher/458/restauranter" title="Restauranter">Restauranter</a></li>
            <li><a href="/brancher/669/sk%C3%B8nheds-og-hudpleje" title="Skønheds- og hudpleje">Skønheds- og hudpleje</a></li>
            <li><a href="/brancher/293/tagd%C3%A6kkere" title="Tagdækkere">Tagdækkere</a></li>
            <li><a href="/brancher/607/tandl%C3%A6ger" title="Tandlæger">Tandlæger</a></li>
            <li><a href="/brancher/288/t%C3%B8mrere-og-snedkere" title="Tømrere og snedkere">Tømrere og snedkere</a></li>
            <li><a href="/brancher/285/vvs-og-blikkenslagere" title="VVS og blikkenslagere">VVS og blikkenslagere</a></li>
            <li><a href="/brancher/571/vinduespolering" title="Vinduespolering">Vinduespolering</a></li>
            <li><a href="/brancher/429/vognmand" title="Vognmand">Vognmand</a></li>
        </ul>
    </div>

    <div class="" style="text-align: center; margin-top: 20px;">
        <a href="/brancher" title="Alle kategorier">Alle kategorier</a>
    </div>
</div>

<div class="container-fluid" id="about-user">
    <div class="container">
        <h2>Det kan du på better.dk</h2>

        <div class="row flex flex-row align-center">
            <div class="col-xs-6">
                <h3 class="bold">Sådan hjælper vi dig</h3>

                <ul>
                    <li>Skriv til alle virksomheder i Danmark (vi sørger for, at beskeden når frem)</li>
                    <li>Book bord hos alle restauranter i Danmark</li>
                    <li>Bestil tid hos alle frisører i Danmark</li>
                    <li>Book tid hos alle behandlere i Danmark</li>
                    <li>Bestil tid hos alle tandlæger i Danmark</li>
                    <li>Få et tilbud fra alle håndværkere i Danmark</li>
                    <li>Bestil tid hos alle autoværksteder i Danmark</li>
                </ul>

                <a href="#" class="btn btn-primary squared" id="about-user-cta">Udforsk Better og find det du søger</a>
            </div>

            <div class="col-xs-6 bg"></div>
        </div>
    </div>
</div>

<div class="container-fluid" id="about-company">
    <div class="container">
        <h2>Vi vækster lokale virksomheder</h2>

        <div class="row flex flex-row align-center">
            <div class="col-xs-6 bg"></div>

            <div class="col-xs-6">
                <h3 class="bold">Vi hjælper din virksomhed med at:</h3>

                <ul>
                    <li>Lave et BetterSite, som vil kunne bruges som dit primære website</li>
                    <li>Få flere lokale kunder</li>
                    <li>Få lavet en Google virksomhedsprofil</li>
                    <li>Bliv fundet i toppen på Google</li>
                    <li>Indsamle bedømmelser og modtage feedback fra kunderne</li>
                    <li>Gøre det nemt at kommunikere med dine kunder</li>
                </ul>

                <a href="mailto:kim@better.dk" class="btn btn-primary squared">Kom i gang gratis nu</a>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid flex flex-row justify-center align-center" id="partners">
    <div class="hungry-logo"></div>
    <div class="tre-byggetilbud-logo"></div>
    <div class="google-logo"></div>
    <div class="bing-logo"></div>
    <div class="yahoo-logo"></div>
</div>