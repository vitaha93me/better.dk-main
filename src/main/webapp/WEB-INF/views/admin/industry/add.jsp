<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url var="actionUrl" value="/admin/industry/add" />

<h1>Add Industries</h1>

<c:if test="${success}">
    The industries were added succesfully!
</c:if>

<form:form action="${actionUrl}" commandName="dto" method="POST" acceptCharset="UTF-8">
    <form:textarea name="industries" path="industries" rows="5" cols="10" />
    <br />
    <form:errors path="industries" />
    <br />
    <form:button id="add-industries">Add Industries</form:button>
</form:form>