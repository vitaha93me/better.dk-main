<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    function addMonths(dateObj, num) {
        var currentMonth = dateObj.getMonth() + dateObj.getFullYear() * 12;
        dateObj.setMonth(dateObj.getMonth() + num);
        var diff = dateObj.getMonth() + dateObj.getFullYear() * 12 - currentMonth;

        // If we don't get the right number, set date to the last day of previous month
        if (diff != num) {
            dateObj.setDate(0);
        }

        return dateObj;
    }

    $(function() {
        $('#statistics-date').change(function() {
            if ($('#affiliate-only-hungry').is(':checked')) {
                window.location = '?period=' + $(this).val() + '&affiliate=hungry';
            } else {
                window.location = '?period=' + $(this).val();
            }
        });

        $('#affiliate-only-hungry').change(function() {
            if ($(this).is(':checked')) {
                window.location = '?period=' + $('#statistics-date').val() + '&affiliate=hungry';
            } else {
                window.location = '?period=' + $('#statistics-date').val();
            }
        });

        analytics.ready(function() {
            var filters = [];

            <c:if test="${!empty affiliate}">
                filters.push({
                    operator: 'eq',
                    property_name: 'company.affiliate',
                    property_value: '<c:out value="${affiliate}" />'
                });
            </c:if>

            var startDate = new Date();
            startDate = addMonths(startDate, -1);
            var endDate = new Date();
            endDate.setDate(endDate.getDate() + 1);

            <c:if test="${period != null}">
                var period = '<c:out value="${period}" />';
                var periodNumber = parseInt(period.substring(0, (period.length - 1)));
                var periodSymbol = period.substring((period.length - 1), period.length);

                $('#statistics-date option[value="' + period + '"]').prop('selected', true);

                if (periodSymbol == 'd') {
                    startDate = new Date();
                    startDate.setDate(startDate.getDate() - periodNumber);
                } else if (periodSymbol == 'm') {
                    startDate = addMonths(new Date(), -periodNumber);
                }
            </c:if>

            var utcOffset = BETTER.getUTCOffsetString();
            var startDateFormatted = startDate.getUTCFullYear() + '-' + BETTER.pad((startDate.getUTCMonth() + 1), 2) + '-' + BETTER.pad(startDate.getUTCDate(), 2);
            var startDateFormattedISO = startDateFormatted + 'T00:00:00.000' + utcOffset;
            var endDateFormatted = endDate.getUTCFullYear() + '-' + BETTER.pad((endDate.getUTCMonth() + 1), 2) + '-' + BETTER.pad(endDate.getUTCDate(), 2);
            var endDateFormattedISO = endDateFormatted + 'T00:00:00.000' + utcOffset;
            endDate.setDate(endDate.getDate() - 1);
            $('#statistics-date-range').text('(between ' + startDate.getFullYear() + '-' + BETTER.pad((startDate.getMonth() + 1), 2) + '-' +BETTER.pad(startDate.getDate(), 2) + ' and ' + endDate.getFullYear() + '-' + BETTER.pad((endDate.getMonth() + 1), 2) + '-' + BETTER.pad(endDate.getDate(), 2) + ')');

            var client = new window.Keen({
                projectId: '<c:out value="${projectId}" />',
                readKey: '<c:out value="${readKey}" />'
            });

            window.Keen.ready(function() {
                var pageViews = new window.Keen.Query('count', {
                    eventCollection: 'Loaded a Page',
                    interval: 'daily',
                    timeframe: {
                        start: startDateFormattedISO,
                        end: endDateFormattedISO
                    },
                    filters: filters
                });

                var callsCount = new window.Keen.Query('count', {
                    eventCollection: 'Call Ended',
                    timeframe: {
                        start: startDateFormattedISO,
                        end: endDateFormattedISO
                    },
                    filters: filters
                });

                var likesCount = new window.Keen.Query('count', {
                    eventCollection: 'User Liked',
                    timeframe: {
                        start: startDateFormattedISO,
                        end: endDateFormattedISO
                    },
                    filters: filters
                });

                var reviewsCount = new window.Keen.Query('count', {
                    eventCollection: 'User Made Review',
                    timeframe: {
                        start: startDateFormattedISO,
                        end: endDateFormattedISO
                    },
                    filters: filters
                });

                <c:if test="${empty affiliate}">
                    // New threads
                    var newThreadsCreated = new window.Keen.Query('count', {
                        eventCollection: 'New Thread Created',
                        timeframe: {
                            start: startDateFormattedISO,
                            end: endDateFormattedISO
                        },
                        filters: filters
                    });

                    var newThreadsCreatedNoEmail = new window.Keen.Query('count', {
                        eventCollection: 'New Thread Created (no e-mail)',
                        timeframe: {
                            start: startDateFormattedISO,
                            end: endDateFormattedISO
                        },
                        filters: filters
                    });

                    client.run([newThreadsCreated, newThreadsCreatedNoEmail], function (result, error) {
                        if (!error) {
                            $('#metric-new-threads').html('<div class="keen-widget keen-metric" style="background-color: #57BDE1; width: 328px;"><span class="keen-metric-value">' + (result[0].result + result[1].result) + '</span><span class="keen-metric-title">New Threads</span></div>');
                        }
                    });
                </c:if>

                <c:if test="${!empty affiliate && affiliate == 'hungry'}">
                    // Clicks on manu card button
                    var menuCardClicks = new window.Keen.Query('count', {
                        eventCollection: 'Clicked Menu Button',
                        timeframe: {
                            start: startDateFormattedISO,
                            end: endDateFormattedISO
                        },
                        filters: filters
                    });

                    client.draw(menuCardClicks, document.getElementById('metric-menu-card-clicks'), {
                        title: 'Menu Button Clicks',
                        colors: ['#57BDE1'],

                        // Legacy keen.min.js
                        width: 328,
                        height: 126
                    });

                    // Clicks on booking button
                    var clicksOnBookingButtonQuery = new window.Keen.Query('count', {
                        eventCollection: 'Clicked Booking Button',
                        timeframe: {
                            start: startDateFormattedISO,
                            end: endDateFormattedISO
                        },
                        filters: filters
                    });

                    var clicksOnBookingButtonSearchQuery = new window.Keen.Query('count', {
                        eventCollection: 'Clicked Booking Button - Search',
                        timeframe: {
                            start: startDateFormattedISO,
                            end: endDateFormattedISO
                        },
                        filters: filters
                    });

                    client.run([clicksOnBookingButtonQuery, clicksOnBookingButtonSearchQuery], function (result, error) {
                        if (!error) {
                            $('#metric-booking-button-clicks').html('<div class="keen-widget keen-metric" style="background-color: #57BDE1; width: 328px;"><span class="keen-metric-value">' + (result[0].result + result[1].result) + '</span><span class="keen-metric-title">Booking Button Clicks</span></div>');
                        }
                    });

                    // Visitors to Hungry
                    var continuedWithoutDiscount = new window.Keen.Query('count', {
                        eventCollection: 'Deal Dialog - Continued Without Discount',
                        timeframe: {
                            start: startDateFormattedISO,
                            end: endDateFormattedISO
                        },
                        filters: filters
                    });

                    var continuedWithDiscount = new window.Keen.Query('count', {
                        eventCollection: 'Discount Dialog - Continued With Discount',
                        timeframe: {
                            start: startDateFormattedISO,
                            end: endDateFormattedISO
                        },
                        filters: filters
                    });

                    client.run([continuedWithDiscount, continuedWithoutDiscount, menuCardClicks], function (result, error) {
                        if (!error) {
                            $('#metric-hungry-visitors').html('<div class="keen-widget keen-metric" style="background-color: #57BDE1; width: 328px;"><span class="keen-metric-value">' + (result[0].result + result[1].result + result[2].result) + '</span><span class="keen-metric-title">Hungry Visitors</span></div>');
                        }
                    });

                    // Statistics table
                    var hungryPageViews = new window.Keen.Query('count', {
                        eventCollection: 'Loaded a Page',
                        filters: filters,
                        groupBy: ['company.id', 'company.name'],
                        timeframe: {
                            start: startDateFormattedISO,
                            end: endDateFormattedISO
                        }
                    });

                    client.run(hungryPageViews, function(result, error) {
                        if (!error) {
                            var topCompanies = [];

                            $.each(result.result, function(key, value) {
                                topCompanies.push({
                                    id: value['company.id'],
                                    name: value['company.name'],
                                    views: value.result
                                })
                            });

                            function orderByViewsDesc(a, b) {
                                if (a.views < b.views) {
                                    return 1;
                                }

                                if (a.views > b.views) {
                                    return -1;
                                }

                                return 0;
                            }

                            topCompanies.sort(orderByViewsDesc);
                            topCompanies = topCompanies.slice(0, 20);

                            // Display
                            $('#top-hungry-loading').fadeOut(250);
                            var table = $('#top-hungry-companies');
                            var html = '<tbody>';

                            for (var i = 0; i < topCompanies.length; i++) {
                                html = '<tr data-company-id="' + topCompanies[i].id + '">';
                                html += '<td><a href="https://' + topCompanies[i].id + '.better.dk" title="Profile for ' + topCompanies[i].name + '" target="_blank">' + topCompanies[i].name + '</td>';
                                html += '<td class="page-views"><span class="value">' + topCompanies[i].views + '</span></td>';
                                html += '<td class="menu-button-clicks"><img src="/images/ajax-loader.gif" alt="Loading..." /></td>';
                                html += '<td class="booking-button-clicks"><img src="/images/ajax-loader.gif" alt="Loading..." /></td>';
                                html += '<td class="hungry-visitors"><img src="/images/ajax-loader.gif" alt="Loading..." /></td>';
                                html += '</tr>';
                                table.append(html);
                            }

                            html = '<tr id="top-hungry-total" style="font-weight: bold;">';
                            html += '<td>Total</td>';
                            html += '<td class="total-page-views"><img src="/images/ajax-loader.gif" alt="Loading..." /></td>';
                            html += '<td class="total-menu-button-clicks"><img src="/images/ajax-loader.gif" alt="Loading..." /></td>';
                            html += '<td class="total-booking-button-clicks"><img src="/images/ajax-loader.gif" alt="Loading..." /></td>';
                            html += '<td class="total-hungry-visitors"><img src="/images/ajax-loader.gif" alt="Loading..." /></td>';
                            html += '</tr>';
                            html += '</tbody>';
                            table.append(html);

                            // Total page views
                            var totalPageViews = 0;
                            $('#top-hungry-companies .page-views .value').each(function() {
                                totalPageViews += parseInt($(this).html());
                            });
                            $('#top-hungry-total .total-page-views').html(totalPageViews);

                            filters.push({
                                operator: 'in',
                                property_name: 'company.id',
                                property_value: topCompanies.map(function(obj) {
                                    return obj.id;
                                })
                            });

                            // Clicks on menu button
                            var menuButtonQuery = new window.Keen.Query('count', {
                                eventCollection: 'Clicked Menu Button',
                                filters: filters,
                                groupBy: 'company.id',
                                timeframe: {
                                    start: startDateFormattedISO,
                                    end: endDateFormattedISO
                                }
                            });

                            client.run(menuButtonQuery, function(result) {
                                $('#top-hungry-companies .menu-button-clicks').html('<span class="value">0</span>');

                                for (var j = 0; j < result.result.length; j++) {
                                    var row = $('#top-hungry-companies tr[data-company-id="' + result.result[j]['company.id'] + '"]');
                                    var pageViews = parseInt(row.find('.page-views .value').html());
                                    var menuButtonClicks = result.result[j].result;
                                    var percentage = ((menuButtonClicks / pageViews) * 100);
                                    row.find('.menu-button-clicks').html('<span class="value">' + menuButtonClicks + '</span> (' + parseFloat(percentage).toFixed(2) + '%)');
                                }

                                // Total
                                var totalMenuButtonClicks = 0;
                                $('#top-hungry-companies .menu-button-clicks .value').each(function() {
                                    totalMenuButtonClicks += parseInt($(this).html());
                                });
                                $('#top-hungry-total .total-menu-button-clicks').html('<span class="value">' + totalMenuButtonClicks + '</span> (' + parseFloat(((totalMenuButtonClicks / totalPageViews) * 100)).toFixed(2) + '%)');


                                // Hungry visitors
                                var hungryContinuedWithoutDiscount = new window.Keen.Query('count', {
                                    eventCollection: 'Deal Dialog - Continued Without Discount',
                                    filters: filters,
                                    groupBy: 'company.id',
                                    timeframe: {
                                        start: startDateFormattedISO,
                                        end: endDateFormattedISO
                                    }
                                });

                                var hungryContinuedWithDiscount = new window.Keen.Query('count', {
                                    eventCollection: 'Discount Dialog - Continued With Discount',
                                    filters: filters,
                                    groupBy: 'company.id',
                                    timeframe: {
                                        start: startDateFormattedISO,
                                        end: endDateFormattedISO
                                    }
                                });

                                client.run([hungryContinuedWithoutDiscount, hungryContinuedWithDiscount], function(result) {
                                    var stats = {};

                                    // Without discount
                                    for (var i = 0; i < result[0].result.length; i++) {
                                        stats[result[0].result[i]['company.id']] = {
                                            withoutDiscount: result[0].result[i].result
                                        };
                                    }

                                    // With discount
                                    for (var n = 0; n < result[1].result.length; n++) {
                                        var companyId = result[1].result[n]['company.id'];
                                        var searchHits = result[1].result[n].result;

                                        if (typeof stats[companyId] === 'undefined') {
                                            stats[companyId] = {
                                                withDiscount: searchHits
                                            };
                                        } else {
                                            stats[companyId].withDiscount = searchHits;
                                        }
                                    }

                                    // Display Hungry visitors
                                    $('#top-hungry-companies .hungry-visitors').html('<span class="value">0</span>');
                                    var totalHungryVisitors = 0;

                                    $.each(topCompanies, function() {
                                        var companyId = this.id;
                                        var row = $('#top-hungry-companies tr[data-company-id="' + companyId + '"]');

                                        var pageViews = parseInt(row.find('.page-views .value').html());
                                        var menuButtonClicks = parseInt(row.find('.menu-button-clicks .value').html());
                                        var withoutDiscount = 0;
                                        var withDiscount = 0;
                                        var percentage = null;

                                        if (typeof stats[companyId] !== 'undefined') {
                                            withoutDiscount = (typeof stats[companyId].withoutDiscount !== 'undefined' ? stats[companyId].withoutDiscount : 0);
                                            withDiscount = (typeof stats[companyId].withDiscount !== 'undefined' ? stats[companyId].withDiscount : 0);
                                        }

                                        var total = (withoutDiscount + withDiscount + menuButtonClicks);
                                        totalHungryVisitors += total;
                                        var html = '<span class="value">' + total + '</span>';

                                        if (total > 0) {
                                            percentage = ((total / pageViews) * 100);
                                            html += ' (' + parseFloat(percentage).toFixed(2) + '%)';
                                        }

                                        row.find('.hungry-visitors').html(html);
                                    });

                                    // Total
                                    var totalHungryVisitors = 0;
                                    $('#top-hungry-companies .hungry-visitors .value').each(function() {
                                        totalHungryVisitors += parseInt($(this).html());
                                    });
                                    $('#top-hungry-total .total-hungry-visitors').html('<span class="value">' + totalHungryVisitors + '</span> (' + parseFloat(((totalHungryVisitors / totalPageViews) * 100)).toFixed(2) + '%)');
                                });
                            });

                            // Clicked Booking Button
                            var clickedBookingButtonProfile = new window.Keen.Query('count', {
                                eventCollection: 'Clicked Booking Button',
                                filters: filters,
                                groupBy: 'company.id',
                                timeframe: {
                                    start: startDateFormattedISO,
                                    end: endDateFormattedISO
                                }
                            });

                            var clickedBookingButtonSearch = new window.Keen.Query('count', {
                                eventCollection: 'Clicked Booking Button - Search',
                                filters: filters,
                                groupBy: 'company.id',
                                timeframe: {
                                    start: startDateFormattedISO,
                                    end: endDateFormattedISO
                                }
                            });

                            client.run([clickedBookingButtonProfile, clickedBookingButtonSearch], function(result) {
                                var stats = {};

                                // Profile
                                for (var i = 0; i < result[0].result.length; i++) {
                                    stats[result[0].result[i]['company.id']] = {
                                        profile: result[0].result[i].result
                                    };
                                }

                                // Search results
                                for (var n = 0; n < result[1].result.length; n++) {
                                    var companyId = result[1].result[n]['company.id'];
                                    var searchHits = result[1].result[n].result;

                                    if (typeof stats[companyId] === 'undefined') {
                                        stats[companyId] = {
                                            search: searchHits
                                        };
                                    } else {
                                        stats[companyId].search = searchHits;
                                    }
                                }

                                // Display booking clicks
                                $('#top-hungry-companies .booking-button-clicks').html('<span class="value">0</span>');

                                for (var propertyValue in stats) {
                                    var search = (typeof stats[propertyValue].search !== 'undefined' ? stats[propertyValue].search : 0);
                                    var profile = (typeof stats[propertyValue].profile !== 'undefined' ? stats[propertyValue].profile : 0);

                                    var row = $('#top-hungry-companies tr[data-company-id="' + propertyValue + '"]');
                                    var pageViews = parseInt(row.find('.page-views .value').html());
                                    var percentage = ((profile / pageViews) * 100);
                                    row.find('.booking-button-clicks').html('<span class="value">' + (search + profile) + '</span> (' + parseFloat(percentage).toFixed(2) + '% *)');
                                }

                                // Total
                                var totalBookingClicks = 0;
                                $('#top-hungry-companies .booking-button-clicks .value').each(function() {
                                    totalBookingClicks += parseInt($(this).html());
                                });
                                $('#top-hungry-total .total-booking-button-clicks').html('<span class="value">' + totalBookingClicks + '</span> (' + parseFloat(((totalBookingClicks / totalPageViews) * 100)).toFixed(2) + '%)');
                            });
                        }
                    });
                </c:if>


                // Draw charts
                client.draw(pageViews, document.getElementById('page-views-count'), {
                    title: 'Page Views',
                    colors: ['#57BDE1'],
                    height: 250,
                    width: 1000, // Legacy keen.min.js
                    chartOptions: {
                        chartArea: {
                            width: '100%',
                            left: 50
                        },
                        vAxis: {
                            viewWindowMode: 'explicit',
                            viewWindow: {
                                min: 0
                            },
                            baselineColor: '#E9E9E9',
                            gridlines: {
                                color: '#E9E9E9'
                            }
                        },
                        titleTextStyle: {
                            color: '#57BDE1',
                            fontName: 'Roboto-Regular',
                            fontSize: 18,
                            bold: false
                        }
                    }
                });

                client.draw(likesCount, document.getElementById('metric-likes-count'), {
                    title: 'Likes',
                    colors: ['#57BDE1'],

                    // Legacy keen.min.js
                    width: 328,
                    height: 126
                });

                client.draw(reviewsCount, document.getElementById('metric-reviews-count'), {
                    title: 'Reviews',
                    colors: ['#57BDE1'],

                    // Legacy keen.min.js
                    width: 328,
                    height: 126
                });

                client.draw(callsCount, document.getElementById('metric-calls-count'), {
                    title: 'Calls',
                    colors: ['#57BDE1'],

                    // Legacy keen.min.js
                    width: 328,
                    height: 126
                });
            });
        });
    });
</script>

<style>
    .metric {
        width: 328px;
        height: 126px;
        float: left;
        margin: 30px 0 0 30px;
    }

    #metrics-wrapper .metric:nth-child(3n+1) {
        margin-left: 0;
    }

    #statistics-date-range {
        font-size: 12px;
    }

    .keen-metric-value,
    .keen-metric-title {
        color: #fff;
    }
</style>

Show statistics for the past
<select id="statistics-date">
    <option value="0d">Today</option>
    <option value="1d">Yesterday</option>
    <option value="14d">14 days</option>
    <option value="1m" selected>1 month</option>
    <option value="3m">3 months</option>
    <option value="6m">6 months</option>
    <option value="9m">9 months</option>
    <option value="12m">12 months</option>
</select>

<span id="statistics-date-range"></span>

<div style="float: right;">
    <input type="checkbox" id="affiliate-only-hungry" <c:if test="${!empty affiliate && affiliate == 'hungry'}">checked</c:if> /> <label for="affiliate-only-hungry"> Only show Hungry profiles</label>
</div>

<br />

<div id="page-views-count"></div>

<div id="metrics-wrapper">
    <div id="metric-calls-count" class="metric"></div>
    <div id="metric-likes-count" class="metric"></div>
    <div id="metric-reviews-count" class="metric"></div>

    <c:if test="${empty affiliate}">
        <div id="metric-new-threads" class="metric"><img src="/images/ajax-loader.gif" alt="Loading..." /></div>
    </c:if>

    <c:if test="${!empty affiliate && affiliate == 'hungry'}">
        <div id="metric-menu-card-clicks" class="metric"></div>
        <div id="metric-booking-button-clicks" class="metric"><img src="/images/ajax-loader.gif" alt="Loading..." /></div>
        <div id="metric-hungry-visitors" class="metric"><img src="/images/ajax-loader.gif" alt="Loading..." /></div>
    </c:if>
</div>

<div style="clear: both;"></div>

<c:if test="${!empty affiliate && affiliate == 'hungry'}">
    <br />
    <h2>Top 20 by Page Views</h2>

    <table class="table table-striped table-hover" id="top-hungry-companies">
        <thead>
            <tr style="font-weight: bold;">
                <td>Company</td>
                <td>Page Views</td>
                <td>Clicked Menu Button</td>
                <td>Clicked Booking Button</td>
                <td>Hungry Visitors</td>
            </tr>
        </thead>
    </table>

    <div id="top-hungry-loading" style="width: 50px; height: 50px; margin-left: auto; margin-right: auto;">
        <img src="/images/ajax-loader.gif" alt="Loading..." />
    </div>

    * The percentage is calculated based on statistics from the profile page only
</c:if>