<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script type="text/javascript">
    var TARGET_BASE_URL = "<c:url value="/admin/company/claimed-since/" />";
    var COMPANIES_CLAIMED_SINCE = "<c:out value="${since}" />";
</script>

<div id="companies-claimed-since-date-picker"></div>
<select multiple size="10" id="filter-industries">
    <c:forEach var="industry" items="${industries}">
        <option value="<c:out value="${industry.id}" />" <c:if test="${industryIds != null && custom:contains(industryIds, industry.id)}">selected</c:if>><c:out value="${industry.name}" /></option>
    </c:forEach>
</select>

<button id="update-companies-claimed-since">Update</button>

<c:choose>
    <c:when test="${since == null}">
        Please choose a date...
    </c:when>

    <c:otherwise>
        <c:choose>
            <c:when test="${!empty companies}">
                <h2>Showing companies claimed since <c:out value="${since}" /></h2>

                <a href="#" id="download-as-csv" data-file-name="<c:out value="better-companies-claimed-since-${since}.csv" />" target="_blank">Download CSV</a> &nbsp; - &nbsp;
                <a href="#" id="copy-as-csv">Copy as CSV</a>

                <table id="companies-claimed-since-table" border="1">
                    <thead>
                    <tr>
                        <td><strong>Company</strong></td>
                        <td><strong>E-mail</strong></td>
                        <td><strong>Industries</strong></td>
                        <td><strong>Postal Code</strong></td>
                        <td><strong>City</strong></td>
                        <td><strong>Profile URL</strong></td>
                    </tr>
                    </thead>

                    <tbody>
                        <c:set var="request" value="${pageContext.request}" />

                        <c:forEach var="company" items="${companies}">
                            <tr>
                                <td><c:out value="${company.name}" /></td>
                                <td><c:out value="${company.email}" /></td>
                                <td>
                                    <c:forEach var="industry" items="${company.industries}">
                                        <div><c:out value="${industry.name}" /></div>
                                    </c:forEach>
                                </td>
                                <td><c:out value="${company.postalCode}" /></td>
                                <td><c:out value="${company.city}" /></td>
                                <td>
                                    <c:forEach var="subdomain" items="${company.subdomains}">
                                        <c:if test="${subdomain.isActive() == true}">
                                            <c:set var="subdomainName" value="${subdomain.name}" />
                                        </c:if>
                                    </c:forEach>

                                    <c:choose>
                                        <c:when test="${subdomainName != null}">
                                            <c:set var="profileLink" value="${request.scheme}://${subdomainName}.${request.serverName}" />
                                            <a href="<c:out value="${profileLink}" />"><c:out value="${profileLink}" /></a>
                                        </c:when>

                                        <c:otherwise>
                                            <c:set var="profileLink" value="${request.scheme}://${company.id}.${request.serverName}" />
                                            <a href="<c:out value="${profileLink}" />"><c:out value="${profileLink}" /></a>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

                <div class="modal fade" id="copy-csv-dialog" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title widget-heading">Copy CSV</h4>
                            </div>

                            <div class="modal-body">
                                <textarea class="form-control" cols="6" rows="10" id="copy-csv-textarea"></textarea>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>

            <c:otherwise>
                No companies claimed in the period...
            </c:otherwise>
        </c:choose>
    </c:otherwise>
</c:choose>