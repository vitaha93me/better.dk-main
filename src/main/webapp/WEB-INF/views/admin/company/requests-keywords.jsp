<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="custom1" uri="/WEB-INF/tlds/tags.tld" %>
<%@ taglib prefix="custom2" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:choose>
    <c:when test="${results.hasContent()}">
        <h2>Showing companies with unaccepted keywords</h2>

        <c:forEach var="company" items="${results.getContent()}">
            <div style="width: 100%; min-height: 200px; border-bottom: 1px solid #000000;">
                <div style="height: 50px; width: 100%;">
                    <a href="<c:out value="${company.profileUrl}" />"><c:out value="${company.name}" /></a>
                </div>

                <%-- improve: avoid looping twice below; separate objects in business logic instead --%>
                <div style="height: 50px; width: 100%;">
                    <strong>Current keywords:</strong>
                    <br />
                    <c:forEach var="ck" items="${company.companyKeywords}">
                        <c:if test="${ck.isActive() == true}">
                            <c:out value="${ck.keyword.name}" />&nbsp;
                        </c:if>
                    </c:forEach>

                    <br /><br />

                    <strong>Requested keywords:</strong>
                    <br />

                    <c:forEach var="ck" items="${company.companyKeywords}">
                        <c:if test="${ck.isActive() == false}">
                            <c:out value="${ck.keyword.name}" />&nbsp;
                        </c:if>
                    </c:forEach>
                </div>
            </div>
        </c:forEach>

        <custom2:pagination numberOfPageLinks="10" page="${page}" totalPages="${results.getTotalPages()}" />
    </c:when>

    <c:otherwise>
        No companies have unaccepted keywords...
    </c:otherwise>
</c:choose>