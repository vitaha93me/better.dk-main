<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>

<script type="text/javascript">
    $(function() {
        $('.accept-text-message').click(function() {
            var button = $(this);
            $(button).prop('disabled', true);
            var tr = $(this).parents('tr');
            var companyId = parseInt(tr.attr('data-company-id'));
            var textMessageId = parseInt(tr.attr('data-text-message-id'));

            $.ajax({
                type: 'POST',
                url: '/ajax/text-message/company/' + companyId + '/confirm/' + textMessageId,
                dataType: 'json',
                contentType: 'application/json'
            }).done(function() {
                alert("Beskeden blev sendt!");
                tr.fadeOut(500, function() {
                    $(this).remove();
                });
            }).fail(function() {
                $(button).prop('disabled', null);
                alert("Oops! The text message could not be sent!");
            });

            return false;
        });

        $('.decline-text-message').click(function() {
            var button = $(this);
            $(button).prop('disabled', true);
            var tr = $(this).parents('tr');
            var companyId = parseInt(tr.attr('data-company-id'));
            var textMessageId = parseInt(tr.attr('data-text-message-id'));

            $.ajax({
                type: 'POST',
                url: '/ajax/text-message/company/' + companyId + '/decline/' + textMessageId,
                dataType: 'json',
                contentType: 'application/json'
            }).done(function() {
                tr.fadeOut(500, function() {
                    $(this).remove();
                });
            }).fail(function() {
                $(button).prop('disabled', null);
                alert("Oops! The text message could not be declined!");
            });

            return false;
        });

        $('.edit-text-message-text').click(function() {
            $(this).parents('.current-text-wrapper').hide();
            $(this).parents('td').find('.new-message-text-wrapper').show();
            return false;
        });

        $('.cancel-text-edit').click(function() {
            $(this).parents('td').find('.new-message-text-wrapper').hide();
            $(this).parents('td').find('.current-text-wrapper').show();
            return false;
        });

        $('.new-message-text').keyup(function() {
            var messageLength = parseInt($(this).val().length);
            var charactersLeft = (160 - messageLength);
            $('.text-message-characters-left').text(charactersLeft);
            var button = $(this).parents('td').find('.update-text');

            if (messageLength < 50 || messageLength > 160) {
                $(button).prop('disabled', 'disabled');
            } else {
                $(button).prop('disabled', null);
            }
        });

        $('.update-text').click(function() {
            var button = $(this);
            $(button).prop('disabled', 'disabled');

            var tr = $(this).parents('tr');
            var companyId = parseInt(tr.attr('data-company-id'));
            var textMessageId = parseInt(tr.attr('data-text-message-id'));
            var newText = button.siblings('.new-message-text').val();

            $.ajax({
                type: 'POST',
                url: '/ajax/text-message/company/' + companyId + '/update-text/' + textMessageId,
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    text: newText
                })
            }).done(function() {
                // Update text
                var currentTextLabel = button.parents('td').find('.current-text-wrapper .current-text');
                currentTextLabel.text(newText);

                // Transition
                button.parents('td').find('.new-message-text-wrapper').hide();
                button.parents('td').find('.current-text-wrapper').show();
            }).fail(function() {
                $(button).prop('disabled', null);
                alert("Oops! The text message could not be updated!");
            });

            return false;
        });
    });
</script>

<h1>Text Messages</h1>

<c:choose>
    <c:when test="${textMessages.hasContent()}">
        <table class="table table-striped table-hover">
            <thead>
                <tr style="font-weight: bold;">
                    <td>Company</td>
                    <td>Name</td>
                    <td>Text</td>
                    <td>Recipients</td>
                    <td>Requested</td>
                    <td>Actions</td>
                </tr>
            </thead>

            <tbody>
                <c:forEach var="textMessage" items="${textMessages.getContent()}">
                    <tr data-company-id="<c:out value="${textMessage.senderCompany.id}" />" data-text-message-id="<c:out value="${textMessage.id}" />">
                        <td><c:out value="${textMessage.senderCompany.name}" /></td>
                        <td><c:out value="${textMessage.senderAccount.firstName} ${textMessage.senderAccount.lastName}" /></td>
                        <td>
                            <div class="current-text-wrapper">
                                <label class="current-text" style="display: inline;"><c:out value="${textMessage.text}" /></label>
                                (<a href="#" class="edit-text-message-text">edit</a>)
                            </div>

                            <div class="new-message-text-wrapper" style="display: none;">
                                <textarea class="new-message-text" cols="50" rows="5"><c:out value="${textMessage.text}" /></textarea>
                                <br />

                                <button class="btn btn-primary update-text" style="display: block; float: left;">Update</button>
                                <button class="btn btn-primary cancel-text-edit" style="display: block; float: left; margin-left: 5px;">Cancel</button>

                                <div style="float: left; margin: 12px 0 0 10px;">
                                    <span class="text-message-characters-left"><c:out value="${160 - fn:length(textMessage.text)}" /></span> tegn tilbage
                                </div>
                            </div>
                        </td>
                        <td><c:out value="${textMessage.numberOfRecipients}" /></td>
                        <td><fmt:formatDate value="${textMessage.created}" dateStyle="FULL" timeStyle="FULL" /></td>
                        <td>
                            <button class="btn btn-primary accept-text-message" style="display: block;">Accept</button>
                            <button class="btn btn-primary decline-text-message" style="display: block; margin-top: 5px;">Decline</button>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

        <custom:pagination numberOfPageLinks="10" page="${page}" totalPages="${textMessages.getTotalPages()}" />
    </c:when>

    <c:otherwise>
        <p>There are no pending text messages right now.</p>
    </c:otherwise>
</c:choose>