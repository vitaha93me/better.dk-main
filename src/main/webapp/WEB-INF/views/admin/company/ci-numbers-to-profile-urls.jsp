<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form method="POST" action="">
    <label for="csv-contents">CSV:</label>
    <br />
    <textarea name="csv-contents" id="csv-contents" rows="10" cols="100"></textarea>
    <br />
    <input type="submit" name="submit" value="Submit" />
</form>

<c:if test="${mappings != null}">
    <c:choose>
        <c:when test="${!empty mappings}">
            <c:forEach var="mapping" items="${mappings}">
                <c:out value="${mapping.ciNumber}" /> - <c:out value="${mapping.profileUrl}" /><br />
            </c:forEach>
        </c:when>

        <c:otherwise>No mappings found...</c:otherwise>
    </c:choose>
</c:if>