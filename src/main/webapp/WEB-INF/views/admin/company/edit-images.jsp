<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<c:if test="${success == true}">
    Yay, everything went well!
</c:if>

<%--<form action="/admin/company/edit/<c:out value="${company.id}" />/uploadImages" enctype="multipart/form-data" method="POST">--%>
<form:form action="/admin/company/edit/${companyId}/uploadImages" method="POST" modelAttribute="upload" enctype="multipart/form-data" acceptCharset="UTF-8">
    <form:errors />

    <div class="row">
        <div class="col-xs-4">Logo:</div>
        <div class="col-xs-4">
            <c:choose>
                <c:when test="${company.logoName != null}">
                    <img src="<custom:staticResourceLink relativePath="${company.logoName}" />" alt="Logo" width="90px" height="80px" />
                </c:when>

                <c:otherwise>
                    This company doesn't have a logo...
                </c:otherwise>
            </c:choose>

            <br />
            <input name="logo" type="file" />
        </div>

        <div class="col-xs-4">
            <form:errors path="logo" />
        </div>
    </div>

    <div class="row">
        <div class="col-xs-4">Banner:</div>
        <div class="col-xs-4">
            <c:choose>
                <c:when test="${company.bannerName != null}">
                    <img src="<custom:staticResourceLink relativePath="${company.bannerName}" />" alt="Banner" width="940px" height="300px" />
                </c:when>

                <c:otherwise>
                    This company doesn't have a banner...
                </c:otherwise>
            </c:choose>

            <br />
            <input name="banner" type="file" />
        </div>

        <div class="col-xs-4">
            <form:errors path="banner" />
        </div>
    </div>

    <div class="row">
        <div class="col-xs-4"></div>
        <div class="col-xs-4">
            <input type="submit" name="uploadFiles" id="uploadFiles" value="Upload Images" />
        </div>
        <div class="col-xs-4"></div>
    </div>
</form:form>
<%--</form>--%>