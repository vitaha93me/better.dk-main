<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:if test="${success == true}">
    Yes! Everything went well it seems. <c:out value="${size}" /> companies were added.
</c:if>

<br />
<p>The industry mapping CSV file must contain two fields in the following order: (CVR industry ID, Local industry ID)</p>

<c:url var="actionUrl" value="/admin/company/cvr/import" />

<form:form action="${actionUrl}" modelAttribute="dto" method="POST" acceptCharset="UTF-8">
    <form:label path="companiesCSV">URL to companies CSV file (from CVR):</form:label>
    <form:input name="companiesCSV" path="companiesCSV" />
    <br />
    <form:errors path="companiesCSV" />

    <br />

    <form:label path="industryMapping">URL to industry mapping CSV file:</form:label>
    <form:input name="industryMapping" path="industryMapping" />

    <form:errors path="industryMapping" />
    <br />

    <form:button class="btn btn-primary" id="cvr-import-all">Import All</form:button>
</form:form>