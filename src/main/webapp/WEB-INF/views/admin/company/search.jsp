<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<form method="GET" action="<c:url value="/admin/company/search" />" class="form-inline">
    <div class="form-group">
        <span class="form-wrapper form-control form-field-search-terms">
            <label for="search-terms">Søg</label>
            <input type="text" name="q1" id="search-terms" class="form-field" placeholder="f.eks. tømrer, tandlæge" value="<c:out value="${param['q1']}" />" />
        </span>

        <span class="form-wrapper form-control form-field-search-location">
            <label for="search-location">Nær</label>
            <input type="text" name="q2" id="search-location" class="form-field" placeholder="f.eks. by, adresse, post nr." value="<c:out value="${param['q2']}" />" />
        </span>

        <button type="submit" class="btn btn-primary"><i class="mdi mdi-magnify"></i></button>
    </div>
</form>

<c:if test="${what != null || where != null}">
    <c:forEach var="hit" items="${searchResults.hits}">
        <c:set var="entry" value="${hit.value.entry}" />
        <c:set var="industries" value="${entry.industries}" />
        <c:set var="highlights" value="${hit.value.highlights}" />

        <h2><a href="<c:out value="${entry.profileUrl}" />"><c:out value="${entry.name}" /></a></h2>
        <c:out value="${entry.streetName} ${entry.streetNumber}, ${entry.postalCode} ${entry.city}" />

        <br /><br />

        <strong>Quick Links:</strong>
        &nbsp;
        <a href="<c:url value="/admin/company/${entry.id}/edit" />">Edit Company</a>
        &nbsp;
        <a href="<c:url value="/admin/company/${entry.id}/edit/images" />">Upload Images</a>
        &nbsp;
        <a href="<c:url value="/admin/company/${entry.id}/subscriptions" />">Subscriptions</a>

        <div class="spacer-20"></div>
    </c:forEach>

    <custom:pagination page="${searchResults.page}" totalPages="${searchResults.totalPages}" numberOfPageLinks="10" pageLinkPrefix="${currentPageUrl}&" />
</c:if>