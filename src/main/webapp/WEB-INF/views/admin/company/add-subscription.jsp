<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript">
    $(function() {
        $('#add-subscription-form').on('submit', function() {
            var button = $('#add-subscription-button');
            button.prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: '/ajax/admin/company/<c:out value="${company.id}" />/subscriptions/add',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    productId: $('#select-product').val(),
                    startDate: $('#subscription-start-date').datepicker().val(),
                    endDate: $('#subscription-end-date').datepicker().val()
                })
            }).done(function() {
                alert('The subscription for the "' + $('#select-product option:selected').text() + '" product has been added!');
                window.location = '/admin/company/<c:out value="${company.id}" />/subscriptions';
            }).fail(function() {
                alert("Oops, an error occurred!");
                button.prop('disabled', null);
            });

            return false;
        });

        $('.datepicker').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });
</script>

<h1>Add Subscription for <c:out value="${company.name}" /></h1>

<p>This will add a subscription to <c:out value="${company.name}" /> for the specified date interval. It will be active by default.</p>
<div class="spacer-10"></div>

<form action="" method="post" class="form-horizontal" id="add-subscription-form">
    <div class="form-group">
        <label for="select-product" class="col-sm-2 control-label">Product</label>
        <div class="col-sm-10">
            <select class="form-control" id="select-product">
                <option value="1">Trial</option>
                <option value="2">Paid</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="select-product" class="col-sm-2 control-label">Start Date</label>
        <div class="col-sm-10">
            <input type="text" class="datepicker form-control" id="subscription-start-date" placeholder="YYYY-MM-DD" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
        </div>
    </div>

    <div class="form-group">
        <label for="select-product" class="col-sm-2 control-label">End Date</label>
        <div class="col-sm-10">
            <input type="text" class="datepicker form-control" id="subscription-end-date" placeholder="YYYY-MM-DD" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary" id="add-subscription-button">Add Subscription</button>
        </div>
    </div>
</form>