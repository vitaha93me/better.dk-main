<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<script type="text/javascript">
    $(function() {
        $('#include-inactive-subscriptions').change(function() {
            window.location = '?includeInactive=' + $(this).is(':checked');
        });
    });
</script>

<h1>Subscriptions for <c:out value="${company.name}" /></h1>

+ <a href="/admin/company/<c:out value="${company.id}" />/subscriptions/add">Add New</a> &nbsp;&nbsp;

<c:choose>
    <c:when test="${!empty subscriptions}">
        <input type="checkbox" id="include-inactive-subscriptions" value="1" <c:if test="${includeInactive == true}">checked</c:if> /> <label for="include-inactive-subscriptions">Include Inactive Subscriptions</label>

        <table class="table">
            <thead>
                <tr>
                    <td>Product</td>
                    <td>Start Date</td>
                    <td>End Date</td>
                    <td>Status</td>
                </tr>
            </thead>

            <tbody>
                <c:forEach var="subscription" items="${subscriptions}">
                    <tr>
                        <td><c:out value="${custom:productName(subscription.productId)}" /></td>
                        <td><c:out value="${subscription.startDate}" /></td>
                        <td><c:out value="${subscription.endDate}" /></td>
                        <td>TODO</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </c:when>

    <c:otherwise>
        <p><c:out value="${company.name}" /> does not have any subscriptions that match the criteria.</p>
    </c:otherwise>
</c:choose>