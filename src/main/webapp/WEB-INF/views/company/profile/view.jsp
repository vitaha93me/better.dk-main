<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<tiles:insertAttribute name="js" />
<c:set var="isCompanyOwnerOrAdmin" value="${custom:isCompanyOwner(company.id) || custom:isAdmin()}" />

<div itemscope itemtype="http://schema.org/LocalBusiness">
    <tiles:insertAttribute name="top" />
    <tiles:insertAttribute name="menu" />

    <div class="spacer-20"></div>

    <div class="container">
        <div class="row">
            <div class="col-xs-8" id="main-profile-content">
                <h2 class="bold">Om <c:out value="${company.name}" /></h2>

                <c:choose>
                    <c:when test="${!empty company.description}">
                        <p><custom:lineToBreakLine value="${fn:escapeXml(company.description)}" /></p>
                    </c:when>

                    <c:otherwise>
                        <p>Velkommen til <c:out value="${company.name}" />s website på Better.dk. Dette website er lavet for at gøre kommunikationen med virksomheden nemmere og gøre det nemt at give en bedømmelse. Du må meget gerne afgive en bedømmelse når du har handlet med virksomheden. En bedømmelse er meget værdifuld for virksomheden, men også andre af virksomhedens potentielle kunder, der hermed kan få et komplet billede af virksomheden.</p>
                        <p><c:out value="${company.name}" /> findes på <c:out value="${company.streetName} ${company.streetNumber}" /> i <c:out value="${company.city}" />. Hvis du gerne vil i kontakt med virksomheden, kan du skrive en besked ved at klikke på "Send besked" knappen ude i højre side. Vi ser frem til at yde en god service.</p>
                    </c:otherwise>
                </c:choose>

                <c:if test="${empty company.accountCompanies}">
                    <!-- Profilside: Banner [async] -->
                    <script type="text/javascript">
                        var keywords = [company.name];
                        $.merge(keywords, company.industries);
                        var match = BETTER.notifications.getMatchingKeyword(keywords, BETTER.notifications.TYPE_PROFILE_BANNER);

                        if (match) {
                            var plc190559 = window.plc190559 || 0;
                            document.write('<'+'div id="placement_190559_'+plc190559+'" style="margin: 20px 0 20px 0;"></'+'div>');
                            AdButler.ads.push({handler: function(opt){ AdButler.register(166948, 190559, [775,300], 'placement_190559_'+opt.place, opt); }, opt: { place: plc190559++, keywords: BETTER.normalize(match), domain: 'servedbyadbutler.com' }});
                        }
                    </script>
                </c:if>

                <c:if test="${!empty company.bookingUrl}">
                    <span>Søgeord: <a href="<custom:domainNameUri />/s/${company.name}/${company.city}">Pizza</a></span>
                </c:if>
            </div>

            <div class="col-xs-4">
                <tiles:insertAttribute name="cta-box" />
                <div class="spacer-20 clear"></div>
                <tiles:insertAttribute name="options-box" />
            </div>
        </div>
    </div>
</div>


<div class="spacer-20"></div>
<div class="container-fluid" id="address-map"></div>

<c:if test="${empty company.accountCompanies}">
    <tiles:insertAttribute name="claim-company-wizard" />
</c:if>

<tiles:insertAttribute name="propose-company-change-dialog" />
<tiles:insertAttribute name="call-company-dialog" />

<c:if test="${!empty company.bookingUrl}">
    <tiles:insertAttribute name="hungry-deal-dialog" />

    <security:authorize access="isAuthenticated()">
        <tiles:insertAttribute name="hungry-discount-dialog" />
    </security:authorize>
</c:if>

<div class="modal fade" id="cta-splash" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" id="cta-splash-zone"></div>
        </div>
    </div>
</div>

<%-- todo: put the below in a tile --%>
<div class="modal fade" id="send-message-success-cta-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title widget-heading">Tak for din besked</h4>
                <p style="text-align: center;">Din besked er nu sendt videre til virksomheden</p>
            </div>

            <div class="modal-body"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="send-message-success-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body flex flex-column justify-center" style="border-radius: 0 0 4px 4px; height: 300px;">
                <h4 style="text-align: center; font-weight: bold; font-size: 24px;">Tak for din besked</h4>
                <p style="text-align: center;">Din besked er nu sendt videre til virksomheden</p>
            </div>
        </div>
    </div>
</div>