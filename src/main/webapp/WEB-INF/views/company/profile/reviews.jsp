<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="custom2" uri="/WEB-INF/tlds/tags.tld" %>

<script type="text/javascript">
    var TEXT_SUBMIT_REVIEW_SUCCESS_TITLE = "<spring:message code="company.profile.view-profile.review-added-successfully-title" />";
    var TEXT_SUBMIT_REVIEW_ERROR_TITLE = "<spring:message code="company.profile.view-profile.review-added-error-title" />";
</script>

<tiles:insertAttribute name="js" />

<div itemscope itemtype="http://schema.org/LocalBusiness">
    <tiles:insertAttribute name="top" />
    <tiles:insertAttribute name="menu" />

    <div class="spacer-20"></div>

    <section id="reviews">
        <div class="container main-container">
            <div class="row">
                <div class="col-xs-8" id="main-profile-content">
                    <c:if test="${reviews == null and externalReviews == null}">
                        <div class="flex flex-column align-center justify-center" style="height: 300px;">
                            <p class="bold" style="font-size: 24px;">Denne virksomhed har endnu ingen bedømmelser</p>
                            <a href="#" class="btn btn-dark squared" id="write-first-review" title="Bedøm <c:out value="${company.name}" />">Vær den første til at bedømme virksomheden</a>
                        </div>
                    </c:if>

                    <!-- Reviews summary -->
                    <c:if test="${reviews != null or externalReviews != null}">
                        <h2 class="semi-bold">Samlet bedømmelse</h2>

                        <div class="row">
                            <div class="col-xs-8 review-summary left">
                                <div class="pie-chart" data-score="<c:out value="${(company.rating / 10) * 100}" />" data-animate="true">
                                    <span class="company-score">
                                        <span class="score-number light"><c:out value="${formattedRating}" /></span>
                                        <span class="score-word"><custom:ratingDescription rating="${company.rating}" /></span>
                                    </span>

                                    <div class="pie">
                                        <div class="left-side half-circle"></div>
                                        <div class="right-side half-circle"></div>
                                    </div>

                                    <div class="shadow"></div>
                                </div>

                                <div class="flex" id="reviews-rating-breakdown">
                                    <div class="wrapper">
                                        <div class="progress-bar-wrapper flex">
                                            <div class="progress-bar">
                                                <div class="progress" data-percentage="<c:out value="${ratingDistribution.containsKey(5) ? ((ratingDistribution[5] / company.numberOfReviews) * 100) : 0}" />"></div>
                                            </div>

                                            <div class="review-type-count"><c:out value="${ratingDistribution.containsKey(5) ? ratingDistribution[5] : 0}" /></div>
                                            <div class="review-type-description">Perfekt</div>
                                        </div>

                                        <div class="progress-bar-wrapper flex">
                                            <div class="progress-bar">
                                                <div class="progress" data-percentage="<c:out value="${ratingDistribution.containsKey(4) ? ((ratingDistribution[4] / company.numberOfReviews) * 100) : 0}" />"></div>
                                            </div>

                                            <div class="review-type-count"><c:out value="${ratingDistribution.containsKey(4) ? ratingDistribution[4] : 0}" /></div>
                                            <div class="review-type-description">God</div>
                                        </div>

                                        <div class="progress-bar-wrapper flex">
                                            <div class="progress-bar">
                                                <div class="progress" data-percentage="<c:out value="${ratingDistribution.containsKey(3) ? ((ratingDistribution[3] / company.numberOfReviews) * 100) : 0}" />"></div>
                                            </div>

                                            <div class="review-type-count"><c:out value="${ratingDistribution.containsKey(3) ? ratingDistribution[3] : 0}" /></div>
                                            <div class="review-type-description">Middel</div>
                                        </div>

                                        <div class="progress-bar-wrapper flex">
                                            <div class="progress-bar">
                                                <div class="progress" data-percentage="<c:out value="${ratingDistribution.containsKey(2) ? ((ratingDistribution[2] / company.numberOfReviews) * 100) : 0}" />"></div>
                                            </div>

                                            <div class="review-type-count"><c:out value="${ratingDistribution.containsKey(2) ? ratingDistribution[2] : 0}" /></div>
                                            <div class="review-type-description">Under middel</div>
                                        </div>

                                        <div class="progress-bar-wrapper flex">
                                            <div class="progress-bar">
                                                <div class="progress" data-percentage="<c:out value="${ratingDistribution.containsKey(1) ? ((ratingDistribution[1] / company.numberOfReviews) * 100) : 0}" />"></div>
                                            </div>

                                            <div class="review-type-count"><c:out value="${ratingDistribution.containsKey(1) ? ratingDistribution[1] : 0}" /></div>
                                            <div class="review-type-description">Dårlig</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-4 review-summary right">
                                <div class="outer-wrapper flex">
                                    <div class="inner-wrapper">
                                        <span class="title semi-bold">Gennemsnitlig bedømmelse</span>
                                        <span class="below-title light">ud af <c:out value="${company.numberOfReviews}" /> bedømmelser</span>
                                        <a href="#" class="review-button btn btn-dark squared">Bedøm virksomheden</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="spacer-30"></div>
                    </c:if>

                    <div class="flex flex-column justify-center align-center" id="write-review">
                        <h3>Bedøm <c:out value="${company.name}" /></h3>

                        <div class="choose-stars">
                            <div class="star outlined" data-value="1"></div>
                            <div class="star outlined" data-value="2"></div>
                            <div class="star outlined" data-value="3"></div>
                            <div class="star outlined" data-value="4"></div>
                            <div class="star outlined" data-value="5"></div>
                            <div class="clear"></div>
                            <span class="rating-description"></span>
                        </div>

                        <form action="" method="post">
                            <input type="text" name="new-review-name" id="new-review-name" class="form-control" placeholder="Dit navn" title="Skal være på mindst 2 tegn" pattern=".{2,}" required />
                            <input type="email" name="new-review-email" id="new-review-email" class="form-control" placeholder="E-mail adresse" required />
                            <input type="text" name="new-review-title" id="new-review-title" class="form-control" placeholder="Overskrift" title="Skal være på mellem 5 og 50 tegn" pattern=".{5,50}" required />
                            <textarea name="new-review-text" id="new-review-text" class="form-control" placeholder="Beggrund din bedømmelse her" rows="4" required></textarea>

                            <div class="buttons-wrapper">
                                <a href="#" class="light" id="abort-reviewing">Fortryd</a> <input type="submit" class="btn btn-dark squared" id="submit-review" value="Indsendt bedømmelse" />
                            </div>
                        </form>
                    </div>

                    <div class="spacer-30"></div>

                    <!-- Reviews -->
                    <c:if test="${reviews != null}">
                        <div id="reviews-wrapper">
                            <c:forEach var="review" items="${reviews}" varStatus="loop">
                                <div class="review" itemprop="review" itemscope itemtype="http://schema.org/Review">
                                    <div class="author flex flex-column">
                                        <c:choose>
                                            <c:when test="${review.account != null}">
                                                <c:choose>
                                                    <c:when test="${review.account != null && review.account.facebookProfileId != null}">
                                                        <div class="photo">
                                                            <img src="https://graph.facebook.com/<c:out value="${review.account.facebookProfileId}" />/picture?size=square" alt="Profil billede" />
                                                        </div>
                                                    </c:when>

                                                    <c:otherwise>
                                                        <div class="photo">
                                                            <img src="<custom2:domainNameUri />/images/default-account-photo.png" alt="Standard profil billede" />
                                                        </div>
                                                    </c:otherwise>
                                                </c:choose>

                                                <c:if test="${review.account.firstName != null}">
                                                    <span class="name" itemprop="author"><c:out value="${review.account.firstName} ${review.account.middleName} ${review.account.lastName}" /></span>
                                                </c:if>
                                            </c:when>

                                            <c:otherwise>
                                                <div class="verified-customer-wrapper flex flex-row justify-center align-center">
                                                    <div class="verified-customer">
                                                        <span class="icon correct"></span> <span class="text">Verificeret kunde</span>
                                                    </div>
                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>

                                    <div class="details">
                                        <c:if test="${review.title != null}">
                                            <h3 class="title" itemprop="name"><c:out value="${review.title}" /></h3>
                                        </c:if>

                                        <span class="date-reviewed">Skrevet for <c:out value="${custom2:prettytime(review.created)}" /></span>
                                        <div class="stars outlined" data-stars="<c:out value="${review.rating}" />"></div>
                                        <span itemprop="ratingValue" style="display: none;"><c:out value="${review.rating}" /></span>

                                        <p><custom2:lineToBreakLine value="${fn:escapeXml(review.text)}" /></p>
                                    </div>
                                </div>

                                <c:if test="${!empty review.comments}">
                                    <div class="review-comments">
                                        <c:forEach var="comment" items="${review.comments}">
                                            <div class="spacer-15"></div>

                                            <div class="review-comment">
                                                <div class="author flex flex-column">
                                                    <div class="photo">
                                                        <c:choose>
                                                            <c:when test="${comment.company.logoName != null}">
                                                                <img src="<custom2:staticResourceLink relativePath="${comment.company.logoName}" />" alt="Virksomhedens logo" />
                                                            </c:when>

                                                            <c:otherwise>
                                                                <img src="<custom2:staticResourceLink relativePath="companies/logos/default/default.png" />" alt="Virksomhedens logo" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </div>
                                                </div>

                                                <div class="details">
                                                    <h3 class="title">Svar fra <c:out value="${review.company.name}" /></h3>
                                                    <span class="date-responded">Skrevet for <c:out value="${custom2:prettytime(comment.created)}" /></span>
                                                    <p><custom2:lineToBreakLine value="${fn:escapeXml(comment.text)}" /></p>
                                                </div>
                                            </div>
                                        </c:forEach>
                                    </div>

                                    <div class="clear"></div>
                                </c:if>

                                <c:if test="${!loop.last}">
                                    <div class="spacer-30"></div>
                                </c:if>
                            </c:forEach>
                        </div>

                        <div class="review" id="review-template">
                            <div class="author flex flex-column">
                                <div class="photo">
                                    <img src="" alt="Profil billede" />
                                </div>

                                <span class="name"></span>

                                <div class="verified-customer-wrapper flex flex-row justify-center align-center">
                                    <div class="verified-customer">
                                        <span class="icon correct"></span> <span class="text">Verificeret kunde</span>
                                    </div>
                                </div>
                            </div>

                            <div class="details">
                                <h3 class="title"></h3>
                                <span class="date-reviewed"></span>
                                <div class="stars outlined" data-stars=""></div>
                                <p></p>
                            </div>
                        </div>

                        <div id="review-comment-template" style="display: none;">
                            <div class="spacer-15"></div>

                            <div class="review-comment">
                                <div class="author flex flex-column">
                                    <div class="photo">
                                        <img src="" alt="Virksomhedens logo" />
                                    </div>
                                </div>

                                <div class="details">
                                    <h3 class="title" data-prefix="Svar fra "></h3>
                                    <span class="date-responded" data-prefix="Skrevet for "></span>
                                    <p></p>
                                </div>
                            </div>
                        </div>

                        <c:if test="${hasMoreReviews}">
                            <div class="spacer-20"></div>

                            <div class="flex flex-column justify-center align-center">
                                <a href="#" class="btn btn-dark squared" id="load-more-reviews" data-next="<custom2:domainNameUri />/api/company/<c:out value="${company.id}" />/reviews?offset=5&limit=5&sort=-created">Indlæs flere bedømmelser</a>
                            </div>
                        </c:if>
                    </c:if>

                    <div id="reviews-wrapper">
                        <jsp:useBean id="dateValue" class="java.util.Date"/>
                        <c:forEach var="review" items="${externalReviews}">
                            <div class="spacer-30"></div>
                            <a href="${review.link}">
                                <div class="review">
                                    <div class="author flex flex-column">

                                        <div class="photo">
                                            <img style="width: 75px; height: auto;" src="<custom2:domainNameUri />/images/${review.site}.png"
                                                 alt="Standard profil billede"/>
                                        </div>

                                        <span class="name">
                                        <c:out value="${review.author}"/>
                                    </span>

                                    </div>

                                    <div class="details">
                                        <jsp:setProperty name="dateValue" property="time" value="${review.id * 1000}"/>
                                        <span class="date-reviewed"><c:out value="${review.publishDate}"/></span>
                                        <div class="stars outlined" data-stars="<c:out value="${review.rating}" />">
                                            <span style="width: ${review.rating * 18}px;"></span>
                                        </div>
                                        <p><custom2:lineToBreakLine value="${fn:escapeXml(review.review)}"/></p>
                                    </div>
                                </div>
                            </a>
                        </c:forEach>
                    </div>

                    <div class="spacer-30"></div>
                </div>

                <div class="col-xs-4">
                    <tiles:insertAttribute name="cta-box" />
                    <div class="spacer-20 clear"></div>
                    <tiles:insertAttribute name="options-box" />
                </div>
            </div>
        </div>
    </section>
</div>

<tiles:insertAttribute name="claim-company-wizard" />
<tiles:insertAttribute name="propose-company-change-dialog" />
<tiles:insertAttribute name="call-company-dialog" />

<c:if test="${!empty company.bookingUrl}">
    <tiles:insertAttribute name="hungry-deal-dialog" />

    <security:authorize access="isAuthenticated()">
        <tiles:insertAttribute name="hungry-discount-dialog" />
    </security:authorize>
</c:if>

<%-- todo: put this in a tile --%>
<div class="modal fade" id="send-message-success-cta-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title widget-heading">Tak for din besked</h4>
                <p style="text-align: center;">Din besked er nu sendt videre til virksomheden</p>
            </div>

            <div class="modal-body"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="send-message-success-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body flex flex-column justify-center" style="border-radius: 0 0 4px 4px; height: 300px;">
                <h4 style="text-align: center; font-weight: bold; font-size: 24px;">Tak for din besked</h4>
                <p style="text-align: center;">Din besked er nu sendt videre til virksomheden</p>
            </div>
        </div>
    </div>
</div>