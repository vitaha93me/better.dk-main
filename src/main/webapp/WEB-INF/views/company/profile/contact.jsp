<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<tiles:insertAttribute name="js" />

<div itemscope itemtype="http://schema.org/LocalBusiness">
    <tiles:insertAttribute name="top" />
    <tiles:insertAttribute name="menu" />

    <div class="spacer-50"></div>

    <div class="container">
        <div class="row">
            <div class="col-xs-8" id="main-profile-content">
                <div class="row">
                    <div class="col-xs-7">
                        <h2 class="ellipsis-overflow">Kontakt <c:out value="${company.name}" /></h2>

                        <p>Hvis du vil i kontakt med os, så send en besked ved at klikke på "Send besked" knappen ude til højre eller ring gratis til os ved at klikke på "Klik og ring gratis" linket under telefon nummeret ude i højre side. Vi ser frem til at høre fra dig.</p>
                    </div>

                    <div class="col-xs-5">
                        <h3>Kontakt oplysninger</h3>

                        <span class="company-name">
                            <c:out value="${company.name}" />
                        </span>

                        <span class="address">
                            <c:out value="${company.streetName} ${company.streetNumber}" />
                        </span>

                        <span class="city">
                            <c:out value="${company.postalCode} ${company.city}" />
                        </span>

                        <c:if test="${!empty company.website}">
                            <a href="<c:out value="${company.website}" />" title="Besøg hjemmesiden for <c:out value="${company.name}" />" target="_blank" rel="external">
                                <c:out value="${company.website}" />
                            </a>
                        </c:if>
                    </div>
                </div>

                <hr />

                <h4>Find os her</h4>
                <div id="address-map"></div>

                <div class="spacer-30"></div>

                <h4>Brancher</h4>

                <c:forEach var="industry" items="${company.industries}">
                    <c:set var="encodedCity" value="${custom:replaceAll(custom:encodeURIComponent(company.city), \"\\\s+\", \"%20\")}" />
                    <c:set var="encodedIndustryName" value="${custom:replaceAll(custom:encodeURIComponent(industry.name), \"\\\s+\", \"%20\")}" />

                    <a class="industry" href="<custom:domainNameUri />/s/<c:out value="${encodedIndustryName}" />/<c:out value="${encodedCity}" />" title="<c:out value="${industry.name} i ${company.city}" />">
                        <c:out value="${industry.name}" />
                    </a>
                </c:forEach>
            </div>

            <div class="col-xs-4">
                <tiles:insertAttribute name="cta-box" />
                <div class="spacer-20 clear"></div>
                <tiles:insertAttribute name="options-box" />
            </div>
        </div>
    </div>
</div>

<div class="spacer-30"></div>

<tiles:insertAttribute name="claim-company-wizard" />
<tiles:insertAttribute name="propose-company-change-dialog" />
<tiles:insertAttribute name="call-company-dialog" />

<c:if test="${!empty company.bookingUrl}">
    <tiles:insertAttribute name="hungry-deal-dialog" />

    <security:authorize access="isAuthenticated()">
        <tiles:insertAttribute name="hungry-discount-dialog" />
    </security:authorize>
</c:if>

<%-- todo: put this in a tile --%>
<div class="modal fade" id="send-message-success-cta-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title widget-heading">Tak for din besked</h4>
                <p style="text-align: center;">Din besked er nu sendt videre til virksomheden</p>
            </div>

            <div class="modal-body"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="send-message-success-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body flex flex-column justify-center" style="border-radius: 0 0 4px 4px; height: 300px;">
                <h4 style="text-align: center; font-weight: bold; font-size: 24px;">Tak for din besked</h4>
                <p style="text-align: center;">Din besked er nu sendt videre til virksomheden</p>
            </div>
        </div>
    </div>
</div>