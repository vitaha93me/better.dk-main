<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="custom" tagdir="/WEB-INF/tags" %>

<script type="text/javascript">
    $(function() {
        var TOTAL_CUSTOMERS_COUNT = <c:out value="${totalCustomers}" />;

        $('#text-message-text').keyup(function() {
            var messageLength = parseInt($(this).val().length);
            var charactersLeft = (160 - messageLength);
            $('#text-message-characters-left').text(charactersLeft);
            var button = $('#send-text-message-to-all-customers');

            if (messageLength < 20 || messageLength > 160) {
                $(button).prop('disabled', 'disabled');
            } else {
                $(button).prop('disabled', null);
            }
        });

        $('#send-text-message-to-all-customers').click(function() {
            var confirmMessage = 'Er du sikker på, at du vil sende følgende besked til ' + TOTAL_CUSTOMERS_COUNT + ' kunder?';
            confirmMessage += '\n\n"' + $('#text-message-text').val() + '"';

            if (confirm(confirmMessage)) {
                var button = $(this);
                $(button).prop('disabled', 'disabled');

                $.ajax({
                    type: 'POST',
                    url: '/ajax/text-message/company/' + COMPANY_ID + '/send-to-all-customers',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        text: $('#text-message-text').val().trim()
                    })
                }).done(function() {
                    alert("Din SMS blev sendt til godkendelse og vil derefter blive sendt til dine kunder!");
                    $('#send-sms-to-all-customers-wrapper').fadeOut(500);
                }).fail(function(xhr) {
                    $(button).prop('disabled', null);
                    var response = $.parseJSON(xhr.responseText);

                    switch (response.code) {
                        case "error.invalid.arguments":
                            alert("Beskeden skal være mellem 20 og 160 tegn lang");
                            break;

                        case "error.internal":
                            alert("Ups, din besked kunne desværre ikke sendes!");
                            break;

                        default: break;
                    }
                });
            }

            return false;
        });

        $('#update-company-phone-number').click(function() {
            var button = $(this);
            $(button).prop('disabled', 'disabled');

            $.ajax({
                type: 'POST',
                url: '/ajax/company/' + COMPANY_ID + '/update/phone-number',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    phoneNumber: $('#company-phone-number').val().trim()
                })
            }).done(function() {
                window.location.reload();
            }).fail(function(xhr) {
                $(button).prop('disabled', null);
                var response = $.parseJSON(xhr.responseText);

                switch (response.code) {
                    case "error.invalid.arguments":
                        alert("Telefon nummeret skal være 8 tegn langt");
                        break;

                    case "error.internal":
                        alert("Ups, telefon nummeret kunne ikke opdateres!");
                        break;

                    default: break;
                }
            });

            return false;
        });
    });
</script>

<style>
    #send-sms-to-all-customers-wrapper h2 {
        font-size: 14px;
        color: #57BDE1;
    }
</style>

<div class="col-xs-8 main-container has-advisor">
    <tiles:insertAttribute name="page-menu" />
    <div class="clear spacer-30"></div>

    <c:choose>
        <c:when test="${!empty customers}">
            <c:choose>
                <c:when test="${empty company.phoneNumber}">
                    <div id="company-phone-number-wrapper">
                        <p>For at kunne sende SMS til dine kunder, skal du oplyse din virksomheds telefon nummer.</p>

                        <input type="text" class="form-control" id="company-phone-number" placeholder="Dit telefon nummer" style="width: 175px; float: left;" maxlength="8" />
                        <button class="btn btn-primary" id="update-company-phone-number" style="float: left; margin-left: 5px;">Opdatér</button>
                    </div>
    
                    <div style="clear: both;"></div>
                    <br />
                </c:when>

                <c:when test="${canSendTextMessage == true}">
                    <div id="send-sms-to-all-customers-wrapper">
                        <h2>Send SMS til alle kunder (<c:out value="${totalCustomers}" />)</h2>
                        <textarea cols="20" rows="4" class="form-control" style="width: 300px;" id="text-message-text"></textarea>

                        <button class="btn btn-primary" id="send-text-message-to-all-customers" disabled="disabled" style="float: left; margin-top: 5px;">Send SMS til <c:out value="${totalCustomers}" /> kunder</button>

                        <div style="float: left; margin: 14px 0 0 10px;">
                            <span id="text-message-characters-left">160</span> tegn tilbage
                        </div>

                        <div style="clear: both;"></div>
                        <p style="font-size: 12px;">Bemærk, at det koster 30 øre pr. SMS. Eksempel: 50 kunder x 30 øre = 15 kr.</p>
                        <br />
                    </div>
                </c:when>

                <c:otherwise>
                    <div id="send-sms-to-all-customers-wrapper" style="margin-bottom: 20px;">
                        <h2>Send SMS til alle kunder</h2>
                        <p>Du kan højst sende én SMS om dagen. Prøv venligst igen senere.</p>
                    </div>
                </c:otherwise>
            </c:choose>

            <table class="table table-striped table-hover">
                <thead>
                    <tr style="font-weight: bold;">
                        <td>Navn</td>
                        <td>Telefon</td>
                        <td>E-mail</td>
                        <td>Oprettet</td>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach var="customer" items="${customers}">
                        <tr>
                            <td>-</td>
                            <td><c:out value="${customer.phoneNumber}" /></td>
                            <td><c:out value="${customer.email}" /></td>
                            <td><fmt:formatDate value="${customer.created}" dateStyle="FULL" /></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

            <custom:pagination page="${page}" totalPages="${totalPages}" numberOfPageLinks="10" />
        </c:when>

        <c:otherwise>
            <p>Du har ikke nogle kunder på din liste endnu.</p>
        </c:otherwise>
    </c:choose>
</div>

<div class="col-xs-3" id="advisors-wrapper">
    <tiles:insertAttribute name="advisor" />
</div>