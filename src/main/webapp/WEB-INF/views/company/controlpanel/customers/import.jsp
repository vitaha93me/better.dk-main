<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<script type="text/javascript">
    $(function() {
        $('#import-customers').on('click', function() {
            if (!BETTER.IS_LOGGED_IN) {
                $('#create-account-dialog').modal('show');
                return false;
            }

            var input = $('#customers-list');

            if (input.val() == '') {
                return false;
            }

            var customersCount = input.val().match(/.+/gi).length;
            var message = 'Er du sikker på, at du vil tilføje ' + customersCount + ' ' + (customersCount > 1 ? 'kunder' : 'kunde') + '?';

            if (customersCount > 500) {
                alert("Max 500 kunder kan inviteres ad gangen.");
                return false;
            }

            if (!confirm(message)) {
                return false;
            }

            var button = $(this);
            var context = button.parents('.import');
            var image = context.find('.import-icon .icon-image');
            $(button).prop('disabled', true);
            image.removeClass().addClass('loading'); // Remove all classes and add spinner

            $.ajax({
                type: 'POST',
                url: BETTER.ROOT_DOMAIN + '/ajax/cp/company/' + BETTER.CP.COMPANY_ID + '/customers/import',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    emails: input.val().trim().split('\n')
                })
            }).done(function(response) {
                input.val('');
                image.removeClass().addClass('correct');
                context.find('textarea').hide();
                context.find('.title:not(.success)').remove();

                if (response.count != customersCount) {
                    context.find('.title.success div').html('Du har tilføjet <span class="import-count bold">' + response.count + '</span> ud af <span class="attempted bold">' + customersCount + '</span> kunder');
                    var html = '<p>Ovenstående tal afviger fordi <span class="bold">' + (customersCount - response.count) + '</span> e-mail adresser var inkorrekte, eller fandtes på din kundeliste i forvejen.</p>';
                    $(html).insertBefore(context.find('.title.success a'));
                }

                context.find('.title.success').show();
                context.find('.btn').hide();
            }).fail(function() {
                image.removeClass().addClass('paper-fly');
                $(button).prop('disabled', null);
                alert("Ups, der skete en fejl!");
            });

            return false;
        });

        $('#customers-list').on('focus', function() {
            if (!BETTER.IS_LOGGED_IN) {
                $('#create-account-dialog').modal('show');
                return false;
            }
        });
    });
</script>

<div class="col-xs-8 main-container has-advisor">
    <tiles:insertAttribute name="page-menu" />
    <div class="clear spacer-30"></div>

    <h1>Tilføj kunder</h1>
    <div class="description">På denne side kan du nemt importere dine kunder til din kunde liste ved at indtaste deres e-mail adresser.</div>
    <hr />

    <div class="spacer-50"></div>

    <div class="flex flex-column align-center import">
        <div class="import-icon">
            <div class="icon-image paper-fly"></div>
        </div>

        <div class="title">Du har indtastet <span class="import-count bold">0</span> kunder</div>
        <div class="title success">
            <div>Du har tilføjet <span class="import-count bold">0</span> kunder</div>

            <a href="/cp/company/<c:out value="${company.id}" />/customers/import<c:out value="${urlPostfix}" />" class="light">Tilføj flere kunder</a>
        </div>

        <div class="spacer-20"></div>
        <textarea name="customers-list" id="customers-list" class="form-control" placeholder="Indtast e-mail adresser på kunder. Én e-mail pr. linje. Max 500 ad gangen." rows="5"></textarea>
        <div class="spacer-20"></div>

        <div class="flex flex-column align-right button-wrapper">
            <button class="btn btn-primary" name="import-customers" id="import-customers" data-pattern="Tilføj {count} kunder">Tilføj kunder</button>
        </div>
    </div>
</div>

<div class="col-xs-3" id="advisors-wrapper">
    <tiles:insertAttribute name="advisor" />
</div>