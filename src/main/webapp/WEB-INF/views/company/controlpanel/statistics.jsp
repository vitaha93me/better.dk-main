<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<security:authorize access="isAuthenticated()" var="isLoggedIn" />

<c:if test="${isLoggedIn}">
    <script type="text/javascript">
        function addMonths(dateObj, num) {
            var currentMonth = dateObj.getMonth() + dateObj.getFullYear() * 12;
            dateObj.setMonth(dateObj.getMonth() + num);
            var diff = dateObj.getMonth() + dateObj.getFullYear() * 12 - currentMonth;

            // If we don't get the right number, set date to the last day of previous month
            if (diff != num) {
                dateObj.setDate(0);
            }

            return dateObj;
        }

        $(function() {
            analytics.ready(function() {
                var client = new window.Keen({
                    projectId: '<c:out value="${keenProjectId}" />',
                    readKey: '<c:out value="${keenScopedKey}" />'
                });

                var filters = [{
                    operator: 'eq',
                    property_name: 'company.id',
                    property_value: '<c:out value="${company.id}" />'
                }];

                window.Keen.ready(function() {
                    <c:if test="${!empty company.phoneNumber}">
                        var callsCount = new window.Keen.Query('count', {
                            eventCollection: 'Call Ended',
                            filters: filters
                        });

                        client.draw(callsCount, document.getElementById('metric-calls-count'), {
                            title: 'Opkald',
                            colors: ['#57BDE1'],

                            // Legacy keen.min.js
                            width: 300,
                            height: 126
                        });
                    </c:if>

                    <%--<c:if test="${!empty company.bookingUrl}">
                        var continuedWithoutDiscount = new window.Keen.Query('count', {
                            eventCollection: 'Deal Dialog - Continued Without Discount',
                            filters: filters
                        });

                        var displayedDiscountDialog = new window.Keen.Query('count', {
                            eventCollection: 'Displayed Discount Dialog',
                            filters: filters
                        });

                        var bookingCountChart = new window.Keen.Dataviz()
                                .el(document.getElementById('metric-booking-count'))
                                .attributes({
                                    title: 'Bookinger',
                                    colors: ['#57BDE1']
                                })
                                .prepare(); // Start spinner

                        client.run([continuedWithoutDiscount, displayedDiscountDialog], function(error, result) {
                            if (error) {
                                bookingCountChart.error(error.message); // Display the API error
                            } else {
                                bookingCountChart.parseRawData({
                                    result: (result[0].result + result[1].result)
                                }).render();
                            }
                        });
                    </c:if>--%>

                    var likesCount = new window.Keen.Query('count', {
                        eventCollection: 'User Liked',
                        filters: filters
                    });

                    var reviewsCount = new window.Keen.Query('count', {
                        eventCollection: 'User Made Review',
                        filters: filters
                    });

                    /*var newThreadsCreated = new window.Keen.Query('count', {
                     eventCollection: 'New Thread Created',
                     filters: filters
                     });

                     var newThreadsCreatedNoEmail = new window.Keen.Query('count', {
                     eventCollection: 'New Thread Created (no e-mail)',
                     filters: filters
                     });*/

                    var utcOffset = BETTER.getUTCOffsetString();
                    var today = new Date();
                    var oneMonthAgo = new Date();
                    oneMonthAgo = addMonths(oneMonthAgo, -1);

                    var pageViews = new Keen.Query('count', {
                        eventCollection: 'Loaded a Page',
                        interval: 'daily',
                        timeframe: {
                            start: oneMonthAgo.getFullYear() + '-' + BETTER.pad((oneMonthAgo.getMonth() + 1), 2) + '-' + BETTER.pad(oneMonthAgo.getDate(), 2) + 'T00:00:00.000' + utcOffset,
                            end: today.getFullYear() + '-' + BETTER.pad((today.getMonth() + 1), 2) + '-' + BETTER.pad(today.getDate(), 2) + 'T00:00:00.000' + utcOffset
                        },
                        filters: filters
                    });

                    // Draw charts
                    client.draw(likesCount, document.getElementById('metric-likes-count'), {
                        title: 'Likes',
                        colors: ['#57BDE1'],

                        // Legacy keen.min.js
                        width: 300,
                        height: 126
                    });

                    client.draw(reviewsCount, document.getElementById('metric-reviews-count'), {
                        title: 'Bedømmelser',
                        colors: ['#57BDE1'],

                        // Legacy keen.min.js
                        width: 300,
                        height: 126
                    });

                    /*var newThreadsChart = new window.Keen.Dataviz()
                     .el(document.getElementById('metric-new-threads-count'))
                     .attributes({
                     title: 'Beskeder',
                     colors: ['#57BDE1']
                     })
                     .prepare(); // Start spinner

                     client.run([newThreadsCreated, newThreadsCreatedNoEmail], function(error, result) {
                     if (error) {
                     newThreadsChart.error(error.message); // Display the API error
                     } else {
                     newThreadsChart.parseRawData({
                     result: (result[0].result + result[1].result)
                     }).render();
                     }
                     });*/

                    client.draw(pageViews, document.getElementById('page-views-count'), {
                        title: 'Sidevisninger',
                        colors: ['#57BDE1'],
                        height: 250,
                        width: 1000, // Legacy keen.min.js
                        chartOptions: {
                            chartArea: {
                                width: '100%',
                                left: 50
                            },
                            vAxis: {
                                viewWindowMode: "explicit",
                                viewWindow: {
                                    min: 0
                                },
                                baselineColor: '#E9E9E9',
                                gridlines: {
                                    color: '#E9E9E9'
                                }
                            },
                            titleTextStyle: {
                                color: '#57BDE1',
                                fontName: 'Roboto-Regular',
                                fontSize: 18,
                                bold: false
                            }
                        }
                    });
                });
            });
        });
    </script>

    <style>
        .metric {
            width: 300px;
            height: 126px;
            float: left;
            margin: 30px 0 0 30px;
        }

        #metrics-wrapper .metric:nth-child(3n+1) {
            margin-left: 0;
        }

        .keen-metric-value,
        .keen-metric-title {
            color: #fff;
        }
    </style>
</c:if>

<div class="col-xs-11 main-container">
    <h1>Statistikker</h1>
    <hr />

    <c:choose>
        <c:when test="${isLoggedIn}">
            <div class="flex flex-row justify-center" id="page-views-count"></div>

            <div class="flex flex-row justify-center" id="metrics-wrapper">
                <!--<div id="metric-new-threads-count" class="metric"></div>-->

                <c:if test="${!empty company.phoneNumber}">
                    <div id="metric-calls-count" class="metric"></div>
                </c:if>

                <%--<c:if test="${!empty company.bookingUrl}">
                    <div id="metric-booking-count" class="metric"></div>
                </c:if>--%>

                <div id="metric-likes-count" class="metric"></div>
                <div id="metric-reviews-count" class="metric"></div>
            </div>
        </c:when>

        <c:otherwise>
            <p>Beklager, men du skal være logget ind for at kunne se statistikker for virksomheden.</p>
        </c:otherwise>
    </c:choose>
</div>