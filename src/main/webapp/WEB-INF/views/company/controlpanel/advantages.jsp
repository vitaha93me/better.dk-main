<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="col-xs-11 main-container">
    <h1>Fordele</h1>
    <div class="description">Som virksomhed på Better, får du adgang til en række fordele, ganske gratis.</div>
    <hr />

    <span class="sub-title bold">Regnskabsprogrammer</span>
    <div class="spacer-20"></div>

    <div class="advantage-row">
        <div class="advantage">
            <a href="https://tracead.com/cTHoz2jd" target="_blank">
                <img src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/images/cp/thumbnails/billys-billing.jpg" alt="Billy's Billing" />
            </a>

            <span class="sub-title bold">
                <a href="https://tracead.com/cTHoz2jd" target="_blank">Få op til 33% rabat</a>
            </span>

            <p class="description">
                BillysBilling betegnes som Danmarks letteste regnskabsprogram. Prøv gratis i 30 dage.
            </p>
        </div>

        <div class="advantage">
            <a href="http://www.e-conomic.dk/?opendocument&ReferralID=2042" target="_blank">
                <img src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/images/cp/thumbnails/e-conomic.png" alt="E-conomic" />
            </a>

            <span class="sub-title bold">
                <a href="http://www.e-conomic.dk/?opendocument&ReferralID=2042" target="_blank">Få første måned gratis</a>
            </span>

            <p class="description">
                Danmarks største og mest integrerede regnskabsskabsprogram. Mere end 70.000 brugere i Danmark. Prøv gratis i 14 dage.
            </p>
        </div>
    </div>

    <div class="clear"></div>
    <span class="sub-title bold">Markedsføring</span>
    <div class="spacer-20"></div>

    <div class="advantage-row">
        <div class="advantage">
            <a href="mailto:info@better.dk?subject=Få%20600%20kr.%20til%20AdWords&body=Hej%0A%0AJeg%20vil%20gerne%20have%20en%20Google%20AdWords%20kupon.">
                <img src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/images/cp/thumbnails/google-markedsfoering.png" alt="Google AdWords" />
            </a>

            <span class="sub-title bold">
                <a href="mailto:info@better.dk?subject=Få%20600%20kr.%20til%20AdWords&body=Hej%0A%0AJeg%20vil%20gerne%20have%20en%20Google%20AdWords%20kupon.">Få 600 kr. til AdWords</a>
            </span>

            <p class="description">
                Brug 200kr på AdWords og få ekstra 600 kr. Vi kan hjælpe med opsætning og administration.<br /><a href="mailto:info@better.dk" target="_blank">Skriv til os her og få hjælp</a>.
            </p>
        </div>

        <div class="advantage">
            <a href="mailto:info@better.dk?subject=Få%20500%20kr.%20til%20Bing%20Ads&body=Hej%0A%0AJeg%20vil%20gerne%20have%20en%20gratis%20kupon%20til%20Bing%20Ads%2C%20som%20giver%20500%20kr.%20gratis%20annoncering%20på%20Bing%20og%20Yahoo%21">
                <img src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/images/cp/thumbnails/bing-ads.png" alt="Bing Ads" />
            </a>

            <span class="sub-title bold">
                <a href="mailto:info@better.dk?subject=Få%20500%20kr.%20til%20Bing%20Ads&body=Hej%0A%0AJeg%20vil%20gerne%20have%20en%20gratis%20kupon%20til%20Bing%20Ads%2C%20som%20giver%20500%20kr.%20gratis%20annoncering%20på%20Bing%20og%20Yahoo%21">Få 500 kr. helt gratis!</a>
            </span>

            <p class="description">
                Annoncering på søgemaskinerne Bing + Yahoo!, som stadig godt brugt i Danmark.<br /><a href="mailto:info@better.dk" target="_blank">Skriv til os her og få hjælp</a>.
            </p>
        </div>
    </div>

    <div class="clear"></div>
    <span class="sub-title bold">Booking systemer</span>
    <div class="spacer-20"></div>

    <div class="advantage-row">
        <div class="advantage">
            <a href="http://better.terapeutbooking.dk/" target="_blank">
                <img src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/images/cp/thumbnails/terapeut-booking.png" alt="Terapeut Booking" />
            </a>

            <span class="sub-title bold">
                <a href="http://better.terapeutbooking.dk/" target="_blank">Få første måned gratis</a>
            </span>

            <p class="description">
                Terapeutbooking er Danmarks største booking system lavet til alle former for behandlere, frisører og lign.<br /><a href="http://better.terapeutbooking.dk/" target="_blank">Prøv deres gratis version her</a>.
            </p>
        </div>
    </div>
</div>