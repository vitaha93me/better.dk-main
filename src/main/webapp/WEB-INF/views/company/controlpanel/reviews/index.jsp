<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>
<%@ taglib prefix="custom2" tagdir="/WEB-INF/tags" %>

<div class="col-xs-8 main-container has-advisor">
    <tiles:insertAttribute name="page-menu" />
    <div class="clear spacer-30"></div>

    <c:choose>
        <c:when test="${!empty reviews}">
            <div class="reviews">
                <c:forEach var="review" items="${reviews}">
                    <div class="review" data-review-id="<c:out value="${review.id}" />" data-review-uuid="<c:out value="${review.uuid}" />">
                        <div class="row">
                            <div class="col-xs-3" style="padding-right: 0;">
                                <div class="review-rating">
                                    <div class="bold">Bedømmelse</div>
                                    <div class="stars outlined" data-stars="<c:out value="${review.rating}" />"></div>
                                </div>

                                <div class="reviewer-profile-image">
                                    <c:choose>
                                        <c:when test="${review.account != null && review.account.facebookProfileId != null}">
                                            <img src="https://graph.facebook.com/<c:out value="${review.account.facebookProfileId}" />/picture?size=square" alt="Profil billede" />
                                        </c:when>

                                        <c:otherwise>
                                            <img src="<custom:domainNameUri />/images/default-account-photo.png" alt="Profil billede" />
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>

                            <div class="col-xs-5" style="padding-right: 0;">
                                <div class="bold reviewer-name">
                                    <c:choose>
                                        <c:when test="${review.account != null && !empty review.account.firstName}">
                                            <c:out value="${custom:fullName(review.account.firstName, review.account.middleName, review.account.lastName)}" />
                                        </c:when>

                                        <c:otherwise>
                                            Verificeret kunde
                                        </c:otherwise>
                                    </c:choose>
                                </div>

                                <c:if test="${!empty review.title}">
                                    <div class="bold">
                                        Emne: <span class="title"><c:out value="${review.title}" /></span>
                                    </div>
                                </c:if>

                                <div class="excerpt ellipsis-overflow"><c:out value="${review.text}" /></div>

                                <div class="review-actions">
                                    <div class="btn btn-dark flex flex-row justify-center align-center view-and-reply">
                                        <span class="icon forward"></span><span>Se og svar</span>
                                    </div>

                                    <div class="btn btn-gray flex flex-row justify-center align-center request-deletion">
                                        <span class="icon flag"></span><span>Anmod om sletning</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-4 right">
                                <div class="bold">Bedømmelsen blev indsendt</div>
                                <div class="light">
                                    <span class="review-time-relative" data-prefix="for" data-timestamp="<fmt:formatNumber type="number" value="${review.created.time}" groupingUsed="false" maxFractionDigits="0"  />"></span>
                                </div>

                                <div class="facebook-share">
                                    <c:set var="facebookShareUrl" value="${review.company.profileUrl}/bedoemmelser/${review.uuid}" />
                                    <a class="icon fb-share" href="https://www.facebook.com/dialog/share?app_id=<c:out value="${facebookAppId}" />&display=page&href=<c:out value="${facebookShareUrl}" />&redirect_uri=<c:out value="${facebookShareUrl}" />"></a>
                                </div>
                            </div>
                        </div>

                        <div class="review-info">
                            <div class="row" style="margin: 30px 0 30px 0;">
                                <div class="col-xs-12" style="padding: 0;">
                                    <p style="margin-top: 20px;"><custom:lineToBreakLine value="${fn:escapeXml(review.text)}" /></p>

                                    <c:if test="${!empty review.comments}">
                                        <div class="spacer-15"></div>
                                        <div class="bold">Svar:</div>

                                        <div class="comments">
                                            <c:forEach var="comment" items="${review.comments}">
                                                <div class="comment info-box">
                                                    <p><custom:lineToBreakLine value="${fn:escapeXml(comment.text)}" /></p>
                                                </div>

                                                <div class="comment-time light">
                                                    Skrevet for <c:out value="${custom:prettytime(comment.created)}" />
                                                </div>

                                                <div class="clear"></div>
                                            </c:forEach>
                                        </div>
                                    </c:if>
                                </div>
                            </div>

                            <c:if test="${empty review.comments}">
                                <div class="write-comment">
                                    <form action="" method="post">
                                        <div class="row">
                                            <div class="col-xs-1 flex flex-column justify-center company-logo">
                                                <c:choose>
                                                    <c:when test="${company.logoName != null}">
                                                        <img src="<custom:staticResourceLink relativePath="${company.logoName}" />" alt="Virksomhedens logo" />
                                                    </c:when>

                                                    <c:otherwise>
                                                        <img src="<custom:staticResourceLink relativePath="companies/logos/default/default.png" />" alt="Virksomhedens logo" />
                                                    </c:otherwise>
                                                </c:choose>
                                            </div>

                                            <div class="col-xs-11">
                                                <textarea name="comment-text" class="form-control comment-text" placeholder="Svar som <c:out value="${company.name}" />" rows="5"></textarea>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="flex flex-row justify-right align-center" style="margin-top: 10px;">
                                                    <a href="#" class="cancel-comment">Fortryd</a>
                                                    <button class="btn btn-primary squared submit-comment">Besvar bedømmelse</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </c:if>
                        </div>

                        <div class="request-deletion-info" style="display: none;">
                            <div class="row" style="margin: 30px 0 30px 0;">
                                <div class="col-xs-12" style="padding: 0;">
                                    <p style="margin-top: 20px;"><custom:lineToBreakLine value="${fn:escapeXml(review.text)}" /></p>

                                    <c:if test="${!empty review.comments}">
                                        <div class="spacer-15"></div>
                                        <div class="bold">Svar:</div>

                                        <div class="comments">
                                            <c:forEach var="comment" items="${review.comments}">
                                                <div class="comment info-box">
                                                    <p><custom:lineToBreakLine value="${fn:escapeXml(comment.text)}" /></p>
                                                </div>

                                                <div class="comment-time light">
                                                    Skrevet for <c:out value="${custom:prettytime(comment.created)}" />
                                                </div>

                                                <div class="clear"></div>
                                            </c:forEach>
                                        </div>
                                    </c:if>
                                </div>
                            </div>

                            <form action="" method="post">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <textarea name="deletion-request" class="form-control deletion-request-reason" placeholder="Beskriv hvorfor bedømmelsen bør slettes" rows="5"></textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="flex flex-row justify-right align-center" style="margin-top: 10px;">
                                            <a href="#" class="cancel-deletion-request">Fortryd</a>
                                            <button class="btn btn-primary squared submit-deletion-request">Indsend anmodning</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </c:forEach>
            </div>

            <c:if test="${numberOfResults > 10}">
                <custom2:pagination page="${page}" totalPages="${totalPages}" numberOfPageLinks="10" />
            </c:if>
        </c:when>

        <c:otherwise>
            <p>Din virksomhed har endnu ingen bedømmelser.</p>
        </c:otherwise>
    </c:choose>
</div>

<div class="col-xs-3" id="advisors-wrapper">
    <tiles:insertAttribute name="advisor" />
</div>