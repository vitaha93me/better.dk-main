<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<tiles:insertAttribute name="js" />

<div itemscope itemtype="http://schema.org/LocalBusiness">
    <tiles:insertAttribute name="top" />
    <tiles:insertAttribute name="menu" />

    <div class="spacer-20"></div>

    <div class="container">
        <div class="row">
            <div class="col-xs-8" id="main-profile-content">
                <h2 class="bold" style="margin-bottom: 10px;"><c:out value="${page.title}" /></h2>

                <c:choose>
                    <c:when test="${!empty content}">
                        ${content}
                    </c:when>

                    <c:otherwise>
                        <p><custom:lineToBreakLine value="${fn:escapeXml(page.content)}" /></p>
                    </c:otherwise>
                </c:choose>
            </div>

            <div class="col-xs-4">
                <tiles:insertAttribute name="cta-box" />
                <div class="spacer-20 clear"></div>
                <tiles:insertAttribute name="options-box" />
            </div>
        </div>
    </div>

    <div class="spacer-20"></div>
</div>

<c:if test="${empty company.accountCompanies}">
    <tiles:insertAttribute name="claim-company-wizard" />
</c:if>

<tiles:insertAttribute name="propose-company-change-dialog" />
<tiles:insertAttribute name="call-company-dialog" />

<c:if test="${!empty company.bookingUrl}">
    <tiles:insertAttribute name="hungry-deal-dialog" />

    <security:authorize access="isAuthenticated()">
        <tiles:insertAttribute name="hungry-discount-dialog" />
    </security:authorize>
</c:if>

<div class="modal fade" id="cta-splash" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" id="cta-splash-zone"></div>
        </div>
    </div>
</div>

<%-- todo: put the below in a tile --%>
<div class="modal fade" id="send-message-success-cta-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title widget-heading">Tak for din besked</h4>
                <p style="text-align: center;">Din besked er nu sendt videre til virksomheden</p>
            </div>

            <div class="modal-body"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="send-message-success-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body flex flex-column justify-center" style="border-radius: 0 0 4px 4px; height: 300px;">
                <h4 style="text-align: center; font-weight: bold; font-size: 24px;">Tak for din besked</h4>
                <p style="text-align: center;">Din besked er nu sendt videre til virksomheden</p>
            </div>
        </div>
    </div>
</div>