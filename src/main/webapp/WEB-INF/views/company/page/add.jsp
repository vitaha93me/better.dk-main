<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<tiles:insertAttribute name="js" />

<script type="text/javascript">
    var URL_PUBLISH_PAGE = '<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/page/add';
</script>

<div itemscope itemtype="http://schema.org/LocalBusiness">
    <tiles:insertAttribute name="top" />
    <tiles:insertAttribute name="menu" />

    <div class="spacer-40"></div>

    <div class="container">
        <div class="row">
            <div class="col-xs-8" id="main-profile-content">
                <h2 class="bold">Tilføj ny side</h2>

                <form action="" method="post" id="new-page-form">
                    <input type="text" name="page-name" id="new-page-name" class="form-control" placeholder="Sidens navn" title="Skal være på mindst 5 tegn" pattern=".{5,}" required />

                    <div id="permanent-link">
                        <span class="bold">Permanent Link:</span> <span class="base-url"><c:out value="${company.profileUrl}" /></span>/<span class="relative-url"></span>
                    </div>

                    <fieldset>
                        <legend class="bold">Egenskaber for siden</legend>

                        Forælder side
                        <select name="parent-page" id="new-page-parent" class="form-control" <c:if test="${empty rootPages}">disabled</c:if>>
                            <option value="-1" selected="selected">(ingen)</option>

                            <c:if test="${!empty rootPages}">
                                <c:forEach var="page" items="${rootPages}">
                                    <option value="<c:out value="${page.id}" />" data-id="<c:out value="${page.id}" />" data-slug="<c:out value="${page.slug}" />">
                                        <c:out value="${page.name}" />
                                    </option>
                                </c:forEach>
                            </c:if>
                        </select>

                        Overskrift
                        <input type="text" name="title" id="new-page-title" class="form-control" placeholder="Overskriften på siden" title="Skal være på mindst 2 tegn" pattern=".{2,}" required />

                        Indhold
                        <textarea name="content" id="new-page-content" class="form-control" rows="8" placeholder="Skriv det indhold der skal fremgå på siden. Jo mere indhold, jo bedre. Minimum 25 tegn."></textarea>
                        Antal ord: <span class="word-count">0</span>
                    </fieldset>

                    <div id="new-page-actions">
                        <a href="/" id="cancel-new-page" title="Annullér">Annullér</a>
                        <button class="btn btn-primary" id="publish-new-page">Udgiv side</button>
                    </div>
                </form>
            </div>

            <div class="col-xs-4">
                <div class="info-box">
                    <h3>Guide</h3>
                    <p>Her kan du oprette undersider til din virksomhedsprofil. Siderne vil blive tilføjet til menuen på din profil, hvor sidernes navne vil blive vist.</p>

                    <p>Undersider giver dig mulighed for at gøre indholdet på din profil unikt samt at tilføje det indhold du ønsker. Ved at skabe godt indhold på din profil,
                        kan du øge din profils placering i søgemaskinerne og dermed få mere trafik (og potentielt flere kunder!).</p>

                    <p>Eksempelvist kan du tilføje en side med navnet "Produkter", hvorefter du kan oprette de enkelte produkter som undersider til denne side.
                        Dette kan du gøre ved at vælge en forælder side i dropdown menuen i "Egenskaber for siden" -sektionen.</p>

                    <p>Såfremt du har spørgsmål, er du altid velkommen til at kontakte din <a href="<custom:domainNameUri />/cp/company/<c:out value="${company.id}" />/messages" class="bold" title="Kontakt din personlige rådgiver" target="_blank">personlige rådgiver</a>.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="spacer-50"></div>
</div>