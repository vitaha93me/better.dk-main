<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="modal fade" id="create-account-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body">
                <h1>Få adgang til alle funktioner - opret en gratis bruger</h1>

                <p class="light">
                    For at få gratis adgang til alle funktionerne i kontrolpanelet (fx at besvare beskeder), skal du oprette en bruger.<br />Vi har gjort det super nemt, så det eneste du skal gøre er at udfylde formularen nedenfor.
                </p>

                <form action="" method="post" id="form-create-account">
                    <input type="email" class="form-control" name="new-account-email" id="new-account-email" value="<c:out value="${company.email}" />" placeholder="E-mail adresse" disabled />
                    <input type="text" class="form-control" name="new-account-name" id="new-account-name" placeholder="Dit navn" title="Skal være på mindst 2 tegn" pattern=".{2,}" required />
                    <input type="password" class="form-control" name="new-account-password" id="new-account-password" placeholder="Ønsket adgangskode" title="Skal være på mindst 8 tegn" pattern=".{8,}" required />
                    <input type="password" class="form-control" name="new-account-password-repeat" id="new-account-password-repeat" placeholder="Bekræft adgangskode" title="Skal være på mindst 8 tegn" pattern=".{8,}" required />

                    <input type="submit" class="btn btn-primary" id="new-account-button" value="Opret gratis bruger" />
                </form>
            </div>
        </div>
    </div>
</div>