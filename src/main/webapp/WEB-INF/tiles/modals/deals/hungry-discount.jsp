<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<div class="modal fade" id="hungry-discount-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close remove-login-hash remove-login-destination" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body" style="text-align: center; border-radius: 0 0 4px 4px;">
                <div style="margin-left: auto; margin-right: auto; width: 63%;">
                    <h3 class="light" style="float: left; margin-bottom: 0; font-size: 26px;">Her er din rabatkode til</h3> <img style="float: left; position: relative; left: 10px; top: -5px;" src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/cta/hungry_logo.png" alt="Hungry.dk logo" />
                </div>

                <div style="clear: both;"></div>
                <p class="light" style="position: relative; top: -7px;">Din rabat fratrækkes ved indtastning af rabatkoden ved checkout</p>

                <div class="flex flex-column justify-center align-center" style="width: 300px; height: 50px; background-color: #eaeaea; border-radius: 5px; margin: 0 auto 10px auto;">
                    <div class="bold" style="float: left; font-size: 28px;">better10</div>
                </div>

                <p style="margin-bottom: 20px;"><a href="<c:out value="${company.bookingUrl}?utm_source=${custom:replaceAll(company.profileUrl, 'http[s]?://', '')}&utm_medium=referral&utm_campaign=bestilknap" />" id="continue-to-hungry-with-discount" rel="external nofollow" target="_blank" style="color: #7aa246; text-decoration: underline;">Fortsæt til Hungry.dk</a></p>
            </div>
        </div>
    </div>
</div>