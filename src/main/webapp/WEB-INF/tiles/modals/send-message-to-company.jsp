<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<security:authorize access="isAuthenticated()" var="isLoggedIn" />

<div class="modal fade" id="send-message-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close remove-login-hash remove-login-destination" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title widget-heading">Send besked til <c:out value="${company.name}" /></h4>
            </div>

            <div class="modal-body">
                <form class="form-horizontal" id="send-message-form">
                    <div class="form-group">
                        <label for="send-message-name" class="col-sm-3 control-label">Dit navn</label>
                        <div class="col-sm-9">
                            <c:choose>
                                <c:when test="${isLoggedIn}">
                                    <custom:principalFullName var="fullName" />
                                    <input type="text" class="form-control" id="send-message-name" placeholder="Dit navn" value="<c:out value="${fullName}" />" disabled />
                                </c:when>

                                <c:otherwise>
                                    <input type="text" class="form-control" id="send-message-name" placeholder="Dit navn" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="send-message-email" class="col-sm-3 control-label">Din e-mail adresse</label>
                        <div class="col-sm-9">
                            <c:choose>
                                <c:when test="${isLoggedIn}">
                                    <security:authentication property="principal.email" var="email" />

                                    <c:choose>
                                        <c:when test="${!empty email}">
                                            <input type="email" class="form-control" id="send-message-email" placeholder="E-mail" value="<c:out value="${email}" />" disabled />
                                        </c:when>

                                        <c:otherwise>
                                            <input type="email" class="form-control" id="send-message-email" placeholder="E-mail" />
                                        </c:otherwise>
                                    </c:choose>

                                </c:when>

                                <c:otherwise>
                                    <input type="email" class="form-control" id="send-message-email" placeholder="E-mail" />
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="send-message-subject" class="col-sm-3 control-label">Emne</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="send-message-subject" placeholder="Emne" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="send-message-text" class="col-sm-3 control-label">Din besked</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="6" id="send-message-text" placeholder="Skriv din besked her"></textarea>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default remove-login-hash remove-login-destination" data-dismiss="modal">Luk</button>
                <button type="button" class="btn btn-primary" id="button-send-message">Send besked</button>
            </div>
        </div>
    </div>
</div>