<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="modal fade" id="call-company-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding: 20px 25px 25px 25px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="widget-heading" style="font-size: 22px; padding: 0; margin: 0;">
                    <c:choose>
                        <c:when test="${fn:length(company.name) > 20}">
                            Ring gratis til<br /><c:out value="${company.name}" />
                        </c:when>

                        <c:otherwise>
                            Ring gratis til <c:out value="${company.name}" />
                        </c:otherwise>
                    </c:choose>
                </h4>
            </div>

            <div class="modal-body" style="padding: 0;">
                <div style="display: table; width: 100%;">
                    <div id="call-company-wrapper" style="display: table-cell; vertical-align: middle; padding: 25px; background-color: #EFEFEF; border-top: 2px solid #EAEAEA; border-bottom: 2px solid #EAEAEA;">
                        <div id="call-company-inputs">
                            <label style="display: block; float: left; margin-top: 7px; font-size: 16px;">Indtast dit telefon nummer</label>
                            <input type="text" class="form-control" id="call-company-phone-number" placeholder="12 34 56 78" style="width: 25%; float: left; height: 36px; margin-left: 15px;" />

                            <a class="btn btn-primary" href="#" id="call-company-initiate-call" style="float: left; display: block; margin-left: 15px;">Ring gratis op nu</a>
                        </div>

                        <div id="call-company-success" style="display: none;">
                            Du bliver nu ringet op.
                        </div>

                        <div id="call-company-failure" style="display: none;">
                            Der skete desværre en fejl. Genindlæs venligst siden og prøv igen.
                        </div>
                    </div>
                </div>

                <div style="padding: 25px;">
                    <h5 style="font-size: 16px; font-weight: bold; margin-bottom: 10px;">Sådan fungerer det</h5>

                    <ol style="padding-left: 25px;">
                        <li style="margin-bottom: 5px;">Indtast dit nummer og klik "Ring gratis op nu"</li>
                        <li style="margin-bottom: 5px;">Din telefon ringer indenfor få sekunder</li>
                        <li>Du stilles straks videre til virksomheden</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>