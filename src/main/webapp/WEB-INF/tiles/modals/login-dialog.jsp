<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<div class="modal fade" id="login-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close remove-login-hash remove-login-destination" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body" style="text-align: center; padding: 10px 25px 25px 25px; border-radius: 0 0 4px 4px;">
                <h4 style="font-size: 24px; color: #4C4C4C; margin-top: 0; font-weight: bold;">Log ind på Better.dk</h4>

                <p>Log ind med Facebook eller med adgangskode</p>

                <form class="facebook-login" action="<custom:domainNameUri />/signin/facebook" method="POST">
                    <input type="hidden" name="hash" value="" />

                    <button type="submit" class="fb-login" style="width: 85%; height: 46px; padding: 11px;">
                        <i class="mdi mdi-facebook-box"></i>Log ind med Facebook
                    </button>
                </form>

                <div style="text-align: center; margin-bottom: 10px;">
                    <hr style="width: 40%; display: inline-block; vertical-align: middle;" />
                    <span>eller</span>
                    <hr style="width: 40%; display: inline-block; vertical-align: middle;" />
                </div>

                <div id="login-with-email-cta" style="position: relative;">
                    <button class="btn btn-primary" style="width: 55%; height: 44px; margin-bottom: 5px;" id="login-with-email">Log ind med e-mail</button>

                    <p style="font-size: 14px; font-family: 'Roboto-Light';">Har du ikke en bruger? <a href="<custom:domainNameUri />/account/create">Så opret en gratis bruger her</a>.</p>
                </div>

                <div id="login-with-email-form-wrapper" style="opacity: 0; position: absolute; width: 92%;">
                    <form class="login form-horizontal" action="<custom:domainNameUri />/login/process" method="POST">
                        <input type="hidden" name="destination" value="" />

                        <div class="form-group">
                            <label for="login-dialog-login-email" class="col-xs-3 control-label">E-mail</label>

                            <div class="col-xs-9">
                                <input name="username" id="login-dialog-login-email" class="form-control" style="width: 90%; height: 40px;" type="email" placeholder="E-mail" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="login-dialog-login-password" class="col-xs-3 control-label">Adgangskode</label>

                            <div class="col-xs-9">
                                <input name="password" id="login-dialog-login-password" class="form-control" style="width: 90%; height: 40px;" type="password" placeholder="Adgangskode" />
                            </div>
                        </div>

                        <input type="submit" class="btn btn-primary" style="width: 85%; height: 44px; margin: 0;" id="login-with-email-submit" value="Log ind" />
                    </form>
                </div>

                <!--<p>Vi sender ingen spam og poster aldrig på din Facebook væg.<br />Facebook login bruges udelukkende til forbedring af brugeroplevelsen på Better.dk</p>-->
            </div>
        </div>
    </div>
</div>