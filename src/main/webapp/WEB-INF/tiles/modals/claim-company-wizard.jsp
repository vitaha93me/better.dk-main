<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<security:authorize access="isAuthenticated()" var="isLoggedIn" />

<c:if test="${isLoggedIn}">
    <security:authentication property="principal" var="account" />
</c:if>

<div class="modal fade" id="claim-company-wizard" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title widget-heading js-title-step"></h4>
            </div>

            <div class="flex flex-column justify-center align-center modal-body">
                <div class="inner-widget modal-inner-content centered">
                    <div class="modal-inner-wrap">
                        <%-- DO NOTE REMOVE: This div is required for the wizard to work, and it is used for calculating percentages for the progress bar --%>
                        <div class="navbar" style="display: none;">
                            <div class="navbar-inner">
                                <div class="container">
                                    <ul>
                                        <li><a href="#claim-company-tab1" data-toggle="tab"></a></li>

                                        <c:if test="${!isLoggedIn}">
                                            <li><a href="#claim-company-tab2" data-toggle="tab"></a></li>
                                        </c:if>

                                        <li><a href="#claim-company-tab3" data-toggle="tab"></a></li>
                                        <li><a href="#claim-company-tab4" data-toggle="tab"></a></li>

                                        <%-- Only show this tab if the profile does not have a new subdomain (i.e. not its ID) --%>
                                        <c:if test="${fn:contains(company.profileUrl, company.id)}">
                                            <li><a href="#claim-company-tab5" data-toggle="tab"></a></li>
                                        </c:if>

                                        <li><a href="#claim-company-tab6" data-toggle="tab"></a></li>
                                        <li><a href="#claim-company-tab7" data-toggle="tab"></a></li>
                                        <li><a href="#claim-company-tab8" data-toggle="tab"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane claim-company-tab-ownership" id="claim-company-tab1">
                                <p class="h1 modal-subtitle semi-bold">Få flere kunder - bekræft dit ejerskab af virksomheden</p>
                                <p class="h3 company-to-claim"><c:out value="${company.name}" /> (CVR: <c:out value="${company.ciNumber}" />)</p>
                                <p>Jeg anerkender, at jeg har bemyndigelse til at låse op for denne konto på vegne af <br/>virksomheden, og at jeg er indforstået med Betters <a href="#">servicevilkår og fortrolighedspolitik.</a></p>

                                <div style="margin-top: 20px;">
                                    <div class="icon check" style="display: inline-block;"></div>
                                    <div style="display: inline-block; font-size: 16px;">Det er 100% gratis at have en profil på Better.dk</div>
                                </div>
                            </div>

                            <c:if test="${!isLoggedIn}">
                                <div class="tab-pane claim-company-tab-login" id="claim-company-tab2">
                                    <p class="h1 modal-subtitle">Log ind for at fortsætte</p>
                                    <p>Vi sender ingen spam og poster aldrig på din Facebook væg.<br />Facebook login bruges udelukkende til forbedring af brugeroplevelsen på Better.dk</p>

                                    <form class="facebook-login" action="<custom:domainNameUri />/signin/facebook" method="POST">
                                        <input type="hidden" name="hash" value="<spring:message code="hash.claim-company-logged-in" />" />

                                        <button type="submit" class="fb-login flex flex-row justify-center" style="margin: auto; width: 65%;">
                                            <span class="icon"></span>Log ind med Facebook
                                        </button>
                                    </form>

                                    <div style="text-align: center;">
                                        <hr style="width: 40%; display: inline-block; vertical-align: middle;" />
                                        <span>eller</span>
                                        <hr style="width: 40%; display: inline-block; vertical-align: middle;" />
                                    </div>

                                    <div id="claim-login-with-email-cta" style="position: relative;">
                                        <button class="btn btn-primary" style="width: 65%; height: 44px; margin-bottom: 5px;" id="claim-login-with-email">Log ind med e-mail</button>

                                        <p style="font-size: 14px; font-family: 'Roboto-Light';">Har du ikke en bruger? <a href="<custom:domainNameUri />/account/create" target="_blank">Så opret en gratis bruger her</a>.</p>
                                    </div>

                                    <div id="claim-login-with-email-form-wrapper" style="opacity: 0; position: absolute; width: 92%;">
                                        <form class="login form-horizontal" action="<custom:domainNameUri />/login/process" method="POST">
                                            <input type="hidden" name="destination" value="" />

                                            <div class="form-group">
                                                <label for="claim-login-dialog-login-email" class="col-xs-3 control-label">E-mail</label>

                                                <div class="col-xs-9">
                                                    <input name="username" id="claim-login-dialog-login-email" class="form-control" style="width: 236px; height: 40px;" type="email" placeholder="E-mail" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="claim-login-dialog-login-password" class="col-xs-3 control-label">Adgangskode</label>

                                                <div class="col-xs-9">
                                                    <input name="password" id="claim-login-dialog-login-password" class="form-control" style="width: 236px; height: 40px;" type="password" placeholder="Adgangskode" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-9 col-xs-offset-3">
                                                    <input type="submit" class="btn btn-primary" style="width: 236px; height: 44px; float: left;" id="claim-login-with-email-submit" value="Log ind" />
                                                    <p class="light" style="font-size: 14px; margin: 0; float: left;">Har du ikke en bruger? <a href="<custom:domainNameUri />/account/create">Så opret en gratis bruger her</a>.</p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </c:if>

                            <div class="tab-pane claim-company-tab-email" id="claim-company-tab3">
                                <p class="h1 modal-subtitle">Indtast virksomhedens e-mail adresse</p>
                                <p class="modal-description">Bekræft nedenstående e-mail adresse eller indtast en ny.</p>

                                <form class="form-horizontal" id="claim-company-email">
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <c:choose>
                                                <c:when test="${!empty company.email}">
                                                    <c:set var="companyEmail" value="${company.email}" />
                                                </c:when>

                                                <c:when test="${isLoggedIn && !empty account.email}">
                                                    <c:set var="companyEmail" value="${account.email}" />
                                                </c:when>

                                                <c:otherwise>
                                                    <c:set var="companyEmail" value="" />
                                                </c:otherwise>
                                            </c:choose>

                                            <input type="text" class="form-control" id="claim-input-company-email" name="company_email" placeholder="F.eks. din@mail.dk" value="${companyEmail}" />
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane claim-company-tab-website" id="claim-company-tab4">
                                <p class="h1 modal-subtitle">Har virksomheden en webadresse?</p>
                                <form class="form-horizontal" id="claim-company-website">
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <input type="text" class="form-control website-group" id="claim-input-company-website" name="company_website" placeholder="F.eks. www.min-virksomhed.dk" />
                                        </div>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <input class="checkbox-input website-group" id="claim-input-company-no-website" name="company_nowebsite" type="checkbox" />
                                            <span class="checkbox-label">Virksomheden har ingen webside</span>
                                        </label>
                                    </div>
                                </form>
                            </div>

                            <%-- Only show this tab if the profile does not have a new subdomain (i.e. not its ID) --%>
                            <c:if test="${fn:contains(company.profileUrl, company.id)}">
                                <div class="tab-pane claim-company-tab-subdomain" id="claim-company-tab5">
                                    <p class="h1 modal-subtitle">Vælg virksomhedsprofilens webadresse</p>
                                    <p class="modal-description">Din Better virksomhedsprofil ligger lige nu på <strong><c:out value="${company.profileUrl}" /></strong>. Vælg noget mere passende, der gør dine kunder i stand til at finde den.</p>

                                    <form class="form-horizontal" id="claim-company-subdomain">
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-3 left">
                                                <input type="text" class="form-control" id="claim-input-company-subdomain" name="company_subdomain" placeholder="Profilnavn" />
                                            </div>

                                            <div class="col-sm-5 right">
                                                <span class="suffix">.better.dk</span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </c:if>

                            <div class="tab-pane claim-company-tab-industries" id="claim-company-tab6">
                                <p class="h1 modal-subtitle">Indtast din virksomheds branche(r)</p>
                                <form class="form-horizontal company-industries">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <c:forEach var="industry" items="${company.industries}">
                                                <c:set var="industriesTokens" value="${industriesTokens},${industry.name}" />
                                            </c:forEach>

                                            <c:set var="industriesTokens" value="${fn:substring(industriesTokens, 1, fn:length(industriesTokens))}" /> <%-- Remove preceding comma --%>
                                            <input type="text" class="form-control" id="company-industry-tokens" placeholder="Indtast branche" value="<c:out value="${industriesTokens}" />" />
                                        </div>
                                    </div>
                                </form>

                                <%--<div class="suggest-industry">
                                    <p class="modal-subtitle">Kunne du ikke finde din branche?</p>
                                    <a href="#">Foreslå andre brancher</a>
                                </div>--%>
                            </div>

                            <div class="tab-pane claim-company-tab-keywords" id="claim-company-tab7">
                                <p class="h1 modal-subtitle">Indtast nøgleord der beskriver virksomheden</p>
                                <p>Indtast de nøgleord der beskriver din virksomhed og tryk på Enter efter hvert nøgleord</p>

                                <form class="form-horizontal company-services">
                                    <div class="form-group">
                                        <div class="col-sm-8 col-sm-push-2">
                                            <input type="text" class="form-control" id="company-service-tokens" placeholder="F.eks. pizza, kørekort, gulvlægning, vinduespolering, osv." value="" />
                                            <div class="helper counter not-valid">Minimum <span class="count">3</span> nøgleord mere</div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane claim-company-tab-description" id="claim-company-tab8">
                                <p class="h1 modal-subtitle">Indtast virksomhedens beskrivelse</p>
                                <p class="modal-description">Beskriv din virksomhed og hvad den tilbyder på minimum 50 tegn. Beskrivelsen vises i toppen af profilen, samt i søgeresultaterne.</p>

                                <form class="form-horizontal" id="claim-company-description">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <textarea rows="3" class="form-control" id="claim-input-company-description" name="company_description"></textarea>
                                            <div class="helper counter"><span class="count">175</span> karakterer tilbage</div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="progress modal-progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0;"></div>
            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-default button-cancel" data-orientation="cancel" data-dismiss="modal" name="claim-company-cancel" value="Cancel" />
                <input type="button" class="btn btn-primary button-next" name="claim-company-next" value="Bekræft" />
                <input type="button" class="btn btn-primary button-finish" name="claim-company-finish" id="claim-company-finish" value="Complete" />
            </div>
        </div>
    </div>
</div>