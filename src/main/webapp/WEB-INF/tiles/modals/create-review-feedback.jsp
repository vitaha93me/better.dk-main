<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="modal fade" id="create-review-feedback-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title widget-heading js-title-step" id="create-review-feedback-title">Tilføjer din bedømmelse...</h4>
            </div>

            <div class="modal-body" style="padding: 0;">
                <div class="inner-widget modal-inner-content centered">
                    <div class="modal-inner-wrap">
                        <div id="create-review-feedback-success" style="display: none;">
                            Din bedømmelse blev tilføjet.
                        </div>

                        <div id="create-review-feedback-error" style="display: none;">
                            Ups, din bedømmelse kunne desværre ikke tilføjes.
                            <br /><br />
                            <strong>Årsag: </strong> <span id="create-review-feedback-reason"></span>

                            <a href="#">Prøv venligst igen</a>
                        </div>

                        <img src="<custom:domainNameUri />/images/ajax-loader.gif" id="create-review-feedback-spinner" />
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-primary button-finish" name="create-review-feedback-close" data-orientation="cancel" data-dismiss="modal" value="Close" />
            </div>
        </div>
    </div>
</div>