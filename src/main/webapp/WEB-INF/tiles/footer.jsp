<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<security:authorize access="isAuthenticated()" var="isLoggedIn" />

<footer class="container-fluid" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-3">
                <span class="light" style="font-size: 38px; color: #fff;">Better</span>
                <p>Vi gør det nemt at finde, kontakte og bedømme alle lokale virksomheder i Danmark. Vi vækster og skaber bedre lokale virksomheder i samarbejde med forbrugere og virksomhederne selv.</p>
            </div>

            <div class="col-xs-2 footer-row">
                <span class="footer-title">For virksomheder</span>

                <ul>
                    <li><a href="#">Om konceptet</a></li>
                    <li><a href="#">Tilføj firma</a></li>
                    <li><a href="#">Produkter</a></li>
                </ul>
            </div>

            <div class="col-xs-2 footer-row">
                <span class="footer-title">For brugere</span>

                <ul>
                    <li><a href="#">Om konceptet</a></li>

                    <c:choose>
                        <c:when test="${isLoggedIn}">
                            <li><a href="<custom:domainNameUri />/logout">Log out</a></li>
                        </c:when>

                        <c:otherwise>
                            <li><a href="#">Opret bruger</a></li>
                            <li><a href="<custom:domainNameUri />/login">Log ind</a></li>
                        </c:otherwise>
                    </c:choose>
                </ul>
            </div>

            <div class="col-xs-2 footer-row">
                <span class="footer-title">Om Better</span>

                <ul>
                    <li><a href="#">Om konceptet</a></li>
                    <li><a href="mailto:info@better.dk">Kontakt os</a></li>
                    <li><a href="<custom:domainNameUri />/terms">Betingelser</a></li>
                </ul>
            </div>

            <div class="col-xs-3 footer-row" id="footer-help-section">
                <p>Har du spørgsmål eller brug for hjælp?</p>

                <div id="need-help-wrapper">
                    <div class="icon speech-balloon"></div><a href="mailto:info@better.dk" title="Få svar på dine spørgsmål">info@better.dk</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="margin-top: 20px;">
        <div class="col-xs-12" style="text-align: center; font-size: 12px; color: #fff;">
            <%--&copy; 2015 - KSS Holding - CVR: 36448865 - info@better.dk--%>
        </div>
    </div>
</footer>