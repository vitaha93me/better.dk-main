<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<security:authentication property="principal.firstName" var="firstName" />

<div class="col-xs-3 sidebar">
    <a href="<c:url value="/admin" />" id="admin-logo">Better Admin</a>

    <div class="list-group">
        <a href="<c:url value="/admin" />" class="list-group-item active">Overview</a>
        <a href="#" class="list-group-item">Users</a>

        <span class="deep-menu">
            <a href="<c:url value="/admin/company/search" />" class="list-group-item">Companies</a>

            <div class="sub-menu">
                <a href="<c:url value="/admin/company/search" />" class="list-group-item">Search</a>
                <a href="<c:url value="/admin/industry/add" />" class="list-group-item">Add Industries</a>
                <a href="<c:url value="/admin/company/cvr/import" />" class="list-group-item">Import</a>
                <a href="<c:url value="/admin/company/added-since" />" class="list-group-item">Added Since</a>
                <a href="<c:url value="/admin/company/claimed-since" />" class="list-group-item">Claimed Since</a>
                <a href="<c:url value="/admin/company/requests/subdomains" />" class="list-group-item">Subdomain Requests</a>
                <a href="<c:url value="/admin/company/requests/keywords" />" class="list-group-item">Keyword Requests</a>
                <a href="<c:url value="/admin/company/subdomains/update" />" class="list-group-item">Update Subdomains</a>
                <a href="<c:url value="/admin/company/text-messages" />" class="list-group-item">Text Messages</a>
            </div>
        </span>

        <span class="deep-menu">
            <span class="list-group-item">Leads</span>

            <div class="sub-menu">
                <a href="<c:url value="/admin/leads/threads" />" class="list-group-item">Latest Threads</a>
                <a href="<c:url value="/admin/leads/reviewed" />" class="list-group-item">Latest Reviews</a>
            </div>
        </span>

        <a href="#" class="list-group-item">Reviews</a>
        <a href="#" class="list-group-item">Global Settings</a>

        <security:authorize access="hasRole('ROLE_DEVELOPER')">
            <span class="deep-menu">
                <a href="#" class="list-group-item">Developers</a>

                <div class="sub-menu">
                    <a href="<c:url value="/admin/company/search-index-identifiers" />" class="list-group-item">Search Index Identifiers (10k limit)</a>
                    <a href="<c:url value="/admin/company/update-search-index-range" />" class="list-group-item">Update Search Index (range)</a>
                    <a href="<c:url value="/admin/company/update-search-index-lines" />" class="list-group-item">Update Search Index (lines)</a>
                    <a href="<c:url value="/admin/developer/company/generate-api-key" />" class="list-group-item">Generate API Key</a>
                    <a href="<c:url value="/admin/logs/errors" />" class="list-group-item">Error Log</a>
                </div>
            </span>
        </security:authorize>
    </div>

    <div class="user-account">
        <div class="logged-in-as">Logget ind som <strong>${firstName}</strong></div>
        <a class="log-out" href="<c:url value="/logout" />">Log ud</a>
    </div>
</div>