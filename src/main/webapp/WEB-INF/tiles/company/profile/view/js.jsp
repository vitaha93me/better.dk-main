<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<script type="text/javascript">
    var COMPANY_ID = <c:out value="${company.id}" />;
    var company = {
        id: COMPANY_ID,
        name: "<c:out value="${company.name}" />",
        profile_url: window.location.toString().split('#')[0],
        postal_code: <c:out value="${company.postalCode}" />,
        city: "<c:out value="${company.city}" />",

        <c:if test="${!empty company.bookingUrl}">
            affiliate: "hungry",
        </c:if>

        industries: [
            <c:forEach var="industry" items="${company.industries}" varStatus="loop">
                "<c:out value="${industry.name}" />"

                <c:if test="${!loop.last}">,</c:if>
            </c:forEach>
        ]
    };
    var ERROR_ALREADY_LIKED_COMPANY = "<spring:message code="error.already.liked.company" />";

    var URL_LIKE_COMPANY = "<custom:domainNameUri />/api/company/<c:out value="${company.id}" />/likes";
    var URL_SUBMIT_REVIEW = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/reviews";
    var URL_CLAIM_COMPANY = "<custom:domainNameUri />/ajax/company/claim/<c:out value="${company.id}" />";
    var URL_CALL_COMPANY = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/call";
    var URL_SEND_MESSAGE = "<custom:domainNameUri />/ajax/thread/add";
    var URL_UPDATE_EMAIL = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/update/email";
    var URL_UPDATE_WEBSITE = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/update/website";
    var URL_UPDATE_TEASER = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/update/teaser";
    var URL_UPDATE_INDUSTRIES = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/update/industries";
    var URL_UPDATE_KEYWORDS = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/update/keywords";
    var URL_REQUEST_SUBDOMAIN = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/request/subdomain";
    var URL_PROPOSE_CHANGES = "<custom:domainNameUri />/ajax/company/<c:out value="${company.id}" />/propose-changes";
    var URL_INDUSTRY_SEARCH = "<custom:domainNameUri />/ajax/industry/search";

    var HASH_SEND_MESSAGE = "<spring:message code="hash.send-message" />";
    var HASH_SEND_EMAIL = "<spring:message code="hash.send-email" />";
    var HASH_CLAIM_COMPANY = "<spring:message code="hash.claim-company" />";
    var HASH_CLAIM_COMPANY_LOGGED_IN = "<spring:message code="hash.claim-company-logged-in" />";
    var HASH_COMPANY_CHANGE_PROPOSAL = "<spring:message code="hash.company-change-proposal" />";
    var HASH_CREATE_REVIEW = "<spring:message code="hash.create-review" />";
    var HASH_LIKE_COMPANY = "<spring:message code="hash.like-company" />";
    var HASH_FAVORITE_COMPANY = "<spring:message code="hash.add-to-favorites" />";
    var HASH_DEAL_HUNGRY = "<spring:message code="hash.hungry-deal" />";
    var HASH_DISCOUNT = "<spring:message code="hash.view-profile.discount" />";

    <%-- todo: add keywords above --%>
</script>