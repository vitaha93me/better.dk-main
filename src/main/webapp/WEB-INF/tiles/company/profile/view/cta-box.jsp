<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<div class="right-box" id="main-cta">

    <c:set var="done" value="false"/>

    <c:forEach var="entry" items="${company.industries}">
        <c:if test="${entry.quoteLink!=null && !done}">
            <a href="#" class="btn btn-primary semi-bold"
               quote-link="${entry.quoteLink}"
               id="give-me-quote-button">Få et tilbud</a>
            <br>
            <c:set var="done" value="true"/>
        </c:if>
    </c:forEach>

    <a href="#" class="btn btn-primary semi-bold" id="send-email-button">Send e-mail</a>

    <c:if test="${!empty company.bookingUrl}">
        <div class="spacer-10"></div>
        <%-- NOTE! The target MUST be _blank for the tracking to work - otherwise change the tracking JavaScript code accordingly! --%>
        <a class="btn btn-cta" id="online-booking-button"
           href="<c:out value="${company.bookingUrl}?utm_source=${custom:replaceAll(company.profileUrl, 'http[s]?://', '')}&utm_medium=referral&utm_campaign=bestilknap" />"
           target="_blank" rel="external nofollow">Bestil online - få 10% rabat</a>
    </c:if>

    <c:if test="${!empty company.phoneNumber}">
        <hr/>

        <p class="phone-number <c:if test="${!empty company.callTrackingNumber}">call-tracking</c:if>">Kontakt os på
            telefon:
            <c:choose>
                <c:when test="${!empty company.callTrackingNumber}">
                    <c:choose>
                        <c:when test="${fn:length(company.callTrackingNumber) == 8}">
                            <c:out value="${custom:replaceAll(company.callTrackingNumber, '(.{2})', '$1 ')}"/>
                        </c:when>

                        <c:otherwise>
                            <c:out value="${company.callTrackingNumber}"/>
                        </c:otherwise>
                    </c:choose>
                </c:when>

                <c:otherwise>
                    <c:choose>
                        <c:when test="${fn:length(company.phoneNumber) == 8}">
                            <c:out value="${custom:replaceAll(company.phoneNumber, '(.{2})', '$1 ')}"/>
                        </c:when>

                        <c:otherwise>
                            <c:out value="${company.phoneNumber}"/>
                        </c:otherwise>
                    </c:choose>
                </c:otherwise>
            </c:choose>
        </p>

        <p>
            <a href="#" id="call-company">Klik & ring gratis</a>
        </p>
    </c:if>
</div>