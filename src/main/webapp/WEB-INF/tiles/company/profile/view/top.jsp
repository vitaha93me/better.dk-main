<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<tiles:insertAttribute name="send-message-to-company-dialog" />
<tiles:insertAttribute name="send-email-to-company-dialog" />

<c:set var="isCompanyOwnerOrAdmin" value="${custom:isCompanyOwner(company.id) || custom:isAdmin()}" />

<c:if test="${isCompanyOwnerOrAdmin}">
    <script type="text/javascript" src="<custom:domainNameUri />/js/company/profile/company-owner.js"></script>
</c:if>

<style>
    <c:choose>
        <c:when test="${company.bannerName != null}">
            #cover-photo {
                background: url(https://s3.eu-central-1.amazonaws.com/betterdk-public/static/images/profile-cover-shadow.png) bottom, url(<custom:staticResourceLink relativePath="${company.bannerName}" />);
            }
        </c:when>

        <c:otherwise>
            #cover-photo {
                background: #2c3d4f url(https://s3.eu-central-1.amazonaws.com/betterdk-public/static/images/profile-cover-shadow.png) bottom repeat-x;
                background-size: auto;
            }
        </c:otherwise>
    </c:choose>
</style>

<div id="cover-photo" class="container-fluid <c:if test="${company.bannerName != null}">multiple</c:if>">
    <c:if test="${isCompanyOwnerOrAdmin}">
        <label id="upload-cover-photo" <c:if test="${company.bannerName == null}">class="visible"</c:if>>
            <form enctype="multipart/form-data" method="POST" action="">
                <input type="file" name="cover-photo-file" id="cover-photo-file" />
            </form>

            <div class="icon-wrapper">
                <img src="<custom:domainNameUri />/images/icon-camera-upload.png" class="icon" alt="Upload cover billede" />
            </div>

            <div class="input-wrapper">
                <span>Upload nyt cover billede</span>
            </div>
        </label>
    </c:if>

    <div id="empty-space"></div>
    <div class="container" id="profile-top-info">
        <div class="col-xs-8" id="profile-top-company-info-wrapper">
            <div id="company-logo">
                <c:if test="${isCompanyOwnerOrAdmin}">
                    <label id="upload-logo-photo">
                        <form enctype="multipart/form-data" method="POST" action="">
                            <input type="file" name="logo-photo-file" id="logo-photo-file" />
                        </form>

                        <div class="icon-wrapper">
                            <img src="<custom:domainNameUri />/images/icon-camera-upload.png" class="icon" alt="Upload logo billede" />
                        </div>
                    </label>
                </c:if>

                <c:choose>
                    <c:when test="${company.logoName != null}">
                        <img src="<custom:staticResourceLink relativePath="${company.logoName}" />" alt="Logo" />
                    </c:when>

                    <c:otherwise>
                        <img src="https://s3.eu-central-1.amazonaws.com/betterdk-companies/companies/logos/default/default.png" alt="Logo" />
                    </c:otherwise>
                </c:choose>
            </div>

            <div id="profile-top-company-info">
                <h1 class="semi-bold ellipsis-overflow" itemprop="name"><c:out value="${company.name}" /></h1>

                <a href="#" class="icon maps-drop"></a>
                <span id="company-address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span itemprop="streetAddress"><c:out value="${company.streetName} ${company.streetNumber}" /></span>, <span itemprop="postalCode"><c:out value="${company.postalCode}" /></span>&nbsp;<span itemprop="addressLocality"><c:out value="${company.city}" /></span>
                </span>

                <div id="profile-top-review-info">
                    <div class="stars <c:if test="${company.numberOfReviews == 0}">outlined</c:if>" data-stars="<c:out value="${company.numberOfReviews > 0 ? (company.rating / 2) : 0}" />"></div>

                    <c:if test="${company.numberOfReviews > 0}">
                        <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" style="float: left;">
                            <span itemprop="ratingValue" style="display: none;">
                                <fmt:formatNumber value="${(company.rating / 2)}" maxFractionDigits="0" />
                            </span>

                            <div class="score"><c:out value="${formattedRating}" /></div> <span class="number-of-reviews">(<span itemprop="reviewCount"><c:out value="${company.numberOfReviews}" /></span> <c:out value="${company.numberOfReviews == 1 ? 'bedømmelse' : 'bedømmelser'}" />)</span>
                        </div>
                    </c:if>

                    <a href="/bedoemmelser#bedoem" title="Bedøm <c:out value="${company.name}" />" class="review-button light">Bedøm</a>
                </div>
            </div>
        </div>

        <div class="col-xs-4" id="profile-top-actions">
            <div id="profile-top-actions-wrapper">
                <a href="#" class="button likes" id="like-company-button">
                    <span class="left semi-bold" data-likes="<c:out value="${company.numberOfLikes}" />"><c:out value="${company.numberOfLikes}" /></span>
                    <span class="right">Synes godt om</span>
                </a>

                <c:set var="encodedCurrentPageUrl" value="${custom:urlEncode(currentPageUrl)}" />
                <a href="https://www.facebook.com/dialog/share?app_id=<c:out value="${facebookAppId}" />&display=page&href=<c:out value="${encodedCurrentPageUrl}" />&redirect_uri=<c:out value="${encodedCurrentPageUrl}" />" class="button facebook" target="_blank" rel="external nofollow">
                    <span class="left">
                        <span class="icon facebook"></span>
                    </span>

                    <span class="right">Del på Facebook</span>
                </a>
            </div>
        </div>
    </div>

    <c:if test="${isCompanyOwnerOrAdmin}">
        <div id="cover-photo-upload-progress" class="progress tiny-progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0;"></div>
        </div>
    </c:if>
</div>