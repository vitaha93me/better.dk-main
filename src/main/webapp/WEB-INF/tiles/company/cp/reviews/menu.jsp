<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<ul class="page-menu flex flex-row align-center">
    <li data-parent="#cp-menu-reviews">
        <a href="/cp/company/<c:out value="${company.id}" />/reviews<c:out value="${urlPostfix}" />"><span class="container-link"></span>Bedømmelser</a>
    </li>

    <li data-parent="#cp-menu-reviews">
        <a href="/cp/company/<c:out value="${company.id}" />/reviews/invite<c:out value="${urlPostfix}" />"><span class="container-link"></span>Invitér kunder</a>
    </li>

    <li data-parent="#cp-menu-reviews">
        <a href="/cp/company/<c:out value="${company.id}" />/reviews/sharing<c:out value="${urlPostfix}" />"><span class="container-link"></span>Bedømmelses link</a>
    </li>

    <li data-parent="#cp-menu-reviews">
        <a href="/cp/company/<c:out value="${company.id}" />/reviews/automation<c:out value="${urlPostfix}" />"><span
                class="container-link"></span>Automatiske bedømmelser</a>
    </li>

    <li data-parent="#cp-menu-reviews">
        <a href="/cp/company/<c:out value="${company.id}" />/reviews/monitoring<c:out value="${urlPostfix}" />"><span
                class="container-link"></span>Monitorering</a>
    </li>
</ul>