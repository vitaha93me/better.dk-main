<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<security:authorize access="isAuthenticated()" var="isLoggedIn" />

<c:if test="${isLoggedIn}">
    <security:authentication property="principal" var="account" />
    <security:authentication property="principal.facebookProfileId" var="facebookProfileId" />
</c:if>

<header class="flex flex-column justify-center" id="topbar">
    <div class="container" style="height: 100%;">
        <div class="row flex flex-row align-center" style="height: 100%;">
            <div class="col-xs-3">
                <a href="<custom:domainNameUri />">
                    <img src="https://s3.eu-central-1.amazonaws.com/betterdk-public/static/images/cp/logo.png" alt="Logo" />
                </a>
            </div>

            <div class="col-xs-9" id="topbar-menu-wrapper" style="height: 100%;">
                <div class="nav-icons flex" style="-ms-justify-content: flex-end; -moz-justify-content: flex-end; -webkit-justify-content: flex-end; justify-content: flex-end; height: 100%;">
                    <div class="dropdown flex flex-row align-center" style="height: 100%; -ms-justify-content: flex-end; -moz-justify-content: flex-end; -webkit-justify-content: flex-end; justify-content: flex-end;">
                        <div style="float: right; text-align: right; padding: 6px 10px 0 0;">
                            <c:if test="${isLoggedIn}">
                                <span style="font-size: 13px; cursor: default; display: block;">
                                    <security:authentication property="principal.firstName" var="firstName" />
                                    <security:authentication property="principal.lastName" var="lastName" />
                                    <c:out value="${firstName} ${lastName}" />
                                </span>
                            </c:if>

                            <a href="<c:out value="${company.profileUrl}" />" id="cp-topbar-profile-link">
                                <c:out value="${company.name}" />
                            </a>
                        </div>

                        <a href="#" style="display: inline-block; float: right;" data-toggle="dropdown">
                            <c:choose>
                                <c:when test="${isLoggedIn && facebookProfileId != null}">
                                    <%-- improve: Find a way to use method in AccountService (without using it directly in the view) to avoid duplicating the below URL --%>
                                    <img src="https://graph.facebook.com/<c:out value="${facebookProfileId}" />/picture?size=square" alt="Profile picture" style="width: 40px; height: 40px; border: 2px #FFFFFF solid; border-radius: 20px;" />
                                </c:when>

                                <c:otherwise>
                                    <img src="/images/default-account-photo.png" alt="Standard profil billede" style="width: 40px; height: 40px; border: 2px #FFFFFF solid; border-radius: 20px;" />
                                </c:otherwise>
                            </c:choose>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-menu-item-account" role="menu" aria-labelledby="menu-item-account" style="border-radius: 0 0 3px 3px;">
                            <li><a href="/">Tilbage til forsiden</a></li>

                            <c:choose>
                                <c:when test="${isLoggedIn}">
                                    <li><a href="/logout"><spring:message code="account.account.logout.title" /></a></li>
                                </c:when>

                                <c:otherwise>
                                    <li><a href="/login"><spring:message code="account.account.login.title" /></a></li>
                                </c:otherwise>
                            </c:choose>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>