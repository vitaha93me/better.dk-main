<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="flex flex-column align-center" id="advisors">
    <div class="title bold">Din personlige rådgiver</div>

    <img src="/images/advisors/kss.jpg" alt="Din personlige rådgiver" />

    <div class="name bold">Kim Sand Sørensen</div>
    <div class="description">
        Kim er ekspert i online markedsføring og har mange års erfaring i branchen. Ligeledes har han flere års erfaring som erhvervsdrivende.
        Har du spørgsmål til brugen af Better eller hvordan du kan få størst muligt udbytte af Better, så er Kim klar til at svare på dine spørgsmål.
    </div>
    <a href="mailto:kim@better.dk" title="Skriv til Kim" class="btn send-message">Skriv direkte til Kim</a>

    <%--<div class="light divider">eller</div>

    <div class="bold phone-title">Ring på telefon i tidsrummet</div>
    <div class="light phone-opening-hours">Mandag - Fredag 08-16</div>
    <div class="bold phone-number">Telefon: 12 34 56 78</div>--%>
</div>