<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="cp-menu-wrapper">
    <a href="/cp/company/<c:out value="${company.id}${urlPostfix}" />" class="menu-item">
        <div class="icon statistics"></div>
        <div class="description">Statistik</div>
    </a>

    <a href="/cp/company/<c:out value="${company.id}" />/customers<c:out value="${urlPostfix}" />" class="menu-item" id="cp-menu-customers" title="Kunder">
        <div class="icon customers"></div>
        <div class="description">Kunder</div>
    </a>

    <a href="/cp/company/<c:out value="${company.id}" />/messages<c:out value="${urlPostfix}" />" class="menu-item" title="Beskeder" id="cp-menu-messages">
        <div class="icon messages">
            <span class="notification" data-count="0" style="display: none;"></span>
        </div>
        <div class="description">Beskeder</div>
    </a>

    <a href="/cp/company/<c:out value="${company.id}" />/reviews<c:out value="${urlPostfix}" />" class="menu-item" id="cp-menu-reviews" title="Anmeldelser">
        <div class="icon reviews"></div>
        <div class="description">Bedømmelser</div>
    </a>

    <a href="/cp/company/<c:out value="${company.id}" />/integrations<c:out value="${urlPostfix}" />" class="menu-item" title="Integrationer">
        <div class="icon integrations"></div>
        <div class="description">Integrationer</div>
    </a>

    <a href="/cp/company/<c:out value="${company.id}" />/advantages<c:out value="${urlPostfix}" />" class="menu-item">
        <div class="icon advantages"></div>
        <div class="description">Fordele</div>
    </a>
</div>