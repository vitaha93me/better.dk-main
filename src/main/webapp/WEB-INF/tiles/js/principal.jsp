<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<security:authorize access="isAuthenticated()">
    <security:authentication var="principal" property="principal" />

    <script type="text/javascript">
        var USER_PRINCIPAL = {
            id: '<c:out value="${principal.uuid}" />',
            firstName: '<c:out value="${principal.firstName}" />',
            middleName: '<c:out value="${principal.middleName}" />',
            lastName: '<c:out value="${principal.lastName}" />',

            <c:choose>
                <c:when test="${principal.facebookProfileId != null}">
                    profilePicture: 'https://graph.facebook.com/<c:out value="${principal.facebookProfileId}" />/picture?size=square'
                </c:when>

                <c:otherwise>
                    profilePicture: '<custom:domainNameUri />/images/default-account-photo.png'
                </c:otherwise>
            </c:choose>
        };
    </script>
</security:authorize>