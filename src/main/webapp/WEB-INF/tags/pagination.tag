<%@ attribute name="page" required="true" type="java.lang.Integer" %>
<%@ attribute name="totalPages" required="true" type="java.lang.Integer" %>
<%@ attribute name="numberOfPageLinks" required="true" type="java.lang.Integer" %>
<%@ attribute name="pageLinkPrefix" required="false" type="java.lang.String" description="A string to prepend the generated page links. Must include everything prior to 'page=X', even the '&'. If empty, '?page=X' is appended to the current URL" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/tags.tld" %>

<c:if test="${empty pageLinkPrefix}">
    <c:set var="pageLinkPrefix" value="?" />
</c:if>

<%-- Avoid generating links with multiple 'page' GET parameters --%>
<c:set var="pageLinkPrefix" value="${custom:replaceAll(pageLinkPrefix, 'page=[0-9]+&?', '')}" />

<c:set var="numberOfPagesOnEachSide" value="${numberOfPageLinks / 2}" />

<c:choose>
    <c:when test="${((page - numberOfPagesOnEachSide) > 1)}">
        <c:set var="pagesBeforeCurrentPage" value="${numberOfPagesOnEachSide}" />
        <c:set var="begin" value="${(page - numberOfPagesOnEachSide)}" />
    </c:when>

    <c:otherwise>
        <c:set var="pagesBeforeCurrentPage" value="${custom:abs(1 - page)}" />
        <c:set var="begin" value="1" />
    </c:otherwise>
</c:choose>

<c:set var="pagesAfterCurrentPage" value="${((custom:abs((pagesBeforeCurrentPage - numberOfPageLinks))) - 1)}" />

<%-- If we don't have enough pages to show after the current page, then show more before (if available) --%>
<c:choose>
    <c:when test="${pagesAfterCurrentPage < numberOfPagesOnEachSide}">
        <c:if test="${(page - pagesBeforeCurrentPage) > 1}">
            <c:set var="pagesBeforeCurrentPage" value="${pagesBeforeCurrentPage + (numberOfPagesOnEachSide - pagesAfterCurrentPage)}" />
            <c:set var="begin" value="${((page - pagesBeforeCurrentPage) > 1) ? (page - pagesBeforeCurrentPage) : 1}" />
        </c:if>
    </c:when>
</c:choose>

<nav class="row pagination-container">
    <div class="col-xs-2 left">
        <div class="first-page-wrapper flex flex-row align-center">
            <span>&lt;</span> <a href="<c:out value="${pageLinkPrefix}page=1" />" class="first-page semi-bold <c:if test="${page == 1}">inactive</c:if>" title="Gå til første side">F&oslash;rste side</a>
        </div>
    </div>

    <div class="col-xs-8 middle flex flex-column justify-center">
        <ul class="semi-bold">
            <%-- Show previous and current pages --%>
            <c:forEach var="tempPage" begin="${begin}" end="${page}" step="1" varStatus="loop">
                <li <c:if test="${tempPage == page}">class="active"</c:if>><a href="<c:out value="${pageLinkPrefix}" />page=${tempPage}">${tempPage}<span class="container-link"></span></a></li>
            </c:forEach>

            <%-- Show next pages --%>
            <c:forEach var="i" begin="1" end="${pagesAfterCurrentPage}" step="1">
                <c:set var="tempPage" value="${i + page}" />

                <c:if test="${tempPage <= totalPages && tempPage <= 1000}">
                    <li><a href="<c:out value="${pageLinkPrefix}" />page=${tempPage}">${tempPage}<span class="container-link"></span></a></li>
                </c:if>
            </c:forEach>
        </ul>
    </div>

    <div class="col-xs-2 right">
        <div class="last-page-wrapper flex flex-row align-center">
            <%-- Temporarily, we ask Google not to crawl pages >= 1000 due to AWS CloudSearch limitations (assuming a page size of 10) --%>
            <a href="<c:out value="${pageLinkPrefix}page=${totalPages}" />" <c:if test="${totalPages >= 1000}">rel="nofollow"</c:if> class="last-page semi-bold <c:if test="${page >= totalPages}">inactive</c:if>" title="Gå til sidste side">Sidste side</a> <span>&gt;</span>
        </div>
    </div>
</nav>