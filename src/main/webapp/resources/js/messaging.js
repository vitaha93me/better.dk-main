var BETTER = BETTER || {};

BETTER.messaging = {};
BETTER.messaging.threadMeta = {};
BETTER.messaging.EVENT_NEW_USER_MESSAGE = 'new-user-message';
BETTER.messaging.EVENT_NEW_COMPANY_MESSAGE = 'new-company-message';
BETTER.messaging.EVENT_NEW_THREAD = 'new-thread';
BETTER.messaging.EVENT_UPDATE_NOTIFICATIONS = 'update-message-notifications';
BETTER.messaging.COOKIE_UNREAD_MESSAGES = 'unread-messages-count';
BETTER.messaging.listeners = [];

BETTER.messaging.initialize = function() {
    BETTER.messaging.refreshUnreadThreadsNotification();
};

BETTER.messaging.refreshUnreadThreadsNotification = function() {
    var cookieValue = BETTER.getCookieValue(BETTER.messaging.COOKIE_UNREAD_MESSAGES);
    this.trigger(this.EVENT_UPDATE_NOTIFICATIONS, (cookieValue == null) ? null : JSON.parse(decodeURIComponent(cookieValue)));
};

BETTER.messaging.handleIncomingUserMessage = function(threadId, message) {
    this.addMessage(threadId, message);
    this.trigger(this.EVENT_NEW_USER_MESSAGE, {
        threadId: threadId,
        message: message
    });
};

BETTER.messaging.handleIncomingCompanyMessage = function(companyId, threadId, message) {
    this.addMessage(threadId, message);
    this.trigger(this.EVENT_NEW_COMPANY_MESSAGE, {
        companyId: companyId,
        threadId: threadId,
        message: message
    });
};

BETTER.messaging.handleNewThread = function(companyId, threadId, subject, message) {
    this.addThread(threadId, message);
    this.trigger(this.EVENT_NEW_THREAD, {
        companyId: companyId,
        threadId: threadId,
        subject: subject,
        message: message
    });
};

BETTER.messaging.addMessages = function(threadId, hasMoreMessages, messages) {
    if (this.threadMeta[threadId] == null) {
        this.threadMeta[threadId] = {
            currentPage: 1,
            hasMoreMessages: hasMoreMessages,
            messages: messages
        };
    } else {
        var currentThreadMeta = this.threadMeta[threadId];
        var currentMessages = currentThreadMeta.messages;

        this.threadMeta[threadId] = {
            currentPage: (currentThreadMeta.currentPage + 1),
            hasMoreMessages: hasMoreMessages,
            messages: messages.concat(currentMessages)
        };
    }
};

BETTER.messaging.addMessage = function(threadId, message) {
    if (this.threadMeta[threadId] == null) {
        this.threadMeta[threadId] = {
            currentPage: 0,
            messages: [message]
        };
    } else {
        var currentThreadMeta = this.threadMeta[threadId];
        var currentMessages = currentThreadMeta.messages;
        var newMessages = $.merge([], [message]); // We need this variable because $.merge alters the input array

        this.threadMeta[threadId] = {
            currentPage: currentThreadMeta.currentPage,
            hasMoreMessages: currentThreadMeta.hasMoreMessages,
            messages: $.merge(currentMessages, newMessages)
        };
    }
};

BETTER.messaging.addThread = function(id, message) {
    this.threadMeta[id] = {
        currentPage: 0,
        messages: [message]
    };
};

BETTER.messaging.addListener = function(eventName, callback) {
    if (typeof callback !== 'function') {
        return;
    }

    var currentListeners = this.listeners[eventName];

    if (currentListeners == null) {
        this.listeners[eventName] = [callback];
    } else {
        var newCallbacks = $.merge([], [callback]); // We need this variable because $.merge alters the input array
        this.listeners[eventName] = $.merge(newCallbacks, currentListeners);
    }
};

BETTER.messaging.trigger = function(eventName, data) {
    var callbacks = this.listeners[eventName];

    if (callbacks != null) {
        for (var i = 0; i < callbacks.length; i++) {
            callbacks[i](data);
        }
    }
};

BETTER.messaging.getThread = function(threadId) {
    return this.threadMeta[threadId];
};

BETTER.messaging.getMessages = function(threadId, pageNumber, callback) {
    // Validate that the page number is the next in the sequence
    var currentThread = this.getThread(threadId);

    if (currentThread != null) {
        if (pageNumber != (currentThread.currentPage + 1)) {
            throw new Error('Incorrect page number specified (' + pageNumber + '). Expected: ' + (currentThread.currentPage + 1), 'RangeError');
        }
    } else if (pageNumber != 1) {
        throw new Error('Incorrect page number specified (' + pageNumber + '). Expected: 1', 'RangeError');
    }

    var accessToken = BETTER.qs('token');
    var checksum = BETTER.qs('checksum');
    var parameters = {
        page: pageNumber
    };

    if (accessToken != null && checksum != null) {
        parameters.token = accessToken;
        parameters.checksum = checksum;
    }

    $.ajax({
        type: 'GET',
        url: BETTER.ROOT_DOMAIN + '/ajax/thread/' + threadId + '/messages?' + $.param(parameters),
        dataType: 'json'
    }).done(function(result) {
        // Cache results
        var messages = result.messages;
        var reversed = messages.slice().reverse();
        BETTER.messaging.addMessages(threadId, result.hasNext, reversed);

        // Invoke callback
        if (typeof callback === 'function') {
            callback(reversed, result.hasNext);
        }
    }).fail(function(xhr) {
        var response = $.parseJSON(xhr.responseText);

        switch (response.code) {
            case 'error.not.authenticated': alert(ERROR_NOT_AUTHENTICATED); break;
            default: alert("Ups, der skete en fejl!"); break;
        }
    });
};

BETTER.messaging.sendNewMessage = function(threadId, messageText, isCompany, successCallback, failureCallback, alwaysCallback) {
    var options = {
        type: 'POST',
        url: BETTER.ROOT_DOMAIN + '/ajax/thread/' + threadId + '/message/add',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({
            messageText: messageText,
            isCompany: isCompany
        })
    };

    var jqxhr = $.ajax(options);
    jqxhr.done(function(result, textStatus, xhr) {
        if (xhr.status == 201) {
            BETTER.messaging.addMessage(threadId, result);

            if (successCallback != null && typeof successCallback === 'function') {
                successCallback(result, textStatus, xhr);
            }
        }
    });

    if (failureCallback != null && typeof failureCallback === 'function') {
        jqxhr.fail(failureCallback);
    }

    if (alwaysCallback != null && typeof alwaysCallback === 'function') {
        jqxhr.always(alwaysCallback);
    }
};

BETTER.messaging.getThreads = function(page, companyId, successCallback, failureCallback, alwaysCallback) {
    var accessToken = BETTER.qs('token');
    var checksum = BETTER.qs('checksum');
    var parameters = {
        page: page
    };

    if (companyId != null) {
        parameters.companyId = companyId;
    }

    if (accessToken != null && checksum != null) {
        parameters.token = accessToken;
        parameters.checksum = checksum;
    }

    var options = {
        type: 'GET',
        url: BETTER.ROOT_DOMAIN + '/ajax/thread/fetch?' + $.param(parameters),
        dataType: 'json',
        contentType: 'application/json'
    };

    var jqxhr = $.ajax(options);
    jqxhr.done(function(result, textStatus, xhr) {
        // todo: add to cache (requires refactoring: add thread meta data to cache)

        if (successCallback != null && typeof successCallback === 'function') {
            successCallback(result, textStatus, xhr);
        }
    });

    if (failureCallback != null && typeof failureCallback === 'function') {
        jqxhr.fail(failureCallback);
    }

    if (alwaysCallback != null && typeof alwaysCallback === 'function') {
        jqxhr.always(alwaysCallback);
    }
};

// data = cookie value // improve: document properly
BETTER.messaging.getNumberOfUnreadMessagesForCompany = function(companyId, data) {
    if (data == null) {
        return 0;
    }

    var unreadThreads = data.companies;
    var keys = (typeof unreadThreads !== 'undefined' ? Object.keys(unreadThreads) : []);

    if (keys.length > 0) {
        if (typeof unreadThreads[companyId] !== 'undefined') {
            return Object.keys(unreadThreads[companyId]).length;
        }
    }

    return 0;
};

// data = cookie value // improve: document properly
BETTER.messaging.getNumberOfUnreadMessagesForUser = function(data) {
    if (data == null) {
        return 0;
    }

    return (typeof data.personal !== 'undefined' ? Object.keys(data.personal).length : 0);
};

// data = cookie value // improve: document properly
BETTER.messaging.getUnreadThreadIds = function(data, companyId) {
    if (companyId != null && typeof companyId !== 'undefined') {
        if (typeof data.companies !== 'undefined') {
            if (typeof data.companies[companyId] !== 'undefined') {
                return Object.keys(data.companies[companyId]);
            }
        }
    } else {
        if (typeof data.personal !== 'undefined') {
            return Object.keys(data.personal);
        }
    }

    return [];
};