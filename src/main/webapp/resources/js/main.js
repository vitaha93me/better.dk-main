$(function() {
    BETTER.initialize();

    if (typeof BETTER.messaging !== 'undefined') {
        BETTER.messaging.initialize();
    }

    $('.fb-login').on('click', function() {
        $(this).parents('form')[0].submit();
    });

    // Account top bar menu
    $('#topbar-account-icon').popover({
        placement: 'bottom',
        html: true,
        content: function() {
            return $('#account-popover-container').html();
        },

        // We specify a template in order to set a class (an ID is overwritten) to the popover for styling purposes
        template: '<div class="popover account-popover ' + (IS_LOGGED_IN ? '' : 'login') + '" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>'
    });

    // Search
    $('.company-search-form').on('submit', function() {
        var what = BETTER.nullifyIfEmpty($(this).find('input.what').val());
        var where = BETTER.nullifyIfEmpty($(this).find('input.where').val());

        if (what != null && where != null) {
            window.location = BETTER.ROOT_DOMAIN + '/s/' + what + '/' + where;
        } else if (where == null && what != null) {
            window.location = BETTER.ROOT_DOMAIN + '/s/' + what;
        } else if (what == null && where != null) {
            window.location = BETTER.ROOT_DOMAIN + '/sl/' + where;
        }

        return false;
    });

    analytics.trackLink($('#footer a'), 'Clicked Footer Link', function(element) {
        return {
            section: $(element).parents('ul').prev('.footer-title').text(),
            text: $(element).text()
        };
    });

    $('#search-terms, #search-location').focus(function() {
        $(this).select();
    });

    $('.remove-login-hash').click(function() {
        $('form.facebook-login input[name=hash]').val(null);
    });

    $('.remove-login-destination').click(function() {
        $('form.login input[name=destination]').val(null);
    });

    // Trigger event before logging in
    $('body').on('click', 'form.facebook-login .fb-login', function(event) {
        var form = $(this).parents('form');
        event.preventDefault();

        analytics.track('Clicked Facebook Login Button', null, null, function() {
            form.submit();
        });

        return false;
    });

    // Login dialog
    $('#login-with-email').click(function() {
        var cta = $('#login-with-email-cta');
        cta.animate({
            top: '-=200px',
            opacity: 0
        }, 500);

        $('#login-dialog .modal-body').animate({
            height: '+=90px'
        }, 500);

        var emailFormWrapper = $('#login-with-email-form-wrapper');
        emailFormWrapper.css('top', (cta.position().top + 200) + 'px');

        emailFormWrapper.animate({
            top: '-=200px',
            opacity: 1
        }, 500);

        return false;
    });

    $('.give-me-quote-button').click(function () {
        var company = $(this).val;
        var link = $(this).attr("quote-link");

        analytics.track('SearchResult - Give me a quote', {
            company: company
        });
        window.location.href = link;
    });

});

if (typeof Pusher !== 'undefined') {
    // Companies
    BETTER.push.subscribeToCompanyNotifications(BETTER.OWNED_COMPANIES);

    // Private user
    if (BETTER.IS_LOGGED_IN && window.location.pathname.indexOf('/cp/company/') == -1) { // Don't subscribe to user events if in company cp
        var map = {
            'new-message': function(data) {
                if (typeof BETTER.messaging.handleIncomingUserMessage == 'function') {
                    BETTER.messaging.handleIncomingUserMessage(data.threadId, data.message);
                }
            }
        };

        BETTER.push.subscribe('private-user-notifications-' + BETTER.principal.id, map, BETTER.push.AUTH_ENDPOINT_USER);
    }
}

