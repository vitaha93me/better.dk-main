$.fn.chooseRating = function() {
    var getRatingDescription = function(rating) {
        switch (rating) {
            case 1: return 'Dårlig';
            case 2: return 'Under middel';
            case 3: return 'Middel';
            case 4: return 'God';
            case 5: return 'Fantastisk';
        }
    };

    return $(this).each(function() {
        $(this).on('click', '.star', function() {
            var rating = parseInt($(this).data('value'));
            var current = $(this);
            var prev = current.prevAll('.star');
            var stars = current.add(prev);

            var wrapper = $(this).parents('.choose-stars');
            wrapper.attr('data-rating', rating);

            // Clear styling
            var allStars = wrapper.find('.star');
            allStars.addClass('outlined');

            // Add styling
            stars.removeClass('outlined');

            wrapper.find('.rating-description').text(getRatingDescription(rating));
        });
    });
};