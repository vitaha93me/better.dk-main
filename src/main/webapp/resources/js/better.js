var BETTER = BETTER || {};
BETTER.ROOT_DOMAIN = URL_ROOT_DOMAIN;
BETTER.IS_LOGGED_IN = IS_LOGGED_IN;
BETTER.principal = (typeof USER_PRINCIPAL !== 'undefined' ? USER_PRINCIPAL : null);
BETTER.OWNED_COMPANIES = (typeof OWNED_COMPANIES !== 'undefined' ? OWNED_COMPANIES : []);

BETTER.initialize = function() {
    if (typeof BETTER.messaging !== 'undefined') {
        // Update personal unread messages count
        BETTER.messaging.addListener(BETTER.messaging.EVENT_UPDATE_NOTIFICATIONS, function(data) {
            var target = $('#topbar-messages .notification');

            if (data == null || !BETTER.IS_LOGGED_IN) {
                target.attr('data-count', 0).empty().hide();
                return;
            }

            var unreadPersonalThreads = (typeof data.personal !== 'undefined' ? Object.keys(data.personal).length : 0);

            if (unreadPersonalThreads > 0) {
                target.attr('data-count', unreadPersonalThreads).text(unreadPersonalThreads).show();
            } else {
                target.attr('data-count', 0).empty().hide();
            }
        });

        // Update companies unread messages count
        BETTER.messaging.addListener(BETTER.messaging.EVENT_UPDATE_NOTIFICATIONS, function(data) {
            var target = $('#topbar-account-icon .notification');
            var popover = $('#account-popover-container').add(target.find('.account-popover'));

            if (data == null || !BETTER.IS_LOGGED_IN) {
                popover.find('.company .notification').hide().empty().attr('data-count', 0);
                target.attr('data-count', 0).empty().hide();
                return;
            }

            var unreadThreads = data.companies;
            var keys = (typeof unreadThreads !== 'undefined' ? Object.keys(unreadThreads) : []);

            if (keys.length > 0) {
                var companyId;
                var length;
                var totalCompaniesUnreadNotifications = 0;

                for (var i = 0; i < keys.length; i++) {
                    companyId = keys[i];
                    length = Object.keys(unreadThreads[companyId]).length;
                    totalCompaniesUnreadNotifications += length;

                    if (length > 0) {
                        popover.find('.company[data-company-id="' + companyId + '"] .notification').html(length).attr('data-count', length).show();
                    } else {
                        popover.find('.company[data-company-id="' + companyId + '"] .notification').hide();
                    }
                }

                if (totalCompaniesUnreadNotifications > 0) {
                    target.attr('data-count', totalCompaniesUnreadNotifications).text(totalCompaniesUnreadNotifications).show();
                } else {
                    target.attr('data-count', 0).empty().hide();
                }
            } else {
                popover.find('.company .notification').hide().empty().attr('data-count', 0);
                target.attr('data-count', 0).empty().hide();
            }
        });
    }

    // CORS support
    $.ajaxPrefilter(function(options) {
        options.crossDomain = {
            crossDomain: true
        };
        options.xhrFields = {
            withCredentials: true
        };
    });

    // Keep session alive
    if (IS_LOGGED_IN) {
        window.setInterval(function() {
            $.get(BETTER.ROOT_DOMAIN + '/ajax/keep-alive');
        }, 600000); // 10 minutes
    }

    var domainName = this.getRootDomain();

    // Handling of new messages/threads for notifications
    if (typeof BETTER.messaging !== 'undefined') {
        BETTER.messaging.addListener(BETTER.messaging.EVENT_NEW_USER_MESSAGE, function(data) {
            var threadId = data.threadId;

            // If the thread is active in the inbox, then don't add notification
            if ($('.user-inbox .conversation.active[data-thread-id="' + threadId + '"]').length > 0) {
                return;
            }

            var cookieValue = BETTER.getCookieValue(BETTER.messaging.COOKIE_UNREAD_MESSAGES);

            if (cookieValue != null) {
                var unreadMessagesPerThread = JSON.parse(decodeURIComponent(cookieValue));

                if (typeof unreadMessagesPerThread.personal[threadId] === 'undefined' ) {
                    unreadMessagesPerThread.personal[threadId] = 1;
                } else {
                    unreadMessagesPerThread.personal[threadId]++;
                }

                BETTER.setCookie(BETTER.messaging.COOKIE_UNREAD_MESSAGES, encodeURIComponent(JSON.stringify(unreadMessagesPerThread)), null, domainName);
            } else {
                var obj = {
                    personal: {}
                };

                obj.personal[threadId] = 1;
                BETTER.setCookie(BETTER.messaging.COOKIE_UNREAD_MESSAGES, encodeURIComponent(JSON.stringify(obj)), null, domainName);
            }

            BETTER.messaging.refreshUnreadThreadsNotification();
        });

        BETTER.messaging.addListener(BETTER.messaging.EVENT_NEW_COMPANY_MESSAGE, function(data) {
            var threadId = data.threadId;
            var companyId = data.companyId;

            // If the thread is active in the inbox, then don't add notification
            if ($('.company-inbox .conversation.active[data-thread-id="' + threadId + '"]').length > 0) {
                return;
            }

            var cookieValue = BETTER.getCookieValue(BETTER.messaging.COOKIE_UNREAD_MESSAGES);

            if (cookieValue != null) {
                var unreadMessagesPerThread = JSON.parse(decodeURIComponent(cookieValue));

                if (typeof unreadMessagesPerThread.companies[companyId] === 'undefined' ) {
                    unreadMessagesPerThread.companies[companyId] = {};
                    unreadMessagesPerThread.companies[companyId][threadId] = 1;
                } else if (typeof unreadMessagesPerThread.companies[companyId][threadId] !== 'undefined') {
                    unreadMessagesPerThread.companies[companyId][threadId]++;
                } else {
                    unreadMessagesPerThread.companies[companyId][threadId] = 1;
                }

                BETTER.setCookie(BETTER.messaging.COOKIE_UNREAD_MESSAGES, encodeURIComponent(JSON.stringify(unreadMessagesPerThread)), null, domainName);
            } else {
                var obj = {
                    companies: {}
                };

                obj.companies[companyId] = {};
                obj.companies[companyId][threadId] = 1;
                BETTER.setCookie(BETTER.messaging.COOKIE_UNREAD_MESSAGES, encodeURIComponent(JSON.stringify(obj)), null, domainName);
            }

            BETTER.messaging.refreshUnreadThreadsNotification();
        });

        BETTER.messaging.addListener(BETTER.messaging.EVENT_NEW_THREAD, function(data) {
            var threadId = data.threadId;
            var companyId = data.companyId;
            var cookieValue = BETTER.getCookieValue(BETTER.messaging.COOKIE_UNREAD_MESSAGES);

            if (cookieValue != null) {
                var unreadMessagesPerThread = JSON.parse(decodeURIComponent(cookieValue));

                if (typeof unreadMessagesPerThread.companies != 'undefined') {
                    if (typeof unreadMessagesPerThread.companies[companyId] === 'undefined' ) {
                        unreadMessagesPerThread.companies[companyId] = {};
                        unreadMessagesPerThread.companies[companyId][threadId] = 1;
                    } else if (typeof unreadMessagesPerThread.companies[companyId][threadId] !== 'undefined') {
                        unreadMessagesPerThread.companies[companyId][threadId]++;
                    } else {
                        unreadMessagesPerThread.companies[companyId][threadId] = 1;
                    }

                    BETTER.setCookie(BETTER.messaging.COOKIE_UNREAD_MESSAGES, encodeURIComponent(JSON.stringify(unreadMessagesPerThread)), null, domainName);
                } else {
                    var obj = {
                        personal: {},
                        companies: {}
                    };

                    obj.companies[companyId] = {};
                    obj.companies[companyId][threadId] = 1;
                    BETTER.setCookie(BETTER.messaging.COOKIE_UNREAD_MESSAGES, encodeURIComponent(JSON.stringify(obj)), null, domainName);
                }
            } else {
                var obj = {
                    personal: {},
                    companies: {}
                };

                obj.companies[companyId] = {};
                obj.companies[companyId][threadId] = 1;
                BETTER.setCookie(BETTER.messaging.COOKIE_UNREAD_MESSAGES, encodeURIComponent(JSON.stringify(obj)), null, domainName);
            }

            BETTER.messaging.refreshUnreadThreadsNotification();
        });
    }
};

BETTER.setCookie = function(name, value, days, domain) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = '; expires=' + date.toGMTString();
    } else {
        expires = '';
    }

    var cookieString = name + '=' + value + expires + '; path=/';

    if (domain) {
        cookieString += '; domain=' + domain
    }

    document.cookie = cookieString;
};

// Source: http://www.quirksmode.org/js/cookies.html
BETTER.getCookieValue = function(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) == ' ') {
            c = c.substring(1, c.length);
        }

        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }

    return null;
};

BETTER.deleteCookie = function(name, path, domain) {
    if (path == null) {
        path = '/';
    }

    var cookieString = name + '=;path=' + path + ';expires=Thu, 01 Jan 1970 00:00:01 GMT;';

    if (domain != null) {
        cookieString += ';domain=' + domain + ';';
    }

    document.cookie = cookieString;
};

BETTER.nullifyIfEmpty = function(value) {
    if (typeof value === 'undefined') {
        return null;
    }

    if (value === '') {
        return null;
    }

    return value;
};

BETTER.nl2br = function(input) {
    return input.replace(/(?:\r\n|\r|\n)/g, '<br />');
};

BETTER.debounce = function(fn, delay) {
    var timer = null;

    return function() {
        var context = this, args = arguments;
        clearTimeout(timer);

        timer = setTimeout(function() {
            fn.apply(context, args);
        }, delay);
    };
};

BETTER.pad = function(source, totalLength, character) {
    if (source.length > totalLength) {
        return source;
    }

    character = typeof character !== 'undefined' ? character : '0';
    var pad = new Array(1 + totalLength).join(character);

    return (pad + source).slice(-pad.length);
};

BETTER.getUTCOffsetString = function() {
    var now = new Date();
    var offsetInSeconds = now.getTimezoneOffset();
    var offsetInHours = (offsetInSeconds != 0 ? parseFloat((offsetInSeconds / 60)).toFixed(0) : 0);
    var minutes = (offsetInSeconds % 60);

    if (offsetInHours >= 0) {
        offsetInHours = '+' + BETTER.pad(offsetInHours, 2);
    } else if (offsetInHours > -10) {
        offsetInHours = '-0' + offsetInHours.toString().substring(offsetInHours.toString().length - 1);
    }

    return offsetInHours + ':' + BETTER.pad(minutes, 2);
};

BETTER.setLoginDestination = function(destination, isRelative) {
    if (typeof isRelative !== 'undefined' && isRelative == true) {
        destination = window.location.href + destination;
    }

    $('form.login input[name=destination]').val(destination);
};

BETTER.setLoginHash = function(hash) {
    if (hash != null && typeof hash !== 'undefined') {
        if (hash.indexOf('#') > -1) {
            hash = hash.substr(1);
        }
    }

    $('form.facebook-login input[name=hash]').val(hash);
};

BETTER.getRootDomain = function(includeHttpScheme) {
    var parts = window.location.host.split('.').reverse();

    if (includeHttpScheme == true) {
        return window.location.protocol + '//' + parts[1] + '.' + parts[0]; // window.location.protocol already contains a semi-colon
    } else {
        return parts[1] + '.' + parts[0];
    }
};

BETTER.generateUUID = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
};

BETTER.upload = function (url, file, inputName, completeCallback, failedCallback, canceledCallback, progressCallback) {
    if (!file) {
        return;
    }

    if (typeof window.FormData === 'undefined') {
        alert("Din browser understøtter desværre ikke moderne web standarder. Opdatér venligst din browser for at benytte denne funktion.");
        return;
    }

    var fd = new FormData();
    fd.append(inputName, file);
    var xhr = new XMLHttpRequest();

    if (typeof progressCallback === 'function') {
        if ('onprogress' in xhr.upload) { // Feature check
            xhr.upload.addEventListener('progress', progressCallback, false);
        }
    }

    // This event is triggered when the server returns a response
    if (typeof completeCallback === 'function') {
        xhr.addEventListener('load', completeCallback, false);
    }

    if (typeof failedCallback === 'function') {
        xhr.addEventListener('error', failedCallback, false);
    }

    // Network error or similar - NOT a response from the server
    if (typeof canceledCallback === 'function') {
        xhr.addEventListener('abort', canceledCallback, false);
    }

    xhr.open('POST', url);
    xhr.withCredentials = true;

    // By using 'text' instead of 'json' and parsing the JSON manually, we don't have to worry about the browser
    // supporting 'json' (this is mainly a problem in IE10 + IE11)
    xhr.responseType = 'text';
    xhr.send(fd);
};

// todo: fully implement this
BETTER.normalize = function(input) {
    input = input.replace(/æ/g, 'ae');
    input = input.replace(/ø/g, 'oe');
    return input.replace(/å/g, 'aa');
};

BETTER.getSlug = function(value) {
    value = this.normalize(value);
    value = value.replace(/[^\sa-zA-Z0-9-]/g, '');
    value = value.replace(/\s/g, '-');
    value = value.replace(/-+/g, '-'); // Clean up consecutive hyphens
    value = value.replace(/-$/g, '');
    value = value.replace(/^-/g, '');

    return value.toLowerCase();
};

// Gets the value of a query string parameter
BETTER.qs = function(name) {
    var query = window.location.search.substring(1); // Remove question mark
    var parameters = query.split('&');

    for (var i = 0; i < parameters.length; i++) {
        var pair = parameters[i].split('=');

        if (pair[0] == name) {
            return pair[1];
        }
    }

    return null;
};

BETTER.getQueryParameter = function(name) {
    return this.qs(name);
};


/***** PUSH *****/
// todo: move this to a separate script when there is enough code to make it worth it - perhaps BETTER.notifications?
BETTER.push = BETTER.push || {};
BETTER.push.API_KEY = PUSHER_KEY;
BETTER.push.AUTH_ENDPOINT_USER = BETTER.ROOT_DOMAIN + '/push/auth/user/notifications';
BETTER.push.AUTH_ENDPOINT_COMPANY = BETTER.ROOT_DOMAIN + '/push/auth/company/notifications';
BETTER.push.transport = 'jsonp';

BETTER.push.subscribe = function(channelName, eventCallbackMap, authEndpoint) {
    if (typeof Pusher === 'undefined') {
        if (console) {
            var errorMessage = "Could not subscribe to push events for '" + eventName + "' because Pusher is not defined!";

            if (console.error) {
                console.error(errorMessage);
            } else {
                console.log(errorMessage);
            }
        }

        return;
    }

    var client = new Pusher(this.API_KEY, {
        authTransport: this.transport,
        authEndpoint: authEndpoint
    });

    var channel = client.subscribe(channelName);
    var keys = Object.keys(eventCallbackMap);

    for (var i = 0; i < keys.length; i++) {
        var callback = eventCallbackMap[keys[i]];

        if (typeof callback === 'function') {
            channel.bind(keys[i], callback);
        }
    }
};

BETTER.push.subscribeToCompanyNotifications = function(companies) {
    var map = {
        'new-message': function(data) {
            if (typeof BETTER.messaging.handleIncomingCompanyMessage == 'function') {
                BETTER.messaging.handleIncomingCompanyMessage(data.companyId, data.threadId, data.message);
            }
        },

        'new-thread': function(data) {
            if (typeof BETTER.messaging.handleNewThread == 'function') {
                BETTER.messaging.handleNewThread(data.companyId, data.threadId, data.subject, data.message);
            }
        }
    };

    for (var i = 0; i < companies.length; i++) {
        BETTER.push.subscribe('private-company-notifications-' + companies[i].id, map, BETTER.push.AUTH_ENDPOINT_COMPANY);
    }
};