$(function() {
    /***** UPLOAD LOGO & COVER PHOTO *****/
    function resetCoverPhotoUpload() {
        $('#upload-cover-photo').find('form')[0].reset();
    }

    function resetLogoPhotoUpload() {
        $('#upload-logo-photo').find('form')[0].reset();
    }

    $('#cover-photo-file').change(function() {
        var file = this.files[0];

        if (file) {
            // Validate file size
            var fileSizeInKilobytes = Math.round(file.size / 1000);

            if (fileSizeInKilobytes > 2048) {
                alert("Størrelsen på den valgte fil er for stor. Max 2048KB (2MB)");
                resetCoverPhotoUpload();
                return;
            }

            // Validate file type
            var permittedMimeTypes = ['image/png', 'image/jpeg'];

            if (permittedMimeTypes.indexOf(file.type) == -1) {
                alert("Den valgte fil type er ikke tilladt. Vælg venligst et JPG eller PNG billede.");
                resetCoverPhotoUpload();
                return;
            }

            var complete = function(event) {
                var statusCode = event.currentTarget.status;

                if (statusCode == 200) {
                    window.location.reload();
                } else if (statusCode == 400) {
                    var defaultMessage = "Billedet kunne ikke gemmes. Tjek venligst at billedet er under 2MB og at det er af typen PNG eller JPG.";

                    try {
                        var response = $.parseJSON(event.currentTarget.responseText);

                        if (response.entries.length > 0) {
                            alert(response.entries[0].message);
                        } else {
                            alert(defaultMessage);
                        }
                    } catch (error) {
                        alert(defaultMessage);
                    }
                } else {
                    alert("Der skete desværre en fejl da billedet skulle gemmes.");
                }

                resetCoverPhotoUpload();
            };

            var failed = function() {
                alert("Billedet kunne ikke gemmes, da der ikke kunne oprettes forbindelse til serveren.");
                resetCoverPhotoUpload();
            };

            var canceled = function() {
                alert("Billedet kunne ikke gemmes, da forbindelsen til serveren blev afbrudt.");
                resetCoverPhotoUpload();
            };

            var progress = function uploadProgress(event) {
                if (event.lengthComputable) {
                    var progressBar = $('#cover-photo-upload-progress').find('.progress-bar');
                    progressBar.show();

                    var percentComplete = Math.round((event.loaded * 100) / event.total);
                    progressBar.css('width', percentComplete.toString() + '%');

                    if (percentComplete == 100) {
                        progressBar.fadeOut(1500);
                    }
                }
            };

            var uploadUrl = BETTER.ROOT_DOMAIN + '/ajax/company/' + COMPANY_ID + '/upload/cover';
            BETTER.upload(uploadUrl, file, 'cover', complete, failed, canceled, progress);
        }
    });

    $('#logo-photo-file').change(function() {
        var file = this.files[0];

        if (file) {
            // Validate file size
            var fileSizeInKilobytes = Math.round(file.size / 1000);

            if (fileSizeInKilobytes > 1024) {
                alert("Størrelsen på den valgte fil er for stor. Max 1024KB (1MB)");
                resetLogoPhotoUpload();
                return;
            }

            // Validate file type
            var permittedMimeTypes = ['image/png', 'image/jpeg'];

            if (permittedMimeTypes.indexOf(file.type) == -1) {
                alert("Den valgte fil type er ikke tilladt. Vælg venligst et JPG eller PNG billede.");
                resetLogoPhotoUpload();
                return;
            }

            var complete = function(event) {
                var statusCode = event.currentTarget.status;

                if (statusCode == 200) {
                    window.location.reload();
                } else if (statusCode == 400) {
                    var defaultMessage = "Billedet kunne ikke gemmes. Tjek venligst at billedet er under 1MB og at det er af typen PNG eller JPG.";

                    try {
                        var response = $.parseJSON(event.currentTarget.responseText);

                        if (response.entries.length > 0) {
                            alert(response.entries[0].message);
                        } else {
                            alert(defaultMessage);
                        }
                    } catch (error) {
                        alert(defaultMessage);
                    }
                } else {
                    alert("Der skete desværre en fejl da billedet skulle gemmes.");
                }

                resetLogoPhotoUpload();
            };

            var failed = function() {
                alert("Billedet kunne ikke gemmes, da der ikke kunne oprettes forbindelse til serveren.");
                resetLogoPhotoUpload();
            };

            var canceled = function() {
                alert("Billedet kunne ikke gemmes, da forbindelsen til serveren blev afbrudt.");
                resetLogoPhotoUpload();
            };

            var uploadUrl = BETTER.ROOT_DOMAIN + '/ajax/company/' + COMPANY_ID + '/upload/logo';
            BETTER.upload(uploadUrl, file, 'logo', complete, failed, canceled);
        }
    });
});