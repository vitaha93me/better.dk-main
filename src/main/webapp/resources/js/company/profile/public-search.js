$(function() {
    $('.stars').stars();

    analytics.trackLink($('.search-result-actions .book-online'), 'Clicked Booking Button - Search', {
        company: {
            id: parseInt($(this).data('company-id'))
        }
    });

    $('.like-company').click(function() {
        var button = $(this);
        button.prop('disabled', true);
        var companyId = button.data('companyId');

        if (IS_LOGGED_IN) {
            $.ajax({
                type: 'POST',
                url: BETTER.ROOT_DOMAIN + '/api/company/' + companyId + '/likes',
                dataType: 'json',
                contentType: 'application/json'
            }).done(function(result, textStatus, xhr) {
                if (xhr.status == 201) { // Created
                    button.addClass('liked');
                }
            }).fail(function() {
                alert("Ups, der skete en fejl!");
                button.prop('disabled', null);
            });
        } else {
            $('#login-dialog').modal('show');
        }

        return false;
    });

    $('.favorite-company').click(function() {
        var button = $(this);
        button.prop('disabled', true);
        var companyId = button.data('companyId');

        if (IS_LOGGED_IN) {
            $.ajax({
                type: 'POST',
                url: BETTER.ROOT_DOMAIN + '/ajax/company/' + companyId + '/favorite',
                dataType: 'json',
                contentType: 'application/json'
            }).done(function() {
                button.addClass('added');
            }).fail(function() {
                alert("Ups, der skete en fejl!");
                button.prop('disabled', null);
            });
        } else {
            $('#login-dialog').modal('show');
        }

        return false;
    });
});