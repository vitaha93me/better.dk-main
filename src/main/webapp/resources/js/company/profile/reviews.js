$.fn.scoreChart = function() {
    var animateRightCircle = function(circle, targetHeight) {
        circle.animate({
            fontSize: targetHeight
        }, {
            duration: 500,
            step: function(now) {
                $(this).css('clip', 'rect(' + now + 'px, 62px, 125px, 0)');
            }
        });
    };

    return $(this).each(function() {
        var chart = $(this);
        var score = parseInt(chart.data('score'));
        var leftCircle = chart.find('.half-circle.left-side');
        var rightCircle = chart.find('.half-circle.right-side');
        var shouldAnimate = chart.data('animate');
        shouldAnimate = (shouldAnimate != null ? shouldAnimate : false);

        rightCircle.css('font-size', '125px');
        //leftCircle.css('font-size', '180px');

        if (score <= 50) {
            leftCircle.remove();
            var chartHeight = chart.height();
            var height = (chartHeight - ((chartHeight / 50) * score));

            if (shouldAnimate) {
                animateRightCircle(rightCircle, height);
            } else {
                rightCircle.css('clip', 'rect(' + height + 'px, 62px, 125px, 0)');
            }
        } else {
            var degree = ((180 / 50) * score);

            if (shouldAnimate) {
                animateRightCircle(rightCircle, 0);
                leftCircle.css('transform', 'rotate(180deg)').show();

                leftCircle.animate({
                    fontSize: degree
                }, {
                    duration: 500,
                    step: function(now) {
                        $(this).css('transform', 'rotate(' + now + 'deg)');
                    }
                });
            } else {
                rightCircle.css('clip', 'rect(0, 62px, 125px, 0)');
                leftCircle.css('transform', 'rotate(' + degree + 'deg)');
            }
        }
    });
};

function goToNewReview() {
    var target = $('#write-review');
    target.css('display', 'flex').hide().fadeIn(1000); // We set the display value, because fadeIn sets the display property back to the previous non-hidden value

    $('html, body').animate({
        scrollTop: target.offset().top
    }, 1000);
}

$(function() {
    $('.pie-chart').scoreChart();
    $('.choose-stars').chooseRating();

    // Animate progress bars
    window.setTimeout(function() {
        $('.progress-bar .progress').each(function(i) {
            var targetWidth = parseInt($(this).data('percentage'));

            if (targetWidth <= 0) {
                return; // Skip iteration
            }

            $(this).delay(i * 150).animate({
                width: targetWidth + '%'
            }, {
                duration: 500
            });
        });
    }, 500);

    $('#write-first-review, .review-button').on('click', function() {
        goToNewReview();
        return false;
    });

    // Submit review
    $('#write-review form').on('submit', function() {
        var button = $('#submit-review');
        $(button).prop('disabled', true);

        var text = $('#new-review-text');
        var rating = $('#write-review .choose-stars').attr('data-rating');
        var title = $('#new-review-title');
        var name = $('#new-review-name');
        var email = $('#new-review-email');

        if (text.val().length < 5) {
            alert("Din begrundelse skal være på mindst 5 tegn");
            $(button).prop('disabled', null);
            return false;
        }

        if (typeof rating === 'undefined') {
            alert("Du skal vælge en bedømmelse");
            $(button).prop('disabled', null);
            return false;
        }

        var obj = {
            rating: rating,
            title: title.val(),
            text: text.val(),
            name: name.val(),
            email: email.val()
        };

        $.ajax({
            type: 'POST',
            url: URL_SUBMIT_REVIEW,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(obj)
        }).done(function(result, textStatus, xhr) {
            if (xhr.status == 201) { // Created
                $('#write-review').fadeOut(1000, function() {
                    alert(TEXT_SUBMIT_REVIEW_SUCCESS_TITLE);
                });

                // Clear review form
                // improve: clear rating
                name.val('');
                email.val('');
                title.val('');
                text.val('');
            }
        }).fail(function(xhr) {
            $('#create-review-feedback-title').text(TEXT_SUBMIT_REVIEW_ERROR_TITLE);
            var response = $.parseJSON(xhr.responseText);

            switch (response.code) {
                case "error.invalid.arguments":
                    var message = response.entries[0].message;

                    if (typeof response.entries[0].field !== 'undefined') {
                        message = response.entries[0].field + " " + message;
                    }

                    alert(TEXT_SUBMIT_REVIEW_ERROR_TITLE + ': ' + message);
                    break;

                case "error.not.authorized": break; // todo: Handle this
                default: break; // todo: Handle other (general) error
            }
        }).always(function() {
            $(button).prop('disabled', null);
        });

        return false;
    });

    $('#abort-reviewing').click(function() {
        $('#write-review').fadeOut(1000);
        return false;
    });

    $('#load-more-reviews').click(function() {
        $(this).prop('disabled', true);
        var loadMoreReviewsButton = $(this);

        $.ajax({
            type: 'GET',
            url: $(this).data('next'),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function(result) {
            var hasNextLink = false;

            if (result.content.length > 0) {
                var template = $('#review-template').clone();
                var commentTemplate = $('#review-comment-template').clone();
                $(commentTemplate).filter(':first').removeAttr('id');
                $(template).filter(':first').removeAttr('id');
                var currentTemplate;
                var review;

                for (var i = 0; i < result.content.length; i++) {
                    review = result.content[i];
                    currentTemplate = $(template).clone();
                    $(currentTemplate).hide();

                    var photo = $(currentTemplate).find('.author .photo');
                    var name = $(currentTemplate).find('.author .name');
                    var verifiedCustomerWrapper = $(currentTemplate).find('.author .verified-customer-wrapper');
                    var title = $(currentTemplate).find('.details .title');

                    // Hide everything and only show them based on the info that is available
                    photo.hide();
                    verifiedCustomerWrapper.css('display', 'flex').hide();
                    name.hide();
                    title.hide();

                    if (review.author != null) {
                        if (review.author.profilePicture != null) {
                            photo.find('img').prop('src', review.author.profilePicture);
                            photo.show();
                        }

                        if (review.author.firstName != null) {
                            name.html(review.author.firstName + (review.author.lastName != null ? ' ' + review.author.lastName : '')).show();
                        }

                        if (review.title != null) {
                            title.html(review.title).show();
                        }
                    } else {
                        verifiedCustomerWrapper.show();
                    }

                    $(currentTemplate).find('.date-reviewed').html('Skrevet for ' + moment(review.created).fromNow());
                    $(currentTemplate).find('.stars').attr('data-stars', review.rating).stars();
                    $(currentTemplate).find('p').html(BETTER.nl2br(review.text));

                    // Comments
                    var commentsHTML = '<div class="review-comments">';

                    for (var j = 0; j < review.comments.length; j++) {
                        var comment = review.comments[j];
                        var currentCommentTemplate = $(commentTemplate).clone();
                        $(currentCommentTemplate).find('.details p').html(BETTER.nl2br(comment.text));
                        $(currentCommentTemplate).find('.author .photo img').attr('src', comment.company.logoUrl);

                        var title = $(currentCommentTemplate).find('.details .title');
                        title.html(title.data('prefix') + comment.company.name);

                        var reviewTime = $(currentCommentTemplate).find('.details .date-responded');
                        reviewTime.html(reviewTime.data('prefix') + moment(comment.created).fromNow());
                        commentsHTML += currentCommentTemplate.html();
                    }

                    if (i == 0) {
                        $('<div class="spacer-30"></div>').appendTo('#reviews-wrapper');
                    }

                    $(currentTemplate).appendTo('#reviews-wrapper');
                    $(commentsHTML + '</div><div class="clear"></div>').appendTo('#reviews-wrapper').show();

                    if (i != (result.content.length - 1)) {
                        $('<div class="spacer-30"></div>').appendTo('#reviews-wrapper');
                    }

                    $(currentTemplate).fadeIn(500);
                }

                // Search for next page link
                if (typeof result.links !== 'undefined') {
                    for (i = 0; i < result.links.length; i++) {
                        if (result.links[i].rel == 'next') {
                            loadMoreReviewsButton.data('next', result.links[i].href);
                            hasNextLink = true;
                        }
                    }
                }
            }

            if (!hasNextLink) {
                loadMoreReviewsButton.hide();
            }
        }).fail(function(xhr) {
            var response = $.parseJSON(xhr.responseText);

            // todo: handle this
            switch (response.code) {
                default: break;
            }
        }).always(function() {
            loadMoreReviewsButton.prop('disabled', null);
        });

        return false;
    });

    /***** HASHES *****/
    if (typeof window.location.hash !== 'undefined') {
        var hash = window.location.hash.substr(1); // Remove #

        switch (hash) {
            case 'anmeld': // Old hash
            case HASH_CREATE_REVIEW:
                window.location.hash = '';

                window.setTimeout(goToNewReview, 500);
                break;

            default: break;
        }
    }
});