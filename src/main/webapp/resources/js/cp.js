var BETTER = BETTER || {};
BETTER.CP = BETTER.CP || {};
BETTER.CP.IS_CLAIMED = IS_CLAIMED;
BETTER.CP.COMPANY_ID = COMPANY_ID;

BETTER.CP.initialize = function() {
    // Update notifications
    if (typeof BETTER.messaging !== 'undefined') {
        BETTER.messaging.addListener(BETTER.messaging.EVENT_UPDATE_NOTIFICATIONS, function(data) {
            var target = $('#cp-menu-messages .notification');
            var clear = function() {
                target.attr('data-count', 0).empty().hide();
            };

            if (data == null) {
                clear();
                return;
            }

            var unreadThreads = data.companies;
            var keys = (typeof unreadThreads !== 'undefined' ? Object.keys(unreadThreads) : []);

            if (keys.length > 0) {
                if (typeof unreadThreads[COMPANY_ID] !== 'undefined') {
                    var length = Object.keys(unreadThreads[COMPANY_ID]).length;
                    target.attr('data-count', length);

                    if (length > 0) {
                        target.html(length).show();
                    } else {
                        target.empty().hide();
                    }
                } else {
                    clear();
                }
            } else {
                clear();
            }
        });
    }

    // Create account dialog
    if (!BETTER.IS_LOGGED_IN && !BETTER.CP.IS_CLAIMED) {
        var openCreateAccountDialog = function() {
            $('#create-account-dialog').modal('show');
            return false;
        };

        window.setTimeout(openCreateAccountDialog, 3000);

        $('#new-message-text, #text-message-text, #send-text-message-to-all-customers, #company-phone-number').on('focus', openCreateAccountDialog);
        $('#send-new-message').on('click', openCreateAccountDialog);
    }
};

$(function() {
    var currentPage = window.location.href; // this.href returns an absolute URL, even if the href is relative
    currentPage = currentPage.split('#')[0]; // Remove hash
    currentPage = currentPage.replace(window.location.search, ''); // Remove query string

    // Mark active page in left menu
    $('.menu-item').each(function() {
        if (this.href == currentPage) {
            $(this).addClass('active');
        }
    });

    // Mark active page in page menu
    $('.page-menu li').each(function() {
        if ($(this).find('a').get(0).href == currentPage) {
            $(this).addClass('active');
            var parent = $(this).data('parent');

            if (typeof parent !== 'undefined') {
                $(parent).addClass('active');
            }
        }
    });

    BETTER.CP.initialize();

    /***** CREATE ACCOUNT DIALOG *****/
    $('#form-create-account').on('submit', function() {
        var button = $('#new-account-button');
        $(button).prop('disabled', true);

        $.ajax({
            type: 'POST',
            url: '/ajax/cp/company/' + BETTER.CP.COMPANY_ID + '/choose-password',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                accessToken: BETTER.qs('token'),
                checksum: BETTER.qs('checksum'),
                name: $('#new-account-name').val(),
                password: $('#new-account-password').val(),
                repeatPassword: $('#new-account-password-repeat').val()
            })
        }).done(function() {
            var redirectPostfix = '';

            if (typeof BETTER.messaging.inbox !== 'undefined') {
                var activeThreadId = BETTER.messaging.inbox.getActiveThreadId();

                if (activeThreadId != null) {
                    redirectPostfix = '?threadId=' + activeThreadId;
                }
            }

            window.location = '/cp/company/' + BETTER.CP.COMPANY_ID + '/messages' + redirectPostfix;
        }).fail(function(xhr) {
            var response = $.parseJSON(xhr.responseText);

            switch (response.code) {
                case 'error.invalid.arguments':
                    var message = response.entries[0].message;

                    if (typeof response.entries[0].field !== 'undefined') {
                        message = response.entries[0].field + ": " + message;
                    }

                    alert(message);
                    break;

                default: break;
            }
        }).always(function() {
            $(button).prop('disabled', null);
        });

        return false;
    });
});