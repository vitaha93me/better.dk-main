var BETTER = BETTER || {};
BETTER.notifications = {};
BETTER.notifications.URL_IMAGE_BASE = 'https://s3.eu-central-1.amazonaws.com/betterdk-public/static/cta/';

BETTER.notifications.TYPE_NOTIFICATION = 'notification';
BETTER.notifications.TYPE_SEARCH_BANNER = 'search_banner';
BETTER.notifications.TYPE_PROFILE_BANNER = 'profile_banner';
BETTER.notifications.TYPE_TEXT = 'text';
BETTER.notifications.TYPE_MODAL = 'modal';
BETTER.notifications.TYPE_SPLASH = 'splash';

// todo: refactor keywords to array of strings
BETTER.notifications.relevantNotificationsKeywords = [
    {
        keywords: [
            { search: 'badeværelse' },
            { search: 'anlægsgartner' },
            { search: 'gartner' },
            { search: 'haveservice' },
            { search: 'havearbejde' },
            { search: 'el-installatør' },
            { search: 'elektriker' },
            { search: 'byg' },
            { search: 'maler' },
            { search: 'murer' },
            { search: 'nedrivning' },
            { search: 'nedbrydning' },
            { search: 'tømrer' },
            { search: 'snedker' },
            { search: 'tømremester' },
            { search: 'vvs' },
            { search: 'blikkenslager' },
            { search: 'glarmester' },
            { search: 'indretningsarkitekt' },
            { search: 'arkitekt' },
            { search: 'brolægger' },
            { search: 'isolering' },
            { search: 'entreprenør' },
            { search: 'ingeniør' },
            { search: 'renovering' },
            { search: 'ventilation' },
            { search: 'kloak' },
            { search: 'entreprise' },
            { search: 'malerarbejde' },
            { search: 'maler' },
            { search: 'tagdækker' },
            { search: 'rengøring' },
            { search: 'entreprise' },
            { search: 'gulvbelægning' },

            // Hungry
            { search: 'pizza' },
            { search: 'pizzeria' },
            { search: 'sushi' },
            { search: 'thaimad' },
            { search: 'restaurant' },
            { search: 'kebab' },
            { search: 'burger' },
            { search: 'fastfood' },
            { search: 'grillmad' },
            { search: 'durum' },

            // Agera
            { search: 'advokat' },
            { search: 'bogholder' },
            { search: 'revisor' },

            // Fragtopgaver
            { search: 'fragt' },
            { search: 'vognmand' },
            { search: 'transport' },
            { search: 'kurer' },
            { search: 'kurér' },

            // Tandbud
            { search: 'tandlæg' },
            { search: 'tandteknik' },
            { search: 'tandprotetik' }
        ],
        types: {
            search_banner: true,
            splash: true
        }
    }, {
        keywords: [
            { search: 'tandlæg' },
            { search: 'tandteknik' },
            { search: 'tandprotetik' }
        ],
        types: {
            profile_banner: true
        }
    }
];

/**
 * Attempts to find a relevant notification based on a given value
 *
 * @param valuesToSearch The value to perform the lookup for
 * @param type The desired type of the notification
 * @return string|NULL
 */
BETTER.notifications.getMatchingKeyword = function(valuesToSearch, type) {
    if (typeof valuesToSearch === 'undefined' || valuesToSearch == null) {
        return null; // improve: trigger error
    }

    if (typeof type === 'undefined' || type == null) {
        return null; // improve: trigger error
    }

    var entries = BETTER.notifications.relevantNotificationsKeywords;

    for (var i = 0; i < entries.length; i++) {
        var entry = entries[i];

        if ($.inArray(type, Object.keys(entry.types)) != -1) { // Verify that the entry supports the desired type
            for (var j = 0; j < entry.keywords.length; j++) {
                var keyword = entry.keywords[j].search;
                var regex = new RegExp(keyword, 'gi');
                var searchValues = (typeof valuesToSearch.constructor === Array ? valuesToSearch : [valuesToSearch]);

                for (var k = 0; k < searchValues.length; k++) {
                    if (regex.test(searchValues[k])) {
                        return keyword;
                    }
                }
            }
        }
    }

    return null;
};