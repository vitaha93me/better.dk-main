$(function() {
    $('.accept-subdomain').click(function() {
        if (!confirm(ACCEPT_CONFIRM_TEXT)) {
            return false;
        }

        var requestId = parseInt($(this).parents('.request').data('request-id'));

        $.ajax({
            type: 'POST',
            url: '/ajax/admin/requests/subdomains/' + requestId + '/accept',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function(result, textStatus, xhr) {
            if (xhr.status == 200) {
                $('div.request[data-request-id="' + requestId + '"').fadeOut(500, function() {
                    $(this).remove();
                });
            }
        }).fail(function(xhr) {
            var response = $.parseJSON(xhr.responseText);
            // improve: handle this
            switch (response.code) {
                case "error.unique.constraint.violation": alert(ERROR_SUBDOMAIN_TAKEN); break;

                default: break;
            }
        });

        return false;
    });

    $('.decline-subdomain').click(function() {
        var requestWrapper = $(this).parents('.request');
        $('#decline-request-for').text(requestWrapper.data('company-name'));
        $('#decline-request-reason').val('');
        $('#decline-reason-dialog').data('request-id', requestWrapper.data('request-id')).modal('show');

        return false;
    });

    $('#button-do-decline-request').click(function() {
        var dialog = $('#decline-reason-dialog');
        var requestId = parseInt(dialog.data('request-id'));
        var reasonTextarea = $('#decline-request-reason');
        var reason = reasonTextarea.val();
        reason = ((reason.length == 0) ? null : reason);

        // Clear dialog data
        $('#decline-request-for').text('');
        reasonTextarea.val('');
        dialog.data('request-id', null);

        $.ajax({
            type: 'POST',
            url: '/ajax/admin/requests/subdomains/' + requestId + '/decline',
            dataType: 'json',
            data: {
                reason: reason
            }
        }).done(function(result, textStatus, xhr) {
            if (xhr.status == 200) {
                $('div.request[data-request-id="' + requestId + '"').fadeOut(500, function() {
                    $(this).remove();
                });
            }
        }).fail(function(xhr) {
            alert("error!"); // improve: handle
        });

        $(dialog).modal('hide');
        return false;
    });
});