$(function() {
    var currentSubdomain = BETTER.nullifyIfEmpty($('#subdomain-name').val().trim());
    //var acknowledgementIndex = 0;
    var serviceIndex = 0;

    var fieldValidationContainerMapping = {
        industries: 'industries-feedback',
        'subdomains[].name': 'subdomain-feedback'
    };

    function clearErrorMessages() {
        $('.error-message').empty();
    }

    function clearNewServiceForm() {
        $('#add-new-service-dialog').find('form')[0].reset();
    }

    /*function refreshAcknowledgements(industryIds) {
        $.ajax({
            type: 'GET',
            url: BETTER.ROOT_DOMAIN + '/api/industry/' + industryIds.join() + '/acknowledgements',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function(result, textStatus, xhr) {
            if (xhr.status == 200) {
                var existingAcknowledgements = $('.acknowledgement input[type="checkbox"]').map(function() {
                    return parseInt(this.value);
                }).get();

                var allAcknowledgements = [];

                $.each(result.content, function() {
                    var id = parseInt(this.id);
                    allAcknowledgements.push(id);

                    // Display the acknowledgement if it is not displayed already
                    if ($.inArray(id, existingAcknowledgements) == -1) {
                        existingAcknowledgements.push(id);
                        acknowledgementIndex++;

                        var html = '<div class="acknowledgement" data-id="' + this.id + '" data-index="' + acknowledgementIndex + '">';
                        html += '<input type="checkbox" name="acknowledgements[' + acknowledgementIndex + '].id" id="acknowledgements' + acknowledgementIndex + '" value="' + this.id + '" />';
                        html += '<label for="acknowledgements' + acknowledgementIndex + '">';
                        html += '<img src="' + BETTER.ROOT_DOMAIN + '/images/certifications/profile/' + this.imageName + '" alt="' + this.name + '" />';
                        html += '</label>';
                        html += '</div>';

                        $('#acknowledgements-wrapper').append(html);
                    }
                });

                // Remove acknowledgements that are no longer available
                $.each(existingAcknowledgements, function(index, value) {
                    if ($.inArray(value, allAcknowledgements) == -1) {
                        serviceIndex--;
                        $('.acknowledgement[data-id="' + value + '"]').remove();
                    }
                });
            }
        }).fail(function(xhr) {
            var response = $.parseJSON(xhr.responseText);
            // todo: handle this
            switch (response.code) {
                case "error.not.authenticated": alert(ERROR_NOT_AUTHENTICATED); break;

                default: break;
            }
        });
    }*/

    function refreshServices(industryIds) {
        $.ajax({
            type: 'GET',
            url: BETTER.ROOT_DOMAIN + '/api/industry/' + industryIds.join() + '/services',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function(result, textStatus, xhr) {
            if (xhr.status == 200) {
                var existingServices = $('.service input[type="checkbox"]').map(function() {
                    return parseInt(this.value);
                }).get();

                var allServices = [];

                $.each(result.content, function() {
                    var id = parseInt(this.id);
                    allServices.push(id);

                    // Display the service if it is not displayed already
                    if ($.inArray(id, existingServices) == -1) {
                        existingServices.push(id);
                        serviceIndex++;

                        var html = '<div class="service" data-id="' + this.id + '" data-index="' + serviceIndex + '">';
                        html += '<input type="checkbox" name="services[' + serviceIndex + '].id" id="services' + serviceIndex + '" value="' + this.id + '" /> ';
                        html += '<label for="services' + serviceIndex + '">' + this.name + '</label>';
                        html += '<textarea name="description" class="service-description" style="display: none;"></textarea>';
                        html += '</div>';

                        $('#services-wrapper').append(html);
                    }
                });

                // Remove services that are no longer available
                $.each(existingServices, function(index, value) {
                    if ($.inArray(value, allServices) == -1) {
                        serviceIndex--;
                        $('.service[data-id="' + value + '"]').remove();
                    }
                });
            }
        }).fail(function(xhr) {
            var response = $.parseJSON(xhr.responseText);
            // todo: handle this
            switch (response.code) {
                case "error.not.authenticated": alert(ERROR_NOT_AUTHENTICATED); break;

                default: break;
            }
        });
    }

    $('.industry input[type="checkbox"]').change(function() {
        var currentIndustryIds = $('.industry input[type="checkbox"]:checked').map(function() {
            return parseInt(this.value);
        }).get();

        if (currentIndustryIds.length > 0) {
            //refreshAcknowledgements(currentIndustryIds);
            refreshServices(currentIndustryIds);
        } else {
            // Clear services and acknowledgements
            $('#services-wrapper').empty();
            $('#acknowledgements-wrapper').empty();
        }
    });

    $('#services-wrapper').on('change', '.service input[type="checkbox"]', function() {
        $(this).siblings('.service-description').toggle();
    });

    $('#submitChanges').click(function() {
        // The following code uses document.getElementById() instead of a jQuery selector because some IDs include periods/dots
        clearErrorMessages();
        var companyId = parseInt($(document.getElementById('id')).val());

        var object = {
            id: companyId,
            name: BETTER.nullifyIfEmpty($(document.getElementById('name')).val()),
            streetName: BETTER.nullifyIfEmpty($(document.getElementById('streetName')).val()),
            streetNumber: BETTER.nullifyIfEmpty($(document.getElementById('streetNumber')).val()),
            phoneNumber: BETTER.nullifyIfEmpty($(document.getElementById('phoneNumber')).val()),
            callTrackingNumber: BETTER.nullifyIfEmpty($(document.getElementById('callTrackingNumber')).val()),
            teaser: BETTER.nullifyIfEmpty($(document.getElementById('teaser')).val()),
            description: BETTER.nullifyIfEmpty($(document.getElementById('description')).val()),
            ciNumber: BETTER.nullifyIfEmpty($(document.getElementById('ciNumber')).val()),
            email: BETTER.nullifyIfEmpty($(document.getElementById('email')).val()),
            website: BETTER.nullifyIfEmpty($(document.getElementById('website')).val()),
            coName: BETTER.nullifyIfEmpty($(document.getElementById('coName')).val()),
            postBox: BETTER.nullifyIfEmpty($(document.getElementById('postBox')).val()),
            postalCode: BETTER.nullifyIfEmpty($(document.getElementById('postalCode')).val()),
            postalDistrict: BETTER.nullifyIfEmpty($(document.getElementById('postalDistrict')).val()),
            city: BETTER.nullifyIfEmpty($(document.getElementById('city')).val()),
            municipal: BETTER.nullifyIfEmpty($(document.getElementById('municipal')).val()),
            additionalAddressText: BETTER.nullifyIfEmpty($(document.getElementById('additionalAddressText')).val()),
            bookingUrl: BETTER.nullifyIfEmpty($(document.getElementById('bookingUrl')).val()),
            meta: {
                id: $(document.getElementById('meta.id')).val(),
                googlePlusUrl: BETTER.nullifyIfEmpty($(document.getElementById('meta.googlePlusUrl')).val()),
                facebookUrl: BETTER.nullifyIfEmpty($(document.getElementById('meta.facebookUrl')).val()),
                yelpUrl: BETTER.nullifyIfEmpty($(document.getElementById('meta.yelpUrl')).val()),
                omdoemmedkUrl: BETTER.nullifyIfEmpty($(document.getElementById('meta.omdoemmedkUrl')).val()),
                bedoemmelsedkUrl: BETTER.nullifyIfEmpty($(document.getElementById('meta.bedoemmelsedkUrl')).val())
            }
        };

        // Subdomain - only add to data if it has been changed
        var subdomain = BETTER.nullifyIfEmpty($(document.getElementById('subdomain-name')).val().trim());

        if (subdomain != currentSubdomain) {
            object.subdomains = [
                {
                    name: subdomain,
                    active: true,
                    company: {
                        id: companyId
                    }
                }
            ];
        }

        // Industries
        object.industries = [];

        $('.industry input[type="checkbox"]:checked').each(function() {
            object.industries.push({
                id: parseInt($(this).val()),
                name: $(this).data('industry-name')
            });
        });

        // Acknowledgements
        /*object.acknowledgements = [];

        $('.acknowledgement input[type="checkbox"]:checked').each(function() {
            object.acknowledgements.push({ id: parseInt($(this).val()) });
        });*/

        // Services
        object.companyServices = [];

        $('.service input[type="checkbox"]:checked').each(function() {
            var description = $(this).siblings('.service-description').val();
            description = ((description != '') ? description : null);

            object.companyServices.push({
                description: description,
                service: {
                    id: parseInt($(this).val())
                },
                company: {
                    id: companyId
                }
            });
        });

        $.ajax({
            type: 'PUT',
            url: BETTER.ROOT_DOMAIN + '/api/company/' + companyId,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(object)
        }).done(function(result, textStatus, xhr) {
            if (xhr.status == 200) {
                alert("The company has been saved."); // improve: translate
                window.location.reload();
            }
        }).fail(function(xhr) {
            var response = $.parseJSON(xhr.responseText);

            switch (response.code) {
                case "error.not.authenticated": alert(ERROR_NOT_AUTHENTICATED); break;

                case "error.invalid.arguments":
                    $.each(response.entries, function() {
                        var container = document.getElementById(this.field);

                        if (container != null && typeof container !== 'undefined') {
                            $(container).parent().next().html(this.message);
                        } else {
                            if (this.field in fieldValidationContainerMapping) {
                                $('#' + fieldValidationContainerMapping[this.field]).html(this.message);
                            }
                        }
                    });

                    break;

                default: alert("An error occurred!"); break; // improve: translate
            }
        });

        return false;
    });

    /***** SUBDOMAIN CHECK *****/
    $('#subdomain-name').keyup(BETTER.debounce(function() {
        var input = $(this);
        var feedbackContainer = $(this).parent('div').next();

        // Clear feedback styling
        feedbackContainer.empty();
        $('#submitChanges').prop('disabled', null);

        if (input.val().length == 0) {
            return;
        }

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: BETTER.ROOT_DOMAIN + '/api/subdomain/' + input.val()
        }).done(function(result, textStatus, xhr) {
            if (xhr.status == 200) {
                feedbackContainer.text('The subdomain is taken'); // improve: translate
                $('#submitChanges').prop('disabled', true);
            }
        }).fail(function(xhr) {
            if (xhr.status == 404) {
                feedbackContainer.text('The subdomain is available'); // improve: translate
            }
        });
    }, 500));

    /***** ADD NEW SERVICE *****/
    $('#add-new-service a').click(function() {
        $('#add-new-service-dialog').modal('show');
        return false;
    });

    $('#add-new-service-button').click(function() {
        var data = {
            name: $(document.getElementById('service-name')).val(),
            defaultDescription: $(document.getElementById('service-default-description')).val(),
            slug: $(document.getElementById('service-slug')).val()
        };

        // Industries
        data.industries = [];

        $('.new-service-industry-wrapper input[type="checkbox"]:checked').each(function() {
            data.industries.push({
                id: parseInt($(this).val())
            });
        });

        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: BETTER.ROOT_DOMAIN + '/api/service',
            data: JSON.stringify(data)
        }).done(function(result, textStatus, xhr) {
            if (xhr.status == 201) { // Created
                alert("The service was created"); // improve: translate
                clearNewServiceForm();

                // Add new service to form
                serviceIndex++;
                var service = $.parseJSON(xhr.responseText);

                var html = '<div class="service" data-id="' + service.id + '" data-index="' + serviceIndex + '">';
                html += '<input type="checkbox" name="services[' + serviceIndex + '].id" id="services' + serviceIndex + '" value="' + service.id + '" /> ';
                html += '<label for="services' + serviceIndex + '">' + service.name + '</label>';
                html += '<textarea name="description" class="service-description" style="display: none;"></textarea>';
                html += '<br />';

                $('#services-wrapper').append(html);
            }
        }).fail(function(xhr) {
            var response = $.parseJSON(xhr.responseText);

            switch (response.code) {
                case "error.not.authenticated": alert(ERROR_NOT_AUTHENTICATED); break;

                case "error.invalid.arguments":
                    $.each(response.entries, function() {
                        $(document.getElementById('service-' + this.field)).parent().next().html(this.message);
                    });

                    break;

                default: break;
            }
        });

        return false;
    });

    $('#new-service-reset').click(function() {
        clearNewServiceForm();
        return false;
    });

    /***** INITIALIZATION *****/
    /*$('div.acknowledgement').each(function() {
        acknowledgementIndex = parseInt($(this).data('index'));
    });*/

    $('div.service').each(function() {
        serviceIndex = parseInt($(this).data('index'));
    });
});