$(function() {
    // Add listeners
    BETTER.messaging.addListener(BETTER.messaging.EVENT_NEW_USER_MESSAGE, function(data) {
        BETTER.messaging.inbox.handleIncomingUserMessage(data.threadId, data.message);
    });

    BETTER.messaging.addListener(BETTER.messaging.EVENT_UPDATE_NOTIFICATIONS, function(data) {
        var unreadMessages = BETTER.messaging.getNumberOfUnreadMessagesForUser(data);

        if (unreadMessages > 0) {
            $('#unread-messages-count').html('(' + unreadMessages + ')').fadeIn(250);
        } else {
            $('#unread-messages-count').empty().fadeOut(250);
        }
    });

    if (!BETTER.IS_LOGGED_IN) {
        var showLoginPrompt = function() {
            $('#user-inbox-login-dialog').modal('show');
        };

        window.setTimeout(showLoginPrompt, 3000);

        $('#new-message-text').on('focus', showLoginPrompt);
        $('#send-new-message').on('click', showLoginPrompt);

        $('#form-create-account').on('submit', function() {
            var button = $('#new-account-button');
            $(button).prop('disabled', true);

            $.ajax({
                type: 'POST',
                url: '/ajax/account/merge',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({
                    accessToken: BETTER.qs('token'),
                    checksum: BETTER.qs('checksum'),
                    name: $('#new-account-name').val(),
                    password: $('#new-account-password').val(),
                    repeatPassword: $('#new-account-password-repeat').val()
                })
            }).done(function() {
                var redirectPostfix = '';

                if (typeof BETTER.messaging.inbox !== 'undefined') {
                    var activeThreadId = BETTER.messaging.inbox.getActiveThreadId();

                    if (activeThreadId != null) {
                        redirectPostfix = '?threadId=' + activeThreadId;
                    }
                }

                window.location = BETTER.ROOT_DOMAIN + '/messages' + redirectPostfix;
            }).fail(function(xhr) {
                var response = $.parseJSON(xhr.responseText);

                switch (response.code) {
                    case 'error.invalid.arguments':
                        var message = response.entries[0].message;

                        if (typeof response.entries[0].field !== 'undefined') {
                            message = response.entries[0].field + ": " + message;
                        }

                        alert(message);
                        break;

                    default: break;
                }
            }).always(function() {
                $(button).prop('disabled', null);
            });

            return false;
        });
    }
});