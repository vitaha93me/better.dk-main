$(function() {
    $('.choose-stars').chooseRating();

    $('#review-email-button').click(function() {
        $('#review-email-wrapper').fadeIn(500);
        return false;
    });

    $('.fb-login').click(function() {
        var rating = $('#review-stars-wrapper').data('rating');

        var obj = {
            rating: (rating != null ? parseInt(rating) : null),
            text: $('#review-text').val(),
            email: BETTER.nullifyIfEmpty($('#review-email').val())
        };

        BETTER.setCookie('landing-review-mobile', JSON.stringify(obj));
    });

    $('#submit-review').click(function() {
        var button = $(this);
        $(button).prop('disabled', true);
        var rating = $('#review-stars-wrapper .choose-stars').data('rating');

        var obj = {
            rating: (rating != null ? parseInt(rating) : null),
            text: $('#review-text').val(),
            email: BETTER.nullifyIfEmpty($('#review-email').val()),
            phoneNumber: PHONE_NUMBER
        };

        $.ajax({
            type: 'POST',
            url: URL_SUBMIT_REVIEW,
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(obj)
        }).done(function() {
            $('#create-review-wrapper').fadeOut(500);
            $('#review-success').fadeIn(500);
        }).fail(function(xhr) {
            $(button).prop('disabled', null);
            var response = $.parseJSON(xhr.responseText);

            switch (response.code) {
                case "error.invalid.arguments":
                    var message = response.entries[0].message;

                    if (typeof response.entries[0].field !== 'undefined') {
                        message = response.entries[0].field + " " + message;
                    }

                    alert(message);
                    break;

                default: alert("Ups, der skete en fejl!"); break;
            }
        });

        return false;
    });

    // Fill in data from cookie
    var cookie = BETTER.getCookieValue('landing-review-mobile');

    if (cookie != null) {
        var json = JSON.parse(cookie);
        $('#review-text').val(json.text);

        if (json.email != null) {
            $('#review-email').val(json.email);
        }

        if (json.rating != null) {
            chooseRating(json.rating);
        }
    }
});