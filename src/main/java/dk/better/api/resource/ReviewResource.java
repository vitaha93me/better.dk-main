package dk.better.api.resource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReviewResource {
    private int id;
    private int rating;
    private String title;
    private String text;
    private Date created;
    private Collection<ReviewCommentResource> comments = new ArrayList<>();
    private AccountResource author;

    // improve: add CompanyResource


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public AccountResource getAuthor() {
        return author;
    }

    public void setAuthor(AccountResource author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Collection<ReviewCommentResource> getComments() {
        return comments;
    }

    public void setComments(Collection<ReviewCommentResource> comments) {
        this.comments = comments;
    }
}