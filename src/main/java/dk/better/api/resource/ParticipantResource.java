package dk.better.api.resource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ParticipantResource {
    private int id;
    private AccountResource account;
    private CompanyResource company;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AccountResource getAccount() {
        return account;
    }

    public void setAccount(AccountResource account) {
        this.account = account;
    }

    public CompanyResource getCompany() {
        return company;
    }

    public void setCompany(CompanyResource company) {
        this.company = company;
    }
}