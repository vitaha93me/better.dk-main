package dk.better.api.resource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceResource {
    private int id;
    private String name;
    private String defaultDescription;
    private String slug;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDefaultDescription() {
        return defaultDescription;
    }

    public void setDefaultDescription(String defaultDescription) {
        this.defaultDescription = defaultDescription;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}