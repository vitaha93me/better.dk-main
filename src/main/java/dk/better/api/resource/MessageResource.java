package dk.better.api.resource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MessageResource {
    private int id;
    private String text;
    private Long created;
    private ParticipantResource sender;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public ParticipantResource getSender() {
        return sender;
    }

    public void setSender(ParticipantResource sender) {
        this.sender = sender;
    }
}