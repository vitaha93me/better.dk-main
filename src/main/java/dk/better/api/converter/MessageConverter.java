package dk.better.api.converter;

import dk.better.account.entity.Account;
import dk.better.account.service.AccountService;
import dk.better.api.resource.AccountResource;
import dk.better.api.resource.CompanyResource;
import dk.better.api.resource.MessageResource;
import dk.better.api.resource.ParticipantResource;
import dk.better.application.util.GeneralUtils;
import dk.better.application.util.ViewUtils;
import dk.better.company.entity.Company;
import dk.better.thread.entity.Message;
import dk.better.thread.entity.Participant;
import org.springframework.core.convert.converter.Converter;
import org.springframework.web.util.HtmlUtils;

/**
 * @author Bo Andersen
 */
public class MessageConverter implements Converter<Message, MessageResource> {
    @Override
    public MessageResource convert(Message source) {
        // Sender (participant)
        Participant participantEntity = source.getSender();
        ParticipantResource participant = new ParticipantResource();
        participant.setId(participantEntity.getId());

        // Account
        Account account = participantEntity.getAccount();

        if (account != null) {
            AccountResource accountResource = new AccountResource();
            accountResource.setId(account.getUuid());
            accountResource.setFirstName(account.getFirstName());
            accountResource.setMiddleName(account.getMiddleName());
            accountResource.setLastName(account.getLastName());

            if (account.getFacebookProfileId() != null) {
                accountResource.setProfilePicture(AccountService.getProfilePictureUrl(account.getFacebookProfileId()));
            } else {
                accountResource.setProfilePicture("https://better.dk/images/default-account-photo.png"); // todo: make this dynamic
            }

            participant.setAccount(accountResource);
        }

        // Company
        Company company = participantEntity.getCompany();

        if (company != null) {
            CompanyResource companyResource = new CompanyResource();
            companyResource.setId(company.getId());
            companyResource.setName(company.getName());
            companyResource.setProfileUrl(company.getProfileUrl());
            String baseUrl = ViewUtils.getStaticResourceUrl();

            if (company.getLogoName() != null) {
                companyResource.setLogoUrl(baseUrl + "/" + company.getLogoName());
            } else {
                companyResource.setLogoUrl(baseUrl + "/companies/logos/default/default.png");
            }

            participant.setCompany(companyResource);
        }

        // Message
        MessageResource target = new MessageResource();
        target.setId(source.getId());
        target.setText(GeneralUtils.nl2br(HtmlUtils.htmlEscape(source.getText())));
        target.setCreated(source.getCreated().getTime());
        target.setSender(participant);

        return target;
    }
}