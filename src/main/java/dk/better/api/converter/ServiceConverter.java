package dk.better.api.converter;

import dk.better.api.resource.ServiceResource;
import dk.better.company.entity.Service;
import org.springframework.core.convert.converter.Converter;
import org.springframework.web.util.HtmlUtils;

/**
 * @author Bo Andersen
 */
public class ServiceConverter implements Converter<Service, ServiceResource> {
    @Override
    public ServiceResource convert(Service service) {
        ServiceResource resource = new ServiceResource();
        resource.setId(service.getId());
        resource.setName(HtmlUtils.htmlEscape(service.getName()));
        resource.setSlug(HtmlUtils.htmlEscape(service.getSlug()));
        resource.setDefaultDescription(HtmlUtils.htmlEscape(service.getDefaultDescription()));

        return resource;
    }
}