package dk.better.api.filter;

import dk.better.application.util.GeneralUtils;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This filter allows CORS requests, which is required for making AJAX requests from subdomains to the TLD
 *
 * @author Bo Andersen
 */
@Component
public class CorsFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException { }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String origin = request.getHeader("Origin"); // Returns NULL if not available

        if (origin == null) {
            response.setHeader("Access-Control-Allow-Origin", null);
        } else {
            // We have an origin; validate that it belongs to the TLD
            String domain = GeneralUtils.getRootDomain(request);
            domain = domain.replace(".", "[.]"); // Prepare string for regex
            String regex = "https?://([a-zA-Z0-9æøå\\-]+[.])*" + domain;
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(origin);

            // If the origin satisfies the regex, then allow it - otherwise don't set the header
            if (matcher.find()) {
                response.setHeader("Access-Control-Allow-Origin", origin);
            }
        }

        response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers")); // Allow all
        response.setHeader("Access-Control-Allow-Credentials", "true");
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() { }
}