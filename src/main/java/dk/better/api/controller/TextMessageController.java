package dk.better.api.controller;

import dk.better.application.model.EmptyJsonResponse;
import dk.better.company.service.TextMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Bo Andersen
 */
@RestController("apiTextMessageController")
@RequestMapping("/api/text-message")
public class TextMessageController {
    @Autowired
    private TextMessageService textMessageService;


    @RequestMapping(value = "/update-status", method = RequestMethod.GET)
    public ResponseEntity<EmptyJsonResponse> updateStatus(@RequestParam("messageid") String messageId, @RequestParam("statuscode") int statusCode) {
        this.textMessageService.updateTextMessageCustomerStatus(messageId, statusCode);
        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }
}