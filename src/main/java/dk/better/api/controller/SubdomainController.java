package dk.better.api.controller;

import dk.better.api.converter.SubdomainConverter;
import dk.better.api.resource.SubdomainResource;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.company.entity.Subdomain;
import dk.better.company.service.SubdomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Bo Andersen
 */
@RestController
@RequestMapping("/api/subdomain")
public class SubdomainController {
    @Autowired
    private SubdomainService subdomainService;

    @RequestMapping(value = "/{subdomainName}", method = RequestMethod.GET)
    public ResponseEntity<Resource<SubdomainResource>> getByName(HttpServletRequest request, @PathVariable("subdomainName") String name) {
        if (name == null || name.equals("")) {
            throw new InvalidArgumentException("A subdomain name must be provided");
        }

        Subdomain subdomain = this.subdomainService.findByName(name);

        if (subdomain == null) {
            return new ResponseEntity<Resource<SubdomainResource>>(null, null, HttpStatus.NOT_FOUND);
        }

        // Links
        Converter<Subdomain, SubdomainResource> converter = new SubdomainConverter();
        Resource<SubdomainResource> payload = new Resource<>(converter.convert(subdomain));
        Collection<Link> links = new ArrayList<>();
        String requestUri = request.getRequestURI();
        String queryString = request.getQueryString();

        if (queryString == null) {
            links.add(new Link(requestUri, "self"));
        } else {
            links.add(new Link(requestUri + "?" + queryString, "self"));
        }

        payload.add(links);
        return new ResponseEntity<Resource<SubdomainResource>>(payload, HttpStatus.OK);
    }
}