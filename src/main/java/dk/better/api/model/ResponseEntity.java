package dk.better.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Bo Andersen
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public interface ResponseEntity { }