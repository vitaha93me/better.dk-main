package dk.better.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bo Andersen
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Response {
    private String code;
    private String message;
    private List<ResponseEntity> entries;


    public Response() {
        this.entries = new ArrayList<>();
    }

    public Response(String code, String message) {
        this(code, message, new ArrayList<>());
    }

    public Response(String code, String message, List<ResponseEntity> entries) {
        this.code = code;
        this.message = message;
        this.entries = entries;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResponseEntity> getEntries() {
        return entries;
    }

    public void setEntries(List<ResponseEntity> entries) {
        this.entries = entries;
    }
}