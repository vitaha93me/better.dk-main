package dk.better.thread.model;

import dk.better.thread.entity.*;
import dk.better.thread.entity.Thread;

/**
 * @author Bo Andersen
 */
public class ThreadSummary {
    private Message latestMessage;
    private dk.better.thread.entity.Thread thread;
    private boolean isRead;


    public Message getLatestMessage() {
        return latestMessage;
    }

    public void setLatestMessage(Message latestMessage) {
        this.latestMessage = latestMessage;
    }

    public Thread getThread() {
        return thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean isRead) {
        this.isRead = isRead;
    }
}