package dk.better.thread.dto;

import dk.better.account.entity.Account;
import dk.better.company.entity.Company;
import dk.better.thread.entity.Thread;

/**
 * @author Bo Andersen
 */
public class UserInboxThreadDTO {
    private Company recipient;
    private Account sender;
    private boolean isReadByRecipient;
    private dk.better.thread.entity.Thread thread;


    public Company getRecipient() {
        return recipient;
    }

    public void setRecipient(Company recipient) {
        this.recipient = recipient;
    }

    public Account getSender() {
        return sender;
    }

    public void setSender(Account sender) {
        this.sender = sender;
    }

    public boolean isReadByRecipient() {
        return isReadByRecipient;
    }

    public void setReadByRecipient(boolean isReadByRecipient) {
        this.isReadByRecipient = isReadByRecipient;
    }

    public dk.better.thread.entity.Thread getThread() {
        return thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }
}