package dk.better.thread.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "message")
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedEntityGraphs({
        @NamedEntityGraph(
                name = "graph.thread.view",
                attributeNodes = {
                        @NamedAttributeNode(value = "sender", subgraph = "participant")
                },
                subgraphs = {
                        @NamedSubgraph(name = "participant", attributeNodes = {
                                @NamedAttributeNode(value = "account"),
                                @NamedAttributeNode(value = "company")
                        })
                }
        )
})
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Length(min = 2, max = 5000)
    @Column(nullable = false)
    private String text;

    @Column(nullable = false)
    private Date created = new Date();

    @Length(min = 7, max = 50)
    @Column(name = "ip_address", nullable = true, length = 50)
    private String ipAddress;

    @ManyToOne(fetch = FetchType.LAZY, optional = false, targetEntity = Thread.class)
    private Thread thread;

    @ManyToOne(fetch = FetchType.LAZY, optional = false, targetEntity = Participant.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private Participant sender;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Thread getThread() {
        return thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public Participant getSender() {
        return sender;
    }

    public void setSender(Participant sender) {
        this.sender = sender;
    }
}