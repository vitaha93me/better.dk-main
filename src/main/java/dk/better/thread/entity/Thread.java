package dk.better.thread.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Formula;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "thread")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Thread {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Length(min = 3, max = 150)
    @Column(nullable = false, length = 150)
    private String subject;

    @Column
    private Date created = new Date();

    @Column(name = "last_activity")
    private Date lastActivity = new Date();

    @Formula("(select m.text from message m where m.thread_id = id order by m.id desc limit 1)")
    private String mostRecentMessageText;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "thread", targetEntity = Participant.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private Set<Participant> participants = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Participant.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "sender_participant_id")
    private Participant sender;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Participant.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "receiver_participant_id")
    private Participant receiver;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(Date lastActivity) {
        this.lastActivity = lastActivity;
    }

    public Set<Participant> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<Participant> participants) {
        this.participants = participants;
    }

    public Participant getSender() {
        return sender;
    }

    public void setSender(Participant sender) {
        this.sender = sender;
    }

    public Participant getReceiver() {
        return receiver;
    }

    public void setReceiver(Participant receiver) {
        this.receiver = receiver;
    }

    public String getMostRecentMessageText() {
        return mostRecentMessageText;
    }

    public void setMostRecentMessageText(String mostRecentMessageText) {
        this.mostRecentMessageText = mostRecentMessageText;
    }
}