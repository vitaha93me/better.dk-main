package dk.better.thread.repository;

import dk.better.application.repository.AbstractEntityManagerRepository;

import java.util.*;

/**
 * @author Bo Andersen
 */
public class ThreadRepositoryImpl extends AbstractEntityManagerRepository implements ThreadRepositoryCustom {
    @Override
    @SuppressWarnings("unchecked")
    public Map<Integer, Long> getUnreadMessagesCountInUnreadThreads(int accountId) {
        String jpql = "select t.id, (select count(*) from Message m left join m.sender.account a where (a.id is null or a.id <> :accountId) and m.thread.id = t.id and m.created > sender.lastActivity) " +
                "from Thread t " +
                "left join t.sender sender " +
                "left join sender.account sa " +
                "where sa.id = :accountId " +
                "and sender.company is null " +
                "and t.lastActivity > sender.lastActivity";

        List<Object[]> results = this.getEntityManager()
                .createQuery(jpql)
                .setParameter("accountId", accountId)
                .getResultList();

        Map<Integer, Long> map = new HashMap<>(results.size());

        for (Object[] arr : results) {
            map.put((Integer) arr[0], (Long) arr[1]);
        }

        return map;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<Integer, Map<Integer, Long>> getUnreadMessagesCountInUnreadThreadsForClaimedCompanies(int accountId) {
        String jpql = "select t.id, c.id, (select count(*) from Message m inner join m.sender s left join s.company sc where m.thread.id = t.id and sc.id is null and (receiver.lastActivity is null or m.created > receiver.lastActivity)) " +
                "from Thread t " +
                "inner join t.receiver receiver " +
                "inner join receiver.company c " +
                "inner join c.accountCompanies ac " +
                "inner join ac.account a " +
                "where a.id = :accountId " +
                "and (receiver.lastActivity is null or t.lastActivity > receiver.lastActivity)";

        List<Object[]> results = this.getEntityManager()
                .createQuery(jpql)
                .setParameter("accountId", accountId)
                .getResultList();

        Map<Integer, Map<Integer, Long>> map = new HashMap<>(results.size());

        for (Object[] arr : results) {
            Integer companyId = (Integer) arr[1];

            if (map.containsKey(companyId)) {
                map.get(companyId).put((Integer) arr[0], (Long) arr[2]);
            } else {
                Map<Integer, Long> temp = new HashMap<>(1);
                temp.put((Integer) arr[0], (Long) arr[2]);
                map.put(companyId, temp);
            }
        }

        return map;
    }
}