package dk.better.thread.repository;

import dk.better.thread.entity.Participant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Bo Andersen
 */
@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Integer> {
    @Query("select p " +
            "from Participant p " +
            "inner join fetch p.account a " +
            "inner join p.thread t " +
            "where a.id = :accountId and t.id = :threadId and p.company is null")
    Participant getThreadParticipant(@Param("accountId") int accountId, @Param("threadId") int threadId);
}