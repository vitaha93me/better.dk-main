package dk.better.thread.repository;

import dk.better.application.repository.EntityManagerAware;

import java.util.Map;

/**
 * @author Bo Andersen
 */
public interface ThreadRepositoryCustom extends EntityManagerAware {
    //List<ThreadSummary> getLatestThreadActivity(int accountId);
    Map<Integer, Long> getUnreadMessagesCountInUnreadThreads(int accountId);
    Map<Integer, Map<Integer, Long>> getUnreadMessagesCountInUnreadThreadsForClaimedCompanies(int accountId);
}