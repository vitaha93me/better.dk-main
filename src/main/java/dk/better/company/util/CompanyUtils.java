package dk.better.company.util;

import dk.better.account.entity.Account;
import dk.better.account.entity.AccountCompany;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.util.GeneralUtils;
import dk.better.application.util.Scheme;
import dk.better.company.entity.Company;
import dk.better.company.entity.Industry;
import dk.better.company.entity.Subdomain;
import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Collection;

/**
 * @author Bo Andersen
 * @see http://stackoverflow.com/questions/7486012/static-classes-in-java
 */
public final class CompanyUtils {
    @Value("${security.checksum.salt}")
    private static String salt;

    private CompanyUtils() { }

    /**
     * Evaluates whether or not a given value is a company ID. Note that this method
     * does not verify that the ID exists, but merely that it is the format of an ID
     *
     * @param companyIdOrCompanySlug The value to evaluate as being a company ID or not
     * @return Whether or not the argument is a company ID
     */
    public static boolean isCompanyId(String companyIdOrCompanySlug) {
        int id = 0;

        try {
            id = Integer.parseInt(companyIdOrCompanySlug);
        } catch (NumberFormatException nfe) { }

        return (id > 0);
    }

    // improve: document
    public static boolean isCompanyOwner(Account account, Company company) {
        Assert.notNull(account);
        Assert.notNull(company);

        for (AccountCompany accountCompany : account.getAccountCompanies()) {
            if (accountCompany.getCompany().getId() == company.getId()) {
                return true;
            }
        }

        return false;
    }

    // improve: document
    public static boolean isCompanyOwner(Account account, int companyId) {
        Assert.notNull(account);

        for (AccountCompany accountCompany : account.getAccountCompanies()) {
            if (accountCompany.getCompany().getId() == companyId) {
                return true;
            }
        }

        return false;
    }

    // improve: document
    public static Subdomain getActiveSubdomain(Collection<Subdomain> subdomains) {
        for (Subdomain subdomain : subdomains) {
            if (subdomain.isActive()) {
                return subdomain;
            }
        }

        return null;
    }

    /**
     * Constructs a fully qualified profile URL
     *
     * @param companyId The ID of the company for which to construct the profile URL for
     * @param subdomains The company's subdomains
     * @param scheme The HTTP scheme to use in the profile URL
     * @param domain The name of the domain name to use in the profile URL
     * @return The fully qualified profile URL
     */
    public static String getProfileUrl(int companyId, Collection<Subdomain> subdomains, Scheme scheme, String domain) {
        Assert.notNull(subdomains);
        String schemeString;

        switch (scheme) {
            case HTTP: schemeString = "http"; break;
            case HTTPS: schemeString = "https"; break;
            default: throw new InvalidArgumentException(String.format("Unable to construct profile URL; illegal scheme (%s)", scheme.toString()));
        }

        Subdomain subdomain = CompanyUtils.getActiveSubdomain(subdomains);

        if (subdomain != null) {
            return schemeString + "://" + subdomain.getName() + "." + domain;
        } else {
            return schemeString + "://" + companyId + "." + domain;
        }
    }

    /**
     * Formats a rating for display
     *
     * @param rating The rating to format
     * @return The rating with maximum one fraction digit
     */
    public static String formatRating(Double rating) {
        Assert.notNull(rating);

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');
        symbols.setGroupingSeparator('.');

        DecimalFormat df = new DecimalFormat("###.#", symbols);
        df.setDecimalSeparatorAlwaysShown(false);
        df.setRoundingMode(RoundingMode.DOWN);

        return df.format(rating);
    }

    public static long generateChecksum(int companyId) {
        return GeneralUtils.crc32(companyId + CompanyUtils.salt);
    }

    @SuppressWarnings("unchecked")
    public static Collection<String> getIndustryNames(Collection<Industry> industries) {
        return (Collection<String>) CollectionUtils.collect(industries, new BeanToPropertyValueTransformer("name"));
    }

    @SuppressWarnings("unchecked")
    public static Collection<Integer> getIndustryIds(Collection<Industry> industries) {
        return (Collection<Integer>) CollectionUtils.collect(industries, new BeanToPropertyValueTransformer("id"));
    }
}