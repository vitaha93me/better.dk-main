package dk.better.company.util;

/**
 * @author Bo Andersen
 */
public enum ReviewSite {
    BETTERDK,
    GOOGLE_PLUS,
    YELP,
    FACEBOOK,
    KRAKDK,
    OMDOEMMEDK,
    BEDOEMMELSEDK
}
