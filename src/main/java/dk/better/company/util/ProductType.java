package dk.better.company.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Bo Andersen
 */
public enum ProductType {
    TRIAL(1),
    PAID(2);

    private int value;
    private static Map<Integer, ProductType> map = new HashMap<>();

    private ProductType(int value) {
        this.value = value;
    }

    static {
        for (ProductType productType : ProductType.values()) {
            map.put(productType.value, productType);
        }
    }

    public static ProductType valueOf(int productType) {
        return map.get(productType);
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return getName(value);
    }

    public static String getName(int value) {
        switch (value) {
            case 1: return "Trial";
            case 2: return "Paid";
            default: return "Unknown";
        }
    }
}
