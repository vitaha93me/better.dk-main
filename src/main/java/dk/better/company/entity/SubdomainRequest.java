package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.account.entity.Account;
import dk.better.application.entity.RequestStatus;
import dk.better.application.validator.annotation.Unique;
import dk.better.company.service.SubdomainService;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "subdomain_request")
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedEntityGraphs({
        @NamedEntityGraph(
                name = "graph.subdomain-request.display",
                attributeNodes = {
                        @NamedAttributeNode(value = "company", subgraph = "companySub"),
                        @NamedAttributeNode(value = "requester"),
                        @NamedAttributeNode(value = "requestStatus")
                },
                subgraphs = {
                        @NamedSubgraph(name = "companySub", attributeNodes = {
                                @NamedAttributeNode(value = "subdomains")
                        })
                }
        ),
})
public class SubdomainRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @NotEmpty
    @Length(min = 3, max = 50)
    @Pattern(regexp = "^[a-z0-9\\-]+$", message = "{subdomain.invalid.format}")
    @Unique(service = SubdomainService.class, fieldName = "requestedSubdomain", message = "{subdomain.unique.violation}")
    @Column(name = "requested_subdomain", length = 50, nullable = false)
    private String requestedSubdomain;

    @Column(nullable = false)
    private Date created = new Date();

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false, targetEntity = Company.class)
    @JoinColumn(name = "company_id")
    private Company company;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false, targetEntity = Account.class)
    @JoinColumn(name = "requester_account_id")
    private Account requester;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false, targetEntity = RequestStatus.class)
    @JoinColumn(name = "request_status_id")
    private RequestStatus requestStatus;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRequestedSubdomain() {
        return requestedSubdomain;
    }

    public void setRequestedSubdomain(String requestedSubdomain) {
        this.requestedSubdomain = requestedSubdomain;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Account getRequester() {
        return requester;
    }

    public void setRequester(Account requester) {
        this.requester = requester;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }
}