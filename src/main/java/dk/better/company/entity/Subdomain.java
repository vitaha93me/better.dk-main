package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.application.validator.annotation.Unique;
import dk.better.company.service.SubdomainService;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "subdomain", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "name" })
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Subdomain {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @NotEmpty
    @Length(min = 3, max = 50)
    @Pattern(regexp = "^[a-z0-9\\-]+$", message = "{subdomain.invalid.format}")
    @Unique(service = SubdomainService.class, fieldName = "name", message = "{subdomain.unique.violation}")
    @Column(nullable = false, length = 50)
    private String name;

    @Column(name = "is_active", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean isActive;

    @Column(name = "time_added", nullable = false)
    private Date timeAdded = new Date();

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Company.class)
    @JoinColumn(name = "company_id")
    private Company company;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getTimeAdded() {
        return timeAdded;
    }

    public void setTimeAdded(Date timeAdded) {
        this.timeAdded = timeAdded;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}