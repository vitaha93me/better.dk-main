package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author vitalii.
 */
@Entity
@Table(name = "review_links")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReviewLink implements Serializable {

    private static final long serialVersionUID = -7049957706738879274L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private int companyId;

    @Column
    private String googlePlaceUrl;

    @Column
    private String facebookUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getGooglePlaceUrl() {
        return googlePlaceUrl;
    }

    public void setGooglePlaceUrl(String googlePlaceUrl) {
        this.googlePlaceUrl = googlePlaceUrl;
    }

    public String getFacebookUrl() {
        return facebookUrl;
    }

    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }
}
