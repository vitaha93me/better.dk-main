package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.account.entity.AccountCompany;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "company")
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedEntityGraphs({
        @NamedEntityGraph(
                name = "graph.company.profile.full",
                attributeNodes = {
                        //@NamedAttributeNode(value = "acknowledgements"),
                        @NamedAttributeNode(value = "pages"),
                        @NamedAttributeNode(value = "industries"),
                        @NamedAttributeNode(value = "accountCompanies", subgraph = "accountCompanies"),
                        @NamedAttributeNode(value = "companyServices", subgraph = "companyServices")
                },
                subgraphs = {
                        @NamedSubgraph(name = "accountCompanies", attributeNodes = {}),
                        @NamedSubgraph(name = "companyServices", attributeNodes = {
                                @NamedAttributeNode(value = "service")
                        })
                }
        ),
        @NamedEntityGraph(
                name = "graph.company.admin.company.edit",
                attributeNodes = {
                        //@NamedAttributeNode(value = "acknowledgements"),
                        @NamedAttributeNode(value = "meta"),
                        @NamedAttributeNode(value = "industries"),
                        @NamedAttributeNode(value = "subdomains"),
                        @NamedAttributeNode(value = "companyServices", subgraph = "companyServices")
                },
                subgraphs = {
                        @NamedSubgraph(name = "companyServices", attributeNodes = {
                                @NamedAttributeNode(value = "service")
                        })
                }
        ),
        @NamedEntityGraph(
                name = "graph.company.cp.view",
                attributeNodes = {
                        @NamedAttributeNode(value = "accountCompanies", subgraph = "accountCompanies")
                },
                subgraphs = {
                        @NamedSubgraph(name = "accountCompanies", attributeNodes = {})
                }
        )
})
public class Company implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @NotEmpty
    @Length(min = 1, max = 150)
    @Column(length = 150, nullable = false)
    private String name;

    @Email
    @Length(max = 100)
    @Column(length = 100, nullable = true)
    private String email;

    @URL
    @Length(max = 100)
    @Column(length = 100, nullable = true)
    private String website;

    @Column(insertable = false, updatable = false)
    private Double rating;

    @Column(name = "number_of_reviews", insertable = false, updatable = false)
    private int numberOfReviews;

    @Column(name = "number_of_likes", insertable = false, updatable = false)
    private int numberOfLikes;

    @NotEmpty
    @Length(min = 2, max = 50)
    @Column(name = "street_name", length = 50)
    private String streetName;

    @Column(name = "postal_code", nullable = false)
    private int postalCode;

    @Length(min = 2, max = 50)
    @Column(name = "postal_district", nullable = false, length = 50)
    private String postalDistrict;

    @Length(min = 2, max = 50)
    @Column(name = "city_name", nullable = false, length = 50)
    private String city;

    @Length(max = 50)
    @Column(nullable = true, length = 50)
    private String municipal;

    @Column(name = "founding_date", nullable = true)
    private Date foundingDate;

    @NotEmpty
    @Length(max = 25)
    @Column(name = "street_number", length = 25)
    private String streetNumber;

    @Length(min = 8, max = 11)
    @Column(name = "phone_number", length = 11)
    private String phoneNumber;

    // improve: uncomment the below, but somehow require a particular length if the value is not null
    //@Length(min = 8, max = 11)
    @Column(name = "call_tracking_number", length = 11, unique = true)
    private String callTrackingNumber;

    @Length(min = 50, max = 175)
    @Column(length = 175, nullable = true)
    private String teaser;

    @Length(min = 100, max = 5000)
    @Column(nullable = true)
    private String description;

    // improve: validate length (the below only works for strings)
    //@Pattern(regexp = "^[0-9]{8}$", message = "bla bla bla")
    @Min(value = 1) // Ensure that a value is provided
    @Column(name = "ci_number", nullable = false)
    private long ciNumber;

    @Column(nullable = false, unique = true)
    private long pnr;

    @Length(max = 50)
    @Column(name = "logo_name", length = 50, nullable = true)
    private String logoName;

    @Length(max = 50)
    @Column(name = "banner_name", length = 50, nullable = true)
    private String bannerName;

    @Length(max = 50)
    @Column(name = "co_name", length = 50, nullable = true)
    private String coName;

    @Length(max = 100)
    @Column(name = "additional_address_text", length = 100, nullable = true)
    private String additionalAddressText;

    @Length(max = 50)
    @Column(name = "postal_box", length = 50, nullable = true)
    private String postBox;

    @Column(nullable = false)
    private Date created = new Date();

    @Column(name = "last_modified", nullable = true)
    private Date lastModified;

    @Column(name = "profile_url", nullable = true)
    private String profileUrl;

    @URL
    @Column(name = "booking_url", nullable = true)
    private String bookingUrl;

    @Column(name = "access_token", nullable = true, length = 36)
    private String accessToken;

    @Valid
    @OneToMany(fetch = FetchType.LAZY, targetEntity = Subdomain.class, mappedBy = "company", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @OrderBy("id DESC")
    private Set<Subdomain> subdomains = new HashSet<>();

    @Valid
    @OneToMany(fetch = FetchType.LAZY, targetEntity = Subdomain.class, mappedBy = "company", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<PredefinedSiteLink> predefinedSiteLinks = new HashSet<>();

    @Valid
    @OneToMany(fetch = FetchType.LAZY, targetEntity = Subdomain.class, mappedBy = "company", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<UserDefinedSiteLink> userDefinedSiteLinks = new HashSet<>();

    @Size(max = 5)
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinTable(name = "company_acknowledgement", joinColumns = @JoinColumn(name = "company_id"), inverseJoinColumns = @JoinColumn(name = "acknowledgement_id"))
    @OrderBy("name")
    private Set<Acknowledgement> acknowledgements = new HashSet<>();

    @Size(min = 1, max = 4)
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    @JoinTable(name = "company_industry", joinColumns = @JoinColumn(name = "company_id"), inverseJoinColumns = @JoinColumn(name = "industry_id"))
    private Set<Industry> industries = new HashSet<>();

    @Size(max = 10)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company", orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<CompanyService> companyServices = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
    private Set<AccountCompany> accountCompanies = new HashSet<>();

    @Size(max = 100)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<CompanyKeyword> companyKeywords = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, targetEntity = Page.class, mappedBy = "company")
    @Where(clause = "parent_id is null")
    // todo: replace this with filter once figured out how to globally enable Hibernate filters
    @OrderBy("name")
    //@Filter(name = "rootPages")
    private Set<Page> pages = new HashSet<>();

    @OneToOne(fetch = FetchType.LAZY, targetEntity = CompanyMeta.class, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private CompanyMeta meta;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCallTrackingNumber() {
        return callTrackingNumber;
    }

    public void setCallTrackingNumber(String callTrackingNumber) {
        this.callTrackingNumber = callTrackingNumber;
    }

    public String getTeaser() {
        return teaser;
    }

    public void setTeaser(String teaser) {
        this.teaser = teaser;
    }

    public long getCiNumber() {
        return ciNumber;
    }

    public void setCiNumber(long ciNumber) {
        this.ciNumber = ciNumber;
    }

    public Set<Acknowledgement> getAcknowledgements() {
        return acknowledgements;
    }

    public void setAcknowledgements(Set<Acknowledgement> acknowledgements) {
        this.acknowledgements = acknowledgements;
    }

    public Set<Industry> getIndustries() {
        return industries;
    }

    public void setIndustries(Set<Industry> industries) {
        this.industries = industries;
    }

    public Set<CompanyService> getCompanyServices() {
        return companyServices;
    }

    public void setCompanyServices(Set<CompanyService> companyServices) {
        this.companyServices = companyServices;
    }

    public Set<AccountCompany> getAccountCompanies() {
        return accountCompanies;
    }

    public void setAccountCompanies(Set<AccountCompany> accountCompanies) {
        this.accountCompanies = accountCompanies;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberOfReviews() {
        return numberOfReviews;
    }

    public void setNumberOfReviews(int numberOfReviews) {
        this.numberOfReviews = numberOfReviews;
    }

    public int getNumberOfLikes() {
        return numberOfLikes;
    }

    public void setNumberOfLikes(int numberOfLikes) {
        this.numberOfLikes = numberOfLikes;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getLogoName() {
        return logoName;
    }

    public void setLogoName(String logoName) {
        this.logoName = logoName;
    }

    public String getBannerName() {
        return bannerName;
    }

    public void setBannerName(String bannerName) {
        this.bannerName = bannerName;
    }

    public Set<Subdomain> getSubdomains() {
        return subdomains;
    }

    public void setSubdomains(Set<Subdomain> subdomains) {
        for (Subdomain subdomain : subdomains) {
            subdomain.setCompany(this);
        }

        this.subdomains = subdomains;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCoName() {
        return coName;
    }

    public void setCoName(String coName) {
        this.coName = coName;
    }

    public String getAdditionalAddressText() {
        return additionalAddressText;
    }

    public void setAdditionalAddressText(String additionalAddressText) {
        this.additionalAddressText = additionalAddressText;
    }

    public String getPostBox() {
        return postBox;
    }

    public void setPostBox(String postBox) {
        this.postBox = postBox;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public Set<CompanyKeyword> getCompanyKeywords() {
        return companyKeywords;
    }

    public void setCompanyKeywords(Set<CompanyKeyword> companyKeywords) {
        this.companyKeywords = companyKeywords;
    }

    public long getPnr() {
        return pnr;
    }

    public void setPnr(long pnr) {
        this.pnr = pnr;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalDistrict() {
        return postalDistrict;
    }

    public void setPostalDistrict(String postalDistrict) {
        this.postalDistrict = postalDistrict;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMunicipal() {
        return municipal;
    }

    public void setMunicipal(String municipal) {
        this.municipal = municipal;
    }

    public Date getFoundingDate() {
        return foundingDate;
    }

    public void setFoundingDate(Date foundingDate) {
        this.foundingDate = foundingDate;
    }

    public String getBookingUrl() {
        return bookingUrl;
    }

    public void setBookingUrl(String bookingUrl) {
        this.bookingUrl = bookingUrl;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Set<Page> getPages() {
        return pages;
    }

    public void setPages(Set<Page> pages) {
        this.pages = pages;
    }

    public CompanyMeta getMeta() {
        return meta;
    }

    public void setMeta(CompanyMeta meta) {
        this.meta = meta;
    }

    public void addIndustry(Industry industry) {
        if (!this.industries.contains(industry)) {
            this.industries.add(industry);
        }
    }

    public Subdomain getActiveSubdomain() {
        if (this.subdomains.isEmpty()) {
            return null;
        }

        for (Subdomain subdomain : this.subdomains) {
            if (subdomain.isActive()) {
                return subdomain;
            }
        }

        return null;
    }

    public Set<PredefinedSiteLink> getPredefinedSiteLinks() {
        return predefinedSiteLinks;
    }

    public void setPredefinedSiteLinks(Set<PredefinedSiteLink> predefinedSiteLinks) {
        this.predefinedSiteLinks = predefinedSiteLinks;
    }

    public Set<UserDefinedSiteLink> getUserDefinedSiteLinks() {
        return userDefinedSiteLinks;
    }

    public void setUserDefinedSiteLinks(Set<UserDefinedSiteLink> userDefinedSiteLinks) {
        this.userDefinedSiteLinks = userDefinedSiteLinks;
    }

    public List<Subdomain> getPreviousSubdomains() {
        if (this.subdomains.isEmpty()) {
            return new ArrayList<>();
        }

        List<Subdomain> previousSubdomains = new ArrayList<>((this.subdomains.size() - 1));

        for (Subdomain subdomain : this.subdomains) {
            if (!subdomain.isActive()) {
                previousSubdomains.add(subdomain);
            }
        }

        return previousSubdomains;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof Company)) {
            return false;
        }

        Company company = (Company) o;
        return (this.id == company.getId());
    }

    @Override
    public int hashCode() {
        return id;
    }
}
