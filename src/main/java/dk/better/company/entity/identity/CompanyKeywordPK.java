package dk.better.company.entity.identity;

import dk.better.company.entity.Company;
import dk.better.company.entity.Keyword;

import java.io.Serializable;

/**
 * @author Bo Andersen
 */
public class CompanyKeywordPK implements Serializable {
    private Company company;
    private Keyword keyword;

    public CompanyKeywordPK() { }

    public CompanyKeywordPK(Company company, Keyword keyword) {
        this.company = company;
        this.keyword = keyword;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Keyword getKeyword() {
        return keyword;
    }

    public void setKeyword(Keyword keyword) {
        this.keyword = keyword;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (obj == null || !(obj instanceof CompanyKeywordPK)) {
            return false;
        }

        CompanyKeywordPK companyServicePK = (CompanyKeywordPK) obj;

        return ((companyServicePK.getCompany().getId() == this.company.getId())
                && companyServicePK.getKeyword().getId() == this.keyword.getId());
    }

    @Override
    public int hashCode() {
        int result = this.company.hashCode();
        result = 31 * result + this.keyword.hashCode();

        return result;
    }
}