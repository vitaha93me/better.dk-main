package dk.better.company.entity;

import dk.better.account.entity.Account;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "page_revision")
public class PageRevision {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(nullable = false, length = 50)
    private String name;

    @Column(nullable = false, length = 25)
    private String slug;

    @Column(nullable = false, length = 50)
    private String title;

    @Column(nullable = true)
    private String content;

    @Column(nullable = true)
    private String data;

    @Column(nullable = false)
    private Date created = new Date();

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Account.class, optional = false)
    @JoinColumn(name = "account_id")
    private Account author;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Page.class, optional = false)
    private Page page;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Account getAuthor() {
        return author;
    }

    public void setAuthor(Account author) {
        this.author = author;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }
}