package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author vitalii.
 */
@Entity
@Table(name = "predefined_sites_link")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PredefinedSiteLink implements Serializable {

    private static final long serialVersionUID = -7049957706738879274L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Company.class)
    @JoinColumn(name = "company_id")
    private Company company;


    @ManyToOne(fetch = FetchType.EAGER, targetEntity = PredefinedSite.class)
    @JoinColumn(name = "site_id")
    private PredefinedSite site;

    @Column
    private String link;

    @Column
    private boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public PredefinedSite getSite() {
        return site;
    }

    public void setSite(PredefinedSite site) {
        this.site = site;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
