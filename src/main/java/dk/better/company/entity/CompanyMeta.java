package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "company_meta")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompanyMeta implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @OneToOne(fetch = FetchType.LAZY, targetEntity = Company.class, mappedBy = "meta")
    private Company company;

    @Column(name = "google_plus_url", length = 100, nullable = true)
    private String googlePlusUrl;

    @Column(name = "yelp_url", length = 100, nullable = true)
    private String yelpUrl;

    @Column(name = "facebook_url", length = 100, nullable = true)
    private String facebookUrl;

    @Column(name = "krakdk_url", length = 100, nullable = true)
    private String krakdkUrl;

    @Column(name = "omdoemmedk_url", length = 100, nullable = true)
    private String omdoemmedkUrl;

    @Column(name = "bedoemmelsedk_url", length = 100, nullable = true)
    private String bedoemmelsedkUrl;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGooglePlusUrl() {
        return googlePlusUrl;
    }

    public void setGooglePlusUrl(String googlePlusUrl) {
        this.googlePlusUrl = googlePlusUrl;
    }

    public String getYelpUrl() {
        return yelpUrl;
    }

    public void setYelpUrl(String yelpUrl) {
        this.yelpUrl = yelpUrl;
    }

    public String getFacebookUrl() {
        return facebookUrl;
    }

    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    public String getKrakdkUrl() {
        return krakdkUrl;
    }

    public void setKrakdkUrl(String krakdkUrl) {
        this.krakdkUrl = krakdkUrl;
    }

    public String getOmdoemmedkUrl() {
        return omdoemmedkUrl;
    }

    public void setOmdoemmedkUrl(String omdoemmedkUrl) {
        this.omdoemmedkUrl = omdoemmedkUrl;
    }

    public String getBedoemmelsedkUrl() {
        return bedoemmelsedkUrl;
    }

    public void setBedoemmelsedkUrl(String bedoemmelsedkUrl) {
        this.bedoemmelsedkUrl = bedoemmelsedkUrl;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}