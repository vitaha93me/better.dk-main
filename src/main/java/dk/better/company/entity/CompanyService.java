package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.company.entity.identity.CompanyServicePK;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "company_service")
@IdClass(CompanyServicePK.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
        @NamedQuery(
                name = "CompanyService.fetch.partial.byCompanyIdAndServiceSlug",
                query = "SELECT cs " +
                        "FROM CompanyService AS cs " +
                        "INNER JOIN FETCH cs.service AS s " +
                        "INNER JOIN cs.company AS c " +
                        "WHERE c.id = :companyId " +
                        "AND s.slug = :serviceSlug"
        ),
        @NamedQuery(
                name = "CompanyService.fetch.partial.byCompanySlugAndServiceSlug",
                query = "SELECT cs " +
                        "FROM CompanyService AS cs " +
                        "INNER JOIN FETCH cs.service AS s " +
                        "INNER JOIN cs.company AS c " +
                        "INNER JOIN c.subdomains AS sd " +
                        "WHERE sd.name = :companySlug " +
                        "AND s.slug = :serviceSlug"
        )
})
public class CompanyService implements Serializable {
    @Id
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Company.class)
    @JoinColumn(name = "company_id")
    private Company company;

    @Id
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Service.class)
    @JoinColumn(name = "service_id")
    private Service service;

    @Column(nullable = true)
    private String description;


    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}