package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.account.entity.Account;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name="upvote", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "account_id", "company_id" })
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Like {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(nullable = false)
    private Date created = new Date();

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Account.class, optional = false)
    private Account account;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Company.class, optional = false)
    private Company company;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}