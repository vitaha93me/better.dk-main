package dk.better.company.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.application.validator.annotation.Unique;
import dk.better.company.service.ServiceService;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "service")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Length(min = 3, max = 50)
    @Column(length = 50, nullable = false)
    private String name;

    @Length(min = 50, max = 5000)
    @Column(name = "default_description", nullable = false)
    private String defaultDescription;

    @Length(min = 3, max = 50)
    @Pattern(regexp = "^[a-z0-9\\-]+$", message = "{slug.invalid.format}")
    @Unique(service = ServiceService.class, fieldName = "slug")
    @Column(length = 50, nullable = false, unique = true)
    private String slug;

    @Size(min = 1)
    @ManyToMany
    @JoinTable(name = "service_industry", joinColumns = @JoinColumn(name = "service_id"), inverseJoinColumns = @JoinColumn(name = "industry_id"))
    private Set<Industry> industries = new HashSet<>();


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Industry> getIndustries() {
        return industries;
    }

    public void setIndustries(Set<Industry> industries) {
        this.industries = industries;
    }

    public String getDefaultDescription() {
        return defaultDescription;
    }

    public void setDefaultDescription(String defaultDescription) {
        this.defaultDescription = defaultDescription;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}