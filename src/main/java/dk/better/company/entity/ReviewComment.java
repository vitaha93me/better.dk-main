package dk.better.company.entity;

import dk.better.account.entity.Account;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "review_comment")
public class ReviewComment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(nullable = false)
    private String text;

    @Column(nullable = false)
    private Date created;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Review.class, optional = false)
    private Review review;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Account.class, optional = false)
    private Account account;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Company.class, optional = false)
    private Company company;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}