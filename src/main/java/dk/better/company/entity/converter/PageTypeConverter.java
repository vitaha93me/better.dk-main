package dk.better.company.entity.converter;

import dk.better.company.util.PageType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * @author Bo Andersen
 */
@Converter
public class PageTypeConverter implements AttributeConverter<PageType, Integer> {
    @Override
    public Integer convertToDatabaseColumn(PageType pageType) {
        return pageType.getValue();
    }

    @Override
    public PageType convertToEntityAttribute(Integer pageType) {
        return PageType.valueOf(pageType);
    }
}