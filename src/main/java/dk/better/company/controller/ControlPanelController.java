package dk.better.company.controller;

import dk.better.account.entity.Account;
import dk.better.application.util.AuthUtils;
import dk.better.company.entity.*;
import dk.better.company.form.SiteReviewForm;
import dk.better.company.service.CompanyService;
import dk.better.company.service.*;
import dk.better.company.util.IntegrationType;
import dk.better.thread.service.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

/**
 * @author Bo Andersen
 */
@Controller
@RequestMapping("/cp/company")
public class ControlPanelController {
    @Autowired
    private CompanyService companyService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private TextMessageService textMessageService;

    @Autowired
    private ThreadService threadService;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private PredefinedSiteLinkService predefinedSiteLinkService;

    @Autowired
    private UserDefinedSiteLinkService userDefinedSiteLinkService;

    @Autowired
    private ReviewSiteService reviewSiteService;

    @Autowired
    private ReviewLinkService reviewLinkService;

    @Autowired
    private PredefinedSiteService predefinedSiteService;

    @Autowired
    private ExternalReviewService externalReviewService;

    @Autowired
    private IntegrationService integrationService;

    @Value("${analytics.keen.project-id}")
    private String keenProjectId;

    @Value("${facebook.app.id}")
    private String facebookAppId;


    @RequestMapping("")
    public String index() {
        return "company-control-panel-index";
    }

    @RequestMapping("/{companyId}")
    public String statistics(@PathVariable("companyId") int companyId,
                             @RequestParam(value = "token", required = false) String accessToken,
                             @RequestParam(value = "checksum", required = false) Long checksum,
                             @Value("${analytics.keen.read-key}") String keenReadKey,
                             Model model) {
        if (!this.companyService.hasAccessToCompany(companyId, checksum, accessToken)) {
            return "redirect:/login";
        }

        if (AuthUtils.isAuthenticated()) {
            if (AuthUtils.isAdmin()) {
                model.addAttribute("keenScopedKey", keenReadKey);
            } else {
                Account account = (Account) AuthUtils.getAuthentication().getPrincipal();

                account.getAccountCompanies().stream().filter(ac -> ac.getCompany().getId() == companyId)
                        .forEach(ac -> model.addAttribute("keenScopedKey", ac.getKeenScopedKey()));
            }
        }

        model.addAttribute("keenProjectId", this.keenProjectId);
        model.addAttribute("company", this.getCompany(companyId));
        model.addAttribute("title", "Statistik");
        return "company-control-panel-statistics";
    }

    @RequestMapping("/{companyId}/advantages")
    public String advantages(@PathVariable("companyId") int companyId,
                             @RequestParam(value = "token", required = false) String accessToken,
                             @RequestParam(value = "checksum", required = false) Long checksum,
                             Model model) {
        if (!this.companyService.hasAccessToCompany(companyId, checksum, accessToken)) {
            return "redirect:/login";
        }

        model.addAttribute("company", this.getCompany(companyId));
        model.addAttribute("title", "Fordele");
        return "company-control-panel-advantages";
    }

    @RequestMapping("/{companyId}/integrations")
    public String integrations(@PathVariable("companyId") int companyId,
                               @RequestParam(value = "token", required = false) String accessToken,
                               @RequestParam(value = "checksum", required = false) Long checksum,
                               Model model) {
        if (!this.companyService.hasAccessToCompany(companyId, checksum, accessToken)) {
            return "redirect:/login";
        }

        model.addAttribute("company", this.getCompany(companyId));
        model.addAttribute("title", "Integrationer");
        return "company-control-panel-integrations";
    }

    @RequestMapping("/{companyId}/customers")
    public String customers(@PathVariable("companyId") int companyId,
                            @RequestParam(value = "token", required = false) String accessToken,
                            @RequestParam(value = "checksum", required = false) Long checksum,
                            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
                            Model model) {
        if (!this.companyService.hasAccessToCompany(companyId, checksum, accessToken)) {
            return "redirect:/login";
        }

        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.DESC, "id"));
        Pageable pageable = new PageRequest((page - 1), 50, sortOrder);
        Page<Customer> customers = this.customerService.find(companyId, pageable);

        model.addAttribute("page", page);
        model.addAttribute("totalPages", customers.getTotalPages());
        model.addAttribute("totalCustomers", customers.getTotalElements());
        model.addAttribute("customers", customers.getContent());
        model.addAttribute("company", this.getCompany(companyId));
        model.addAttribute("canSendTextMessage", !this.textMessageService.hasSentTextMessageWithinOneDay(companyId));
        model.addAttribute("title", "Kunder");

        return "company-control-panel-customers";
    }

    @RequestMapping("/{companyId}/customers/import")
    public String importCustomers(@PathVariable("companyId") int companyId,
                                  @RequestParam(value = "token", required = false) String accessToken,
                                  @RequestParam(value = "checksum", required = false) Long checksum,
                                  Model model) {
        if (!this.companyService.hasAccessToCompany(companyId, checksum, accessToken)) {
            return "redirect:/login";
        }

        model.addAttribute("company", this.getCompany(companyId));
        model.addAttribute("title", "Tilføj kunder");

        return "company-control-panel-import-customers";
    }

    @RequestMapping("/{companyId}/messages")
    public String viewCompanyInbox(@PathVariable("companyId") int companyId,
                                   @RequestParam(value = "token", required = false) String accessToken,
                                   @RequestParam(value = "checksum", required = false) Long checksum,
                                   Model model) {
        if (!this.companyService.hasAccessToCompany(companyId, checksum, accessToken)) {
            throw new AccessDeniedException("User is not allowed to view this inbox!");
        }

        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.DESC, "lastActivity"));
        Pageable pageable = new PageRequest(0, 20, sortOrder);
        Page<dk.better.thread.entity.Thread> threads = this.threadService.getThreadsForCompanyInbox(companyId, checksum, accessToken, pageable);

        model.addAttribute("title", "Beskeder");
        model.addAttribute("threads", threads.getContent());
        model.addAttribute("hasMoreThreads", threads.hasNext());
        model.addAttribute("company", this.getCompany(companyId));

        return "view-company-inbox";
    }

    @RequestMapping("/{companyId}/reviews")
    public String reviews(@PathVariable("companyId") int companyId,
                          @RequestParam(value = "page", defaultValue = "1") int page,
                          @RequestParam(value = "token", required = false) String accessToken,
                          @RequestParam(value = "checksum", required = false) Long checksum,
                          Model model) {
        if (!this.companyService.hasAccessToCompany(companyId, checksum, accessToken)) {
            return "redirect:/login";
        }

        Sort sort = new Sort(Sort.Direction.DESC, "id");
        PageRequest pageRequest = new PageRequest((page - 1), 10, sort);
        Page<Review> reviews = this.reviewService.findByCompanyId(companyId, pageRequest);

        model.addAttribute("title", "Bedømmelser");
        model.addAttribute("reviews", reviews.getContent());
        model.addAttribute("numberOfResults", reviews.getTotalElements());
        model.addAttribute("totalPages", reviews.getTotalPages());
        model.addAttribute("page", page);
        model.addAttribute("company", this.getCompany(companyId));
        model.addAttribute("facebookAppId", this.facebookAppId);

        return "company-control-panel-reviews";
    }

    @RequestMapping("/{companyId}/reviews/invite")
    public String inviteCustomers(@PathVariable("companyId") int companyId,
                                  @RequestParam(value = "token", required = false) String accessToken,
                                  @RequestParam(value = "checksum", required = false) Long checksum,
                                  Model model) {
        if (!this.companyService.hasAccessToCompany(companyId, checksum, accessToken)) {
            return "redirect:/login";
        }

        model.addAttribute("company", this.getCompany(companyId));
        model.addAttribute("title", "Invitér kunder");

        return "company-control-panel-invite-customers";
    }

    @RequestMapping("/{companyId}/reviews/sharing")
    public String reviewLink(@PathVariable("companyId") int companyId,
                             @RequestParam(value = "token", required = false) String accessToken,
                             @RequestParam(value = "checksum", required = false) Long checksum,
                             Model model) {
        if (!this.companyService.hasAccessToCompany(companyId, checksum, accessToken)) {
            return "redirect:/login";
        }

        Company company = getCompany(companyId);

        List<PredefinedSiteLink> siteLinks = predefinedSiteLinkService.findByCompany(company);

        model.addAttribute("siteLinks", siteLinks);
        model.addAttribute("company", company);
        model.addAttribute("title", "Bedømmelse link");
        populateDefaultModel(model, company);
        model.addAttribute("siteLink", new PredefinedSiteLink());

        return "company-control-panel-review-link";
    }

    @RequestMapping(value = "/{companyId}/reviews/sharing/add", method = RequestMethod.POST)
    public String addSite(@PathVariable("companyId") int companyId,
                          @RequestParam("siteName") String site,
                          @RequestParam("link") String link,
                          @RequestParam(value = "token", required = false) String accessToken,
                          @RequestParam(value = "checksum", required = false) Long checksum,
                          Model model) {

        PredefinedSite predefinedSite = predefinedSiteService.findByName(site);


        Company company = getCompany(companyId);

        if (predefinedSite != null) {
            PredefinedSiteLink reviewSite = new PredefinedSiteLink();
            reviewSite.setCompany(company);
            reviewSite.setLink(link);
            reviewSite.setSite(predefinedSite);
            predefinedSiteLinkService.save(reviewSite);
        } else {
            UserDefinedSiteLink reviewSite = new UserDefinedSiteLink();
            reviewSite.setCompany(company);
            reviewSite.setLink(link);
            reviewSite.setName(site);
            userDefinedSiteLinkService.save(reviewSite);
        }

        populateDefaultModel(model, company);
        return "company-control-panel-review-link";


    }

    @RequestMapping(value = "/{companyId}/reviews/sharing/update_status", method = RequestMethod.POST)
    public String updateStatus(@PathVariable("companyId") int companyId,
                               @RequestParam("name") String name,
                               @RequestParam("status") boolean status,
                               @RequestParam(value = "token", required = false) String accessToken,
                               @RequestParam(value = "checksum", required = false) Long checksum,
                               Model model) {

        Company company = getCompany(companyId);

        PredefinedSite predefinedSite = predefinedSiteService.findByName(name);
        if (predefinedSite != null) {
            PredefinedSiteLink siteLink = predefinedSiteLinkService.findByCompanyAndSite(company, predefinedSite);
            siteLink.setActive(status);
            predefinedSiteLinkService.save(siteLink);
        } else {
            UserDefinedSiteLink siteLink = userDefinedSiteLinkService.findByCompanyAndName(company, name);
            if (siteLink != null) {
                siteLink.setActive(status);
                userDefinedSiteLinkService.save(siteLink);
            }
        }

        populateDefaultModel(model, company);
        return "company-control-panel-review-link";
    }

    @RequestMapping(value = "/{companyId}/reviews/sharing/update_link", method = RequestMethod.POST)
    public String updateLink(@PathVariable("companyId") int companyId,
                             @RequestParam("name") String name,
                             @RequestParam("link") String link,
                             @RequestParam(value = "token", required = false) String accessToken,
                             @RequestParam(value = "checksum", required = false) Long checksum,
                             Model model) {

        Company company = getCompany(companyId);

        PredefinedSite predefinedSite = predefinedSiteService.findByName(name);
        if (predefinedSite != null) {
            PredefinedSiteLink siteLink = predefinedSiteLinkService.findByCompanyAndSite(company, predefinedSite);
            siteLink.setLink(link);
            predefinedSiteLinkService.save(siteLink);
        } else {
            UserDefinedSiteLink siteLink = userDefinedSiteLinkService.findByCompanyAndName(company, name);
            if (siteLink != null) {
                siteLink.setLink(link);
                userDefinedSiteLinkService.save(siteLink);
            }
        }

        populateDefaultModel(model, company);
        return "company-control-panel-review-link";
    }

    @RequestMapping({"/{companyId}/reviews/automation"})
    public String reviewAutomation(@PathVariable("companyId") int companyId, @RequestParam(
            value = "token",
            required = false
    ) String accessToken, @RequestParam(
            value = "checksum",
            required = false
    ) Long checksum, Model model) {
        if (!this.companyService.hasAccessToCompany(companyId, checksum, accessToken)) {
            return "redirect:/login";
        } else {
            Map tempStatuses = this.integrationService.getIntegrationStatuses(companyId);
            HashMap statuses = new HashMap(tempStatuses.size());
            Iterator var7 = tempStatuses.entrySet().iterator();

            while (var7.hasNext()) {
                Map.Entry entry = (Map.Entry) var7.next();
                statuses.put(((IntegrationType) entry.getKey()).getValue(), entry.getValue());
            }

            model.addAttribute("company", this.getCompany(companyId));
            model.addAttribute("integrationStatuses", statuses);
            model.addAttribute("title", "Automatiske bedømmelser");
            return "company-control-panel-review-automation";
        }
    }

    @RequestMapping(value = "/{companyId}/reviews/monitoring", method = {RequestMethod.GET, RequestMethod.POST})
    public String reviewMonitoring(@PathVariable("companyId") int companyId,
                                   @RequestParam(value = "token", required = false) String accessToken,
                                   @RequestParam(value = "googlePlaceUrl", required = false) String googlePlaceUrl,
                                   @RequestParam(value = "facebookUrl", required = false) String facebookUrl,
                                   @RequestParam(value = "checksum", required = false) Long checksum,
                                   HttpServletRequest request,
                                   Model model) {
        if (!this.companyService.hasAccessToCompany(companyId, checksum, accessToken)) {
            return "redirect:/login";
        }
        Company company = getCompany(companyId);

        String method = request.getMethod();
        if (containsIgnoreCase(method, "post")) {
            ReviewLink reviewLink = reviewLinkService.findByCompanyId(companyId);
            reviewLink.setFacebookUrl(facebookUrl);
            reviewLink.setGooglePlaceUrl(googlePlaceUrl);
            reviewLinkService.save(reviewLink);
        }
        List<ExternalReview> reviews = externalReviewService.findByCompanyId(companyId);
        model.addAttribute("reviews", reviews);
        model.addAttribute("company", company);
        model.addAttribute("reviewLink", reviewLinkService.findByCompanyId(companyId));
        model.addAttribute("title", "Bedømmelse link");
        populateDefaultModel(model, company);

        return "company-control-panel-review-monitoring";
    }

    private Company getCompany(int companyId) {
        return this.companyService.getForControlPanelViewById(companyId);
    }

    private void populateDefaultModel(Model model, Company company) {

        List<SiteReviewForm> siteLinks = reviewSiteService.getReviewSitesForCompany(company);
        Map<String, PredefinedSite> sites = reviewSiteService.getAvailablePredefinedSites(company);

        model.addAttribute("siteLinks", siteLinks);
        model.addAttribute("predefinedSites", sites);
        model.addAttribute("company", company);
        model.addAttribute("title", "Bedømmelse link");
    }
}