package dk.better.company.repository;

import dk.better.company.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * @author Bo Andersen
 */
@Repository
public interface ServiceRepository extends JpaRepository<Service, Integer>, ServiceRepositoryCustom {
    List<Service> findByIndustriesId(int industryId);
    List<Service> findByIndustriesIdIn(Collection<Integer> industryIds);

    @Query("select case when count(s) > 0 then true else false end from Service s where s.slug = :slug")
    boolean existsByName(@Param("slug") String slug);
}