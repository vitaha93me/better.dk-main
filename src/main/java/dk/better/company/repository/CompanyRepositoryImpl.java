package dk.better.company.repository;

import dk.better.application.repository.AbstractEntityManagerRepository;
import dk.better.company.dto.ProfileUrlsByCiNumbersDTO;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

/**
 * @author Bo Andersen
 */
public class CompanyRepositoryImpl extends AbstractEntityManagerRepository {
    @SuppressWarnings("unchecked")
    public List<ProfileUrlsByCiNumbersDTO> getProfileUrlsByCiNumbers(@Param("ciNumbers") Collection<Integer> ciNumbers) {
        Session session = this.getEntityManager().unwrap(Session.class);
        SQLQuery query = session.createSQLQuery("select distinct c.ci_number as ciNumber, case " +
                "when temp.ci_count = 1 then c.profile_url " +
                "else NULL " +
                "end as profileUrl " +
                "from company as c " +
                "inner join ( " +
                "SELECT " +
                "ci_number, " +
                "count(*) AS ci_count " +
                "FROM company " +
                "WHERE ci_number IN (:ciNumbers) " +
                "GROUP BY ci_number " +
                ") as temp on (temp.ci_number = c.ci_number)")
                .addScalar("ciNumber", IntegerType.INSTANCE)
                .addScalar("profileUrl", StringType.INSTANCE);

        query.setParameterList("ciNumbers", ciNumbers);
        query.setResultTransformer(Transformers.aliasToBean(ProfileUrlsByCiNumbersDTO.class));

        return (List<ProfileUrlsByCiNumbersDTO>) query.list();
    }
}