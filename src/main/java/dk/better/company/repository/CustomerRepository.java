package dk.better.company.repository;

import dk.better.company.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Bo Andersen
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer>, JpaSpecificationExecutor<Customer> {
    @Query("select case when count(c) > 0 then true else false end from Customer c where c.company.id = :companyId and c.phoneNumber = :phoneNumber")
    boolean exists(@Param("companyId") int companyId, @Param("phoneNumber") String phoneNumber);

    @Query("select c from Customer c where c.company.id = :companyId and c.phoneNumber = :phoneNumber")
    Customer find(@Param("companyId") int companyId, @Param("phoneNumber") String phoneNumber);

    @Query("select c from Customer c where c.company.id = :companyId and c.email = :email")
    Customer findByEmail(@Param("companyId") int companyId, @Param("email") String email);

    @Query("select c from Customer c where c.company.id = :companyId and ((:email is not null and c.email = :email) or (:phoneNumber is not null and c.phoneNumber = :phoneNumber))")
    Customer findByEmailOrPhoneNumber(@Param("companyId") int companyId, @Param("email") String email, @Param("phoneNumber") String phoneNumber);

    Page<Customer> findByCompanyId(@Param("companyId") int companyId, Pageable pageable);

    // todo: Change the below to using the "upsert" feature in PostgreSQL 9.5 after upgrading the database. This will also be "transaction-safe"
    @Query(
            value = "INSERT INTO customer (email, uuid, type, company_id) " +
                    "(SELECT * FROM (VALUES(:email, :uuid, :type, :companyId)) AS temp " +
                    "WHERE NOT EXISTS (" +
                        "SELECT 1 FROM customer c where c.email = :email and c.company_id = :companyId" +
                    "))",

            nativeQuery = true
    )
    @Modifying
    int addByEmail(@Param("companyId") int companyId, @Param("email") String email, @Param("uuid") String uuid, @Param("type") int type);
}