package dk.better.company.repository;

import dk.better.company.entity.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Bo Andersen
 */
@Repository
public interface ReviewRepository extends JpaRepository<Review, Integer>, JpaSpecificationExecutor<Review>, ReviewRepositoryCustom {
    // NOTE: The below queries could be defined exclusively by method names, but with the Query annotation, we can ensure that
    // only a single SQL query is executed

    @Query(
            value = "select r " +
                    "from Review r " +
                    "inner join fetch r.company c " +
                    "left join fetch r.comments com " +
                    "left join fetch com.account " +
                    "left join fetch com.company " +
                    "left join fetch r.account a " +
                    "where c.id = :companyId",

            countQuery = "select count(r) " +
                    "from Review r " +
                    "inner join r.company c " +
                    "where c.id = :companyId"
    )
    Page<Review> findByCompanyId(@Param("companyId") int companyId, Pageable pageable);

    @Query(
            value = "select r " +
                    "from Review r " +
                    "inner join r.company c " +
                    "left join fetch r.comments com " +
                    "left join fetch com.account " +
                    "left join fetch com.company " +
                    "left join fetch r.account a " +
                    "where r.rating >= 4 " +
                    "and c.id = :companyId",

            countQuery = "select count(r) " +
                    "from Review r " +
                    "inner join r.company c " +
                    "where r.rating >= 4 " +
                    "and c.id = :companyId"
    )
    Page<Review> findByCompanyIdWithPositiveRating(@Param("companyId") int companyId, Pageable pageable);

    @Query(
            value = "select r " +
                    "from Review r " +
                    "inner join r.company c " +
                    "left join fetch r.comments com " +
                    "left join fetch com.account " +
                    "left join fetch com.company " +
                    "left join fetch r.account a " +
                    "where r.rating <= 2 " +
                    "and c.id = :companyId",

            countQuery = "select count(r) " +
                    "from Review r " +
                    "inner join r.company c " +
                    "where r.rating <= 2 " +
                    "and c.id = :companyId"
    )
    Page<Review> findByCompanyIdWithNegativeRating(@Param("companyId") int companyId, Pageable pageable);

    @Query(
            value = "select r " +
                    "from Review r " +
                    "inner join fetch r.company " +
                    "left join fetch r.comments com " +
                    "left join fetch com.account " +
                    "left join fetch com.company " +
                    "left join fetch r.account",

            countQuery = "select count(r) " +
                    "from Review r " +
                    "inner join r.company "
    )
    Page<Review> find(Pageable pageable);

    @Query(
            value = "select case when count(r) > 0 then true else false end " +
                    "from Review r " +
                    "left join r.account ra " +
                    "left join r.company c " +
                    "left join c.accountCompanies ac " +
                    "left join ac.account aca " +
                    "where r.id = :reviewId " +
                    "and (ra.id = :accountId or aca.id = :accountId)"
    )
    boolean hasAccessToReview(@Param("reviewId") int reviewId, @Param("accountId") int accountId);

    @Query(
            value = "select r " +
                    "from Review r " +
                    "inner join fetch r.company c " +
                    "left join fetch r.comments com " +
                    "left join fetch com.account " +
                    "left join fetch com.company " +
                    "left join fetch r.account a " +
                    "where r.uuid = :uuid",

            countQuery = "select count(r) " +
                    "from Review r " +
                    "inner join r.company c " +
                    "where r.uuid = :uuid"
    )
    Review findByUuid(@Param("uuid") String uuid);

    @Query(
            value = "select r " +
                    "from Review r " +
                    "inner join fetch r.company c " +
                    "left join fetch r.comments com " +
                    "left join fetch com.account " +
                    "left join fetch com.company " +
                    "left join fetch r.account a " +
                    "where r.uuid = :uuid " +
                    "and c.id = :companyId",

            countQuery = "select count(r) " +
                    "from Review r " +
                    "inner join r.company c " +
                    "where r.uuid = :uuid " +
                    "and c.id = :companyId"
    )
    Review findByUuid(@Param("companyId") int companyId, @Param("uuid") String uuid);
}