package dk.better.company.repository;

import dk.better.company.entity.Acknowledgement;
import dk.better.company.entity.Industry;

import java.util.Collection;

/**
 * @author Bo Andersen
 */
public interface AcknowledgementRepositoryCustom {
    /**
     * Validates whether or not a collection of acknowledgements are linked to a collection of industries.
     * Returns true only if all acknowledgements are linked to one of the provided industries.
     *
     * @param acknowledgements The acknowledgements to check against the industries
     * @param industries The industries for which the acknowledgements must be linked
     * @return True if all acknowledgements are linked to one of the provided industries. False otherwise.
     */
    boolean areLinkedToIndustries(Collection<Acknowledgement> acknowledgements, Collection<Industry> industries);
}