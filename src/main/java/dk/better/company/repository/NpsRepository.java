package dk.better.company.repository;

import dk.better.company.entity.NpsScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @author vitalii.
 */

@Repository
public interface NpsRepository extends JpaRepository<NpsScore, Integer> {
    @Query("select case when count(ns) > 0 then true else false end from NpsScore ns where ns.company.id = :companyId and ns.created >= :since and ns.ipAddress = :ipAddress")
    boolean hasScoredSince(@Param("companyId") int var1, @Param("since") Date var2, @Param("ipAddress") String var3);

    @Query("select case when count(ns) > 0 then true else false end from NpsScore ns where ns.company.id = :companyId and ns.account.id = :accountId and ns.created >= :since")
    boolean hasScoredSince(@Param("companyId") int var1, @Param("since") Date var2, @Param("accountId") int var3);
}
