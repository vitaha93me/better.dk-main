package dk.better.company.repository;

import dk.better.company.entity.ReviewComment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Bo Andersen
 */
@Repository
public interface ReviewCommentRepository extends JpaRepository<ReviewComment, Integer> {
    @Query(
            "select rc " +
            "from ReviewComment rc " +
            "inner join fetch rc.company " +
            "inner join fetch rc.account " +
            "where rc.review.id = :reviewId " +
            "order by rc.created asc"
    )
    List<ReviewComment> getComments(@Param("reviewId") int reviewId);
}