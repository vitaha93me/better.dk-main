package dk.better.company.config;

import dk.better.company.service.TextMessageService;
import dk.better.company.service.TextMessageServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Bo Andersen
 */
@Configuration
public class BeansConfig {
    @Bean
    public TextMessageService textMessageService(@Value("${application.domain.name}") String domainName, @Value("${application.http.scheme}") String httpScheme) {
        return new TextMessageServiceImpl(httpScheme + "://" + domainName + TextMessageService.STATUS_CALLBACK_RELATIVE_URL);
    }
}