package dk.better.company.service;

import dk.better.company.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Collection;

/**
 * @author Bo Andersen
 */
public interface CustomerService {
    static final String EVENT_IMPORTED_EMAILS = "Imported Customers (e-mails)";

    enum Type {
        CUSTOMER(1),
        LEAD(2);

        private int value;

        private Type(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    Customer save(Customer customer);
    boolean exists(int companyId, String phoneNumber);
    Page<Customer> find(int companyId, Pageable pageable);
    Customer find(int companyId, String phoneNumber);
    Customer findByEmail(int companyId, String email);
    Customer findByEmailOrPhoneNumber(int companyId, String email, String phoneNumber);

    /**
     * Imports a list of e-mail addresses as customers for a company
     *
     * @param companyId The ID of the company
     * @param emails The list of e-mail addresses
     * @param type The type of customers
     * @return The number of successfully imported customers
     */
    @PreAuthorize("isAuthenticated()")
    int importEmails(int companyId, Collection<String> emails, CustomerService.Type type);
}