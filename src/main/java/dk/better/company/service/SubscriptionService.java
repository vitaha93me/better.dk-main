package dk.better.company.service;

import dk.better.company.entity.Subscription;
import dk.better.company.util.ProductType;

import java.util.Date;
import java.util.List;

/**
 * @author Bo Andersen
 */
public interface SubscriptionService {
    void subscribe(int companyId, ProductType productType, Date startDate, Date endDate);

    boolean isSubscribed(int companyId, ProductType productType);

    /**
     * Checks whether or not a given company is subscribed to a product in at a given date
     *
     * @param companyId The ID of the company
     * @param productType The product to check
     * @param startDate The start date of the subscription
     * @return True if the company is subscribed and false otherwise
     */
    boolean isSubscribed(int companyId, ProductType productType, Date startDate);

    List<Subscription> getSubscriptions(int companyId, boolean includeInactive);
}