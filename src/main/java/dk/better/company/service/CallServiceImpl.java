package dk.better.company.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.segmentio.models.Props;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.service.AnalyticsService;
import dk.better.application.util.GeneralUtils;
import dk.better.company.entity.Call;
import dk.better.company.entity.Company;
import dk.better.company.entity.Customer;
import dk.better.company.entity.Industry;
import dk.better.company.repository.CallRepository;
import dk.better.company.util.CompanyUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Bo Andersen
 */
@Service
public class CallServiceImpl implements CallService {
    private static Logger logger = LogManager.getLogger(CallService.class);

    @Autowired
    private CallRepository callRepository;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private AnalyticsService analyticsService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${phone.pbx.number}")
    private String pbxNumber;

    @Value("${phone.secret}")
    private String secret;

    @Value("${phone.base-url}")
    private String baseUrl;


    @Override
    @Transactional
    public void call(String fromNumber, int companyId, String ipAddress) {
        Assert.notNull(fromNumber);
        Assert.isTrue(fromNumber.length() > 0);
        Company receiver = this.companyService.find(companyId);

        if (receiver == null) {
            throw new InvalidArgumentException(String.format("No company found with ID: %d", companyId));
        }

        if (receiver.getPhoneNumber() == null) {
            throw new RuntimeException(String.format("Cannot call company (ID: %d) because no phone number is available", companyId));
        }

        try {
            if (!GeneralUtils.isNumeric(fromNumber)) {
                fromNumber = null;
            }

            // Save call to database
            Call call = new Call();
            call.setFromNumber(fromNumber);
            call.setToNumber(receiver.getPhoneNumber());
            call.setReceiver(receiver);
            call.setIpAddress(ipAddress);
            this.callRepository.save(call);

            // Initiate call
            HttpURLConnection connection = (HttpURLConnection) new URL(this.baseUrl + String.format("/api/?company=%s&secret=%s&mode=call&from=%s&to=%d&show_from=%s&show_to=%s", this.pbxNumber, this.secret, fromNumber, 5001, receiver.getPhoneNumber(), fromNumber)).openConnection();
            connection.setRequestProperty("Accept-Charset", "UTF-8");

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder sb = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine);
            }

            in.close();
            String json = sb.toString();
            Map<String, String> map = this.objectMapper.readValue(json, new TypeReference<HashMap<String, String>>(){});

            // Check if everything went well
            if (!map.containsKey("status")) {
                throw new RuntimeException(String.format("No status field in JSON response! JSON: %s", json));
            }

            if (!map.get("status").equals("ok")) {
                throw new RuntimeException(String.format("Error initializing call. Message: %s. JSON: %s", map.get("msg"), json));
            }

            // Trigger analytics event
            Props props = new Props();
            props.put("caller_phone_number", fromNumber);
            props.put("company", new Props()
                    .put("id", receiver.getId())
                    .put("name", receiver.getName())
                    .put("phone_number", receiver.getPhoneNumber())
                    .put("call_tracking_number", receiver.getCallTrackingNumber())
                    .put("city", receiver.getCity())
                    .put("postal_code", receiver.getPostalCode())
                    .put("email", receiver.getEmail())
                    .put("affiliate", (receiver.getBookingUrl() != null ? "hungry" : null)));
            this.analyticsService.track(AnalyticsService.ANONYMOUS_USER_ID, "Call Initiated", props);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Call getLatestCallByFromNumber(String fromPhoneNumber) {
        if (!GeneralUtils.isNumeric(fromPhoneNumber)) {
            fromPhoneNumber = null;
        }

        return this.callRepository.findTop1ByFromNumberAndEndTimeIsNullOrderByIdDesc(fromPhoneNumber);
    }

    @Override
    @Transactional
    public void endCall(String fromNumber, String toNumber, Date startTime, Date connectTime, Date endTime, String uniqueId) {
        if (!GeneralUtils.isNumeric(fromNumber)) {
            fromNumber = null;
        }

        Call call = this.callRepository.findTop1ByFromNumberAndToNumberAndEndTimeIsNullOrderByIdDesc(fromNumber, toNumber);

        if (call != null) {
            Customer customer = null;
            call.setStartTime(startTime);
            call.setConnectTime(connectTime);
            call.setEndTime(endTime);
            call.setUniqueId(uniqueId);

            // Trigger analytics events
            Company receiver = call.getReceiver();
            Props props = new Props();
            props.put("caller_phone_number", fromNumber);
            props.put("company", new Props()
                    .put("id", receiver.getId())
                    .put("name", receiver.getName())
                    .put("phone_number", receiver.getPhoneNumber())
                    .put("call_tracking_number", receiver.getCallTrackingNumber())
                    .put("city", receiver.getCity())
                    .put("postal_code", receiver.getPostalCode())
                    .put("industries", CompanyUtils.getIndustryNames(receiver.getIndustries()))
                    .put("email", receiver.getEmail())
                    .put("affiliate", (receiver.getBookingUrl() != null ? "hungry" : null)));
            this.analyticsService.track(AnalyticsService.ANONYMOUS_USER_ID, "Call Ended", props);

            // todo: differentiate between industries here
            if (startTime != null && endTime != null && fromNumber != null && fromNumber.length() == 8) {
                long differenceInSeconds = ((endTime.getTime() - startTime.getTime()) / 1000);

                if (differenceInSeconds >= 10) {
                    customer = this.customerService.find(receiver.getId(), fromNumber);

                    if (customer == null) {
                        customer = this.companyService.addCustomer(receiver.getId(), fromNumber, CustomerService.Type.LEAD);
                    }

                    // Only send review SMS for restaurants, pizzerias, etc.
                    for (Industry industry : receiver.getIndustries()) {
                        if (industry.getId() == 458 || industry.getId() == 459 || industry.getId() == 461) { // 458 = Restauranter, 459 = Pizzeriaer, grillbarer, isbarer mv., 461 = Cafeér, værtshuse, diskoteker mv.
                            this.analyticsService.track(AnalyticsService.ANONYMOUS_USER_ID, "Send Review SMS", props);
                            break;
                        }
                    }
                }
            }

            call.setCustomer(customer);
            this.callRepository.save(call);
        } else {
            logger.info(String.format("Could not find latest call from %s to %s (method: endCall)", fromNumber, toNumber));
        }
    }

    @Override
    public Company initializeCallTrackingCall(String fromNumber, String callTrackingNumber) {
        Company company = this.companyService.findByCallTrackingNumber(callTrackingNumber);

        if (company != null) {
            if (!GeneralUtils.isNumeric(fromNumber)) {
                fromNumber = null;
            }

            // Save call
            Call call = new Call();
            call.setFromNumber(fromNumber);
            call.setToNumber(company.getPhoneNumber());
            call.setReceiver(company);
            this.callRepository.save(call);

            // Trigger analytics event
            Props props = new Props();
            props.put("caller_phone_number", fromNumber);
            props.put("company", new Props()
                    .put("id", company.getId())
                    .put("name", company.getName())
                    .put("phone_number", company.getPhoneNumber())
                    .put("call_tracking_number", company.getCallTrackingNumber())
                    .put("city", company.getCity())
                    .put("postal_code", company.getPostalCode())
                    .put("email", company.getEmail())
                    .put("affiliate", (company.getBookingUrl() != null ? "hungry" : null)));
            this.analyticsService.track(AnalyticsService.ANONYMOUS_USER_ID, "Call Tracking Call Initialized", props);

            return company;
        }

        return null;
    }
}