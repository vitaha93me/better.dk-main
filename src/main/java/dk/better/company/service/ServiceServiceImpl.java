package dk.better.company.service;

import dk.better.company.entity.Industry;
import dk.better.company.entity.Service;
import dk.better.company.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.List;

/**
 * @author Bo Andersen
 */
@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {
    @Autowired
    private ServiceRepository serviceRepository;


    @Override
    public Service getReference(Object identifier) {
        return this.serviceRepository.getEntityManager().getReference(Service.class, identifier);
    }

    @Override
    public Object getReference(Class type, Object identifier) {
        return this.serviceRepository.getEntityManager().getReference(type, identifier);
    }

    @Override
    public List<Service> findByIndustryId(int industryId) {
        return this.serviceRepository.findByIndustriesId(industryId);
    }

    @Override
    public List<Service> findByIndustryIds(Collection<Integer> industryIds) {
        return this.serviceRepository.findByIndustriesIdIn(industryIds);
    }

    @Override
    public Service find(int id) {
        return this.serviceRepository.findOne(id);
    }

    @Override
    public Service save(Service service) {
        return this.serviceRepository.save(service);
    }

    @Override
    public Service saveAndFlush(Service service) {
        return this.serviceRepository.saveAndFlush(service);
    }

    @Override
    public boolean fieldValueExists(Object value, String fieldName) throws UnsupportedOperationException {
        Assert.notNull(fieldName);

        if (!fieldName.equals("slug")) {
            throw new UnsupportedOperationException("Field name not supported");
        }

        if (value == null) {
            return false;
        }

        return this.serviceRepository.existsByName(value.toString());
    }

    @Override
    public boolean areLinkedToIndustries(Collection<Service> services, Collection<Industry> industries) {
        return this.serviceRepository.areLinkedToIndustries(services, industries);
    }
}