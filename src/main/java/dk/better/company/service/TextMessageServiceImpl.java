package dk.better.company.service;

import com.github.segmentio.models.Props;
import dk.better.account.entity.Account;
import dk.better.application.exception.LimitExceededException;
import dk.better.application.service.AnalyticsService;
import dk.better.application.service.HttpService;
import dk.better.application.util.AuthUtils;
import dk.better.application.util.GeneralUtils;
import dk.better.company.entity.Company;
import dk.better.company.entity.Customer;
import dk.better.company.entity.TextMessage;
import dk.better.company.entity.TextMessageCustomer;
import dk.better.company.repository.TextMessageRepository;
import dk.better.company.util.CompanyUtils;
import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.transaction.Transactional;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * @author Bo Andersen
 */
@Service
public class TextMessageServiceImpl implements TextMessageService {
    @Autowired
    private CustomerService customerService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private AnalyticsService analyticsService;

    @Autowired
    private HttpService httpService;

    @Autowired
    private TextMessageRepository textMessageRepository;

    private String statusCallbackUrl;


    public TextMessageServiceImpl() { }

    public TextMessageServiceImpl(String statusCallbackUrl) {
        this.statusCallbackUrl = statusCallbackUrl;
    }

    @Override
    @Transactional
    public void sendToAllCustomers(int companyId, String text) throws LimitExceededException {
        Account account = (Account) AuthUtils.getAuthentication().getPrincipal();

        if (!CompanyUtils.isCompanyOwner(account, companyId)) {
            throw new RuntimeException(String.format("The account is not allowed to sent text message to company %s's customers", companyId));
        }

        // Verify that a message has not been sent within 24 hours
        if (this.hasSentTextMessageWithinOneDay(companyId)) {
            throw new LimitExceededException("Only one text message may be sent per day");
        }

        Company company = this.companyService.getForProfileViewById(companyId); // Because of the check above, we know that the company exists

        // Trigger sent text message event
        Props props = new Props();
        props.put("message", text);
        props.put("company", new Props()
                .put("id", company.getId())
                .put("name", company.getName())
                .put("phone_number", company.getPhoneNumber())
                .put("call_tracking_number", company.getCallTrackingNumber())
                .put("city", company.getCity())
                .put("postal_code", company.getPostalCode())
                .put("email", company.getEmail())
                .put("affiliate", (company.getBookingUrl() != null ? "hungry" : null)));
        this.analyticsService.track(AnalyticsService.ANONYMOUS_USER_ID, "Sent Text Message To All Customers", props);

        int itemsPerPage = 1000;
        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.ASC, "id"));
        Pageable pageable = new PageRequest(0, itemsPerPage, sortOrder);
        Page<Customer> page = this.customerService.find(companyId, pageable);
        List<Customer> customers = page.getContent();

        if (page.hasNext()) {
            throw new LimitExceededException("Can only send SMS to 1,000 customers");
        }

        Set<TextMessageCustomer> textMessageCustomers = new HashSet<>(customers.size());
        TextMessage textMessage = new TextMessage();
        textMessage.setText(text);
        textMessage.setNumberOfRecipients((int) page.getTotalElements()); // This cast should be safe for a looooong time
        textMessage.setSenderAccount(account);
        textMessage.setSenderCompany(company);

        for (Customer customer : customers) {
            textMessageCustomers.add(new TextMessageCustomer(textMessage, customer, GeneralUtils.getRandomUUID()));
        }

        textMessage.setTextMessageCustomers(textMessageCustomers);
        this.textMessageRepository.save(textMessage);
    }

    @Override
    public boolean hasSentTextMessageWithinOneDay(int companyId) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return this.textMessageRepository.existsWithinOneDay(companyId, calendar.getTime());
    }

    private String getSendMessageXml(TextMessage textMessage, String senderName, String callbackUrl, String countryCode) {
        Assert.notNull(textMessage);
        Assert.notNull(senderName);
        Assert.notNull(countryCode);
        Assert.isTrue(!countryCode.isEmpty());

        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

            // Root
            Document document = documentBuilder.newDocument();
            Element rootElement = document.createElement("request");

            // Authentication
            Element authentication = document.createElement("authentication");
            authentication.setAttribute("apikey", "62fad8d3-6adf-44b4-b182-16b6422e1dfd");
            rootElement.appendChild(authentication);

            /***** Data *****/
            Element data = document.createElement("data");

            if (callbackUrl != null && !callbackUrl.isEmpty()) {
                Element callback = document.createElement("statuscallbackurl");
                callback.appendChild(document.createTextNode(callbackUrl));
                data.appendChild(callback);
            }

            /***** Messages *****/
            Element message = document.createElement("message");

            // Sender name
            Element senderNameElement = document.createElement("sendername");
            senderNameElement.appendChild(document.createTextNode(senderName));
            message.appendChild(senderNameElement);

            // Text
            Element text = document.createElement("text");
            text.appendChild(document.createTextNode(textMessage.getText()));
            message.appendChild(text);

            // Recipients
            Element recipients = document.createElement("recipients");

            for (TextMessageCustomer textMessageCustomer : textMessage.getTextMessageCustomers()) {
                Element msisdn = document.createElement("msisdn");
                msisdn.setAttribute("id", textMessageCustomer.getUuid());
                msisdn.setTextContent(countryCode + textMessageCustomer.getCustomer().getPhoneNumber());
                recipients.appendChild(msisdn);
            }

            message.appendChild(recipients);
            data.appendChild(message);
            rootElement.appendChild(data);
            document.appendChild(rootElement);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);

            OutputStream baos = new ByteArrayOutputStream();
            StreamResult result =  new StreamResult(baos);
            transformer.transform(source, result);

            return baos.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void sendTextMessage(TextMessage textMessage) {
        Company sender = textMessage.getSenderCompany();

        if (sender.getPhoneNumber() == null || sender.getPhoneNumber().isEmpty()) {
            throw new RuntimeException("Sender must have a phone number!");
        }

        String xml = this.getSendMessageXml(textMessage, sender.getPhoneNumber(), this.statusCallbackUrl, "45");
        Map<String, Object> parameters = new HashMap<>(1);
        parameters.put("xml", xml);
        HttpResponse response = this.httpService.post("https://mm.inmobile.dk/Api/V2/SendMessages", parameters);

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException(String.format("Could not send text message. HTTP response: %d: %s", response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase()));
        }
    }

    @Override
    @Transactional
    public void confirm(int companyId, int textMessageId) {
        TextMessage textMessage = this.textMessageRepository.findForConfirmation(companyId, textMessageId);

        if (textMessage == null) {
            throw new RuntimeException(String.format("Text message with ID: %d not found!", textMessageId));
        }

        textMessage.setStatus(TextMessageService.STATUS_ACCEPTED);
        this.textMessageRepository.save(textMessage);
        this.sendTextMessage(textMessage);
    }

    @Override
    @Transactional
    public void decline(int companyId, int textMessageId) {
        TextMessage textMessage = this.textMessageRepository.findForDecline(companyId, textMessageId);

        if (textMessage == null) {
            throw new RuntimeException(String.format("Text message with ID: %d not found!", textMessageId));
        }

        textMessage.setStatus(TextMessageService.STATUS_DECLINED);
        this.textMessageRepository.save(textMessage);
    }

    @Override
    public Page<TextMessage> getPendingTextMessages(Pageable pageable) {
        return this.textMessageRepository.getPendingTextMessages(pageable);
    }

    @Override
    @Transactional
    public void updateText(int companyId, int textMessageId, String newText) {
        this.textMessageRepository.updateText(companyId, textMessageId, newText);
    }

    @Override
    @Transactional
    public void updateTextMessageCustomerStatus(String uuid, int status) {
        this.textMessageRepository.updateTextMessageCustomerStatus(uuid, status, new Date());
    }
}