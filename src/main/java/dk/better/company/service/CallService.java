package dk.better.company.service;

import dk.better.company.entity.Call;
import dk.better.company.entity.Company;

import java.util.Date;

/**
 * @author Bo Andersen
 */
public interface CallService {
    void call(String fromNumber, int companyId, String ipAddress);
    Call getLatestCallByFromNumber(String fromPhoneNumber);
    void endCall(String fromNumber, String toNumber, Date startTime, Date connectTime, Date endTime, String uniqueId);
    Company initializeCallTrackingCall(String fromNumber, String callTrackingNumber);
}