package dk.better.company.service;

import dk.better.company.entity.Company;
import dk.better.company.entity.PredefinedSite;
import dk.better.company.entity.PredefinedSiteLink;
import dk.better.company.entity.UserDefinedSiteLink;
import dk.better.company.form.SiteReviewForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static dk.better.company.form.SiteType.PREDEFINED;

/**
 * @author vitalii.
 */
@Service
public class ReviewSiteService {

    @Autowired
    private PredefinedSiteLinkService predefinedSiteLinkService;

    @Autowired
    private UserDefinedSiteLinkService userDefinedSiteLinkService;

    @Autowired
    private PredefinedSiteService predefinedSiteService;

    public List<SiteReviewForm> getReviewSitesForCompany(Company company) {
        List<PredefinedSiteLink> predefinedSiteLinks = predefinedSiteLinkService.findByCompany(company);
        List<UserDefinedSiteLink> userDefinedSiteLinks = userDefinedSiteLinkService.findByCompany(company);

        List<SiteReviewForm> siteLinks = new ArrayList<>();

        predefinedSiteLinks.forEach(predefinedSite -> {
            SiteReviewForm reviewForm = new SiteReviewForm();
            reviewForm.setName(predefinedSite.getSite().getName());
            reviewForm.setLink(predefinedSite.getLink());
            reviewForm.setStatus(predefinedSite.isActive());
            reviewForm.setType(PREDEFINED);
            reviewForm.setCss(predefinedSite.getSite().getCss());
            siteLinks.add(reviewForm);
        });

        userDefinedSiteLinks.forEach(userDefinedSite -> {
            SiteReviewForm reviewForm = new SiteReviewForm();
            reviewForm.setName(userDefinedSite.getName());
            reviewForm.setLink(userDefinedSite.getLink());
            reviewForm.setStatus(userDefinedSite.isActive());
            reviewForm.setType(PREDEFINED);
            reviewForm.setCss(userDefinedSite.getCss());
            siteLinks.add(reviewForm);
        });

        if (!siteLinks.isEmpty()) {
            for (SiteReviewForm siteLink : siteLinks) {
                if (siteLink.getCss().contains("better")) {
                    return siteLinks;
                }
            }
        }

        if (!siteLinks.isEmpty()) {
            SiteReviewForm betterDkSiteReview = new SiteReviewForm();
            betterDkSiteReview.setCss("better_dk");
            betterDkSiteReview.setName("Better.dk");
            betterDkSiteReview.setLink(company.getProfileUrl() + "/bedoemmelser#bedoem");
            betterDkSiteReview.setStatus(true);
            betterDkSiteReview.setType(PREDEFINED);
            siteLinks.add(0, betterDkSiteReview);
        }

        return siteLinks;
    }

    public Map<String, PredefinedSite> getAvailablePredefinedSites(Company company) {

        Map<String, PredefinedSite> sites = new LinkedHashMap<>();
        List<PredefinedSiteLink> predefinedSiteLinks = predefinedSiteLinkService.findByCompany(company);

        List<PredefinedSite> allPredefinedSites = predefinedSiteService.findAll();

        List<PredefinedSite> toRemove = new ArrayList<>();
        for (PredefinedSite site : allPredefinedSites) {
            toRemove.addAll(predefinedSiteLinks
                    .stream()
                    .filter(predefinedSiteLink -> site
                            .getName()
                            .equals(predefinedSiteLink.getSite().getName()))
                    .map(predefinedSiteLink -> site).collect(Collectors.toList()));


        }
        allPredefinedSites.removeAll(toRemove);

        allPredefinedSites.forEach(predefinedSite ->
                sites.put(predefinedSite.getName(), predefinedSite));

        return sites;
    }
}
