package dk.better.company.service;

import dk.better.application.exception.UniqueConstraintViolationException;
import dk.better.application.service.AbstractService;
import dk.better.company.entity.CompanyMeta;
import dk.better.company.entity.Review;
import dk.better.company.entity.ReviewComment;
import dk.better.company.util.ReviewType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Bo Andersen
 */
public interface ReviewService extends AbstractService<Review> {
    static final String EVENT_REVIEW_INVITE = "Invited Customers To Review";

    Page<Review> find(Pageable pageable);
    Page<Review> findByCompanyId(int companyId, Pageable pageable);
    Page<Review> findByCompanyId(int companyId, Pageable pageable, ReviewType reviewType);
    Review findByUuid(String uuid);
    Review findByUuid(int companyId, String uuid);
    Review create(int companyId, int rating, String title, String text) throws UniqueConstraintViolationException;
    Review create(int companyId, int rating, String title, String text, String name, String email) throws UniqueConstraintViolationException;
    Review create(int companyId, int rating, String title, String text, String name, String email, String phoneNumber) throws UniqueConstraintViolationException;
    Map<Long, Long> getRatingDistribution(int companyId);
    List<ReviewComment> getComments(int reviewId);
    boolean hasAccessToReview(int reviewId, int accountId);
    String getReviewRedirectUrl(CompanyMeta meta, String userAgent);

    @PreAuthorize("isAuthenticated()")
    ReviewComment addCommentAsCompany(int reviewId, String text);

    @PreAuthorize("isAuthenticated()")
    void requestDeletion(String reviewUuid, String reason);

    /**
     * Invites customers to review the company and optionally adds the customers to the customer list
     *
     * @param companyId The ID of the company
     * @param emails The list of e-mail addresses
     * @param addToCustomerList Whether or not the customers should be added to the customer list
     * @param type The type of customers
     * @return The number of successfully imported customers if added to the customer list; otherwise NULL
     */
    @PreAuthorize("isAuthenticated()")
    Integer inviteToReview(int companyId, Collection<String> emails, boolean addToCustomerList, CustomerService.Type type);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void sendEmailToReviewedCompany(int reviewId);
}