package dk.better.company.service;

import com.github.segmentio.models.Props;
import dk.better.account.entity.Account;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.exception.UniqueConstraintViolationException;
import dk.better.application.service.AnalyticsService;
import dk.better.application.util.AuthUtils;
import dk.better.application.util.RequestStatus;
import dk.better.company.entity.Company;
import dk.better.company.entity.Subdomain;
import dk.better.company.entity.SubdomainRequest;
import dk.better.company.repository.SubdomainRepository;
import dk.better.company.repository.SubdomainRequestRepository;
import dk.better.company.util.CompanyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * @author Bo Andersen
 */
@Service
public class SubdomainServiceImpl implements SubdomainService {
    private static final String EVENT_SUBDOMAIN_ACCEPTED = "Subdomain Accepted";
    private static final String EVENT_SUBDOMAIN_DECLINED = "Subdomain Declined";

    @Value("${application.domain.name}")
    private String domainName;

    @Value("${application.http.scheme}")
    private String httpScheme;

    @Autowired
    private SubdomainRepository subdomainRepository;

    @Autowired
    private SubdomainRequestRepository subdomainRequestRepository;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private AnalyticsService analyticsService;


    @Override
    public Subdomain getReference(Object identifier) {
        return this.subdomainRepository.getEntityManager().getReference(Subdomain.class, identifier);
    }

    @Override
    public Object getReference(Class type, Object identifier) {
        return this.subdomainRepository.getEntityManager().getReference(type, identifier);
    }

    @Override
    public Subdomain find(int id) {
        return this.subdomainRepository.findOne(id);
    }

    @Override
    public Subdomain save(Subdomain subdomain) {
        return this.subdomainRepository.save(subdomain);
    }

    @Override
    public Subdomain saveAndFlush(Subdomain subdomain) {
        return this.subdomainRepository.saveAndFlush(subdomain);
    }

    @Override
    public List<Subdomain> findAllCompanySubdomainsByName(String subdomain) {
        return this.subdomainRepository.findAllCompanySubdomainsByName(subdomain);
    }

    @Override
    public List<Subdomain> findAllCompanySubdomainsByCompanyId(int companyId) {
        return this.subdomainRepository.findAllCompanySubdomainsByCompanyId(companyId);
    }

    @Override
    public boolean fieldValueExists(Object value, String fieldName) throws UnsupportedOperationException {
        Assert.notNull(fieldName);

        if (!fieldName.equals("name") && !fieldName.equals("requestedSubdomain")) {
            throw new UnsupportedOperationException("Field name not supported");
        }

        if (value == null) {
            return false;
        }

        return this.subdomainRepository.existsByName(value.toString());
    }

    @Override
    public boolean exists(String subdomain) {
        return this.subdomainRepository.existsByName(subdomain);
    }

    @Override
    public Subdomain findByName(String subdomain) {
        return this.subdomainRepository.findByName(subdomain);
    }

    @Override
    public Page<SubdomainRequest> getSubdomainRequests(Pageable pageable) {
        return this.subdomainRequestRepository.findAll(pageable);
    }

    @Override
    public Page<SubdomainRequest> getSubdomainRequests(RequestStatus requestStatus, Pageable pageable) {
        int requestStatusId = 0;

        switch (requestStatus) {
            case PENDING: requestStatusId = SubdomainService.REQUEST_STATUS_PENDING_ID; break;
            case ACCEPTED: requestStatusId = SubdomainService.REQUEST_STATUS_ACCEPTED_ID; break;
            case DECLINED: requestStatusId = SubdomainService.REQUEST_STATUS_DECLINED_ID; break;
        }

        return this.subdomainRequestRepository.findByRequestStatusId(requestStatusId, pageable);
    }

    @Override
    @Transactional
    public void acceptSubdomainRequest(int subdomainRequestId) {
        SubdomainRequest request = this.subdomainRequestRepository.findById(subdomainRequestId);

        if (request == null) {
            throw new InvalidArgumentException(String.format("A subdomain request with the specified ID does not exist (ID: %d)", subdomainRequestId));
        }

        if (request.getRequestStatus().getId() != SubdomainService.REQUEST_STATUS_PENDING_ID) {
            throw new RuntimeException(String.format("Subdomains must have the status 'pending' to be accepted! (ID: %d)", subdomainRequestId));
        }

        String requestedSubdomain = request.getRequestedSubdomain();

        if (this.subdomainRepository.existsByName(requestedSubdomain)) {
            throw new UniqueConstraintViolationException(String.format("Cannot accept subdomain '%s' because it is already taken", requestedSubdomain));
        }

        request.setRequestStatus(this.subdomainRequestRepository.getEntityManager().getReference(dk.better.application.entity.RequestStatus.class, SubdomainService.REQUEST_STATUS_ACCEPTED_ID));
        this.subdomainRequestRepository.save(request);
        this.companyService.deactivateCompanySubdomains(request.getCompany().getId());

        Subdomain subdomain = new Subdomain();
        subdomain.setName(requestedSubdomain);
        subdomain.setActive(true);
        subdomain.setTimeAdded(new Date());
        subdomain.setCompany(request.getCompany());
        this.save(subdomain);

        this.companyService.updateProfileUrl(request.getCompany().getId(), request.getRequestedSubdomain(), this.httpScheme, this.domainName);

        // Analytics events
        Props props = new Props();
        props.put("new_profile_url", this.httpScheme + "://" + request.getRequestedSubdomain() + "." + this.domainName);
        this.analyticsService.track(request.getRequester().getUuid(), EVENT_SUBDOMAIN_ACCEPTED, props);
    }

    @Override
    public void declineSubdomainRequest(int subdomainRequestId) {
        this.declineSubdomainRequest(subdomainRequestId, null);
    }

    @Override
    @Transactional
    public void declineSubdomainRequest(int subdomainRequestId, String reason) {
        SubdomainRequest request = this.subdomainRequestRepository.findById(subdomainRequestId);

        if (request == null) {
            throw new InvalidArgumentException(String.format("A subdomain request with the specified ID does not exist (ID: %d)", subdomainRequestId));
        }

        if (request.getRequestStatus().getId() != SubdomainService.REQUEST_STATUS_PENDING_ID) {
            throw new RuntimeException(String.format("Subdomains must have the status 'pending' to be declined! (ID: %d)", subdomainRequestId));
        }

        request.setRequestStatus(this.subdomainRequestRepository.getEntityManager().getReference(dk.better.application.entity.RequestStatus.class, SubdomainService.REQUEST_STATUS_DECLINED_ID));
        this.subdomainRequestRepository.save(request);

        // Analytics events
        Props props = new Props();
        props.put("subdomain", request.getRequestedSubdomain());
        props.put("current_profile_url", request.getCompany().getProfileUrl());

        if (reason != null) {
            props.put("reason", reason);
            this.analyticsService.track(request.getRequester().getUuid(), EVENT_SUBDOMAIN_DECLINED, props);
        } else {
            this.analyticsService.track(request.getRequester().getUuid(), EVENT_SUBDOMAIN_DECLINED);
        }
    }

    @Override
    public void deactivateCompanySubdomains(int companyId) {
        Assert.isTrue(companyId > 0);
        this.subdomainRepository.deactivateCompanySubdomains(companyId);
    }

    @Override
    @Transactional
    public void addSubdomainRequest(int companyId, String subdomain) {
        Assert.isTrue(companyId > 0);
        Assert.notNull(subdomain);
        Assert.isTrue(!subdomain.isEmpty());
        subdomain = subdomain.trim();

        // Validate that the user is the owner of the company or is an administrator
        Account account = (Account) AuthUtils.getAuthentication().getPrincipal();

        if (!CompanyUtils.isCompanyOwner(account, companyId) && !AuthUtils.isAdmin()) {
            throw new AccessDeniedException(String.format("User must be either the owner of company (ID: %d) or an administrator", companyId));
        }

        // Validate that the subdomain does not already exist
        if (this.exists(subdomain)) {
            throw new RuntimeException(String.format("Cannot request existing subdomain: %s", subdomain));
        }

        // Save request
        SubdomainRequest request = new SubdomainRequest();
        request.setCompany(this.subdomainRequestRepository.getEntityManager().getReference(Company.class, companyId));
        request.setRequestedSubdomain(subdomain);
        request.setRequester(account);
        request.setRequestStatus(this.subdomainRequestRepository.getEntityManager().getReference(dk.better.application.entity.RequestStatus.class, REQUEST_STATUS_PENDING_ID));

        this.subdomainRequestRepository.save(request);
    }

    @Override
    @Transactional
    public void updateSubdomain(int companyId, String newSubdomain) {
        Assert.notNull(newSubdomain);
        Assert.isTrue(!newSubdomain.isEmpty());
        newSubdomain = newSubdomain.trim();

        if (this.subdomainRepository.existsByName(newSubdomain)) {
            throw new UniqueConstraintViolationException(String.format("Cannot update to subdomain '%s' because it is already taken", newSubdomain));
        }

        // Deactivate any existing subdomains
        this.companyService.deactivateCompanySubdomains(companyId);

        // Save subdomain
        Subdomain subdomain = new Subdomain();
        subdomain.setName(newSubdomain);
        subdomain.setActive(true);
        subdomain.setTimeAdded(new Date());
        subdomain.setCompany(this.companyService.getReference(companyId));
        this.save(subdomain);

        // Update profile URL
        this.companyService.updateProfileUrl(companyId, newSubdomain, this.httpScheme, this.domainName);
    }

    @Override
    public List<Subdomain> getSubdomains(String companyIdOrSlug) {
        if (CompanyUtils.isCompanyId(companyIdOrSlug)) {
            return this.findAllCompanySubdomainsByCompanyId(Integer.parseInt(companyIdOrSlug));
        } else {
            return this.findAllCompanySubdomainsByName(companyIdOrSlug); // The value is a company's slug
        }
    }
}