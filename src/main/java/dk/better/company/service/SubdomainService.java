package dk.better.company.service;

import dk.better.application.service.AbstractService;
import dk.better.application.service.FieldValueExists;
import dk.better.application.util.RequestStatus;
import dk.better.company.entity.Subdomain;
import dk.better.company.entity.SubdomainRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

/**
 * @author Bo Andersen
 */
public interface SubdomainService extends FieldValueExists, AbstractService<Subdomain> {
    static final int REQUEST_STATUS_PENDING_ID = 1;
    static final int REQUEST_STATUS_ACCEPTED_ID = 2;
    static final int REQUEST_STATUS_DECLINED_ID = 3;

    List<Subdomain> findAllCompanySubdomainsByName(String subdomain);
    List<Subdomain> findAllCompanySubdomainsByCompanyId(int companyId);
    boolean exists(String subdomain);
    Subdomain findByName(String subdomain);
    void deactivateCompanySubdomains(int companyId);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    Page<SubdomainRequest> getSubdomainRequests(Pageable pageable);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    Page<SubdomainRequest> getSubdomainRequests(RequestStatus requestStatus, Pageable pageable);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void acceptSubdomainRequest(int subdomainRequestId);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void declineSubdomainRequest(int subdomainRequestId);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void declineSubdomainRequest(int subdomainRequestId, String reason);

    @PreAuthorize("isAuthenticated()")
    void addSubdomainRequest(int companyId, String subdomain);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void updateSubdomain(int companyId, String newSubdomain);

    List<Subdomain> getSubdomains(String companyIdOrSlug);
}