package dk.better.company.service;

import dk.better.company.entity.Company;
import dk.better.company.entity.PredefinedSite;
import dk.better.company.entity.PredefinedSiteLink;
import dk.better.company.entity.ReviewLink;
import dk.better.company.repository.PredefinedSiteLinkRepository;
import dk.better.company.repository.ReviewLinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author vitalii
 */
@Service
public class ReviewLinkService {

    @Autowired
    private ReviewLinkRepository reviewLinkRepository;

    public ReviewLink save(ReviewLink site) {
        return this.reviewLinkRepository.save(site);
    }

    public List<ReviewLink> findAll() {
        return reviewLinkRepository.findAll();
    }

    public ReviewLink findByCompanyId(int companyId) {
        List<ReviewLink> links = reviewLinkRepository.findByCompanyId(companyId);
        if (links.isEmpty()) {
            ReviewLink reviewLink = new ReviewLink();
            reviewLink.setCompanyId(companyId);
            return reviewLinkRepository.save(reviewLink);
        }
        return links.get(0);
    }
}