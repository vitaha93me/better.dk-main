package dk.better.company.service;

import dk.better.application.service.AbstractService;
import dk.better.company.entity.Page;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

/**
 * @author Bo Andersen
 */
public interface PageService extends AbstractService<Page> {
    List<Page> findRootPagesForCompany(int companyId);
    Page findRootPageBySlug(String slug, int companyId);
    Page findChildPageBySlug(String parentSlug, String childSlug, int companyId);

    @PreAuthorize("isAuthenticated()")
    Page add(Page page, int companyId);

    @PreAuthorize("isAuthenticated()")
    Page add(Page page, int companyId, Integer parentId);

    boolean exists(String slug, int companyId, Integer parentId);
}