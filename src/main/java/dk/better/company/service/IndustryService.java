package dk.better.company.service;

import dk.better.company.entity.Industry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Collection;
import java.util.List;

/**
 * @author Bo Andersen
 */
public interface IndustryService {
    static final int EMPTY_INDUSTRY_ID = 99997;
    static final int UNKNOWN_INDUSTRY_ID = 99998;
    static final int UNDISCLOSED_INDUSTRY_ID = 99999;

    List<Industry> findAll();
    List<Industry> findAll(Sort sort);
    Industry find(int industryId);
    Page<Industry> search(String query, Pageable pageable);

    Industry save(Industry industry);
    Industry saveAndFlush(Industry industry);
    void add(Collection<Industry> industries);
}