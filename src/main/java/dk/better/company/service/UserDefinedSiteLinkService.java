package dk.better.company.service;

import dk.better.company.entity.Company;
import dk.better.company.entity.UserDefinedSiteLink;
import dk.better.company.repository.UserDefinedSiteLinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author vitalii
 */
@Service
public class UserDefinedSiteLinkService {

    @Autowired
    private UserDefinedSiteLinkRepository userDefinedSiteRepository;

    public UserDefinedSiteLink save(UserDefinedSiteLink site) {
        return this.userDefinedSiteRepository.save(site);
    }

    public List<UserDefinedSiteLink> findAll() {
        return userDefinedSiteRepository.findAll();
    }

    public List<UserDefinedSiteLink> findByCompany(Company company) {
        return userDefinedSiteRepository.findByCompany(company);
    }

    public UserDefinedSiteLink findByCompanyAndName(Company company, String name) {
        return userDefinedSiteRepository.findByCompanyAndName(company, name);
    }
}