package dk.better.company.service;

import dk.better.company.entity.Integration;
import dk.better.company.util.IntegrationType;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Map;

/**
 * @author vitalii.
 */
public interface IntegrationService {
    @PreAuthorize("isAuthenticated()")
    void activate(int var1, IntegrationType var2);

    @PreAuthorize("isAuthenticated()")
    void activate(int var1, IntegrationType var2, Map<String, Object> var3);

    @PreAuthorize("isAuthenticated()")
    void deactivate(int var1, IntegrationType var2);

    @PreAuthorize("isAuthenticated()")
    Map<IntegrationType, Boolean> getIntegrationStatuses(int var1);

    Integration find(int var1, IntegrationType var2);
}
