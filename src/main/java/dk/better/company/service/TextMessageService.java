package dk.better.company.service;

import dk.better.application.exception.LimitExceededException;
import dk.better.company.entity.TextMessage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 * @author Bo Andersen
 */
public interface TextMessageService {
    public static final String STATUS_CALLBACK_RELATIVE_URL = "/api/text-message/update-status";
    public static final int STATUS_PENDING_REVIEW = 1;
    public static final int STATUS_DECLINED = 2;
    public static final int STATUS_ACCEPTED = 3;


    @PreAuthorize("isAuthenticated()")
    void sendToAllCustomers(int companyId, String text) throws LimitExceededException;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void confirm(int companyId, int textMessageId);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void decline(int companyId, int textMessageId);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void updateText(int companyId, int textMessageId, String newText);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    Page<TextMessage> getPendingTextMessages(Pageable pageable);

    boolean hasSentTextMessageWithinOneDay(int companyId);
    void updateTextMessageCustomerStatus(String uuid, int status);
}