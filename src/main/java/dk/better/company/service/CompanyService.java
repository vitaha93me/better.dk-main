package dk.better.company.service;

import dk.better.account.entity.Account;
import dk.better.application.exception.ObjectNotFoundException;
import dk.better.application.exception.ResourceNotFoundException;
import dk.better.application.exception.UniqueConstraintViolationException;
import dk.better.application.model.SearchResults;
import dk.better.application.service.AbstractService;
import dk.better.company.dto.ProfileUrlsByCiNumbersDTO;
import dk.better.company.entity.Company;
import dk.better.company.entity.Customer;
import dk.better.company.entity.Industry;
import dk.better.company.entity.Like;
import dk.better.company.util.CompanyUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Bo Andersen
 */
public interface CompanyService
        extends AbstractService<Company> {
    String IMAGES_EXTENSION = "jpg";

    static long generateChecksum(int companyId) {
        return CompanyUtils.generateChecksum((int) companyId);
    }

    Company getForControlPanelViewById(int var1);

    Company getForProfileViewById(int var1);

    Company getFullWithMeta(int var1);

    Company findOneByIdForAdminCompanyEdit(int var1);

    SearchResults<Company> search(String var1, int var2);

    SearchResults<Company> search(String var1, String var2, int var3);

    SearchResults<Company> searchByLocation(String var1, int var2);

    Page<Company> getCompaniesAddedBetween(Pageable var1, Date var2, Date var3);

    Page<Company> getCompaniesAddedBetweenInIndustries(Pageable var1, Date var2, Date var3, Integer[] var4);

    List<Company> getCompaniesClaimedBetween(Date var1, Date var2);

    List<Company> getCompaniesClaimedBetweenInIndustries(Date var1, Date var2, Integer[] var3);

    dk.better.company.entity.CompanyService getService(int var1, String var2);

    void claim(int var1, Account var2) throws RuntimeException;

    @PreAuthorize(value = "isAuthenticated()")
    Like addLike(int var1, Account var2) throws ObjectNotFoundException;

    @PreAuthorize(value = "isAuthenticated()")
    Like addLike(Company var1, Account var2) throws UniqueConstraintViolationException;

    @PreAuthorize(value = "isAuthenticated()")
    void updateLogo(int var1, File var2) throws RuntimeException;

    @PreAuthorize(value = "isAuthenticated()")
    void updateCoverPhoto(int var1, File var2) throws RuntimeException;

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    void deactivateCompanySubdomains(int var1);

    @PreAuthorize(value = "isAuthenticated()")
    void updateWebsite(int var1, String var2);

    @PreAuthorize(value = "isAuthenticated()")
    void updatePhoneNumber(int var1, String var2);

    @PreAuthorize(value = "isAuthenticated()")
    void updateEmail(int var1, String var2);

    @PreAuthorize(value = "isAuthenticated()")
    void updateTeaser(int var1, String var2);

    @PreAuthorize(value = "isAuthenticated()")
    void addIndustries(int var1, int[] var2);

    @PreAuthorize(value = "isAuthenticated()")
    void addIndustries(int var1, Collection<Industry> var2);

    boolean isOwnerOfCompany(int var1, Account var2);

    boolean isCompanyClaimed(int var1);

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    Company adminSaveAndFlush(Company var1);

    Company save(Company var1);

    Company saveAndFlush(Company var1);

    void add(Collection<Company> var1);

    void incrementReviewCount(int var1);

    void updateReviewRating(int var1);

    void initializeProfileUrls(String var1, String var2);

    void updateProfileUrl(int var1, String var2, String var3, String var4);

    Company findByCallTrackingNumber(String var1);

    Company findByAccessToken(String var1);

    List<Company> findByVatNumberAndPostalCode(long var1, int var3);

    List<Company> findByVatNumber(long var1);

    Customer addCustomer(int var1, String var2, CustomerService.Type var3);

    boolean hasAccessToCompany(int var1);

    boolean hasAccessToCompany(int var1, Long var2, String var3);

    void refreshSearchIndex(Company var1);

    void refreshSearchIndex(Collection<Company> var1);

    @PreAuthorize(value = "hasRole('ROLE_DEVELOPER')")
    void refreshSearchIndex(List<Integer> var1);

    @PreAuthorize(value = "hasRole('ROLE_DEVELOPER')")
    void refreshSearchIndexForCompanyRange(int var1, int var2);

    void suggestChanges(int var1, String var2) throws ResourceNotFoundException;

    @PreAuthorize(value = "hasRole('ROLE_DEVELOPER')")
    int[] getIdentifiersInSearchIndex(Long var1);

    @PreAuthorize(value = "hasRole('ROLE_DEVELOPER')")
    int[] getAllIdentifiersInSearchIndex();

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    Page<Company> findCompaniesWithUnapprovedKeywords(Pageable var1);

    List<ProfileUrlsByCiNumbersDTO> getProfileUrlsByCiNumbers(String var1);

    List<ProfileUrlsByCiNumbersDTO> getProfileUrlsByCiNumbers(Collection<Integer> var1);

    Page<Company> getCompaniesInIndustryWithoutSubdomain(Pageable var1, int var2);

    Page<Company> getCompaniesInIndustryWithoutSubdomain(Pageable var1, int var2, boolean var3);
}

