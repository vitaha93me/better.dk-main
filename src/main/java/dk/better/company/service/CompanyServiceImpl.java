package dk.better.company.service;

import com.amazonaws.services.cloudsearchdomain.AmazonCloudSearchDomain;
import com.amazonaws.services.cloudsearchdomain.model.*;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.github.segmentio.models.Props;
import com.github.segmentio.models.Traits;
import dk.better.account.entity.Account;
import dk.better.account.entity.AccountCompany;
import dk.better.account.service.AccountService;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.exception.ObjectNotFoundException;
import dk.better.application.exception.ResourceNotFoundException;
import dk.better.application.exception.UniqueConstraintViolationException;
import dk.better.application.model.SearchResultEntry;
import dk.better.application.model.SearchResults;
import dk.better.application.service.AnalyticsService;
import dk.better.application.service.ImageService;
import dk.better.application.service.ResourceService;
import dk.better.application.util.AuthUtils;
import dk.better.application.util.DateUtils;
import dk.better.application.util.FileUtils;
import dk.better.application.util.GeneralUtils;
import dk.better.company.converter.CompanySearchJsonConverter;
import dk.better.company.dto.ProfileUrlsByCiNumbersDTO;
import dk.better.company.entity.*;
import dk.better.company.model.CompanySearchJson;
import dk.better.company.repository.CompanyRepository;
import dk.better.company.util.CompanyUtils;
import dk.better.thread.service.ThreadService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.imageio.ImageIO;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * @author Bo Andersen
 */
@Service
public class CompanyServiceImpl implements CompanyService {
    private static Logger logger = LogManager.getLogger(CompanyService.class);
    private static final String EVENT_CHANGE_PROPOSAL = "Company Change Proposal";
    private static final String EVENT_LIKE_COMPANY = "User Liked";
    private static final int GROUP_COMPANY_OWNERS_ID = 2;
    private static final Long SEARCH_RESULTS_PER_PAGE = 10L;

    @Value("${aws.search.batch.size}")
    private int searchIndexBatchSize;

    @Value("${application.domain.name}")
    private String domainName;

    @Value("${application.http.scheme}")
    private String httpScheme;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private LikeService likeService;

    @Autowired
    private AmazonCloudSearchDomain searchService;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private AcknowledgementService acknowledgementService;

    @Autowired
    private SubdomainService subdomainService;

    @Autowired
    private ServiceService serviceService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private AnalyticsService analyticsService;

    @Autowired
    private KeywordService keywordService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ThreadService threadService;


    @Override
    public Company getReference(Object identifier) {
        return this.companyRepository.getEntityManager().getReference(Company.class, identifier);
    }

    @Override
    public Object getReference(Class type, Object identifier) {
        return this.companyRepository.getEntityManager().getReference(type, identifier);
    }

    @Override
    public Company getForProfileViewById(int companyId) {
        return this.companyRepository.findOneByIdForProfileView(companyId);
    }

    @Override
    @Transactional
    public Company getFullWithMeta(int companyId) {
        Company company = this.getForProfileViewById(companyId);

        // todo: do not initialize association like this, because Hibernate is more of a dependency
        if (company != null) {
            Hibernate.initialize(company.getMeta());
        }

        return company;
    }

    @Override
    public Company getForControlPanelViewById(int companyId) {
        return this.companyRepository.findOneByIdForControlPanel(companyId);
    }

    @Override
    public Company findOneByIdForAdminCompanyEdit(int companyId) {
        return this.companyRepository.findOneByIdForAdminCompanyEdit(companyId);
    }

    @Override
    public dk.better.company.entity.CompanyService getService(int companyId, String serviceSlug) {
        return this.companyRepository.getService(companyId, serviceSlug);
    }

    @Override
    public Company find(int id) {
        return this.companyRepository.findOne(id);
    }

    @Override
    @Transactional
    public Like addLike(int companyId, Account account) throws ObjectNotFoundException {
        Assert.notNull(account);

        if (!this.companyRepository.exists(companyId)) {
            throw new ObjectNotFoundException(String.format("No company found with ID %d", companyId));
        }

        Company company = this.companyRepository.findFullProfileById(companyId); // Fetching full profile for search index update
        return this.addLike(company, account);
    }

    @Override
    @Transactional
    public Like addLike(Company company, Account account) throws UniqueConstraintViolationException {
        Assert.notNull(company);
        Assert.notNull(account);

        int likeCount = this.likeService.countByCompanyIdAndAccountId(company.getId(), account.getId());

        if (likeCount > 0) {
            throw new UniqueConstraintViolationException("The account has already liked this company");
        }

        Like like = new Like();
        like.setAccount(account);
        like.setCompany(company);
        Like saved = this.likeService.save(like);
        this.companyRepository.incrementLikeCount(company.getId());
        company.setNumberOfLikes((company.getNumberOfLikes() + 1));
        this.refreshSearchIndex(company);

        try {
            Props props = new Props();
            Props companyProps = new Props()
                    .put("id", company.getId())
                    .put("name", company.getName())
                    .put("email", company.getEmail())
                    .put("profile_url", company.getProfileUrl())
                    .put("city", company.getCity())
                    .put("postal_code", company.getPostalCode())
                    .put("phone_number", company.getPhoneNumber())
                    .put("call_tracking_number", company.getCallTrackingNumber())
                    .put("is_claimed", company.getAccountCompanies().size() > 0)
                    .put("founding_date", (company.getFoundingDate() == null ? null : DateUtils.getFormattedDate("dd-MM-yyyy", company.getFoundingDate())));

            if (company.getBookingUrl() != null) {
                companyProps.put("affiliate", "hungry");
            }

            for (AccountCompany ac : company.getAccountCompanies()) {
                if (ac.isOwner()) {
                    Account owner = ac.getAccount();
                    companyProps.put("owner", new Props()
                        .put("first_name", owner.getFirstName())
                        .put("middle_name", owner.getMiddleName())
                        .put("last_name", owner.getLastName()));

                    break;
                }
            }

            props.put("company", companyProps);
            this.analyticsService.track(account.getUuid(), EVENT_LIKE_COMPANY, props);
        } catch (Exception e) {
            logger.error(String.format("Could not trigger '%s' event", EVENT_LIKE_COMPANY), e);
        }

        return saved;
    }

    @Override
    public SearchResults<Company> search(String what, int page) {
        Assert.notNull(what);
        what = what.toLowerCase().trim();

        //String query = what.replace(" ", " |"); // Must contain one of the "tokens", but not necessarily all
        int fuzzyNumber = this.getFuzzyNumber(what);
        String query = ((fuzzyNumber > 0) ? what + "~" + fuzzyNumber : what);

        // Execute query
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.setQueryParser(QueryParser.Simple);
        searchRequest.setQuery(query);
        searchRequest.setHighlight(this.getWhatHighlightingString());
        searchRequest.setQueryOptions("{" + this.getFieldsOptionString(this.getWhatQueryFields()) + "}");
        searchRequest.setSort("expr_rating desc");
        searchRequest.setSize(SEARCH_RESULTS_PER_PAGE);
        searchRequest.setStart(((page - 1) * SEARCH_RESULTS_PER_PAGE));

        return this.getSearchResults(this.searchService.search(searchRequest));
    }

    @Override
    public SearchResults<Company> searchByLocation(String where, int page) {
        Assert.notNull(where);
        where = where.toLowerCase().trim();

        //String query = where.replace(" ", " |"); // Must contain one of the "tokens", but not necessarily all
        int fuzzyNumber = this.getFuzzyNumber(where);
        String query = ((fuzzyNumber > 0) ? where + "~" + fuzzyNumber : where);

        // Execute query
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.setQueryParser(QueryParser.Simple);
        searchRequest.setQuery(query);
        searchRequest.setQueryOptions("{" + this.getFieldsOptionString(this.getWhereQueryFields()) + "}");
        searchRequest.setSort("expr_rating desc");
        searchRequest.setSize(SEARCH_RESULTS_PER_PAGE);
        searchRequest.setStart(((page - 1) * SEARCH_RESULTS_PER_PAGE));

        return this.getSearchResults(this.searchService.search(searchRequest));
    }

    @Override
    public SearchResults<Company> search(String what, String where, int page) {
        what = what.toLowerCase().trim();
        where = where.toLowerCase().trim();

        if (what.isEmpty() && where.isEmpty()) {
            throw new InvalidArgumentException("Both 'what' and 'where' can't be empty!");
        }

        SearchRequest searchRequest = new SearchRequest();
        Long start = ((page - 1) * SEARCH_RESULTS_PER_PAGE);
        String filterQuery = "";
        String query;

        if (!where.isEmpty() && where.matches("^([æøåÆØÅa-zA-Z0-9])*(\\s)*([æøåÆØÅa-zA-Z0-9])*$")) {
            where = where.replace("århus", "Aarhus");
            where = where.replace("ålborg", "Aalborg");

            query = what.replace(" ", " |"); // Must contain one of the "tokens", but not necessarily all
            int fuzzyNumber = this.getFuzzyNumber(what);
            query = ((fuzzyNumber > 0) ? query + "~" + fuzzyNumber : query);

            String[] parts = where.split(" ");
            int postalCode = 0;
            int i = 0;

            while (i < parts.length && postalCode == 0) {
                // Check if we have a postal code
                if (parts[i].length() == 4) {
                    try {
                        postalCode = Integer.parseInt(parts[i]);
                        parts[i] = null;
                    } catch (NumberFormatException nfe) {
                        /* Do nothing */
                    }
                }

                i++;
            }

            if (postalCode > 0) {
                filterQuery += "(and(term field=postal_code '" + postalCode + "')";

                if (parts.length > 1) { // Postal code + city/address
                    String addressOrCity = ((parts[0] != null) ? parts[0] : parts[1]); // We know that we have two strings at this point
                    filterQuery += "(or (term field=city '" + addressOrCity + "') (term field=address '" + addressOrCity + "'))";
                }

                filterQuery += ")";
            } else {
                if (parts.length > 1) { // Two strings
                    filterQuery = "(or";
                    filterQuery += "(term field=city '" + parts[0] + "')";
                    filterQuery += "(term field=city '" + parts[1] + "')";
                    filterQuery += "(term field=address '" + parts[0] + "')";
                    filterQuery += "(term field=address '" + parts[1] + "')";
                    filterQuery += ")";
                } else { // Single string
                    filterQuery = "(or";
                    filterQuery += "(term field=city '" + parts[0] + "')";
                    filterQuery += "(term field=address '" + parts[0] + "')";
                    filterQuery += ")";
                }
            }

            searchRequest.setFilterQuery(filterQuery);
        } else { // Fallback
            query = what.replace(" ", " |") + " " + where.replace(" ", " |"); // Must contain one of the "tokens", but not necessarily all
            int fuzzyNumber = this.getFuzzyNumber(what + " " + where);
            query = ((fuzzyNumber > 0) ? query + "~" + fuzzyNumber : query);
        }

        // Execute query
        searchRequest.setQueryParser(QueryParser.Simple);
        searchRequest.setQuery(query);
        searchRequest.setHighlight(this.getWhatHighlightingString());
        searchRequest.setSort("expr_rating desc");
        searchRequest.setSize(SEARCH_RESULTS_PER_PAGE);
        searchRequest.setStart(start);
        searchRequest.setReturn("_no_fields");

        Map<String, Double> whereFields = this.getWhereQueryFields();
        Map<String, Double> whatFields = this.getWhatQueryFields();
        Map<String, Double> fields = new HashMap<>((whereFields.size() + whatFields.size()));
        fields.putAll(whereFields);
        fields.putAll(whatFields);
        searchRequest.setQueryOptions("{" + this.getFieldsOptionString(fields) + "}");

        return this.getSearchResults(this.searchService.search(searchRequest));
    }

    /**
     * Gets a string ready to be embedded into the "q.options" parameter object
     *
     * @param fields Key = field name, value = weight/boost
     * @return A string ready to be embedded into the "q.options" parameter object
     */
    private String getFieldsOptionString(Map<String, Double> fields) {
        StringBuilder sb = new StringBuilder();
        sb.append("fields: [");

        for (Map.Entry<String, Double> entry : fields.entrySet()) {
            sb.append("'");
            sb.append(entry.getKey());

            if (entry.getValue() != null) {
                sb.append("^");
                sb.append(entry.getValue());
            }

            sb.append("',");
        }

        sb.deleteCharAt((sb.length() - 1)); // Remove last comma
        sb.append("]");

        return sb.toString();
    }

    /**
     * Determines the "fuzziness" of a search query
     *
     * @param query The search query to be executed
     * @return The fuzzy number
     */
    private int getFuzzyNumber(String query) {
        if (query.length() > 4) { // Applying a fuzzy search to very short searches renders inaccurate results
            return Math.round(((query.length() / 4) + 1)); // Every 4th character may differ (appearing anywhere)
        }

        return 0;
    }

    /**
     * Gets a map of the fields used in the "what" part of a search query, along with weights/boosts
     *
     * @return The map of fields
     */
    private Map<String, Double> getWhatQueryFields() {
        // K = field name, V = weight/boost
        Map<String, Double> fields = new HashMap<>();
        fields.put("company_name", 5.0);
        fields.put("teaser", null);
        fields.put("description", null);
        fields.put("ci_number", 2.0);
        fields.put("phone_number", 2.0);
        fields.put("industries", 3.0);
        fields.put("services", 3.0);

        return fields;
    }

    /**
     * Gets a map of the fields used in the "where" part of a search query, along with weights/boosts
     *
     * @return The map of fields
     */
    private Map<String, Double> getWhereQueryFields() {
        // K = field name, V = weight/boost
        Map<String, Double> fields = new HashMap<>();
        fields.put("address", 3.0);
        fields.put("postal_code", 3.0);
        fields.put("city", 3.0);
        fields.put("postal_district", 1.0);
        fields.put("municipal", 1.0);

        return fields;
    }

    /**
     * Gets the highlight JSON string for the "what" part of the search
     *
     * @return JSON highlight string
     */
    private String getWhatHighlightingString() {
        // Note: The string must not contain spaces
        String teaser = "\"teaser\":{\"max_phrases\":3,\"pre_tag\":\"<strong>\",\"post_tag\":\"</strong>\"}";
        String description = "\"description\":{\"max_phrases\":3,\"pre_tag\":\"<strong>\",\"post_tag\":\"</strong>\"}";
        return "{" + teaser + "," + description + "}";
    }

    /**
     * Fetches complete search results based on a SearchResult object
     *
     * @param searchResult The search results from the search provider
     * @return Complete search results
     */
    private SearchResults<Company> getSearchResults(SearchResult searchResult) {
        // Extract company IDs from search results
        LinkedHashMap<Integer, SearchResultEntry<Company>> entryMap = new LinkedHashMap<>(SEARCH_RESULTS_PER_PAGE.intValue());
        Map<Integer, Map<String, String>> highlights = new HashMap<>(SEARCH_RESULTS_PER_PAGE.intValue());
        int companyId;

        for (Hit hit : searchResult.getHits().getHit()) {
            companyId = Integer.parseInt(hit.getId());
            entryMap.put(companyId, null);
            highlights.put(companyId, hit.getHighlights());
        }

        // Set search results meta data
        Set<Integer> companyIds = entryMap.keySet();
        Long found = searchResult.getHits().getFound();
        Long start = searchResult.getHits().getStart();
        SearchResults<Company> results = new SearchResults<>();
        results.setNumberOfHits(found);
        results.setStart(start);
        results.setPage((Math.round(start / SEARCH_RESULTS_PER_PAGE) + 1));
        results.setTotalPages((int) Math.ceil((found / SEARCH_RESULTS_PER_PAGE)));

        // Look up companies in the database
        if (!companyIds.isEmpty()) {
            List<Company> companies = this.companyRepository.findByCompanyIdsForSearchResults(companyIds);
            SearchResultEntry<Company> entry;

            for (Company company : companies) {
                companyId = company.getId();
                entry = new SearchResultEntry<>();
                entry.setEntry(company);
                entry.setHighlights(highlights.get(companyId));
                entryMap.put(companyId, entry);
            }

            results.setHits(entryMap);
        }

        return results;
    }

    @Override
    @Transactional
    public Company save(Company company) {
        Assert.notNull(company);

        // Validate that the acknowledgements and services are linked to the industries
        Set<Acknowledgement> acknowledgements = company.getAcknowledgements();
        Collection<dk.better.company.entity.Service> services = this.extractServices(company);

        if (!acknowledgements.isEmpty()) {
            if (!this.acknowledgementService.areLinkedToIndustries(company.getAcknowledgements(), company.getIndustries())) {
                throw new InvalidArgumentException("One or more acknowledgements are not linked to the industries");
            }
        }

        if (!services.isEmpty()) {
            if (!this.serviceService.areLinkedToIndustries(services, company.getIndustries())) {
                throw new InvalidArgumentException("One or more services are not linked to the industries");
            }
        }

        /** Remove spaces in phone numbers **/
        if (company.getPhoneNumber() != null) {
            String phoneNumber = company.getPhoneNumber();
            company.setPhoneNumber(phoneNumber.replace(" ", ""));
        }

        if (company.getCallTrackingNumber() != null) {
            String callTrackingNumber = company.getCallTrackingNumber();
            company.setCallTrackingNumber(callTrackingNumber.replace(" ", ""));
        }

        Company saved = this.companyRepository.save(company);
        this.refreshSearchIndex(saved);

        return saved;
    }

    @Override
    @Transactional
    public Company saveAndFlush(Company company) {
        Assert.notNull(company);

        // Validate that the acknowledgements and services are linked to the industries
        Set<Acknowledgement> acknowledgements = company.getAcknowledgements();
        Collection<dk.better.company.entity.Service> services = this.extractServices(company);

        if (!acknowledgements.isEmpty()) {
            if (!this.acknowledgementService.areLinkedToIndustries(company.getAcknowledgements(), company.getIndustries())) {
                throw new InvalidArgumentException("One or more acknowledgements are not linked to the industries");
            }
        }

        if (!services.isEmpty()) {
            if (!this.serviceService.areLinkedToIndustries(services, company.getIndustries())) {
                throw new InvalidArgumentException("One or more services are not linked to the industries");
            }
        }

        /** Remove spaces in phone numbers **/
        if (company.getPhoneNumber() != null) {
            String phoneNumber = company.getPhoneNumber();
            company.setPhoneNumber(phoneNumber.replace(" ", ""));
        }

        if (company.getCallTrackingNumber() != null) {
            String callTrackingNumber = company.getCallTrackingNumber();
            company.setCallTrackingNumber(callTrackingNumber.replace(" ", ""));
        }

        Company saved = this.companyRepository.saveAndFlush(company);
        Collection<Company> tempCollection = new HashSet<>(1);
        tempCollection.add(saved);
        this.refreshSearchIndex(tempCollection);

        return saved;
    }

    @Override
    @Transactional
    public void add(Collection<Company> companies) {
        Assert.notEmpty(companies);

        // todo: perform validation and stuff like above?

        this.companyRepository.save(companies);
        this.companyRepository.flush();
        List<Integer> companyIdsWithEmptyMunicipal = this.companyRepository.getCompanyIdsWithEmptyMunicipal();
        this.companyRepository.initializeMissingMunicipals();

        this.refreshSearchIndex(companies);
        List<Company> companiesWithoutMunicipal = new ArrayList<>(companyIdsWithEmptyMunicipal.size());

        for (Integer i : companyIdsWithEmptyMunicipal) {
            companiesWithoutMunicipal.add(this.companyRepository.findFullProfileById(i));
        }

        this.refreshSearchIndex(companiesWithoutMunicipal);
    }

    @Override
    @Transactional
    public void updateLogo(int companyId, File original) throws RuntimeException {
        Assert.notNull(original);
        Company company = this.companyRepository.findOne(companyId);

        if (company == null) {
            throw new ResourceNotFoundException(String.format("Company with identifier %d not found!", companyId));
        }

        // Validate that the user is the owner of the company or is an administrator
        Authentication authentication = AuthUtils.getAuthentication(); // We know that the user is signed in because of @PreAuthorize
        Account principal = (Account) authentication.getPrincipal();

        if (!CompanyUtils.isCompanyOwner(principal, company) && !AuthUtils.isAdmin()) {
            throw new AccessDeniedException(String.format("User must be either the owner of company %d or an administrator", companyId));
        }

        try {
            // Prevent image colors from being strange
            BufferedImage originalBufferedImage = ImageIO.read(original);

            if (originalBufferedImage.getColorModel().hasAlpha()) {
                originalBufferedImage = this.imageService.removeAlphaChannel(originalBufferedImage);
            }

            // Resize original image
            int width = 145;
            int height = 90;
            BufferedImage resized = this.imageService.resize(originalBufferedImage, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, width, height);

            Path tempPath = FileUtils.getTemporaryDirectoryPath(companyId + "_");
            Path resizedFilePath = Paths.get(tempPath.toString() + "/resized." + CompanyService.IMAGES_EXTENSION);
            File resizedFile = new File(resizedFilePath.toString());
            ImageIO.write(resized, CompanyService.IMAGES_EXTENSION, resizedFile);
            String mimeType = URLConnection.guessContentTypeFromName(resizedFilePath.toString());

            // Convert to JPEG if necessary
            if (!"image/jpeg".equals(mimeType)) {
                resizedFile = this.imageService.convertToJpeg(resizedFile);
            }

            // Compress resized image
            resizedFile = this.imageService.compressJpeg(resizedFile, 0.8f);

            // Update company in the database
            String logoName = "companies/logos/" + width + "x" + height + "/" + companyId + "." + CompanyService.IMAGES_EXTENSION;
            company.setLogoName(logoName);
            company.setLastModified(new Date());
            this.save(company);

            // Upload banner & original banner to storage
            String originalLogoName = "companies/logos/originals/" + companyId + "." + CompanyService.IMAGES_EXTENSION;
            this.resourceService.putObject(logoName, resizedFile);
            this.resourceService.putObject(originalLogoName, original);

            // Trigger event
            Account account = (Account) AuthUtils.getAuthentication().getPrincipal();
            Props props = new Props();
            props.put("company", new Props()
                    .put("id", company.getId())
                    .put("name", company.getName())
                    .put("profile_url", company.getProfileUrl())
                    .put("email", company.getEmail())
                    .put("phone_number", company.getPhoneNumber())
                    .put("call_tracking_number", company.getCallTrackingNumber())
                    .put("postal_code", company.getPostalCode())
                    .put("city", company.getCity())
                    .put("industries", CompanyUtils.getIndustryNames(company.getIndustries())));

            this.analyticsService.track(account.getUuid(), "Uploaded Company Logo", props);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    @Transactional
    public void updateCoverPhoto(int companyId, File original) throws RuntimeException {
        Assert.notNull(original);
        Company company = this.companyRepository.findOne(companyId);

        if (company == null) {
            throw new ResourceNotFoundException(String.format("Company with identifier %d not found!", companyId));
        }

        // Validate that the user is the owner of the company or is an administrator
        Authentication authentication = AuthUtils.getAuthentication(); // We know that the user is signed in because of @PreAuthorize
        Account principal = (Account) authentication.getPrincipal();

        if (!CompanyUtils.isCompanyOwner(principal, company) && !AuthUtils.isAdmin()) {
            throw new AccessDeniedException(String.format("User must be either the owner of company %d or an administrator", companyId));
        }

        try {
            // Prevent image colors from being strange
            BufferedImage originalBufferedImage = ImageIO.read(original);

            if (originalBufferedImage.getColorModel().hasAlpha()) {
                originalBufferedImage = this.imageService.removeAlphaChannel(originalBufferedImage);
            }

            // Resize original image
            int width = 1440;
            int height = 450;
            BufferedImage resized = this.imageService.resize(originalBufferedImage, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, width, height);

            Path tempPath = FileUtils.getTemporaryDirectoryPath(companyId + "_");
            Path resizedFilePath = Paths.get(tempPath.toString() + "/resized." + CompanyService.IMAGES_EXTENSION);
            File resizedFile = new File(resizedFilePath.toString());
            ImageIO.write(resized, CompanyService.IMAGES_EXTENSION, resizedFile);
            String mimeType = URLConnection.guessContentTypeFromName(resizedFilePath.toString());

            // Convert to JPEG if necessary
            if (!"image/jpeg".equals(mimeType)) {
                resizedFile = this.imageService.convertToJpeg(resizedFile);
            }

            // Compress resized image
            resizedFile = this.imageService.compressJpeg(resizedFile, 0.8f);

            // Update company in the database
            String bannerName = "companies/covers/" + width + "x" + height + "/" + companyId + "." + CompanyService.IMAGES_EXTENSION;
            company.setBannerName(bannerName);
            company.setLastModified(new Date());
            this.save(company);

            // Upload banner & original banner to storage
            String originalBannerName = "companies/covers/originals/" + companyId + "." + CompanyService.IMAGES_EXTENSION;
            this.resourceService.putObject(bannerName, resizedFile);
            this.resourceService.putObject(originalBannerName, original);

            // Trigger event
            Account account = (Account) AuthUtils.getAuthentication().getPrincipal();
            Props props = new Props();
            props.put("company", new Props()
                    .put("id", company.getId())
                    .put("name", company.getName())
                    .put("profile_url", company.getProfileUrl())
                    .put("email", company.getEmail())
                    .put("phone_number", company.getPhoneNumber())
                    .put("call_tracking_number", company.getCallTrackingNumber())
                    .put("postal_code", company.getPostalCode())
                    .put("city", company.getCity())
                    .put("industries", CompanyUtils.getIndustryNames(company.getIndustries())));

            this.analyticsService.track(account.getUuid(), "Uploaded Company Cover Photo", props);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    @Transactional
    public Page<Company> getCompaniesAddedBetween(Pageable pageable, Date from, Date to) {
        return this.companyRepository.getCompaniesAddedBetween(pageable, from, to);
    }

    @Override
    @Transactional
    public Page<Company> getCompaniesAddedBetweenInIndustries(Pageable pageable, Date from, Date to, Integer[] industryIds) {
        return this.companyRepository.getCompaniesAddedBetweenInIndustries(pageable, from, to, industryIds);
    }

    @Override
    public List<Company> getCompaniesClaimedBetween(Date from, Date to) {
        return this.companyRepository.getCompaniesClaimedBetween(from, to);
    }

    @Override
    public List<Company> getCompaniesClaimedBetweenInIndustries(Date from, Date to, Integer[] industryIds) {
        return this.companyRepository.getCompaniesClaimedBetweenInIndustries(from, to, industryIds);
    }

    @Override
    @Transactional
    public void refreshSearchIndexForCompanyRange(int firstId, int lastId) {
        Assert.isTrue(firstId > 0);
        Assert.isTrue(lastId > 0);
        Assert.isTrue(lastId >= firstId);

        if (Math.abs((firstId - lastId)) > searchIndexBatchSize) {
            throw new InvalidArgumentException(String.format("Cannot refresh more than %d companies at a time!", searchIndexBatchSize));
        }

        List<Company> companies = this.companyRepository.findFullProfileWithIdsBetween(firstId, lastId);
        this.refreshSearchIndex(companies);
    }

    @Override
    @Transactional
    public void refreshSearchIndex(List<Integer> companyIds) {
        Assert.notNull(companyIds);
        Assert.notEmpty(companyIds);
        this.refreshSearchIndex(this.companyRepository.findProfilesByIds(companyIds));
    }

    @Override
    public void refreshSearchIndex(Company company) {
        Collection<Company> collection = new HashSet<>(1);
        collection.add(company);
        this.refreshSearchIndex(collection);
    }

    @Override
    @Transactional
    public void refreshSearchIndex(Collection<Company> companies) {
        Assert.notNull(companies);

        if (companies.isEmpty()) {
            return;
        }

        // Convert to "JSON objects"
        CompanySearchJsonConverter converter = new CompanySearchJsonConverter();
        List<CompanySearchJson> jsonList = new ArrayList<>(companies.size());

        for (Company c : companies) {
            jsonList.add(converter.convert(c));
        }

        // Convert objects to JSON
        String json = "[";
        ObjectMapper mapper = new ObjectMapper();
        int jsonListSize = jsonList.size();

        // Wrap in a try-catch block to throw an unchecked (runtime) exception so users are not forced to catch exceptions
        try {
            for (int i = 0; i < jsonListSize; i++) {
                json += mapper.writeValueAsString(jsonList.get(i)) + ",";

                // Upload batch if batch size or end of list is reached
                if (i == (jsonListSize - 1) || (i >= searchIndexBatchSize && ((i % searchIndexBatchSize) == 0))) {
                    json = json.substring(0, (json.length() - 1)); // Remove last comma
                    json += "]";
                    this.uploadDocumentsToSearchIndex(json);
                    json = "["; // Restart JSON
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    // improve: document
    private void uploadDocumentsToSearchIndex(String json) {
        byte[] bytes = json.getBytes(Charset.forName("UTF-8"));
        InputStream stream = new ByteArrayInputStream(bytes);
        UploadDocumentsRequest request = new UploadDocumentsRequest();
        request.setDocuments(stream);
        request.setContentType(ContentType.Applicationjson);
        request.setContentLength((long) bytes.length);

        this.searchService.uploadDocuments(request);
    }

    @Override
    public int[] getIdentifiersInSearchIndex(Long limit) {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.setQueryParser(QueryParser.Structured);
        searchRequest.setQuery("matchall");
        searchRequest.setReturn("_no_fields");
        searchRequest.setSize(limit);
        SearchResult result = this.searchService.search(searchRequest);
        Long found = result.getHits().getFound();
        int[] results = new int[found.intValue()];
        int resultsIndex = 0;

        for (Hit hit : result.getHits().getHit()) {
            results[resultsIndex++] = Integer.parseInt(hit.getId());
        }

        return results;
    }

    @Override
    public int[] getAllIdentifiersInSearchIndex() {
        Long pageSize = 10000L;
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.setQueryParser(QueryParser.Structured);
        searchRequest.setQuery("matchall");
        searchRequest.setReturn("_no_fields");
        searchRequest.setCursor("initial");
        searchRequest.setSize(pageSize);
        SearchResult result = this.searchService.search(searchRequest);
        Long found = result.getHits().getFound();
        int[] results = new int[found.intValue()];
        int resultsIndex = 0;

        for (Hit hit : result.getHits().getHit()) {
            results[resultsIndex++] = Integer.parseInt(hit.getId());
        }

        if (found > pageSize) {
            String cursor = result.getHits().getCursor();
            Long remaining = (found - pageSize);

            while (remaining > 0) {
                searchRequest.setCursor(cursor);
                result = this.searchService.search(searchRequest);
                cursor = result.getHits().getCursor();
                List<Hit> hits = result.getHits().getHit();
                remaining -= hits.size();

                for (Hit hit : hits) {
                    results[resultsIndex++] = Integer.parseInt(hit.getId());
                }
            }
        }

        return results;
    }

    @Override
    @Transactional
    public Company adminSaveAndFlush(Company company) {
        boolean hasNewProfileUrl = false;

        if (company.getSubdomains().size() > 0) {
            this.deactivateCompanySubdomains(company.getId());
            hasNewProfileUrl = true;
        }

        Company saved = this.saveAndFlush(company);
        this.refreshSearchIndex(saved);

        if (hasNewProfileUrl) {
            this.updateProfileUrl(company.getId(), company.getSubdomains().iterator().next().getName(), this.httpScheme, this.domainName);
        }

        return saved;
    }

    @Override
    public void deactivateCompanySubdomains(int companyId) {
        this.subdomainService.deactivateCompanySubdomains(companyId);
    }

    // improve: document
    private Collection<dk.better.company.entity.Service> extractServices(Company company) {
        Set<dk.better.company.entity.CompanyService> companyServices = company.getCompanyServices();
        Collection<dk.better.company.entity.Service> services = new HashSet<>(companyServices.size());

        for (dk.better.company.entity.CompanyService cs : companyServices) {
            services.add(cs.getService());
        }

        return services;
    }

    // improve: distinguish between different types of exceptions so that the "user" can know what went wrong
    @Override
    @Transactional
    public void claim(int companyId, Account account) throws RuntimeException {
        Assert.notNull(account);

        if (this.isOwnerOfCompany(companyId, account)) {
            throw new RuntimeException("This account has already claimed this company!");
        }

        // Verify that the account does not have too many companies
        int count = 0;

        for (AccountCompany ac : account.getAccountCompanies()) {
            if (ac.isOwner()) {
                count++;
            }
        }

        if (count > 4) { // Maximum 5 allowed
            throw new RuntimeException("This account is not allowed to claim more companies!");
        }

        // Verify that company exists
        Company company = this.companyRepository.findOne(companyId);

        if (company == null) {
            throw new RuntimeException(String.format("Cannot claim company (ID: %d) because it does not exist!", companyId));
        }

        // Verify that company has not already been claimed
        if (this.isCompanyClaimed(companyId)) {
            throw new RuntimeException("This company has already been claimed!");
        }

        AccountCompany ac = new AccountCompany();
        ac.setCompany(company);
        ac.setOwner(true);
        ac.setKeenScopedKey(this.analyticsService.generateKeenScopedKey(companyId));
        account.add(ac);

        this.accountService.saveAndFlush(account);
        this.refreshSearchIndex(company);

        // Trigger events
        Props props = new Props();
        props.put("company", new Props()
                .put("id", companyId)
                .put("name", company.getName())
                .put("city", company.getCity())
                .put("postal_code", company.getPostalCode())
                .put("profile_url", company.getProfileUrl())
                .put("email", company.getEmail())
                .put("phone_number", company.getPhoneNumber())
                .put("call_tracking_number", company.getCallTrackingNumber())
                .put("accessToken", company.getAccessToken())
                .put("industries", CompanyUtils.getIndustryNames(company.getIndustries()))
                .put("founding_date", (company.getFoundingDate() == null ? null : DateUtils.getFormattedDate("dd-MM-yyyy", company.getFoundingDate()))));

        this.analyticsService.track(account.getUuid(), "Claimed Company", props);

        // Add user to company owners group
        Traits traits = new Traits();
        traits.put("name", "Company Owners");
        this.analyticsService.group(account.getUuid(), String.valueOf(GROUP_COMPANY_OWNERS_ID), traits);

        // Send welcome message
        this.threadService.sendWelcomeMessageToCompany(companyId, AccountService.BETTER_USER_ID);
    }

    @Override
    public boolean isOwnerOfCompany(int companyId, Account account) {
        for (AccountCompany ac : account.getAccountCompanies()) {
            if (ac.isOwner() && ac.getCompany().getId() == companyId) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean isCompanyClaimed(int companyId) {
        return this.companyRepository.isCompanyClaimed(companyId);
    }

    @Override
    @Transactional
    public void updateWebsite(int companyId, String website) {
        // Validate that the user is the owner of the company or is an administrator
        Account account = (Account) AuthUtils.getAuthentication().getPrincipal();

        if (!CompanyUtils.isCompanyOwner(account, companyId) && !AuthUtils.isAdmin()) {
            throw new AccessDeniedException(String.format("User must be either the owner of company (ID: %d) or an administrator", companyId));
        }

        this.companyRepository.updateWebsite(companyId, website);
        this.refreshSearchIndex(this.companyRepository.findOne(companyId));
    }

    @Override
    @Transactional
    public void updatePhoneNumber(int companyId, String phoneNumber) {
        // Validate that the user is the owner of the company or is an administrator
        Account account = (Account) AuthUtils.getAuthentication().getPrincipal();

        if (!CompanyUtils.isCompanyOwner(account, companyId) && !AuthUtils.isAdmin()) {
            throw new AccessDeniedException(String.format("User must be either the owner of company (ID: %d) or an administrator", companyId));
        }

        this.companyRepository.updatePhoneNumber(companyId, phoneNumber);
        this.refreshSearchIndex(this.companyRepository.findOne(companyId));
    }

    @Override
    @Transactional
    public void updateEmail(int companyId, String email) {
        // Validate that the user is the owner of the company or is an administrator
        Account account = (Account) AuthUtils.getAuthentication().getPrincipal();

        if (!CompanyUtils.isCompanyOwner(account, companyId) && !AuthUtils.isAdmin()) {
            throw new AccessDeniedException(String.format("User must be either the owner of company (ID: %d) or an administrator", companyId));
        }

        this.companyRepository.updateEmail(companyId, email);
    }

    @Override
    @Transactional
    public void updateTeaser(int companyId, String teaser) {
        // Validate that the user is the owner of the company or is an administrator
        Account account = (Account) AuthUtils.getAuthentication().getPrincipal();

        if (!CompanyUtils.isCompanyOwner(account, companyId) && !AuthUtils.isAdmin()) {
            throw new AccessDeniedException(String.format("User must be either the owner of company (ID: %d) or an administrator", companyId));
        }

        this.companyRepository.updateTeaser(companyId, teaser);
        this.refreshSearchIndex(this.companyRepository.findOne(companyId));
    }

    @Override
    public void suggestChanges(int companyId, String changes) {
        Company company = this.companyRepository.findOne(companyId);

        if (company == null) {
            throw new ResourceNotFoundException(String.format("Company not found (ID: %d)!", companyId));
        }

        Props props = new Props();
        props.put("changes", changes)
            .put("company", new Props()
            .put("id", companyId)
            .put("name", company.getName())
            .put("profile_url", company.getProfileUrl()));

        String uuid;

        if (AuthUtils.isAuthenticated()) {
            uuid = AuthUtils.getPrincipal().getUuid();
        } else {
            uuid = AnalyticsService.ANONYMOUS_USER_ID;
        }

        this.analyticsService.track(uuid, EVENT_CHANGE_PROPOSAL, props);
    }

    @Override
    @Transactional
    public void incrementReviewCount(int companyId) {
        this.companyRepository.incrementReviewCount(companyId);
    }

    @Override
    @Transactional
    public void updateReviewRating(int companyId) {
        this.companyRepository.updateReviewRating(companyId);
    }

    @Override
    @Transactional
    public void initializeProfileUrls(String httpScheme, String domainName) {
        this.companyRepository.initializeProfileUrls(httpScheme, domainName);
    }

    @Override
    @Transactional
    public void updateProfileUrl(int companyId, String subdomain, String httpScheme, String domainName) {
        this.companyRepository.updateProfileUrl(companyId, subdomain, httpScheme, domainName);
    }

    @Override
    @Transactional
    public void addIndustries(int companyId, int[] industryIds) {
        Assert.notNull(companyId);
        Assert.isTrue(industryIds.length > 0);
        Collection<Industry> industries = new HashSet<>(industryIds.length);

        for (int industryId : industryIds) {
            industries.add(this.companyRepository.getEntityManager().getReference(Industry.class, industryId));
        }

        this.addIndustries(companyId, industries);
    }

    @Override
    @Transactional
    public void addIndustries(int companyId, Collection<Industry> industries) {
        Company company = this.companyRepository.findOneWithIndustries(companyId);

        if (company == null) {
            throw new InvalidArgumentException(String.format("A company with ID: %d does not exist!", companyId));
        }

        for (Industry industry : industries) {
            company.addIndustry(industry);
        }

        // Validate industries
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Company>> errors = validator.validateProperty(company, "industries");

        if (!errors.isEmpty()) {
            ConstraintViolation<Company> violation = errors.iterator().next();
            throw new InvalidArgumentException(violation.getPropertyPath().toString() + " " + violation.getMessage());
        }

        this.save(company);
        this.refreshSearchIndex(company);
    }

    @Override
    public Page<Company> findCompaniesWithUnapprovedKeywords(Pageable pageable) {
        Page<Company> page = this.companyRepository.findCompaniesWithUnapprovedKeywords(pageable);

        if (page.hasContent()) {
            for (Company company : page.getContent()) {
                company.setCompanyKeywords(this.keywordService.getCompanyKeywords(company.getId()));
            }
        }

        return page;
    }

    @Override
    @Transactional
    public List<ProfileUrlsByCiNumbersDTO> getProfileUrlsByCiNumbers(String csv) {
        try {
            CsvSchema schema = CsvSchema.emptySchema().withHeader().withColumnSeparator(';');
            CsvMapper mapper = new CsvMapper();
            MappingIterator<Map<String, String>> mappingIterator = mapper.reader(Map.class).with(schema).readValues(csv);
            List<Map<String, String>> rows = mappingIterator.readAll();
            Collection<Integer> ciNumbers = new HashSet<>(rows.size());

            for (Map<String, String> row : rows) {
                ciNumbers.add(Integer.parseInt(row.get("cinumber")));
            }

            return this.getProfileUrlsByCiNumbers(ciNumbers);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    @Transactional
    public List<ProfileUrlsByCiNumbersDTO> getProfileUrlsByCiNumbers(Collection<Integer> ciNumbers) {
        return this.companyRepository.getProfileUrlsByCiNumbers(ciNumbers);
    }

    @Override
    public Page<Company> getCompaniesInIndustryWithoutSubdomain(Pageable pageable, int industryId) {
        return this.getCompaniesInIndustryWithoutSubdomain(pageable, industryId, true);
    }

    @Override
    public Page<Company> getCompaniesInIndustryWithoutSubdomain(Pageable pageable, int industryId, boolean showCompaniesWithoutBookingUrl) {
        if (showCompaniesWithoutBookingUrl) {
            return this.companyRepository.getCompaniesInIndustryWithoutSubdomain(pageable, industryId);
        } else {
            return this.companyRepository.getCompaniesInIndustryWithBookingUrlAndWithoutSubdomain(pageable, industryId);
        }
    }

    @Override
    public Company findByCallTrackingNumber(String callTrackingNumber) {
        return this.companyRepository.findByCallTrackingNumber(callTrackingNumber);
    }

    @Override
    @Transactional
    public Customer addCustomer(int companyId, String phoneNumber, CustomerService.Type type) {
        Customer customer = new Customer();
        customer.setUuid(GeneralUtils.getRandomUUID());
        customer.setType(type.getValue());
        customer.setPhoneNumber(phoneNumber);
        customer.setCompany((Company) this.getReference(Company.class, companyId));

        return this.customerService.save(customer);
    }

    @Override
    public boolean hasAccessToCompany(int companyId) {
        return this.hasAccessToCompany(companyId, null, null);
    }

    @Override
    public boolean hasAccessToCompany(int companyId, Long checksum, String accessToken) {
        boolean isOwnerOfCompany = (AuthUtils.isAuthenticated() && CompanyUtils.isCompanyOwner(AuthUtils.getPrincipal(), companyId));

        if (isOwnerOfCompany || AuthUtils.isAdmin()) {
            return true;
        }

        if (checksum != null && accessToken != null && !accessToken.isEmpty()) {
            // Generate a checksum and verify that the provided checksum matches
            long targetChecksum = CompanyService.generateChecksum(companyId);
            return (targetChecksum == checksum) && this.companyRepository.isValidAccessToken(companyId, accessToken);
        }

        return false;
    }

    @Override
    public List<Company> findByVatNumberAndPostalCode(long vatNumber, int postalCode) {
        return this.companyRepository.findByCiNumberAndPostalCode(vatNumber, postalCode);
    }

    @Override
    public List<Company> findByVatNumber(long vatNumber) {
        return this.companyRepository.findByCiNumber(vatNumber);
    }

    @Override
    public Company findByAccessToken(String accessToken) {
        Assert.notNull((Object) accessToken);
        return this.companyRepository.findByAccessToken(accessToken);
    }
}