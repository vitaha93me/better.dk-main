package dk.better.company.service;

import com.github.segmentio.models.Props;
import dk.better.account.entity.Account;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.exception.ResourceNotFoundException;
import dk.better.application.service.AnalyticsService;
import dk.better.application.util.AuthUtils;
import dk.better.application.util.GeneralUtils;
import dk.better.company.entity.Company;
import dk.better.company.entity.Customer;
import dk.better.company.repository.CustomerRepository;
import dk.better.company.util.CompanyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Collection;
import java.util.Set;

/**
 * @author Bo Andersen
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private AnalyticsService analyticsService;


    @Override
    @Transactional
    public Customer save(Customer customer) {
        return this.customerRepository.save(customer);
    }

    @Override
    public boolean exists(int companyId, String phoneNumber) {
        Assert.notNull(phoneNumber);
        return this.customerRepository.exists(companyId, phoneNumber);
    }

    @Override
    public Page<Customer> find(int companyId, Pageable pageable) {
        return this.customerRepository.findByCompanyId(companyId, pageable);
    }

    @Override
    public Customer find(int companyId, String phoneNumber) {
        Assert.notNull(phoneNumber);
        return this.customerRepository.find(companyId, phoneNumber);
    }

    @Override
    public Customer findByEmail(int companyId, String email) {
        Assert.notNull(email);
        return this.customerRepository.findByEmail(companyId, email);
    }

    @Override
    public Customer findByEmailOrPhoneNumber(int companyId, String email, String phoneNumber) {
        if (GeneralUtils.isNullOrEmpty(email) && GeneralUtils.isNullOrEmpty(phoneNumber)) {
            throw new InvalidArgumentException("Either an e-mail address or phone number must be supplied!");
        }

        if ("".equals(email)) {
            email = null;
        }

        if ("".equals(phoneNumber)) {
            phoneNumber = null;
        }

        return this.customerRepository.findByEmailOrPhoneNumber(companyId, email, phoneNumber);
    }

    @Override
    @Transactional
    public int importEmails(int companyId, Collection<String> emails, Type type) {
        Assert.notNull(emails);

        if (emails.size() > 1000) {
            throw new RuntimeException("Maximum 1.000 e-mails may be added at a time!");
        }

        Account account = AuthUtils.getPrincipal();
        Company company = this.companyService.find(companyId);

        if (company == null) {
            throw new ResourceNotFoundException(String.format("Company not found (ID: %d)", companyId));
        }

        if (!CompanyUtils.isCompanyOwner(account, companyId) && !AuthUtils.isAdmin()) {
            throw new AccessDeniedException(String.format("User (ID: %d) is not allowed to import e-mail addresses (company ID: %d)!", account.getId(), companyId));
        }

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        int importedCount = 0;

        for (String email : emails) {
            Set<ConstraintViolation<Customer>> errors = validator.validateValue(Customer.class, "email", email);

            if (!errors.isEmpty()) {
                continue; // Ignore invalid e-mail
            }

            importedCount += this.customerRepository.addByEmail(companyId, email, GeneralUtils.getRandomUUID(), type.getValue());
        }

        // Trigger event
        Props props = new Props();
        props.put("count", importedCount);
        props.put("company", new Props()
                .put("id", companyId)
                .put("name", company.getName())
                .put("city", company.getCity())
                .put("postalCode", company.getPostalCode())
                .put("profileUrl", company.getProfileUrl())
                .put("email", company.getEmail())
                .put("phoneNumber", company.getPhoneNumber())
                .put("callTrackingNumber", company.getCallTrackingNumber())
                .put("industries", CompanyUtils.getIndustryNames(company.getIndustries())));

        this.analyticsService.track(account.getUuid(), CustomerService.EVENT_IMPORTED_EMAILS, props);
        return importedCount;
    }
}