package dk.better.company.service;

import dk.better.company.entity.CompanyKeyword;
import dk.better.company.entity.Keyword;

import java.util.Set;

/**
 * @author Bo Andersen
 */
public interface KeywordService {
    boolean existsByName(String keyword);
    Keyword findByName(String keyword);
    boolean hasCompanyKeyword(int companyId, int keywordId);
    void addKeywordsToCompany(int companyId, Set<String> keywords);
    Set<CompanyKeyword> getCompanyKeywords(int companyId);
}