package dk.better.company.service;

import dk.better.company.entity.ExternalReview;
import dk.better.company.repository.ExternalReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author vitalii
 */
@Service
public class ExternalReviewService {

    @Autowired
    private ExternalReviewRepository externalReviewRepository;

    public List<ExternalReview> findByCompanyId(int companyId) {
        return externalReviewRepository.findByCompanyId(companyId);
    }
}