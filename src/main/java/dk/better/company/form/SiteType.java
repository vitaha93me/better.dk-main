package dk.better.company.form;

public enum SiteType {
    USER_DEFINED, PREDEFINED
}
