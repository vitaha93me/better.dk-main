package dk.better.account.util;

import dk.better.application.util.GeneralUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Bo Andersen
 */
public final class AccountUtils {
    @Value("${security.checksum.salt}")
    private static String salt;

    public static final String FIRST_NAME = "first_name";
    public static final String MIDDLE_NAME = "middle_name";
    public static final String LAST_NAME = "last_name";

    private AccountUtils() { }

    // improve: document
    public static String getFullName(String firstName, String middleName, String lastName) {
        String fullName = firstName;

        if (middleName != null && !middleName.isEmpty()) {
            fullName += " " + middleName;
        }

        fullName += " " + lastName;
        return fullName.trim();
    }

    // improve: document
    public static Map<String, String> getNameParts(String name) {
        Assert.notNull(name);
        Map<String, String> map = new HashMap<>(3);

        String[] parts = name.trim().split(" ");
        map.put(AccountUtils.FIRST_NAME, parts[0]);
        String middleName = "";
        String lastName = null;

        if (parts.length > 2) {
            for (int i = 1; i < (parts.length - 1); i++) {
                middleName += parts[i] + " ";
            }

            middleName = middleName.trim();
            lastName = parts[(parts.length - 1)];
        } else if (parts.length == 2) {
            lastName = parts[1];
        }

        map.put(AccountUtils.MIDDLE_NAME, (middleName.isEmpty() ? null : middleName));
        map.put(AccountUtils.LAST_NAME, lastName);

        return map;
    }

    public static long generateChecksum(int accountId) {
        return GeneralUtils.crc32(accountId + AccountUtils.salt);
    }
}