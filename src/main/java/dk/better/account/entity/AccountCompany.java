package dk.better.account.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.account.entity.identity.AccountCompanyPK;
import dk.better.company.entity.Company;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "account_company")
@IdClass(AccountCompanyPK.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountCompany implements Serializable {
    @Id
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Account.class)
    @JoinColumn(name = "account_id")
    private Account account;

    @Id
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Company.class)
    @JoinColumn(name = "company_id")
    private Company company;

    @Column(name = "is_owner", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean isOwner;

    @Column(name = "keen_scoped_key", nullable = false)
    private String keenScopedKey;

    @Column(nullable = false)
    private Date created = new Date();


    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public boolean isOwner() {
        return isOwner;
    }

    public void setOwner(boolean isOwner) {
        this.isOwner = isOwner;
    }

    public String getKeenScopedKey() {
        return keenScopedKey;
    }

    public void setKeenScopedKey(String keenScopedKey) {
        this.keenScopedKey = keenScopedKey;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}