package dk.better.account.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dk.better.company.entity.Company;
import dk.better.company.entity.Like;
import dk.better.company.entity.Vote;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Bo Andersen
 */
@Entity
@Table(name = "account")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Account implements Serializable, UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(nullable = false, length = 36, unique = true)
    private String uuid;

    @Email
    @Column(length = 100, nullable = true)
    private String email;

    @Column(length = 60, nullable = true)
    private String password;

    @Column(name = "facebook_profile_id", nullable = true)
    private Long facebookProfileId;

    @Length(min = 2, max = 50)
    @Column(name = "first_name", length = 50, nullable = true)
    private String firstName;

    @Column(name = "middle_name", length = 50, nullable = true)
    private String middleName;

    @Column(name = "last_name", length = 50, nullable = true)
    private String lastName;

    @Column(nullable = false)
    private Date created = new Date();

    @Column(name = "is_enabled", nullable = false)
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean isEnabled;

    @Column(name = "access_token", nullable = true, length = 36)
    private String accessToken;

    @Column(name = "phone_number", nullable = true, length = 15)
    private String phoneNumber;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = Vote.class, mappedBy = "account")
    private Set<Vote> votes = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, targetEntity = Like.class, mappedBy = "account")
    private Set<Like> likes = new HashSet<>();

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    @JoinTable(name = "account_role", joinColumns = @JoinColumn(name = "account_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @Size(max = 5)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account", cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    private Set<AccountCompany> accountCompanies = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "account_favorite", joinColumns = @JoinColumn(name = "account_id"), inverseJoinColumns = @JoinColumn(name = "company_id"))
    private Set<Company> favorites = new HashSet<>();


    public void add(AccountCompany accountCompany) {
        accountCompany.setAccount(this);
        this.accountCompanies.add(accountCompany);
    }

    public void add(Role role) {
        this.roles.add(role);
    }

    public void addFavorite(Company favorite) {
        this.favorites.add(favorite);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Vote> getVotes() {
        return votes;
    }

    public void setVotes(Set<Vote> votes) {
        this.votes = votes;
    }

    public Set<Like> getLikes() {
        return likes;
    }

    public void setLikes(Set<Like> likes) {
        this.likes = likes;
    }

    public Set<Role> getRoles() {
        return this.roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Set<AccountCompany> getAccountCompanies() {
        return accountCompanies;
    }

    public void setAccountCompanies(Set<AccountCompany> accountCompanies) {
        this.accountCompanies = accountCompanies;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getFacebookProfileId() {
        return facebookProfileId;
    }

    public void setFacebookProfileId(Long facebookProfileId) {
        this.facebookProfileId = facebookProfileId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public Set<Company> getFavorites() {
        return favorites;
    }

    public void setFavorites(Set<Company> favorites) {
        this.favorites = favorites;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.getRoles();
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}