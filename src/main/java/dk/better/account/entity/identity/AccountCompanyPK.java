package dk.better.account.entity.identity;

import dk.better.account.entity.Account;
import dk.better.company.entity.Company;

import java.io.Serializable;

/**
 * @author Bo Andersen
 */
public class AccountCompanyPK implements Serializable {
    private Account account;
    private Company company;

    public AccountCompanyPK() { }

    public AccountCompanyPK(Account account, Company company) {
        this.account = account;
        this.company = company;
    }


    @Override
    public int hashCode() {
        int result = ((this.account != null) ? this.account.hashCode() : 0);
        result = 31 * result + (this.company != null ? this.company.hashCode() : 0);

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (obj == null || !(obj instanceof AccountCompanyPK)) {
            return false;
        }

        AccountCompanyPK accountCompanyPK = (AccountCompanyPK) obj;

        return ((accountCompanyPK.getAccount().getId() == this.account.getId())
                && accountCompanyPK.getCompany().getId() == this.company.getId());
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}