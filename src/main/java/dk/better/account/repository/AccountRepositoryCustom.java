package dk.better.account.repository;

import dk.better.application.repository.EntityManagerAware;

/**
 * @author Bo Andersen
 */
public interface AccountRepositoryCustom extends EntityManagerAware { }