package dk.better.account.repository;

import dk.better.account.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Bo Andersen
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Integer>, AccountRepositoryCustom {
    @Query("select a from Account a left join fetch a.accountCompanies ac left join fetch ac.company left join fetch a.roles where a.id = :accountId")
    Account findByIdForSignIn(@Param("accountId") int id);

    @Query("select a from Account a left join fetch a.accountCompanies ac left join fetch ac.company left join fetch a.roles where a.email = :email")
    Account findByEmailForSignIn(@Param("email") String email);

    Account findByEmail(String email);
    Account findByAccessToken(String accessToken);
}