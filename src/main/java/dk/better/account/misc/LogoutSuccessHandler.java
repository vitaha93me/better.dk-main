package dk.better.account.misc;

import com.github.segmentio.AnalyticsClient;
import dk.better.account.entity.Account;
import dk.better.application.service.AnalyticsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Bo Andersen
 */
@Component
public class LogoutSuccessHandler implements org.springframework.security.web.authentication.logout.LogoutSuccessHandler {
    private static Logger logger = LogManager.getLogger(LogoutSuccessHandler.class);
    private static final String EVENT_LOGGED_OUT = "Logged Out";

    @Autowired
    private AnalyticsClient analyticsClient; // Here we use the client directly to avoid problems with accessing the request object within AnalyticsService

    @Value("${application.domain.name}")
    private String domainName;


    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        // Delete unread messages count cookie
        try {
            Cookie cookie = new Cookie("unread-messages-count", null);
            cookie.setDomain("." + this.domainName);
            cookie.setPath("/");
            cookie.setMaxAge(0);
            response.addCookie(cookie);
        } catch (Exception e) {
            logger.error("Error deleting unread messages count cookie", e);
        }

        // Trigger logout event
        try {
            if (authentication != null) {
                Account account = (Account) authentication.getPrincipal();
                this.analyticsClient.track(account.getUuid(), EVENT_LOGGED_OUT);
            }
        } catch (Exception e) {
            logger.error("Error triggering logout event", e);
        }

        response.sendRedirect("/goodbye");
    }
}