package dk.better.account.service;

import com.github.segmentio.models.Props;
import com.github.segmentio.models.Traits;
import dk.better.account.entity.Account;
import dk.better.account.entity.ActivationRequest;
import dk.better.account.entity.Role;
import dk.better.account.repository.AccountRepository;
import dk.better.account.repository.ActivationRequestRepository;
import dk.better.account.service.exception.AccountAlreadyExistsException;
import dk.better.account.util.AccountUtils;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.service.AnalyticsService;
import dk.better.application.util.AuthUtils;
import dk.better.application.util.DateUtils;
import dk.better.application.util.GeneralUtils;
import dk.better.company.entity.Company;
import dk.better.company.service.CompanyService;
import dk.better.company.service.ReviewService;
import dk.better.company.util.CompanyUtils;
import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.*;

/**
 * @author Bo Andersen
 */
@Service
public class AccountServiceImpl implements AccountService {
    private static Logger logger = LogManager.getLogger(AccountService.class);

    @Autowired
    private CompanyService companyService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private AnalyticsService analyticsService;

    @Autowired
    private ActivationRequestRepository activationRequestRepository;

    @Autowired
    private AccountRepository accountRepository;


    @Override
    public Account getReference(Object identifier) {
        return this.accountRepository.getEntityManager().getReference(Account.class, identifier);
    }

    @Override
    public Object getReference(Class type, Object identifier) {
        return this.accountRepository.getEntityManager().getReference(type, identifier);
    }

    @Override
    public Account findByEmail(String email) {
        Assert.notNull(email);
        return this.accountRepository.findByEmail(email);
    }

    @Override
    public Account findByAccessToken(String accessToken) {
        Assert.notNull(accessToken);
        return this.accountRepository.findByAccessToken(accessToken);
    }

    @Override
    @Transactional
    public String execute(Connection<?> connection) {
        UserProfile userProfile = connection.fetchUserProfile();
        String email = userProfile.getEmail();
        String fullName = userProfile.getName();
        String firstName = userProfile.getFirstName();
        String lastName = userProfile.getLastName();

        // Resolve middle name
        String middleName = fullName;
        middleName = middleName.replace(firstName, "");
        middleName = middleName.replace(lastName, "");
        middleName = middleName.trim();
        middleName = (middleName.isEmpty() ? null : middleName);

        // Save account. If a partial account exists, then fill in additional data
        Account account = null;

        if (email != null && !email.isEmpty()) {
            account = this.accountRepository.findByEmail(email);
        }

        if (account == null) {
            account = new Account();
            account.setUuid(AccountService.generateUUID());
            account.setAccessToken(GeneralUtils.getRandomUUID());
            account.setEmail(email);
        }

        // Overwrite existing name with Facebook data (in case a partial account existed already)
        account.setFirstName(firstName);
        account.setMiddleName(middleName);
        account.setLastName(lastName);
        account.setCreated(new Date());
        account.setFacebookProfileId(Long.parseLong(connection.getKey().getProviderUserId()));
        account.setEnabled(true);

        account.add(this.accountRepository.getEntityManager().getReference(Role.class, AccountService.ROLE_USER_ID)); // Add ROLE_USER as default
        Account newAccount = this.createAndFlush(account);

        return String.valueOf(newAccount.getId());
    }

    @Override
    public Account find(int id) {
        return this.accountRepository.findOne(id);
    }

    @Override
    @Transactional
    public Account save(Account account) {
        if (account.getUuid() == null) {
            account.setUuid(AccountService.generateUUID());
        }

        if (account.getAccessToken() == null) {
            account.setAccessToken(GeneralUtils.getRandomUUID());
        }

        return this.accountRepository.save(account);
    }

    @Override
    @Transactional
    public Account saveAndFlush(Account account) {
        if (account.getUuid() == null) {
            account.setUuid(AccountService.generateUUID());
        }

        if (account.getAccessToken() == null) {
            account.setAccessToken(GeneralUtils.getRandomUUID());
        }

        return this.accountRepository.saveAndFlush(account);
    }

    @Override
    @Transactional
    public Account createAndFlush(Account account) {
        if (account.getUuid() == null) {
            account.setUuid(AccountService.generateUUID());
        }

        if (account.getAccessToken() == null) {
            account.setAccessToken(GeneralUtils.getRandomUUID());
        }

        Account newAccount = this.saveAndFlush(account);

        // Ignore analytics error
        try {
            this.analyticsService.identify(newAccount.getUuid(), new Traits()
                    .put("firstName", newAccount.getFirstName())
                    .put("middleName", newAccount.getMiddleName())
                    .put("lastName", newAccount.getLastName())
                    .put("email", newAccount.getEmail())
                    .put("createdAt", newAccount.getCreated().getTime())
                    .put("avatar", AccountService.getProfilePictureUrl(newAccount.getFacebookProfileId())));

            Traits traits = new Traits();
            traits.put("name", "Regular Users");
            this.analyticsService.group(newAccount.getUuid(), String.valueOf(AccountService.GROUP_USER_ID), traits);
        } catch (Exception e) {
            logger.fatal(String.format("Error triggering events on sign up for user (ID: %d)", newAccount.getId()), e);
        }

        return newAccount;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Account account = null;

        if (email != null && !email.isEmpty()) {
            account = this.accountRepository.findByEmailForSignIn(email);
        }

        if (account == null) {
            throw new UsernameNotFoundException("Could not find user");
        }

        return account;
    }

    @Override
    @Transactional
    public Account create(String name, String email, String clearTextPassword) {
        Assert.notNull(name);
        Assert.isTrue(!name.isEmpty());

        Assert.notNull(email);
        Assert.isTrue(!email.isEmpty());

        Assert.notNull(clearTextPassword);
        Assert.isTrue(!clearTextPassword.isEmpty());

        // Lookup existing account by e-mail
        Account existing = this.accountRepository.findByEmail(email);
        Account newAccount = null;

        if (existing != null && existing.getPassword() != null) {
            // Cannot create a new account because an account with a password already exists for this e-mail address
            throw new AccountAlreadyExistsException("An account with this e-mail address already exists");
        }

        if (existing == null) {
            // Create partial account
            Account account = new Account();
            account.setUuid(AccountService.generateUUID());
            account.setAccessToken(GeneralUtils.getRandomUUID());
            account.setEmail(email);
            account.setEnabled(false);
            account.add(this.accountRepository.getEntityManager().getReference(Role.class, AccountService.ROLE_USER_ID)); // Add ROLE_USER as default

            newAccount = this.accountRepository.save(account);
        }

        // Create activation request
        Map<String, String> parts = AccountUtils.getNameParts(name);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, 7); // Add 7 days to current time

        ActivationRequest activationRequest = new ActivationRequest();
        activationRequest.setToken(GeneralUtils.getRandomUUID());
        activationRequest.setPassword(this.encryptPassword(clearTextPassword));
        activationRequest.setFirstName(parts.get(AccountUtils.FIRST_NAME));
        activationRequest.setMiddleName(parts.get(AccountUtils.MIDDLE_NAME));
        activationRequest.setLastName(parts.get(AccountUtils.LAST_NAME));
        activationRequest.setExpires(calendar.getTime());
        activationRequest.setAccount((newAccount != null) ? newAccount : existing);
        this.activationRequestRepository.save(activationRequest);

        /***** Trigger events *****/
        Account referencedAccount = activationRequest.getAccount();
        String analyticsFirstName = ((referencedAccount.getMiddleName() != null) ? referencedAccount.getFirstName() + " " + referencedAccount.getMiddleName() : referencedAccount.getFirstName());

        // Identify
        this.analyticsService.identify(referencedAccount.getUuid(), new Traits()
                .put("firstName", analyticsFirstName)
                .put("lastName", referencedAccount.getLastName())
                .put("email", referencedAccount.getEmail())
                .put("createdAt", referencedAccount.getCreated().getTime()));

        // Group
        Traits traits = new Traits();
        traits.put("name", "Regular Users");
        this.analyticsService.group(referencedAccount.getUuid(), String.valueOf(AccountService.GROUP_USER_ID), traits);

        // Signed up
        Props props = new Props();
        props.put("token", activationRequest.getToken());
        this.analyticsService.track(referencedAccount.getUuid(), "User Signed Up (e-mail)", props);

        return activationRequest.getAccount();
    }

    @Override
    public String encryptPassword(String clearTextPassword) {
        return this.passwordEncoder.encode(clearTextPassword);
    }

    @Override
    @Transactional
    public boolean activate(String email, String token) {
        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.DESC, "id"));
        Pageable pageable = new PageRequest(0, 1, sortOrder);
        Slice<ActivationRequest> slice = this.activationRequestRepository.findRequestByEmailAndToken(pageable, email, token);

        if (!slice.hasContent()) {
            throw new InvalidArgumentException("Cannot activate account because the parameters are invalid");
        }

        ActivationRequest request = slice.getContent().get(0);

        if (request.getExpires().before(new Date()) || request.getTimeUsed() != null) {
            return false;
        }

        request.setTimeUsed(new Date());
        Account account = request.getAccount();
        account.setPassword(request.getPassword());
        account.setEnabled(true);

        // Only set name if we have not extracted it from Facebook already
        if (account.getFacebookProfileId() == null) {
            account.setFirstName(request.getFirstName());
            account.setMiddleName(request.getMiddleName());
            account.setLastName(request.getLastName());
        }

        this.accountRepository.save(account);
        this.activationRequestRepository.save(request);

        this.analyticsService.track(account.getUuid(), "Confirmed Account");
        return true;
    }

    @Override
    @Transactional
    public Account createAndClaim(String name, String email, String clearTextPassword, int companyId) {
        Assert.notNull(name);
        Assert.isTrue(!name.isEmpty());

        Assert.notNull(email);
        Assert.isTrue(!email.isEmpty());

        Assert.notNull(clearTextPassword);
        Assert.isTrue(!clearTextPassword.isEmpty());

        // Lookup existing account by e-mail
        Account existing = this.accountRepository.findByEmail(email);
        Account newAccount = null;

        if (existing != null && existing.getPassword() != null) {
            // Cannot create a new account because an account with a password already exists for this e-mail address
            throw new AccountAlreadyExistsException("An account with this e-mail address already exists");
        }

        if (existing == null) {
            Account account = new Account();
            account.setUuid(AccountService.generateUUID());
            account.setAccessToken(GeneralUtils.getRandomUUID());
            account.setEmail(email);
            account.setEnabled(true);
            account.setPassword(this.encryptPassword(clearTextPassword));
            account.add(this.accountRepository.getEntityManager().getReference(Role.class, AccountService.ROLE_USER_ID)); // Add ROLE_USER as default

            Map<String, String> nameParts = AccountUtils.getNameParts(name);
            account.setFirstName(nameParts.get(AccountUtils.FIRST_NAME));
            account.setMiddleName(nameParts.get(AccountUtils.MIDDLE_NAME));
            account.setLastName(nameParts.get(AccountUtils.LAST_NAME));

            newAccount = this.accountRepository.save(account);
        } else {
            existing.setPassword(this.encryptPassword(clearTextPassword));
        }

        Account account = (existing != null ? existing : newAccount);

        // Trigger analytics events and claim
        String analyticsFirstName = ((account.getMiddleName() != null) ? account.getFirstName() + " " + account.getMiddleName() : account.getFirstName());
        this.analyticsService.identify(account.getUuid(), new Traits()
                .put("firstName", analyticsFirstName)
                .put("lastName", account.getLastName())
                .put("email", account.getEmail())
                .put("createdAt", account.getCreated().getTime()));

        this.analyticsService.track(account.getUuid(), "Signed Up And Claimed (e-mail)");
        this.companyService.claim(companyId, account);

        Hibernate.initialize(account.getRoles());
        Hibernate.initialize(account.getAccountCompanies());
        return account;
    }

    @Override
    @Transactional
    public void addCompanyToFavorites(int companyId) {
        Company company = this.companyService.getForProfileViewById(companyId);

        if (company == null) {
            throw new RuntimeException(String.format("Company not found (ID: %d)!", companyId));
        }

        Account account = AuthUtils.getPrincipal();

        // Here we merge the entity because at this point, the session associated to the entity has been closed,
        // and we want to avoid storing the entire collection (potentially huge!) in the "web session"
        account = this.accountRepository.getEntityManager().merge(account);

        Set<Company> favorites = account.getFavorites();
        favorites.add(company);
        account.setFavorites(favorites);

        this.accountRepository.save(account);

        // Trigger event
        Props props = new Props();
        props.put("company", new Props()
                .put("id", companyId)
                .put("name", company.getName())
                .put("city", company.getCity())
                .put("postalCode", company.getPostalCode())
                .put("profileUrl", company.getProfileUrl())
                .put("email", company.getEmail())
                .put("phoneNumber", company.getPhoneNumber())
                .put("callTrackingNumber", company.getCallTrackingNumber())
                .put("accessToken", company.getAccessToken())
                .put("checksum", CompanyUtils.generateChecksum(companyId))
                .put("industries", CompanyUtils.getIndustryNames(company.getIndustries()))
                .put("foundingDate", (company.getFoundingDate() == null ? null : DateUtils.getFormattedDate("dd-MM-yyyy", company.getFoundingDate()))));

        this.analyticsService.track(account.getUuid(), "Added Company Favorite", props);
    }

    @Override
    @Transactional
    public Account merge(String name, String clearTextPassword, String accessToken, Long checksum) {
        Assert.notNull(name);
        Assert.notNull(clearTextPassword);
        Assert.notNull(accessToken);
        Assert.notNull(checksum);

        Account account = this.accountRepository.findByAccessToken(accessToken);

        if (account == null) {
            throw new AccessDeniedException("Account not found!");
        }

        if (checksum != AccountUtils.generateChecksum(account.getId())) {
            throw new AccessDeniedException(String.format("Invalid checksum (%d)!", checksum));
        }

        if (account.getPassword() != null) {
            throw new RuntimeException(String.format("This account already has a password (ID: %d)", account.getId()));
        }

        Map<String, String> nameParts = AccountUtils.getNameParts(name);
        account.setFirstName(nameParts.get(AccountUtils.FIRST_NAME));
        account.setMiddleName(nameParts.get(AccountUtils.MIDDLE_NAME));
        account.setLastName(nameParts.get(AccountUtils.LAST_NAME));
        account.setPassword(this.encryptPassword(clearTextPassword));
        account.setEnabled(true);

        Account saved = this.accountRepository.save(account);
        Hibernate.initialize(saved.getRoles());
        Hibernate.initialize(saved.getAccountCompanies());

        // Update user
        this.analyticsService.identify(account.getUuid(), new Traits()
                .put("firstName", account.getFirstName())
                .put("lastName", account.getLastName())
                .put("name", name)
                .put("email", account.getEmail())
                .put("createdAt", account.getCreated().getTime()));


        this.analyticsService.track(account.getUuid(), "Merged Account");
        return saved;
    }

    @Override
    public boolean hasAccessToReview(int reviewId, int accountId) {
        return this.reviewService.hasAccessToReview(reviewId, accountId);
    }
}