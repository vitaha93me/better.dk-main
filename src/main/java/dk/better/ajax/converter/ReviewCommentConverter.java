package dk.better.ajax.converter;

import dk.better.account.entity.Account;
import dk.better.account.service.AccountService;
import dk.better.api.resource.AccountResource;
import dk.better.api.resource.CompanyResource;
import dk.better.api.resource.ReviewCommentResource;
import dk.better.application.util.ViewUtils;
import dk.better.company.entity.Company;
import dk.better.company.entity.ReviewComment;
import org.springframework.core.convert.converter.Converter;

/**
 * @author Bo Andersen
 */
public class ReviewCommentConverter implements Converter<ReviewComment, ReviewCommentResource> {
    @Override
    public ReviewCommentResource convert(ReviewComment source) {
        ReviewCommentResource target = new ReviewCommentResource();
        target.setId(source.getId());
        target.setCreated(source.getCreated());
        target.setText(source.getText());

        // Account
        Account account = source.getAccount();
        AccountResource accountResource = new AccountResource();
        accountResource.setId(account.getUuid());
        accountResource.setFirstName(account.getFirstName());
        accountResource.setMiddleName(account.getMiddleName());
        accountResource.setLastName(account.getLastName());

        if (account.getFacebookProfileId() != null) {
            accountResource.setProfilePicture(AccountService.getProfilePictureUrl(account.getFacebookProfileId()));
        } else {
            accountResource.setProfilePicture("https://better.dk/images/default-account-photo.png"); // todo: make this dynamic
        }

        target.setAccount(accountResource);

        // Company
        if (source.getCompany() != null) {
            Company company = source.getCompany();
            CompanyResource companyResource = new CompanyResource();
            companyResource.setId(company.getId());
            companyResource.setName(company.getName());
            companyResource.setProfileUrl(company.getProfileUrl());
            String baseUrl = ViewUtils.getStaticResourceUrl();

            if (company.getLogoName() != null) {
                companyResource.setLogoUrl(baseUrl + "/" + company.getLogoName());
            } else {
                companyResource.setLogoUrl(baseUrl + "/companies/logos/default/default.png");
            }

            target.setCompany(companyResource);
        }

        return target;
    }
}