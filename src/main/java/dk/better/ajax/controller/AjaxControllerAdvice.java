package dk.better.ajax.controller;

import dk.better.account.service.exception.AccountAlreadyExistsException;
import dk.better.ajax.annotation.AjaxController;
import dk.better.api.model.Response;
import dk.better.api.model.ResponseEntity;
import dk.better.api.model.ValidationResponseEntry;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.exception.NotAuthenticatedException;
import dk.better.application.exception.ResourceNotFoundException;
import dk.better.application.exception.UniqueConstraintViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Bo Andersen
 */
@ControllerAdvice(annotations = AjaxController.class)
public class AjaxControllerAdvice {
    private static Logger logger = LogManager.getLogger(AjaxControllerAdvice.class);

    private static final String ERROR_INTERNAL = "error.internal";
    private static final String ERROR_NOT_AUTHENTICATED = "error.not.authenticated";
    private static final String ERROR_NOT_AUTHORIZED = "error.not.authorized";
    private static final String ERROR_UNIQUE_CONSTRAINT_VIOLATION = "error.unique.constraint.violation";
    private static final String ERROR_INVALID_ARGUMENTS = "error.invalid.arguments";
    private static final String ERROR_RESOURCE_NOT_FOUND = "error.resource.not.found";

    @Autowired
    private MessageSource messageSource;


    @ResponseBody
    @ExceptionHandler(NotAuthenticatedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Response notAuthenticated(NotAuthenticatedException ex) {
        logger.error("AJAX call not authenticated", ex);
        return new Response(ERROR_NOT_AUTHENTICATED, ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Response resourceNotFound(ResourceNotFoundException ex) {
        logger.error("Resource not found", ex);
        return new Response(ERROR_RESOURCE_NOT_FOUND, ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(UniqueConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response uniqueConstraintViolation(UniqueConstraintViolationException ex) {
        logger.error("Unique constraint violation", ex);
        return new Response(ERROR_UNIQUE_CONSTRAINT_VIOLATION, ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(InvalidArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response illegalArgument(InvalidArgumentException ex) {
        logger.error("Invalid argument", ex);

        if (ex.getValue() != null) {
            Response response = new Response();
            response.setCode(ERROR_INVALID_ARGUMENTS);

            List<ResponseEntity> entries = new ArrayList<>(1);
            entries.add(new ValidationResponseEntry(null, ex.getMessage(), null, ex.getValue().toString()));
            response.setEntries(entries);

            return response;
        } else {
            return new Response(ERROR_INVALID_ARGUMENTS, ex.getMessage());
        }
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response argumentValidationError(MethodArgumentNotValidException ex) {
        // This exception is thrown if binding of an object failed

        logger.error("Object binding error", ex);
        return new Response(ERROR_INVALID_ARGUMENTS, ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Response accessDenied(AccessDeniedException ex) {
        logger.error("Access denied", ex);
        return new Response(ERROR_NOT_AUTHORIZED, ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response bindingError(BindException ex) {
        List<ResponseEntity> entries = new ArrayList<>();

        for (ObjectError globalError : ex.getGlobalErrors()) {
            entries.add(new ValidationResponseEntry(null, globalError.getCode()));
        }

        for (FieldError fieldError : ex.getFieldErrors()) {
            entries.add(new ValidationResponseEntry(fieldError.getField(), fieldError.getDefaultMessage()));
        }

        Response response = new Response();
        response.setCode(ERROR_INVALID_ARGUMENTS);
        response.setEntries(entries);

        return response;
    }

    @ResponseBody
    @ExceptionHandler({ AccountAlreadyExistsException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response globalErrors(Exception ex) {
        List<ResponseEntity> entries = new ArrayList<>();
        entries.add(new ValidationResponseEntry(null, ex.getMessage()));

        Response response = new Response();
        response.setCode(ERROR_INVALID_ARGUMENTS);
        response.setEntries(entries);

        return response;
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Response general(Exception ex) {
        logger.error("Internal server error", ex);
        String message = ex.getMessage();

        if (message == null || message.isEmpty()) {
            message = this.messageSource.getMessage("ajax.internal.error", null, new Locale("dk")); // improve: read from config
        }

        return new Response(ERROR_INTERNAL, message); // todo: don't expose message to client when not in development mode
    }
}