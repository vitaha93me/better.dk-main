package dk.better.ajax.controller;

import dk.better.account.entity.Account;
import dk.better.account.service.AccountService;
import dk.better.ajax.annotation.AjaxController;
import dk.better.ajax.dto.*;
import dk.better.ajax.util.AjaxUtils;
import dk.better.api.converter.ReviewConverter;
import dk.better.api.resource.ReviewResource;
import dk.better.application.exception.UniqueConstraintViolationException;
import dk.better.application.model.EmptyJsonResponse;
import dk.better.application.util.AuthUtils;
import dk.better.application.util.FileUtils;
import dk.better.company.entity.Page;
import dk.better.company.entity.Review;
import dk.better.company.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.MapBindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * @author Bo Andersen
 */
@AjaxController
@RequestMapping({"/ajax/company"})
public class CompanyController {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private PageService pageService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private SubdomainService subdomainService;
    @Autowired
    private KeywordService keywordService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private CallService callService;
    @Autowired
    private NpsService npsService;

    public CompanyController() {
    }

    @RequestMapping(
            value = {"/claim/{companyId}"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> claimCompany(@PathVariable("companyId") int companyId) {
        AjaxUtils.validateAuthenticated();
        Account account = (Account)AuthUtils.getAuthentication().getPrincipal();
        this.companyService.claim(companyId, account);
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(
            value = {"/{companyId}/update/website"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> updateWebsite(@PathVariable("companyId") int companyId, @Valid @RequestBody UpdateWebsiteDTO dto, BindingResult bindingResult) throws BindException {
        AjaxUtils.validateAuthenticated();
        if(bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            this.companyService.updateWebsite(companyId, dto.getWebsite());
            return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
        }
    }

    @RequestMapping(
            value = {"/{companyId}/update/phone-number"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> updatePhoneNumber(@PathVariable("companyId") int companyId, @Valid @RequestBody UpdatePhoneNumberDTO dto, BindingResult bindingResult) throws BindException {
        AjaxUtils.validateAuthenticated();
        if(bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            this.companyService.updatePhoneNumber(companyId, dto.getPhoneNumber());
            return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
        }
    }

    @RequestMapping(
            value = {"/{companyId}/update/email"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> updateEmail(@PathVariable("companyId") int companyId, @Valid @RequestBody UpdateEmailDTO dto, BindingResult bindingResult) throws BindException {
        AjaxUtils.validateAuthenticated();
        if(bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            this.companyService.updateEmail(companyId, dto.getEmail());
            return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
        }
    }

    @RequestMapping(
            value = {"/{companyId}/update/teaser"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> updateTeaser(@PathVariable("companyId") int companyId, @Valid @RequestBody UpdateTeaserDTO dto, BindingResult bindingResult) throws BindException {
        AjaxUtils.validateAuthenticated();
        if(bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            this.companyService.updateTeaser(companyId, dto.getTeaser());
            return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
        }
    }

    @RequestMapping(
            value = {"/{companyId}/update/industries"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> addIndustries(@PathVariable("companyId") int companyId, @Valid @RequestBody UpdateIndustriesDTO dto, BindingResult bindingResult) throws BindException {
        AjaxUtils.validateAuthenticated();
        if(bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            this.companyService.addIndustries(companyId, dto.getIndustries());
            return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
        }
    }

    @RequestMapping(
            value = {"/{companyId}/update/keywords"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> addKeywords(@PathVariable("companyId") int companyId, @Valid @RequestBody UpdateKeywordsDTO dto, BindingResult bindingResult) throws BindException {
        AjaxUtils.validateAuthenticated();
        if(bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            this.keywordService.addKeywordsToCompany(companyId, dto.getKeywords());
            return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
        }
    }

    @RequestMapping(
            value = {"/{companyId}/propose-changes"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> suggestCompanyChanges(@PathVariable("companyId") int companyId, @Valid @RequestBody ProposeCompanyChangesDTO dto, BindingResult bindingResult) throws BindException {
        if(bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            String changes = dto.getChanges();
            this.companyService.suggestChanges(companyId, changes);
            return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
        }
    }

    @RequestMapping(
            value = {"/{companyId}/request/subdomain"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> requestSubdomain(@PathVariable("companyId") int companyId, @Valid @RequestBody RequestSubdomainDTO dto, BindingResult bindingResult) throws BindException {
        AjaxUtils.validateAuthenticated();
        if(bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            this.subdomainService.addSubdomainRequest(companyId, dto.getSubdomain());
            return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
        }
    }

    @RequestMapping(
            value = {"/{companyId}/upload/cover"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> uploadCoverPhoto(@PathVariable("companyId") int companyId, @ModelAttribute("cover") MultipartFile cover) throws IOException, BindException {
        AjaxUtils.validateAuthenticated();
        HashSet permittedContentTypes = new HashSet();
        permittedContentTypes.add("image/jpeg");
        permittedContentTypes.add("image/png");
        if(cover != null && !cover.isEmpty() && cover.getSize() >= 1L) {
            if(!permittedContentTypes.contains(cover.getContentType())) {
                MapBindingResult coverFile2 = new MapBindingResult(new HashMap(), MultipartFile.class.getName());
                coverFile2.rejectValue("cover", (String)null, this.messageSource.getMessage("error.file.invalid.type", (Object[])null, new Locale("dk")));
                throw new BindException(coverFile2);
            } else {
                File coverFile1 = FileUtils.convert(cover);
                BufferedImage bufferedImage = ImageIO.read(coverFile1);
                int width = bufferedImage.getWidth();
                int height = bufferedImage.getHeight();
                if(width <= 2000 && height <= 2000) {
                    long fileSizeInKilobytes1 = cover.getSize() / 1000L;
                    if(fileSizeInKilobytes1 > 2048L) {
                        MapBindingResult bindingResult = new MapBindingResult(new HashMap(), MultipartFile.class.getName());
                        bindingResult.rejectValue("cover", (String)null, this.messageSource.getMessage("error.file.too-large", (Object[])null, new Locale("dk")));
                        throw new BindException(bindingResult);
                    } else {
                        this.companyService.updateCoverPhoto(companyId, coverFile1);
                        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
                    }
                } else {
                    MapBindingResult fileSizeInKilobytes = new MapBindingResult(new HashMap(), MultipartFile.class.getName());
                    fileSizeInKilobytes.rejectValue("cover", (String)null, this.messageSource.getMessage("error.file.too-large-dimensions", (Object[])null, new Locale("dk")));
                    throw new BindException(fileSizeInKilobytes);
                }
            }
        } else {
            BeanPropertyBindingResult coverFile = new BeanPropertyBindingResult(cover, MultipartFile.class.getName());
            coverFile.reject("cover", this.messageSource.getMessage("error.file.none.chosen", (Object[])null, new Locale("dk")));
            throw new BindException(coverFile);
        }
    }

    @RequestMapping(
            value = {"/{companyId}/upload/logo"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> uploadLogoPhoto(@PathVariable("companyId") int companyId, @ModelAttribute("logo") MultipartFile logo) throws IOException, BindException {
        AjaxUtils.validateAuthenticated();
        HashSet permittedContentTypes = new HashSet();
        permittedContentTypes.add("image/jpeg");
        permittedContentTypes.add("image/png");
        if(logo != null && !logo.isEmpty() && logo.getSize() >= 1L) {
            if(!permittedContentTypes.contains(logo.getContentType())) {
                MapBindingResult coverFile2 = new MapBindingResult(new HashMap(), MultipartFile.class.getName());
                coverFile2.rejectValue("logo", (String)null, this.messageSource.getMessage("error.file.invalid.type", (Object[])null, new Locale("dk")));
                throw new BindException(coverFile2);
            } else {
                File coverFile1 = FileUtils.convert(logo);
                BufferedImage bufferedImage = ImageIO.read(coverFile1);
                int width = bufferedImage.getWidth();
                int height = bufferedImage.getHeight();
                if(width <= 1000 && height <= 1000) {
                    long fileSizeInKilobytes1 = logo.getSize() / 1000L;
                    if(fileSizeInKilobytes1 > 1024L) {
                        MapBindingResult bindingResult = new MapBindingResult(new HashMap(), MultipartFile.class.getName());
                        bindingResult.rejectValue("logo", (String)null, this.messageSource.getMessage("error.file.too-large", (Object[])null, new Locale("dk")));
                        throw new BindException(bindingResult);
                    } else {
                        this.companyService.updateLogo(companyId, coverFile1);
                        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
                    }
                } else {
                    MapBindingResult fileSizeInKilobytes = new MapBindingResult(new HashMap(), MultipartFile.class.getName());
                    fileSizeInKilobytes.rejectValue("logo", (String)null, this.messageSource.getMessage("error.file.too-large-dimensions", (Object[])null, new Locale("dk")));
                    throw new BindException(fileSizeInKilobytes);
                }
            }
        } else {
            BeanPropertyBindingResult coverFile = new BeanPropertyBindingResult(logo, MultipartFile.class.getName());
            coverFile.reject("logo", this.messageSource.getMessage("error.file.none.chosen", (Object[])null, new Locale("dk")));
            throw new BindException(coverFile);
        }
    }

    @RequestMapping(
            value = {"/{companyId}/reviews"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<ReviewResource> addReview(@Valid @RequestBody AddReviewDTO dto, BindingResult bindingResult, @PathVariable("companyId") int companyId) throws BindException {
        if(bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            try {
                Review e = this.reviewService.create(companyId, dto.getRating(), dto.getTitle(), dto.getText(), dto.getName(), dto.getEmail());
                ReviewConverter converter = new ReviewConverter();
                return new ResponseEntity(converter.convert(e), HttpStatus.CREATED);
            } catch (UniqueConstraintViolationException var6) {
                bindingResult.reject(this.messageSource.getMessage("error.review.email-already-in-use", (Object[])null, new Locale("dk")));
                throw new BindException(bindingResult);
            }
        }
    }

    @RequestMapping(
            value = {"/{companyId}/call"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> call(@Valid @RequestBody InitializeCallDTO dto, BindingResult bindingResult, @PathVariable("companyId") int companyId, HttpServletRequest request) throws BindException {
        if(bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            this.callService.call(dto.getFrom(), companyId, request.getRemoteAddr());
            return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
        }
    }

    @RequestMapping(
            value = {"/{companyId}/favorite"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> addFavorite(@PathVariable("companyId") int companyId) {
        AjaxUtils.validateAuthenticated();
        this.accountService.addCompanyToFavorites(companyId);
        return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(
            value = {"/{companyId}/page/add"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> addPage(@RequestBody @Valid AddCompanyPageDTO dto, BindingResult bindingResult, @PathVariable("companyId") int companyId) throws BindException {
        AjaxUtils.validateAuthenticated();
        if(bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            Page page = new Page();
            page.setName(dto.getName());
            page.setTitle(dto.getTitle());
            page.setContent(dto.getContent());
            page.setSlug(dto.getSlug());
            this.pageService.add(page, companyId, dto.getParentId());
            return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
        }
    }

    @RequestMapping(
            value = {"/{companyId}/nps/add"},
            method = {RequestMethod.POST}
    )
    public ResponseEntity<EmptyJsonResponse> addNpsScore(@RequestBody @Valid AddNpsDTO dto, BindingResult bindingResult, @PathVariable("companyId") int companyId, HttpServletRequest request) throws BindException {
        if(bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        } else {
            if(AuthUtils.isAuthenticated()) {
                this.npsService.addScore(companyId, dto.getScore(), request.getRemoteAddr(), AuthUtils.getPrincipal().getId());
            } else {
                this.npsService.addScore(companyId, dto.getScore(), request.getRemoteAddr());
            }

            return new ResponseEntity(new EmptyJsonResponse(), HttpStatus.OK);
        }
    }
}
