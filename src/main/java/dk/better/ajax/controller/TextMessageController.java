package dk.better.ajax.controller;

import dk.better.ajax.annotation.AjaxController;
import dk.better.ajax.dto.SendTextMessageToAllCustomersDTO;
import dk.better.ajax.dto.UpdateTextMessageTextDTO;
import dk.better.ajax.util.AjaxUtils;
import dk.better.application.model.EmptyJsonResponse;
import dk.better.company.service.TextMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * @author Bo Andersen
 */
@AjaxController
@RequestMapping("/ajax/text-message")
public class TextMessageController {
    @Autowired
    private TextMessageService textMessageService;


    @RequestMapping(value = "/company/{companyId}/send-to-all-customers", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> sendToAllCompanyCustomers(@PathVariable("companyId") int companyId, @Valid @RequestBody SendTextMessageToAllCustomersDTO dto, BindingResult bindingResult) throws BindException {
        AjaxUtils.validateAuthenticated();

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        this.textMessageService.sendToAllCustomers(companyId, dto.getText());
        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/company/{companyId}/confirm/{textMessageId}", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> confirm(@PathVariable("companyId") int companyId, @PathVariable("textMessageId") int textMessageId) {
        AjaxUtils.validateIsAdmin();
        this.textMessageService.confirm(companyId, textMessageId);

        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/company/{companyId}/decline/{textMessageId}", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> decline(@PathVariable("companyId") int companyId, @PathVariable("textMessageId") int textMessageId) {
        AjaxUtils.validateIsAdmin();
        this.textMessageService.decline(companyId, textMessageId);

        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/company/{companyId}/update-text/{textMessageId}", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> updateText(@PathVariable("companyId") int companyId, @PathVariable("textMessageId") int textMessageId, @Valid @RequestBody UpdateTextMessageTextDTO dto, BindingResult bindingResult) throws BindException {
        AjaxUtils.validateIsAdmin();

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        this.textMessageService.updateText(companyId, textMessageId, dto.getText());
        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }
}