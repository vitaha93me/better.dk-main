package dk.better.ajax.controller;

import dk.better.ajax.annotation.AjaxController;
import dk.better.ajax.dto.AddSubscriptionDTO;
import dk.better.ajax.dto.UpdateSubdomainDTO;
import dk.better.ajax.util.AjaxUtils;
import dk.better.application.model.EmptyJsonResponse;
import dk.better.company.service.ReviewService;
import dk.better.company.service.SubdomainService;
import dk.better.company.service.SubscriptionService;
import dk.better.company.util.ProductType;
import dk.better.thread.service.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@AjaxController
@RequestMapping("/ajax/admin")
public class AdminController {
    @Autowired
    private SubdomainService subdomainService;

    @Autowired
    private ThreadService threadService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private ReviewService reviewService;


    @RequestMapping(value = "/requests/subdomains/{subdomainRequestId}/accept", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> acceptSubdomainRequest(@PathVariable("subdomainRequestId") int subdomainRequestId) {
        AjaxUtils.validateIsAdmin();
        this.subdomainService.acceptSubdomainRequest(subdomainRequestId);

        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/requests/subdomains/{subdomainRequestId}/decline", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> declineSubdomainRequest(@PathVariable("subdomainRequestId") int subdomainRequestId, @RequestParam(value = "reason", required = false) String reason) {
        AjaxUtils.validateIsAdmin();

        if (reason.isEmpty()) {
            reason = null;
        }

        this.subdomainService.declineSubdomainRequest(subdomainRequestId, reason);
        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/company/{companyId}/subdomains/update", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> updateSubdomain(@PathVariable("companyId") int companyId, @RequestBody @Valid UpdateSubdomainDTO dto, BindingResult bindingResult) throws BindException {
        AjaxUtils.validateIsAdmin();

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        this.subdomainService.updateSubdomain(companyId, dto.getSubdomain());
        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/company/{companyId}/subscriptions/add", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> addSubscription(@PathVariable("companyId") int companyId, @RequestBody @Valid AddSubscriptionDTO dto, BindingResult bindingResult) throws BindException, ParseException {
        AjaxUtils.validateIsAdmin();

        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = formatter.parse(dto.getStartDate());
        Date endDate = formatter.parse(dto.getEndDate());

        this.subscriptionService.subscribe(companyId, ProductType.valueOf(dto.getProductId()), startDate, endDate);
        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/thread/{threadId}/send-email-to-receiver", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> sendEmailToReceiver(@PathVariable("threadId") int threadId) {
        AjaxUtils.validateIsAdmin();
        this.threadService.sendEmailToReceiver(threadId);

        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }

    @RequestMapping(value = "/review/{reviewId}/send-email-to-reviewed-company", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> sendEmailToReviewedCompany(@PathVariable("reviewId") int reviewId) {
        AjaxUtils.validateIsAdmin();
        this.reviewService.sendEmailToReviewedCompany(reviewId);

        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
    }
}