package dk.better.ajax.controller;

import dk.better.account.service.AccountService;
import dk.better.ajax.annotation.AjaxController;
import dk.better.ajax.dto.LandingPageAddReview;
import dk.better.api.converter.ReviewConverter;
import dk.better.api.resource.ReviewResource;
import dk.better.application.exception.UniqueConstraintViolationException;
import dk.better.company.entity.Review;
import dk.better.company.service.ReviewService;
import dk.better.thread.service.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Locale;

/**
 * @author Bo Andersen
 */
@AjaxController("ajaxLandingPageController")
@RequestMapping("/ajax/landing")
public class LandingPageController {
    @Autowired
    private ReviewService reviewService;

    @Autowired
    private ThreadService threadService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private MessageSource messageSource;


    @RequestMapping(value = "/company/{companyId}/reviews", method = RequestMethod.POST)
    public ResponseEntity<ReviewResource> addReview(@Valid @RequestBody LandingPageAddReview dto, BindingResult bindingResult, @PathVariable("companyId") int companyId) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        try {
            Review saved = this.reviewService.create(companyId, dto.getRating(), null, dto.getText(), null, dto.getEmail(), dto.getPhoneNumber());
            Converter<Review, ReviewResource> converter = new ReviewConverter();

            return new ResponseEntity<>(converter.convert(saved), HttpStatus.CREATED);
        } catch (UniqueConstraintViolationException e) {
            bindingResult.reject(this.messageSource.getMessage("error.review.email-already-in-use", null, new Locale("dk"))); // improve: read from config
            throw new BindException(bindingResult);
        }
    }
}