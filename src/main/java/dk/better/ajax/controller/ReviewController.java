package dk.better.ajax.controller;

import dk.better.ajax.annotation.AjaxController;
import dk.better.ajax.dto.CreateReviewCommentDTO;
import dk.better.application.model.EmptyJsonResponse;
import dk.better.company.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * @author Bo Andersen
 */
@AjaxController
@RequestMapping("/ajax/review")
public class ReviewController {
    @Autowired
    private ReviewService reviewService;


    @RequestMapping(value = "/{reviewId}/comments/add", method = RequestMethod.POST)
    public ResponseEntity<EmptyJsonResponse> addCommentAsCompany(@Valid @RequestBody CreateReviewCommentDTO dto, BindingResult bindingResult, @PathVariable("reviewId") int reviewId) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }

        if (dto.getCompanyId() != null) {
            this.reviewService.addCommentAsCompany(reviewId, dto.getText());
        } else {
            throw new RuntimeException("Adding review comments as user is not implemented yet!");
        }
        
        return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.CREATED);
    }
}