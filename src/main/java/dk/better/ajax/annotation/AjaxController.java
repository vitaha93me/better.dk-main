package dk.better.ajax.annotation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Bo Andersen
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Controller
@ResponseBody
public @interface AjaxController {
    String value() default "";
}
