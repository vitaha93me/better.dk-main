package dk.better.ajax.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.Size;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateIndustriesDTO {
    // Each company has at least one industry from CVR, and the max is 4. Leaves a loophole for companies with > 1 industries
    // from CVR, but...
    @Size(min = 1, max = 3)
    private int[] industries;


    public int[] getIndustries() {
        return industries;
    }

    public void setIndustries(int[] industries) {
        this.industries = industries;
    }
}