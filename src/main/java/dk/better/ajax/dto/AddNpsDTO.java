package dk.better.ajax.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Range;

/**
 * @author vitalii.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddNpsDTO {
    @Range(min = 0L, max = 10L)
    private int score;

    public AddNpsDTO() {
    }

    public int getScore() {
        return this.score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}