package dk.better.ajax.dto;

/**
 * @author vitalii.
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class DeactivateIntegrationDTO {
    private int integration;

    public DeactivateIntegrationDTO() {
    }

    public int getIntegration() {
        return this.integration;
    }

    public void setIntegration(int integration) {
        this.integration = integration;
    }
}