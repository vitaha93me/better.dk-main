package dk.better.ajax.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImportCustomersDTO {
    @Size(min = 1, max = 1000)
    private List<String> emails;


    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }
}