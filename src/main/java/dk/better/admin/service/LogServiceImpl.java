package dk.better.admin.service;

import com.amazonaws.services.logs.AWSLogs;
import com.amazonaws.services.logs.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Bo Andersen
 */
@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private AWSLogs awsLogsClient;


    @Override
    public List<OutputLogEvent> getLogs(String logGroupName) {
        List<OutputLogEvent> events = new ArrayList<>();

        // Add log events for all log streams
        for (LogStream logStream : this.getLogStreams(logGroupName)) {
            GetLogEventsRequest request = new GetLogEventsRequest();
            request.setLimit(1000);
            request.setLogGroupName(logGroupName);
            request.setLogStreamName(logStream.getLogStreamName());
            GetLogEventsResult result = this.awsLogsClient.getLogEvents(request);
            events.addAll(result.getEvents());
        }

        // improve: define the direction below (asc/desc)
        // Sort events by timestamp
        Collections.sort(events, new CompareOutputLogEventByTimestampComparator());
        return events;
    }

    @Override
    public List<LogGroup> getLogGroups() {
        DescribeLogGroupsResult result = this.awsLogsClient.describeLogGroups();
        return result.getLogGroups();
    }

    private List<LogStream> getLogStreams(String logGroupName) {
        DescribeLogStreamsRequest request = new DescribeLogStreamsRequest();
        request.setLogGroupName(logGroupName);
        DescribeLogStreamsResult result = this.awsLogsClient.describeLogStreams(request);

        return result.getLogStreams();
    }
}

class CompareOutputLogEventByTimestampComparator implements Comparator<OutputLogEvent> {
    @Override
    public int compare(OutputLogEvent o1, OutputLogEvent o2) {
        long timestamp1 = o1.getTimestamp();
        long timestamp2 = o2.getTimestamp();

        if (timestamp1 < timestamp2) {
            return -1;
        }

        if (timestamp1 == timestamp2) {
            return 0;
        }

        return 1;
    }
}