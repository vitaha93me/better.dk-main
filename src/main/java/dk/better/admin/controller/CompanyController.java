package dk.better.admin.controller;

import dk.better.admin.dto.ImportAllCvrDTO;
import dk.better.admin.dto.RefreshSearchIndexListDTO;
import dk.better.admin.service.CvrService;
import dk.better.application.exception.InvalidArgumentException;
import dk.better.application.exception.ResourceNotFoundException;
import dk.better.application.model.SearchResults;
import dk.better.application.service.ResourceService;
import dk.better.application.util.FileUtils;
import dk.better.application.util.RequestStatus;
import dk.better.company.dto.ProfileUrlsByCiNumbersDTO;
import dk.better.company.entity.*;
import dk.better.company.model.LogoAndBannerUpload;
import dk.better.company.service.CompanyService;
import dk.better.company.service.*;
import dk.better.company.util.CompanyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * @author Bo Andersen
 */
@Controller("adminCompanyController")
@RequestMapping("/admin/company")
public class CompanyController {
    @Value("${aws.s3.internal.bucket.name}")
    private String internalBucketName;

    @Autowired
    private CompanyService companyService;

    //@Autowired
    //private AcknowledgementService acknowledgementService;

    @Autowired
    private IndustryService industryService;

    @Autowired
    private ServiceService serviceService;

    @Autowired
    private CvrService cvrService;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private SubdomainService subdomainService;

    @Autowired
    private TextMessageService textMessageService;

    @Autowired
    private SubscriptionService subscriptionService;


    @RequestMapping("/search")
    public String search(Model model, HttpServletRequest request, @RequestParam(required = false) String q1,
                         @RequestParam(required = false) String q2, @RequestParam(required = false, defaultValue = "1") int page) {
        if (page < 1) {
            throw new InvalidArgumentException("Page must be at least 1");
        }

        String what = q1;
        String where = q2;
        SearchResults<Company> searchResults = null;

        boolean isWhatEmpty = (what == null || what.isEmpty());
        boolean isWhereEmpty = (where == null || where.isEmpty());

        if (!isWhatEmpty && !isWhereEmpty) {
            searchResults = this.companyService.search(what, where, page); // Both
        } else if (isWhereEmpty && !isWhatEmpty) {
            searchResults = this.companyService.search(what, page); // What only
        } else if (isWhatEmpty && !isWhereEmpty) {
            searchResults = this.companyService.searchByLocation(where, page); // Location only
        }

        String currentPageUrl = request.getScheme() + "://" + request.getServerName() + request.getRequestURI() + "?" + request.getQueryString();
        model.addAttribute("currentPageUrl", currentPageUrl);
        model.addAttribute("searchResults", searchResults);
        model.addAttribute("what", what);
        model.addAttribute("where", where);

        return "admin-company-search";
    }

    @RequestMapping(value = "/{companyId}/edit", method = RequestMethod.GET)
    public String edit(Model model, @PathVariable("companyId") int companyId) {
        Company company = this.companyService.findOneByIdForAdminCompanyEdit(companyId);

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with ID %s", companyId));
        }

        //Collection<Integer> acknowledgementIds = (Collection<Integer>) CollectionUtils.collect(company.getAcknowledgements(), new BeanToPropertyValueTransformer("id"));
        Collection<Integer> industryIds = CompanyUtils.getIndustryIds(company.getIndustries());

        // Get acknowledgements by industries
        /*List<Acknowledgement> acknowledgements = new ArrayList<>();

        if (!industryIds.isEmpty()) {
            acknowledgements = this.acknowledgementService.findByIndustryIds(industryIds);
        }*/

        // Get services by industries
        List<Service> services = new ArrayList<>();

        if (!industryIds.isEmpty()) {
            services = this.serviceService.findByIndustryIds(industryIds);
        }

        // Prepare current services for view
        Map<Integer, String> currentServices = new HashMap<>();

        for (dk.better.company.entity.CompanyService companyService : company.getCompanyServices()) {
            currentServices.put(companyService.getService().getId(), companyService.getDescription());
        }

        // Subdomains
        Subdomain currentSubdomain = null;
        Set<Subdomain> subdomains = company.getSubdomains();
        List<Subdomain> previousSubdomains = new ArrayList<>((subdomains.size() > 1) ? (subdomains.size() - 1) : 0);

        for (Subdomain subdomain : subdomains) {
            if (subdomain.isActive()) {
                currentSubdomain = subdomain;
            } else {
                previousSubdomains.add(subdomain);
            }
        }

        model.addAttribute("company", company);
        model.addAttribute("industries", this.industryService.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "name"))));
        //model.addAttribute("acknowledgements", acknowledgements);
        model.addAttribute("services", services);
        //model.addAttribute("currentAcknowledgements", acknowledgementIds);
        model.addAttribute("currentIndustries", industryIds);
        model.addAttribute("currentServices", currentServices);
        model.addAttribute("currentSubdomain", currentSubdomain);
        model.addAttribute("previousSubdomains", previousSubdomains);

        return "admin-company-edit";
    }

    @RequestMapping(value = "/{companyId}/subscriptions", method = RequestMethod.GET)
    public String subscriptions(Model model, @PathVariable("companyId") int companyId, @RequestParam(value = "includeInactive", required = false, defaultValue = "true") boolean includeInactive) {
        Company company = this.companyService.find(companyId);

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with identifier %d found", companyId));
        }

        model.addAttribute("title", String.format("Subscriptions for %s", company.getName()));
        model.addAttribute("includeInactive", includeInactive);
        model.addAttribute("company", company);
        model.addAttribute("subscriptions", this.subscriptionService.getSubscriptions(companyId, includeInactive));

        return "admin-company-subscriptions";
    }

    @RequestMapping(value = "/{companyId}/subscriptions/add", method = RequestMethod.GET)
    public String addsubscription(Model model, @PathVariable("companyId") int companyId) {
        Company company = this.companyService.find(companyId);

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with identifier %d found", companyId));
        }

        model.addAttribute("title", String.format("Add Subscription for %s", company.getName()));
        model.addAttribute("company", company);
        return "admin-company-add-subscription";
    }

    @RequestMapping("{companyId}/edit/images")
    public String editImages(@PathVariable("companyId") int companyId, @RequestParam(value = "success", required = false) String success, Model model) {
        Company company = this.companyService.find(companyId);

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with identifier %d found", companyId));
        }

        model.addAttribute("company", company);
        model.addAttribute("success", (success != null));

        return "admin-company-edit-images";
    }

    @RequestMapping(value = "{companyId}/edit/uploadImages", method = RequestMethod.POST)
    public String uploadCompanyLogoAndOrBanner(@PathVariable("companyId") int companyId, @ModelAttribute("upload") LogoAndBannerUpload upload, BindingResult bindingResult) throws IOException {
        Set<String> permittedContentTypes = new HashSet<>();
        permittedContentTypes.add("image/jpeg");
        permittedContentTypes.add("image/png");
        MultipartFile logo = upload.getLogo();
        MultipartFile coverPhoto = upload.getBanner();

        if (bindingResult.hasErrors()) {
            return "admin-company-edit-images";
        } else if (logo.isEmpty() && coverPhoto.isEmpty()) {
            bindingResult.reject("error.file.none.chosen");
            return "admin-company-edit-images";
        } else {
            // Logo
            if (!logo.isEmpty()) {
                if (!permittedContentTypes.contains(logo.getContentType())) {
                    bindingResult.rejectValue("logo", "error.file.invalid.type");
                } else {
                    this.companyService.updateLogo(companyId, FileUtils.convert(logo));
                }
            }

            // Banner
            if (!coverPhoto.isEmpty()) {
                if (!permittedContentTypes.contains(coverPhoto.getContentType())) {
                    bindingResult.rejectValue("coverPhoto", "error.file.invalid.type");
                } else {
                    this.companyService.updateCoverPhoto(companyId, FileUtils.convert(coverPhoto));
                }
            }
        }

        return "redirect:/admin/company/" + companyId + "/edit/images?success";
    }

    @RequestMapping(value = "/cvr/import", method = RequestMethod.GET)
    public String importAllCompanies(Model model, @RequestParam(value = "success", required = false) String success, @RequestParam(value = "size", required = false) Integer size) {
        model.addAttribute("dto", new ImportAllCvrDTO());
        model.addAttribute("success", (success != null));
        model.addAttribute("size", size);

        return "admin-cvr-import";
    }

    @RequestMapping(value = "/cvr/import", method = RequestMethod.POST)
    public String doImportAllCompanies(@Valid @ModelAttribute("dto") ImportAllCvrDTO dto, BindingResult bindingResult) throws IOException {
        if (bindingResult.hasErrors()) {
            return "admin-cvr-import";
        }

        // improve: check file types and if they could be read...

        File companiesFile = this.resourceService.getObject(this.internalBucketName, dto.getCompaniesCSV());
        File mappingFile = this.resourceService.getObject(this.internalBucketName, dto.getIndustryMapping());

        Resource mappingResource = new FileSystemResource(mappingFile);
        Resource companiesResource = new FileSystemResource(companiesFile);
        int companiesAdded = this.cvrService.importCompanies(companiesResource, mappingResource);

        if (companiesAdded == 0) {
            bindingResult.rejectValue("companiesCSV", "error.cvr.import.no.companies");
            return "cvr-import-all";
        }

        return "redirect:/admin/company/cvr/import?success&size=" + companiesAdded;
    }

    @RequestMapping("/added-since")
    public String showAddedSinceIndex(Model model) {
        model.addAttribute("industries", this.industryService.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "name"))));
        return "admin-companies-added-since";
    }

    @RequestMapping("/added-since/{since}")
    public String showAddedSince(@PathVariable("since") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date since, @RequestParam(value = "industries", required = false) Integer[] industryIds, @RequestParam(value = "page", required = false, defaultValue = "1") int page, Model model) {
        model.addAttribute("industries", this.industryService.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "name"))));

        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.ASC, "id"));
        Pageable pageable = new PageRequest((page - 1), 1000, sortOrder);
        Date dayAfterSince = new Date(since.getTime() + (1000 * 60 * 60 * 24));

        if (industryIds != null && industryIds.length > 0) {
            model.addAttribute("industryIds", new HashSet<>(Arrays.asList(industryIds)));
            model.addAttribute("companies", this.companyService.getCompaniesAddedBetweenInIndustries(pageable, since, dayAfterSince, industryIds));
        } else {
            model.addAttribute("companies", this.companyService.getCompaniesAddedBetween(pageable, since, dayAfterSince));
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(since);
        int year = calendar.get(Calendar.YEAR);
        String month = String.format("%02d", (calendar.get(Calendar.MONTH) + 1));
        String day = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
        model.addAttribute("since", year + "-" + month + "-" + day);

        return "admin-companies-added-since";
    }

    @RequestMapping("/claimed-since")
    public String showClaimedSinceIndex(Model model) {
        model.addAttribute("industries", this.industryService.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "name"))));
        return "admin-companies-claimed-since";
    }

    @RequestMapping("/claimed-since/{since}")
    public String showClaimedSince(@PathVariable("since") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date since, @RequestParam(value = "industries", required = false) Integer[] industryIds, Model model) {
        model.addAttribute("industries", this.industryService.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "name"))));

        if (industryIds != null && industryIds.length > 0) {
            model.addAttribute("industryIds", new HashSet<>(Arrays.asList(industryIds)));
            model.addAttribute("companies", this.companyService.getCompaniesClaimedBetweenInIndustries(since, new Date(), industryIds));
        } else {
            model.addAttribute("companies", this.companyService.getCompaniesClaimedBetween(since, new Date()));
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(since);
        int year = calendar.get(Calendar.YEAR);
        String month = String.format("%02d", (calendar.get(Calendar.MONTH) + 1));
        String day = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH));
        model.addAttribute("since", year + "-" + month + "-" + day);

        return "admin-companies-claimed-since";
    }

    @RequestMapping("/search-index-identifiers")
    public String searchIndexIdentifiers(Model model) {
        model.addAttribute("identifiers", this.companyService.getIdentifiersInSearchIndex(10000L));
        return "admin-company-search-index-identifiers";
    }

    @RequestMapping(value = "/update-search-index-range", method = RequestMethod.GET)
    public String updateSearchIndexRange(@RequestParam(value = "first", required = false) Integer firstId, @RequestParam(value = "last", required = false) Integer lastId, Model model) {
        if (firstId != null && lastId != null) {
            if (firstId > 0 && lastId > 0) {
                this.companyService.refreshSearchIndexForCompanyRange(firstId, lastId);
                model.addAttribute("success", true);
            }
        }

        return "admin-company-update-search-index-range";
    }

    @RequestMapping(value = "/update-search-index-lines", method = RequestMethod.GET)
    public String add(Model model, @RequestParam(value = "success", required = false) String success) {
        model.addAttribute("dto", new RefreshSearchIndexListDTO());
        model.addAttribute("success", (success != null));
        return "admin-company-update-search-index-lines";
    }

    @RequestMapping(value = "/update-search-index-lines", method = RequestMethod.POST)
    public String updateSearchIndexLines(@Valid @ModelAttribute("dto") RefreshSearchIndexListDTO dto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "admin-company-update-search-index-lines";
        }

        String[] companyIds = dto.getCompanyIds().split("\\r\\n");
        List<Integer> companyIdsList = new ArrayList<>(companyIds.length);

        for (String companyId : companyIds) {
            companyIdsList.add(Integer.parseInt(companyId));
        }

        this.companyService.refreshSearchIndex(companyIdsList);
        return "redirect:/admin/company/update-search-index-lines?success";
    }

    @RequestMapping("/requests/subdomains")
    public String viewSubdomainRequests(Model model) {
        // improve: support type param

        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.DESC, "id"));
        Pageable pageable = new PageRequest(0, 10, sortOrder);
        Page<SubdomainRequest> requests = this.subdomainService.getSubdomainRequests(RequestStatus.PENDING, pageable);
        model.addAttribute("requests", requests.getContent());
        return "admin-requests-subdomains";
    }

    @RequestMapping("/requests/keywords")
    public String viewKeywordRequests(@RequestParam(value = "page", required = false, defaultValue = "1") int page, Model model) {
        Pageable pageable = new PageRequest((page - 1), 10);
        Page<Company> results = this.companyService.findCompaniesWithUnapprovedKeywords(pageable);
        model.addAttribute("results", results);
        model.addAttribute("page", page);

        return "admin-requests-keywords";
    }

    @RequestMapping(value = "/ci-numbers-to-profile-urls", method = RequestMethod.GET)
    public String ciNumbersToProfileUrls() {
        return "admin-ci-numbers-to-profile-urls";
    }

    @RequestMapping(value = "/ci-numbers-to-profile-urls", method = RequestMethod.POST)
    public String ciNumbersToProfileUrls(@RequestParam("csv-contents") String csvContents, Model model) {
        List<ProfileUrlsByCiNumbersDTO> mappings = this.companyService.getProfileUrlsByCiNumbers(csvContents);
        model.addAttribute("mappings", mappings);

        return "admin-ci-numbers-to-profile-urls";
    }

    @RequestMapping("/subdomains/update")
    public String updateSubdomainsInIndustry(HttpServletRequest request, @RequestParam(value = "page", required = false, defaultValue = "1") int page, @RequestParam(value = "industryId", required = false) Integer industryId, @RequestParam(value = "with-booking-url", required = false, defaultValue = "false") boolean onlyWithBookingUrl, Model model) {
        if (industryId != null) {
            Pageable pageable = new PageRequest((page - 1), 50);
            Page<Company> results = this.companyService.getCompaniesInIndustryWithoutSubdomain(pageable, industryId, !onlyWithBookingUrl);
            model.addAttribute("results", results);
            model.addAttribute("page", page);
        }

        String currentPageUrl = request.getScheme() + "://" + request.getServerName() + request.getRequestURI() + "?" + request.getQueryString();
        model.addAttribute("currentPageUrl", currentPageUrl);
        model.addAttribute("industryId", industryId);
        model.addAttribute("onlyWithBookingUrl", onlyWithBookingUrl);
        model.addAttribute("industries", this.industryService.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "name"))));

        return "admin-update-subdomains-by-industry";
    }

    @RequestMapping("/text-messages")
    public String pendingTextMessages(@RequestParam(value = "page", defaultValue = "1") int page, Model model) {
        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.DESC, "id"));
        Pageable pageable = new PageRequest((page - 1), 20, sortOrder);
        Page<TextMessage> textMessages = this.textMessageService.getPendingTextMessages(pageable);

        model.addAttribute("title", "Text Messages");
        model.addAttribute("page", page);
        model.addAttribute("textMessages", textMessages);

        return "admin-text-messages";
    }
}