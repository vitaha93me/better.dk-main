package dk.better.admin.controller;

import dk.better.admin.service.LogService;
import dk.better.company.entity.Review;
import dk.better.company.service.ReviewService;
import dk.better.thread.service.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Bo Andersen
 */
@Controller("adminIndexController")
@RequestMapping("/admin")
public class IndexController {
    @Autowired
    private ThreadService threadService;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private LogService logService;


    @RequestMapping("")
    public String index(@Value("${analytics.keen.read-key}") String keenReadKey, @Value("${analytics.keen.project-id}") String keenProjectId, @RequestParam(value = "period", required = false) String period, @RequestParam(value = "affiliate", required = false) String affiliate, Model model) {
        model.addAttribute("affiliate", affiliate);
        model.addAttribute("period", period);
        model.addAttribute("readKey", keenReadKey);
        model.addAttribute("projectId", keenProjectId);
        model.addAttribute("title", "Overblik");

        return "admin-index";
    }

    @RequestMapping("/logs/errors")
    public String errorLogsIndex(Model model) {
        model.addAttribute("logGroups", this.logService.getLogGroups());
        return "admin-error-logs-index";
    }

    @RequestMapping("/logs/errors/{logGroupName}")
    public String displayErrorLogs(@PathVariable("logGroupName") String logGroupName, Model model) {
        model.addAttribute("events", this.logService.getLogs(logGroupName));
        return "admin-display-error-logs";
    }

    @RequestMapping("/leads/threads")
    public String threadsLeads(@RequestParam(value = "page", defaultValue = "1") int page, Model model) {
        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.DESC, "id"));
        Pageable pageable = new PageRequest((page - 1), 20, sortOrder);
        Page<dk.better.thread.entity.Thread> threadPage = this.threadService.getUnreadThreadsLeads(pageable);

        model.addAttribute("title", "Thread leads");
        model.addAttribute("page", page);
        model.addAttribute("threads", threadPage);
        return "admin-leads-threads";
    }

    @RequestMapping("/leads/reviewed")
    public String reviewedLeads(@RequestParam(value = "page", defaultValue = "1") int page, Model model) {
        Sort sortOrder = new Sort(new Sort.Order(Sort.Direction.DESC, "id"));
        Pageable pageable = new PageRequest((page - 1), 20, sortOrder);
        Page<Review> reviewPage = this.reviewService.find(pageable);

        model.addAttribute("title", "Review Leads");
        model.addAttribute("page", page);
        model.addAttribute("reviews", reviewPage);
        return "admin-leads-reviewed";
    }
}