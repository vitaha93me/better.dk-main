package dk.better.admin.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Bo Andersen
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImportAllCvrDTO {
    @NotEmpty
    @Length(min = 2, max = 150)
    private String companiesCSV;

    @NotEmpty
    @Length(min = 2, max = 150)
    private String industryMapping;


    public String getCompaniesCSV() {
        return companiesCSV;
    }

    public void setCompaniesCSV(String companiesCSV) {
        this.companiesCSV = companiesCSV;
    }

    public String getIndustryMapping() {
        return industryMapping;
    }

    public void setIndustryMapping(String industryMapping) {
        this.industryMapping = industryMapping;
    }
}