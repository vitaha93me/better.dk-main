package dk.better.application.security;

import com.github.segmentio.models.Context;
import com.github.segmentio.models.Options;
import dk.better.account.entity.Account;
import dk.better.application.service.AnalyticsService;
import dk.better.application.util.AuthUtils;
import dk.better.application.util.HttpUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.web.ProviderSignInController;
import org.springframework.social.connect.web.ProviderSignInInterceptor;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.request.WebRequest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bo Andersen
 */
@Component
public class RedirectToPreviousPageInterceptor implements ProviderSignInInterceptor<Facebook> {
    private static Logger logger = LogManager.getLogger(RedirectToPreviousPageInterceptor.class);
    private static final String EVENT_LOGGED_IN = "Logged In (Facebook)";

    @Autowired
    private ProviderSignInController providerSignInController;

    @Autowired
    private AnalyticsService analyticsService;

    private String domainName;
    private String httpScheme;


    public RedirectToPreviousPageInterceptor() { }

    public RedirectToPreviousPageInterceptor(ProviderSignInController providerSignInController, AnalyticsService analyticsService, String httpScheme, String domainName) {
        this.providerSignInController = providerSignInController;
        this.analyticsService = analyticsService;
        this.httpScheme = httpScheme;
        this.domainName = domainName;
    }

    @Override
    public void preSignIn(ConnectionFactory<Facebook> connectionFactory, MultiValueMap<String, String> multiValueMap, WebRequest webRequest) {
        String redirectTo = webRequest.getHeader("referer");
        String hash = webRequest.getParameter("hash");

        // Check if a hash is provided that can be used to trigger events once signed in
        if (hash != null) {
            hash = hash.trim();

            if (!hash.isEmpty()) {
                // Even though the hash is supplied by the browser, we should be safe (as long as the value is not output anywhere)
                // because the value is appended after the hash tag
                redirectTo += "#" + hash;
            }
        }

        if (redirectTo != null) {
            String expression = "^" + this.httpScheme + "://" + "([a-z0-9\\-\\.])*" + this.domainName;
            Pattern pattern = Pattern.compile(expression);
            Matcher matcher = pattern.matcher(redirectTo);

            if (matcher.find()) {
                webRequest.setAttribute("preLoginPage", redirectTo, WebRequest.SCOPE_SESSION);
            }
        }
    }

    @Override
    public void postSignIn(Connection<Facebook> connection, WebRequest webRequest) {
        Object ref = webRequest.getAttribute("preLoginPage", WebRequest.SCOPE_SESSION);
        String referer = null;

        if (ref != null) {
            referer = ref.toString();
        }

        webRequest.removeAttribute("preLoginPage", WebRequest.SCOPE_SESSION);

        // Track event
        try {
            Account account = (Account) AuthUtils.getAuthentication().getPrincipal();
            Context context = new Context();
            context.put("ip", HttpUtils.getRequest().getRemoteAddr());
            Options options = new Options();

            if (referer != null && !referer.isEmpty()) {
                String[] parts = referer.split("#");
                context.put("referer", parts[0]);
                options.setContext(context);

                this.analyticsService.track(account.getUuid(), EVENT_LOGGED_IN, null, options);
            } else {
                options.setContext(context);
                this.analyticsService.track(account.getUuid(), EVENT_LOGGED_IN);
            }
        } catch (Exception e) {
            logger.error(String.format("Error triggering '%s' event", EVENT_LOGGED_IN), e);
        }

        // Redirect to referer
        if (referer != null && !referer.isEmpty()) {
            // Here we exploit that the ProviderSignInController bean is a singleton so that we know that we have the correct
            // instance and thus can manipulate the post sign in URL
            this.providerSignInController.setPostSignInUrl(referer);
        }
    }
}