package dk.better.application.model;

import java.util.Map;

/**
 * @author Bo Andersen
 */
public class SearchResultEntry<T> {
    private T entry;
    private Map<String, String> highlights;


    public T getEntry() {
        return entry;
    }

    public void setEntry(T entry) {
        this.entry = entry;
    }

    public Map<String, String> getHighlights() {
        return highlights;
    }

    public void setHighlights(Map<String, String> highlights) {
        this.highlights = highlights;
    }
}