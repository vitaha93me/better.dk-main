package dk.better.application.util;

import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

/**
 * @author Bo Andersen
 * @see http://stackoverflow.com/questions/7486012/static-classes-in-java
 */
public final class GeneralUtils {
    private GeneralUtils() { }

    /**
     * Gets the root domain based on a request. "example.com" returns "example.com" and "subdomain.example.com" returns "example.com"
     *
     * @param request The HTTP request
     * @return The root domain including the TLD
     */
    public static String getRootDomain(HttpServletRequest request) {
        String serverName = request.getServerName();
        String[] parts = serverName.split("\\.");
        int partsLength = parts.length;

        Assert.isTrue(partsLength >= 2); // Minimum: domain.tld
        return parts[(partsLength - 2)] + "." + parts[(partsLength - 1)];
//        return serverName;
    }

    /**
     * Replaces all occurrences of line breaks within a string with the HTML break line
     *
     * @param input The string to add HTML break lines to
     * @return The input string with HTML break lines instead of line breaks
     */
    public static String nl2br(String input) {
        Scanner scanner = new Scanner(input);
        List<String> lines = new ArrayList<>();

        do {
            lines.add(scanner.nextLine());
        } while (scanner.hasNextLine());

        return String.join("<br />", lines);
    }

    /**
     * Converts a given string into a URL-friendly slug
     *
     * @param value The value for which to generate the slug for
     * @return The slug
     */
    public static String getSlug(String value) {
        Assert.notNull(value);

        value = value.toLowerCase();
        value = value.replace("'", "");
        value = value.replace("-", "");
        value = value.replace(" ", "-");
        value = value.replace("&", "og"); // improve: translate
        value = value.replace(",", "");
        value = value.replace(".", "");
        value = value.replace("/", "-");
        value = value.replace(":", "");

        return value;
    }

    public static String getRandomUUID() {
        return UUID.randomUUID().toString();
    }

    public static boolean isNumeric(String s) {
        if (s == null) {
            return false;
        }

        try {
            Float.parseFloat(s);
            return true;
        } catch (NumberFormatException nfe) { }

        return false;
    }

    public static long crc32(String input) {
        Assert.notNull(input);

        byte[] bytes = input.getBytes();
        Checksum checksum = new CRC32();
        checksum.update(bytes, 0, bytes.length);

        return checksum.getValue();
    }

    public static String nullifyIfEmpty(String value) {
        if (value != null && !"".equals(value)) {
            return value;
        }

        return null;
    }

    public static boolean isNullOrEmpty(String value) {
        return (value == null || "".equals(value));
    }
}