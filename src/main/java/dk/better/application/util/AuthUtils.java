package dk.better.application.util;

import dk.better.account.entity.Account;
import dk.better.account.entity.Role;
import dk.better.account.service.AccountService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author Bo Andersen
 */
public final class AuthUtils {
    private AuthUtils() { }

    /**
     * Checks whether or not the user who made the request is authenticated (and not an anonymous user)
     *
     * @return boolean Whether or not the user is authenticated
     */
    public static boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        GrantedAuthority auth = new SimpleGrantedAuthority(AccountService.ROLE_ANONYMOUS);

        return (authentication != null && !authentication.getAuthorities().contains(auth));
    }

    // improve: document
    public static Authentication getAuthentication() {
        if (isAuthenticated()) {
            return SecurityContextHolder.getContext().getAuthentication();
        } else {
            return null;
        }
    }

    // improve: document
    public static boolean isAdmin() {
        Authentication authentication = getAuthentication();
        Role auth = new Role(AccountService.ROLE_ADMIN);

        return (authentication != null && authentication.getAuthorities().contains(auth));
    }

    // improve: document
    public static Account getPrincipal() {
        if (isAuthenticated()) {
            return (Account) AuthUtils.getAuthentication().getPrincipal();
        } else {
            return null;
        }
    }
}