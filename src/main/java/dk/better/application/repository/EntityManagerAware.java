package dk.better.application.repository;

import javax.persistence.EntityManager;

/**
 * @author Bo Andersen
 */
public interface EntityManagerAware {
    EntityManager getEntityManager();
    void setEntityManager(EntityManager entityManager);
}