package dk.better.application.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import java.util.Locale;

/**
 * @author Bo Andersen
 */
@Configuration
@ComponentScan(basePackages = { "dk.better.application", "dk.better.company", "dk.better.booking", "dk.better.account", "dk.better.thread", "dk.better.api", "dk.better.ajax", "dk.better.admin" })
//@EnableAspectJAutoProxy(proxyTargetClass = true)
public class AppConfig {
    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();

        String[] basenames = { "classpath:messages" };
        messageSource.setBasenames(basenames);
        messageSource.setDefaultEncoding("UTF-8");

        return messageSource;
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
        validator.setValidationMessageSource(this.messageSource());

        return validator;
    }

    @Bean
    public CookieLocaleResolver localeResolver() {
        Locale locale = new Locale("dk"); // improve: externalize this
        CookieLocaleResolver localeResolver = new CookieLocaleResolver();
        localeResolver.setDefaultLocale(locale);

        return localeResolver;
    }

    @Bean
    public PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        String activeProfile = System.getProperty("spring.profiles.active");

        if (activeProfile == null || activeProfile.isEmpty()) {
            throw new RuntimeException("Active profile must be set, e.g. as a JVM parameter: -Dspring.profiles.active=\"development\"");
        }

        Resource[] resources = {
                new ClassPathResource(activeProfile + "/application.properties"),
                new ClassPathResource(activeProfile + "/security.properties"),
                new ClassPathResource(activeProfile + "/persistence.properties"),
                new ClassPathResource(activeProfile + "/analytics.properties"),
                new ClassPathResource(activeProfile + "/aws.properties"),
                new ClassPathResource(activeProfile + "/social.properties"),
                new ClassPathResource(activeProfile + "/phone.properties"),
                new ClassPathResource(activeProfile + "/pusher.properties")
        };

        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
        propertySourcesPlaceholderConfigurer.setLocations(resources);
        propertySourcesPlaceholderConfigurer.setIgnoreUnresolvablePlaceholders(true);

        return propertySourcesPlaceholderConfigurer;
    }
}