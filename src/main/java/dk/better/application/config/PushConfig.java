package dk.better.application.config;

import com.pusher.rest.Pusher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Bo Andersen
 */
@Configuration
public class PushConfig {
    @Value("${pusher.appId}")
    private String appId;

    @Value("${pusher.key}")
    private String key;

    @Value("${pusher.secret}")
    private String secret;


    @Bean
    public Pusher pusher() {
        Pusher pusher = new Pusher(this.appId, this.key, this.secret);
        pusher.setEncrypted(true);

        return pusher;
    }
}