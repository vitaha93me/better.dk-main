package dk.better.application.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.segmentio.Analytics;
import dk.better.application.security.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * @author Bo Andersen
 */
@Configuration
@EnableWebMvc // Equivalent to <mvc:annotation-driven/>
@EnableSpringDataWebSupport // http://docs.spring.io/spring-data/jpa/docs/1.7.1.RELEASE/reference/html/#core.web
@ComponentScan(basePackages = { "dk.better.application.controller", "dk.better.company.controller", "dk.better.booking.controller", "dk.better.account.controller", "dk.better.api.controller", "dk.better.ajax.controller" })
@WebListener
public class WebConfig extends WebMvcConfigurerAdapter implements ServletContextListener {
    @Autowired
    private LocalValidatorFactoryBean validator;

    @Value("${max.upload.size}")
    private long maxUploadSize;

    @Value("${max.upload.memory.size}")
    private int maxInMemorySize;


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/css/**").addResourceLocations("/resources/css/");
        registry.addResourceHandler("/js/**").addResourceLocations("/resources/js/");
        registry.addResourceHandler("/images/**").addResourceLocations("/resources/images/");
        registry.addResourceHandler("/fonts/**").addResourceLocations("/resources/fonts/");
        registry.addResourceHandler("/robots.txt").addResourceLocations("/resources/");
        registry.addResourceHandler("/favicon.ico").addResourceLocations("/resources/");
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(this.jsonMessageConverter());

        StringHttpMessageConverter stringConverter = new StringHttpMessageConverter();
        List<MediaType> mediaTypes = Arrays.asList(
                new MediaType("text", "plain", Charset.forName("UTF-8")),
                new MediaType("text", "html", Charset.forName("UTF-8")),
                new MediaType("application", "javascript", Charset.forName("UTF-8"))
        );
        stringConverter.setSupportedMediaTypes(mediaTypes);
        converters.add(stringConverter);
    }

    @Bean
    public MappingJackson2HttpMessageConverter jsonMessageConverter() {
        return new MappingJackson2HttpMessageConverter(this.objectMapper());
    }

    @Bean
    public ObjectMapper objectMapper() {
        //ObjectMapper objectMapper = new ObjectMapper();
        //objectMapper.registerModule(new MrBeanModule());
        return new ObjectMapper();
    }

    @Bean
    public MultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setDefaultEncoding("UTF-8");
        multipartResolver.setMaxUploadSize(this.maxUploadSize);
        multipartResolver.setMaxInMemorySize(this.maxInMemorySize);

        return multipartResolver;
    }

    @Bean
    public LoginInterceptor loginInterceptor() {
        return new LoginInterceptor();
    }

    @Override
    public Validator getValidator() {
        return this.validator;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this.loginInterceptor())
                .addPathPatterns("/signin/**")
                .addPathPatterns("/login");
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            ServletContext context = servletContextEvent.getServletContext();
            String activeProfile = System.getProperty("spring.profiles.active");

            // Segment
            Resource analyticsResource = new ClassPathResource(activeProfile + "/analytics.properties");
            Properties analyticsProperties = PropertiesLoaderUtils.loadProperties(analyticsResource);
            String analyticsWriteKey = analyticsProperties.getProperty("analytics.segment.write-key");

            if (analyticsWriteKey == null || analyticsWriteKey.isEmpty()) {
                throw new RuntimeException("Analytics write key not configured!");
            }

            context.setAttribute("segmentWriteKey", analyticsWriteKey);

            // Pusher
            Resource pusherResource = new ClassPathResource(activeProfile + "/pusher.properties");
            Properties pusherProperties = PropertiesLoaderUtils.loadProperties(pusherResource);
            String pusherKey = pusherProperties.getProperty("pusher.key");

            if (pusherKey == null || pusherKey.isEmpty()) {
                throw new RuntimeException("Pusher key not configured!");
            }
            context.setAttribute("pusherKey", (Object) pusherKey);
            try {
                String[] fieldsToMap = new String[]{"id", "about", "age_range", "address", "bio", "birthday", "context", "cover", "currency", "devices", "education", "email", "favorite_athletes", "favorite_teams", "first_name", "gender", "hometown", "inspirational_people", "installed", "install_type", "is_verified", "languages", "last_name", "link", "locale", "location", "meeting_for", "middle_name", "name", "name_format", "political", "quotes", "payment_pricepoints", "relationship_status", "religion", "security_settings", "significant_other", "sports", "test_group", "timezone", "third_party_id", "updated_time", "verified", "viewer_can_send_gift", "website", "work"};
                Field field = Class.forName("org.springframework.social.facebook.api.UserOperations").getDeclaredField("PROFILE_FIELDS");
                field.setAccessible(true);
                Field modifiers = field.getClass().getDeclaredField("modifiers");
                modifiers.setAccessible(true);
                modifiers.setInt(field, field.getModifiers() & -17);
                field.set(null, fieldsToMap);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (IOException e) {
            throw new RuntimeException("Could not load properties!", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        Analytics.flush(); // Flush any queued analytics events that have not yet been flushed
    }
}