package dk.better.application.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;
import org.trimou.engine.MustacheEngine;
import org.trimou.engine.MustacheEngineBuilder;
import org.trimou.engine.config.EngineConfigurationKey;
import org.trimou.servlet.locator.ServletContextTemplateLocator;

/**
 * @author Bo Andersen
 */
@Configuration
public class ViewTemplateConfig {
    @Bean
    public TilesViewResolver tilesViewResolver() {
        return new TilesViewResolver();
    }

    @Bean
    public TilesConfigurer tilesConfigurer() {
        TilesConfigurer tilesConfigurer = new TilesConfigurer();
        String[] definitions = new String[] {
            "/WEB-INF/layouts/definitions/main.xml",
            "/WEB-INF/layouts/definitions/application.xml",
            "/WEB-INF/layouts/definitions/company.xml",
            "/WEB-INF/layouts/definitions/account.xml",
            "/WEB-INF/layouts/definitions/admin.xml",
            "/WEB-INF/layouts/definitions/thread.xml"
        };

        tilesConfigurer.setDefinitions(definitions);
        return tilesConfigurer;
    }

    @Bean
    @Profile({ "production", "staging" })
    public MustacheEngine mustacheEngine() {
        return MustacheEngineBuilder
                .newBuilder()
                .addTemplateLocator(new ServletContextTemplateLocator(1, "/WEB-INF/templates", "html"))
                .build();
    }

    @Bean
    @Profile("development")
    public MustacheEngine mustacheEngineDevelopment() {
        return MustacheEngineBuilder
                .newBuilder()
                .addTemplateLocator(new ServletContextTemplateLocator(1, "/WEB-INF/templates", "html"))
                .setProperty(EngineConfigurationKey.TEMPLATE_CACHE_ENABLED, false)
                .build();
    }
}