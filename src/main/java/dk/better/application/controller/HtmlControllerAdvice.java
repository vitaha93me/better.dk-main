package dk.better.application.controller;

import dk.better.application.exception.ResourceNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.WebAttributes;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Globally performs exception handling. As such, exceptions may be raised by
 * any controller, and they will be handled here - provided that they have not
 * been handled in the controller already.
 *
 * @author Bo Andersen
 */
@ControllerAdvice(basePackages = { "dk.better.application", "dk.better.admin", "dk.better.account", "dk.better.company", "dk.better.thread" })
public class HtmlControllerAdvice {
    private static Logger logger = LogManager.getLogger(HtmlControllerAdvice.class);
    public static final String DEFAULT_ERROR_VIEW = "general-error";
    public static final String RESOURCE_NOT_FOUND_ERROR_VIEW = "resource-not-found";

    @ExceptionHandler(value = ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView resourceNotFound(HttpServletRequest request, Exception e) throws Exception {
        logger.info("Resource not found", e);

        // If the exception is annotated with @ResponseStatus, rethrow it and let the framework handle it
        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null) {
            throw e;
        }

        // Otherwise set up and send data to a view
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", request.getRequestURL());
        mav.setViewName(RESOURCE_NOT_FOUND_ERROR_VIEW);

        return mav;
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public void accessDenied(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws Exception {
        logger.info("Access denied", e);

        // If the exception is annotated with @ResponseStatus, rethrow it and let the framework handle it
        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null) {
            throw e;
        }

        // Put exception into request scope (perhaps of use to a view)
        request.setAttribute(WebAttributes.ACCESS_DENIED_403, e);

        // Forward to error page
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/access-denied");
        dispatcher.forward(request, response);
    }

    // improve: make a detailed error page for development, and one without info for production (and switch between profiles)
    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ModelAndView defaultErrorHandler(HttpServletRequest request, Exception e) throws Exception {
        logger.error("General error", e);

        // If the exception is annotated with @ResponseStatus, rethrow it and let the framework handle it
        if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null) {
            throw e;
        }

        // Otherwise set up and send data to a default error view
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", request.getRequestURL());
        mav.setViewName(DEFAULT_ERROR_VIEW);

        return mav;
    }
}