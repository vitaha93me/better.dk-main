package dk.better.application.controller;

import com.github.segmentio.models.Props;
import dk.better.application.exception.ResourceNotFoundException;
import dk.better.application.service.AnalyticsService;
import dk.better.application.util.EventType;
import dk.better.company.entity.Company;
import dk.better.company.service.CompanyService;
import dk.better.company.service.ReviewService;
import dk.better.company.service.SubscriptionService;
import dk.better.company.util.CompanyUtils;
import dk.better.company.util.ProductType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Bo Andersen
 */
@Controller
@RequestMapping("/landing")
public class LandingPageController {
    @Autowired
    private CompanyService companyService;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private AnalyticsService analyticsService;


    @RequestMapping("/review/{companyId}/{phoneNumber}")
    public ModelAndView createReviewMobile(@PathVariable("companyId") int companyId, @PathVariable("phoneNumber") int phoneNumber, Model model, HttpServletRequest request) {
        Company company = this.companyService.getFullWithMeta(companyId);

        if (company == null) {
            throw new ResourceNotFoundException(String.format("No company with ID %d", companyId));
        }

        if (this.subscriptionService.isSubscribed(company.getId(), ProductType.TRIAL) || this.subscriptionService.isSubscribed(company.getId(), ProductType.PAID)) {
            String redirectUrl = this.reviewService.getReviewRedirectUrl(company.getMeta(), request.getHeader("User-Agent"));

            if (redirectUrl != null) {
                Props props = new Props();
                props.put("redirectUrl", redirectUrl);
                props.put("isInternalRedirect", false);
                props.put("company", new Props()
                        .put("id", company.getId())
                        .put("name", company.getName())
                        .put("profileUrl", company.getProfileUrl())
                        .put("email", company.getEmail())
                        .put("phoneNumber", company.getPhoneNumber())
                        .put("callTrackingNumber", company.getCallTrackingNumber())
                        .put("postalCode", company.getPostalCode())
                        .put("city", company.getCity())
                        .put("industries", CompanyUtils.getIndustryNames(company.getIndustries())));
                this.analyticsService.track(AnalyticsService.ANONYMOUS_USER_ID, EventType.REVIEW_REDIRECT.getValue(), props);

                RedirectView rv = new RedirectView(redirectUrl);
                rv.setStatusCode(HttpStatus.TEMPORARY_REDIRECT);
                
                return new ModelAndView(rv);
            }
        }

        model.addAttribute("viewport", "width=device-width, initial-scale=1, maximum-scale=1");
        model.addAttribute("phoneNumber", phoneNumber);
        model.addAttribute("company", company);
        model.addAttribute("title", String.format("Bedøm %s", company.getName()));

        return new ModelAndView("create-review-mobile");
    }
}