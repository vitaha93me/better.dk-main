package dk.better.application.tag;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class DomainNameUri extends TagSupport {
    private static Logger logger = LogManager.getLogger(DomainNameUri.class);
    private boolean includePort = false;

    @Override
    public int doStartTag() throws JspException {
        try {
            ServletRequest request = this.pageContext.getRequest();
            String serverName = request.getServerName();
            String[] parts = serverName.split("\\.");
            int partsLength = parts.length;
            String domain;

            // Get domain name and strip subdomains
            if (partsLength >= 2) {
                domain = parts[partsLength - 2] + "." + parts[partsLength - 1];
            } else {
                domain = serverName; // This should never happen, but let's have a meaningful fallback
            }

            // Build domain name URI
            String uri = request.getScheme() + "://" + domain;

            if (this.includePort) {
                uri += ":" + request.getServerPort();
            }

            this.pageContext.getOut().write(uri);
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
        }

        return super.doStartTag();
    }

    public void setIncludePort(boolean includePort) {
        this.includePort = includePort;
    }
}