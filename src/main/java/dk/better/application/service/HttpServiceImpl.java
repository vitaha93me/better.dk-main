package dk.better.application.service;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Bo Andersen
 */
@Service
public class HttpServiceImpl implements HttpService {
    @Override
    public HttpResponse post(String url, Map<String, Object> parameters) {
        return this.post(url, parameters, HttpService.DEFAULT_ENCODING);
    }

    @Override
    public HttpResponse post(String url, Map<String, Object> parameters, String encoding) {
        Assert.notNull(url);
        Assert.notNull(encoding);
        List<NameValuePair> params = new ArrayList<>(parameters != null ? parameters.size() : 0);

        if (parameters != null) {
            for (Map.Entry<String, Object> entry : parameters.entrySet()) {
                params.add(new BasicNameValuePair(entry.getKey(), entry.getValue().toString()));
            }
        }

        try {
            HttpClient httpClient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new UrlEncodedFormEntity(params, encoding));

            return httpClient.execute(httpPost);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getResponseBody(HttpResponse httpResponse) {
        return this.getResponseBody(httpResponse, HttpService.DEFAULT_ENCODING);
    }

    @Override
    public String getResponseBody(HttpResponse httpResponse, String encoding) {
        try {
            HttpEntity entity = httpResponse.getEntity();

            if (entity != null) {
                InputStream inputStream = entity.getContent();
                StringWriter writer = new StringWriter();
                IOUtils.copy(inputStream, writer, encoding);

                return writer.toString();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return null;
    }
}