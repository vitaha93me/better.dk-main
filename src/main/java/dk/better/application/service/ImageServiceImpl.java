package dk.better.application.service;

import dk.better.application.util.FileUtils;
import org.imgscalr.Scalr;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;

/**
 * @author Bo Andersen
 */
@Service
public class ImageServiceImpl implements ImageService {
    @Override
    public BufferedImage resize(BufferedImage original, Scalr.Method quality, Scalr.Mode mode, int width, int height) {
        return Scalr.resize(original, quality, mode, width, height);
    }

    @Override
    public File compressJpeg(File image, float quality) {
        Assert.notNull(image);
        Assert.isTrue(quality > 0);

        try {
            RenderedImage renderedImage = ImageIO.read(image);

            // Get an ImageWriter for the image format
            Iterator<ImageWriter> writers = ImageIO.getImageWritersBySuffix("jpeg");

            if (!writers.hasNext()) {
                throw new IllegalStateException("No writers found");
            }

            ImageWriter writer = writers.next();

            // Create the ImageWriteParam to compress the image
            ImageWriteParam param = writer.getDefaultWriteParam();
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(quality);

            // The output will be a ByteArrayOutputStream (in memory)
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageOutputStream ios = ImageIO.createImageOutputStream(bos);
            writer.setOutput(ios);
            writer.write(null, new IIOImage(renderedImage, null, null), param);
            ios.flush(); // Otherwise the buffer size will be zero!

            // Save compressed file
            Path tempPath = FileUtils.getTemporaryDirectoryPath();
            File file = new File(tempPath.toString() + "/compressed.jpeg");
            FileImageOutputStream output = new FileImageOutputStream(file);
            writer.setOutput(output);
            writer.write(null, new IIOImage(renderedImage, null, null), param);

            return file;
        } catch (IOException e) {
            throw new RuntimeException("Could not compress image", e);
        }
    }

    @Override
    public File convertToJpeg(File image) {
        try {
            Path tempPath = FileUtils.getTemporaryDirectoryPath();

            // The conversion is handled transparently by the writer's ImageTranscoder
            BufferedImage bufferedImage = ImageIO.read(image);
            File jpegFile = new File(tempPath.toString() + "/converted.jpg");
            ImageIO.write(bufferedImage, "jpg", jpegFile);

            return jpegFile;
        } catch (IOException e) {
            throw new RuntimeException("Could not convert image to JPEG", e);
        }
    }

    @Override
    public BufferedImage removeAlphaChannel(BufferedImage source) {
        BufferedImage convertedImage = new BufferedImage(source.getWidth(), source.getHeight(), BufferedImage.TYPE_INT_RGB);
        convertedImage.getGraphics().drawImage(source, 0, 0, convertedImage.getWidth(), convertedImage.getHeight(), Color.WHITE, null);

        return convertedImage;
    }
}