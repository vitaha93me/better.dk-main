package dk.better.application.service;

import org.apache.http.HttpResponse;

import java.util.Map;

/**
 * @author Bo Andersen
 */
public interface HttpService {
    public static final String DEFAULT_ENCODING = "UTF-8";

    HttpResponse post(String url, Map<String, Object> parameters);
    HttpResponse post(String url, Map<String, Object> parameters, String encoding);

    String getResponseBody(HttpResponse httpResponse);
    String getResponseBody(HttpResponse httpResponse, String encoding);
}