package dk.better.application.service;

import dk.better.account.entity.Account;
import dk.better.account.entity.Role;
import dk.better.account.repository.AccountRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.HashSet;

@Service
public class SpringSecuritySignInAdapter implements SignInAdapter {
    private static Logger logger = LogManager.getLogger(SpringSecuritySignInAdapter.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RememberMeServices rememberMeServices;

    public SpringSecuritySignInAdapter() { }

    public SpringSecuritySignInAdapter(AccountRepository accountRepository, RememberMeServices rememberMeServices) {
        this.accountRepository = accountRepository;
        this.rememberMeServices = rememberMeServices;
    }

    @Override
    public String signIn(String localUserId, Connection<?> connection, NativeWebRequest request) {
        try {
            Account account = this.accountRepository.findByIdForSignIn(Integer.parseInt(localUserId));

            if (account != null) {
                Collection<Role> authorities = account.getRoles();

                if (authorities == null) {
                    authorities = new HashSet<>();
                }

                UsernamePasswordAuthenticationToken upat = new UsernamePasswordAuthenticationToken(account, null, authorities);
                SecurityContextHolder.getContext().setAuthentication(upat);

                // Add remember me cookie
                this.rememberMeServices.loginSuccess((HttpServletRequest) request.getNativeRequest(), (HttpServletResponse) request.getNativeResponse(), upat);
            } else {
                logger.fatal("Could not fetch local user with ID: " + localUserId + " during sign in");
            }
        } catch (Exception e) {
            logger.fatal("Error signing user in (ID: " + localUserId + ")", e);
        }

        return null;
    }
}