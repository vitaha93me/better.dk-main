package dk.better.application.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@Service
public class ResourceServiceImpl implements ResourceService {
    //private static Logger logger = LogManager.getLogger(ResourceServiceImpl.class);

    @Value("${aws.s3.bucket.name}")
    private String s3BucketName;

    //@Value("${aws.cdn.distribution.id}")
    //private String cdnDistributionId;

    @Autowired
    private AmazonS3 storageClient;

    //@Autowired
    //private AmazonCloudFront cdnClient;

    @Override
    public void putObject(String key, File file) throws RuntimeException {
        Assert.notNull(key);
        Assert.notNull(file);
        Assert.isTrue((!key.isEmpty()));

        this.storageClient.putObject(new PutObjectRequest(this.s3BucketName, key, file));
    }

    @Override
    public void putObject(String bucketName, String key, File file) throws RuntimeException {
        Assert.notNull(bucketName);
        Assert.notNull(key);
        Assert.notNull(file);
        Assert.isTrue((!bucketName.isEmpty()));
        Assert.isTrue((!key.isEmpty()));

        this.storageClient.putObject(new PutObjectRequest(bucketName, key, file));
    }

    @Override
    public File getObject(String key) throws IOException {
        Assert.notNull(key);
        Assert.isTrue((!key.isEmpty()));

        return this.fetchObjectAsFile(this.s3BucketName, key);
    }

    @Override
    public File getObject(String bucketName, String key) throws IOException {
        Assert.notNull(bucketName);
        Assert.notNull(key);
        Assert.isTrue((!bucketName.isEmpty()));
        Assert.isTrue((!key.isEmpty()));

        return this.fetchObjectAsFile(bucketName, key);
    }

    private File fetchObjectAsFile(String bucketName, String key) throws IOException {
        GetObjectRequest request = new GetObjectRequest(bucketName, key);
        S3Object object = this.storageClient.getObject(request);
        Path tempDirPath = Paths.get(System.getProperty("java.io.tmpdir"));
        Date date = new Date();
        File file = new File(tempDirPath.toString() + "/" + date.getTime());
        IOUtils.copy(object.getObjectContent(), new FileOutputStream(file));

        return file;
    }

/*@Override
    public void invalidateObjects(Collection<String> items) {
        Date date = new Date();
        String timestamp = String.valueOf(date.getTime());

        Paths paths = new Paths();
        paths.setItems(items);
        paths.setQuantity(items.size());

        InvalidationBatch batch = new InvalidationBatch();
        batch.setCallerReference(timestamp); // http://stackoverflow.com/questions/6514983/what-is-callerreference-for-when-posting-an-invalidation-request-to-amazon-cloud
        batch.setPaths(paths);

        CreateInvalidationRequest request = new CreateInvalidationRequest();
        request.setDistributionId(cdnDistributionId);
        request.setInvalidationBatch(batch);

        // Ignore errors because the file will be updated automatically in the CDN anyways
        try {
            this.cdnClient.createInvalidation(request);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }*/
}