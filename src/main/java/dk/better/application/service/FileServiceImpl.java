package dk.better.application.service;

import dk.better.application.util.FileUtils;
import dk.better.application.util.GeneralUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.zip.GZIPOutputStream;

/**
 * @author Bo Andersen
 */
@Service
public class FileServiceImpl implements FileService {
    @Override
    public File gzip(File source) {
        try {
            Path tempPath = FileUtils.getTemporaryDirectoryPath("gzipped_");
            String fileExtension = FileUtils.getFileExtension(source.getAbsolutePath());
            String compressedFilePath = tempPath.toString() + "/" + GeneralUtils.getRandomUUID() + (fileExtension != null ? "." + fileExtension : "");

            byte[] buffer = new byte[1024];
            FileInputStream sourceInputStream = new FileInputStream(source);
            GZIPOutputStream compressedOutputStream = new GZIPOutputStream(new FileOutputStream(compressedFilePath));
            int bytesRead;

            while ((bytesRead = sourceInputStream.read(buffer)) > 0) {
                compressedOutputStream.write(buffer, 0, bytesRead);
            }

            sourceInputStream.close();
            compressedOutputStream.close();

            return new File(compressedFilePath);
        } catch (IOException e) {
            throw new RuntimeException("Could not gzip file", e);
        }
    }
}