package dk.better.application.exception;

/**
 * @author Bo Andersen
 */
public class InvalidArgumentException extends RuntimeException {
    private Object value;

    public InvalidArgumentException(String message) {
        super(message);
    }

    public InvalidArgumentException(Throwable cause) {
        super(cause);
    }

    public InvalidArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidArgumentException(String message, Object value) {
        super(message);
        this.value = value;
    }

    public InvalidArgumentException(String message, Object value, Throwable cause) {
        super(message, cause);
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}