package dk.better.application.exception;

/**
 * @author Bo Andersen
 */
public class UniqueConstraintViolationException extends RuntimeException {
    private String key;
    private Object value;

    public UniqueConstraintViolationException(String message) {
        super(message);
    }

    public UniqueConstraintViolationException(Throwable cause) {
        super(cause);
    }

    public UniqueConstraintViolationException(String message, Throwable cause) {
        super(message, cause);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}