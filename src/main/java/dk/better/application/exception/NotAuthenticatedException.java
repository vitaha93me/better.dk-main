package dk.better.application.exception;

/**
 * @author Bo Andersen
 */
public class NotAuthenticatedException extends RuntimeException {
    private static final String DEFAULT_MESSAGE = "Authentication is required";

    public NotAuthenticatedException() {
        super(DEFAULT_MESSAGE);
    }

    public NotAuthenticatedException(String message) {
        super(message);
    }

    public NotAuthenticatedException(Throwable cause) {
        super(cause);
    }

    public NotAuthenticatedException(String message, Throwable cause) {
        super(message, cause);
    }
}