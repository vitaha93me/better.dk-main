package dk.better.booking.entity;

import dk.better.account.entity.Account;
import dk.better.booking.entity.converter.BookingTypeConverter;
import dk.better.booking.util.BookingType;
import dk.better.company.entity.Company;
import dk.better.company.entity.Customer;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Bo Andersen
 */
@Entity
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;

    @Column(nullable = false)
    private Date created;

    @Column(name = "appointment_time", nullable = false)
    private Date appointmentTime;

    @Column(nullable = false)
    @Convert(converter = BookingTypeConverter.class)
    private BookingType type;

    @Column(name = "customer_email", nullable = true, length = 100)
    private String customerEmail;

    @Column(name = "customer_first_name", nullable = true, length = 50)
    private String customerFirstName;

    @Column(name = "customer_middle_name", nullable = true, length = 50)
    private String customerMiddleName;

    @Column(name = "customer_last_name", nullable = true, length = 50)
    private String customerLastName;

    @Column(name = "customer_phone_number", nullable = true, length = 15)
    private String customerPhoneNumber;

    @ManyToOne(fetch = FetchType.LAZY, optional = false, targetEntity = Company.class)
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY, optional = true, targetEntity = Account.class, cascade = CascadeType.MERGE)
    private Account account;

    @ManyToOne(fetch = FetchType.LAZY, optional = false, targetEntity = Customer.class, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    private Customer customer;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(Date appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public BookingType getType() {
        return type;
    }

    public void setType(BookingType type) {
        this.type = type;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerMiddleName() {
        return customerMiddleName;
    }

    public void setCustomerMiddleName(String customerMiddleName) {
        this.customerMiddleName = customerMiddleName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}