package dk.better.booking.service;

import dk.better.booking.dto.AddBookingDTO;
import dk.better.booking.util.BookingType;

/**
 * @author Bo Andersen
 */
public interface BookingService {
    void add(AddBookingDTO dto, BookingType type, String apiKey);
}