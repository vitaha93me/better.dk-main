Environment Properties (AWS Beanstalk):
- RDS_* params
- ENVIRONMENT: staging or production


JVM command line options (in AWS Beanstalk): -Dspring.profiles.active="staging"

Enable connection draining in load balancer configuration (30 seconds)

Enable session stickiness in load balancer configuration

Configure "EC2 Instance Health Check" in Beanstalk configuration; set health check url to "/ping"

Install keypair

Configure updates batch size to 30%

Configure auto scaling (do this in .ebextensions?)

TODO: document moving dist/config/* to correct folder on installation