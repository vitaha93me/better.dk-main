START TRANSACTION;

CREATE TABLE persistent_logins
(
  username VARCHAR(64) NOT NULL,
  series VARCHAR(64) PRIMARY KEY,
  token VARCHAR(64) NOT NULL,
  last_used TIMESTAMP NOT NULL
);

CREATE TABLE account
(
  id SERIAL PRIMARY KEY NOT NULL,
  uuid CHAR(36) NOT NULL,
  email VARCHAR(100),
  password CHAR(60),
  first_name VARCHAR(50),
  middle_name VARCHAR(50),
  last_name VARCHAR(50),
  facebook_profile_id BIGINT,
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  is_enabled SMALLINT DEFAULT 1 NOT NULL,
  access_token VARCHAR(36),
  phone_number VARCHAR(15)
);

CREATE TABLE activation_request
(
  id SERIAL PRIMARY KEY NOT NULL,
  token VARCHAR(36) NOT NULL,
  password CHAR(60),
  first_name VARCHAR(50) NOT NULL,
  middle_name VARCHAR(50),
  last_name VARCHAR(50),
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc') NOT NULL,
  expires TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc') NOT NULL,
  time_used TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  account_id INT NOT NULL
);

CREATE TABLE account_company
(
  account_id INT NOT NULL,
  company_id INT NOT NULL,
  is_owner SMALLINT DEFAULT 1 NOT NULL,
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  keen_scoped_key TEXT NOT NULL,
  PRIMARY KEY (account_id, company_id)
);

CREATE TABLE account_role
(
  account_id INT NOT NULL,
  role_id INT NOT NULL,
  PRIMARY KEY (account_id, role_id)
);

CREATE TABLE acknowledgement
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL,
  description VARCHAR NOT NULL,
  image_name VARCHAR(50) NOT NULL
);

CREATE TABLE acknowledgement_industry
(
  acknowledgement_id INT NOT NULL,
  industry_id INT NOT NULL,
  PRIMARY KEY (acknowledgement_id, industry_id)
);

CREATE TABLE company
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(150) COLLATE "C" NOT NULL,
  profile_url VARCHAR(100),
  street_name VARCHAR(50),
  street_number VARCHAR(25),
  phone_number VARCHAR(11),
  call_tracking_number VARCHAR(11),
  teaser VARCHAR(175),
  ci_number INT NOT NULL,
  pnr INT NOT NULL,
  description VARCHAR,
  rating NUMERIC(4,2),
  number_of_reviews INT DEFAULT 0 NOT NULL,
  number_of_likes INT DEFAULT 0 NOT NULL,
  logo_name VARCHAR(75),
  banner_name VARCHAR(75),
  email VARCHAR(100),
  website VARCHAR(100),
  co_name VARCHAR(50),
  additional_address_text VARCHAR(100),
  postal_box VARCHAR(50),
  postal_code INT,
  postal_district VARCHAR(50),
  city_name VARCHAR(50) COLLATE "C",
  municipal VARCHAR(50),
  founding_date DATE,
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  last_modified TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  booking_url VARCHAR(150),
  access_token VARCHAR(36),
  meta_id INT
);

CREATE TABLE company_meta
(
  id SERIAL PRIMARY KEY NOT NULL,
  google_plus_url VARCHAR(100),
  yelp_url VARCHAR(100),
  facebook_url VARCHAR(100),
  krakdk_url VARCHAR(100),
  omdoemmedk_url VARCHAR(100),
  bedoemmelsedk_url VARCHAR(100)
);

CREATE TABLE company_acknowledgement
(
  acknowledgement_id INT NOT NULL,
  company_id INT NOT NULL,
  PRIMARY KEY (acknowledgement_id, company_id)
);

CREATE TABLE company_industry
(
  company_id INT NOT NULL,
  industry_id INT NOT NULL,
  PRIMARY KEY (company_id, industry_id)
);

CREATE TABLE company_service
(
  company_id INT NOT NULL,
  service_id INT NOT NULL,
  description VARCHAR,
  PRIMARY KEY (company_id, service_id)
);

CREATE TABLE account_favorite
(
  company_id INT NOT NULL,
  account_id INT NOT NULL,
  PRIMARY KEY (company_id, account_id)
);

CREATE TABLE customer
(
  id SERIAL PRIMARY KEY NOT NULL,
  uuid VARCHAR(36) NOT NULL,
  phone_number VARCHAR(11),
  email VARCHAR(100),
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  type SMALLINT NOT NULL,
  first_name VARCHAR(50),
  middle_name VARCHAR(50),
  last_name VARCHAR(50),
  company_id INT NOT NULL
);

CREATE TABLE keyword
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(50) COLLATE "C" NOT NULL,
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc')
);

CREATE TABLE company_keyword
(
  company_id INT NOT NULL,
  keyword_id INT NOT NULL,
  is_active SMALLINT DEFAULT 0 NOT NULL,
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  PRIMARY KEY (company_id, keyword_id)
);

CREATE TABLE booking
(
  id SERIAL PRIMARY KEY NOT NULL,
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  appointment_time TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  type SMALLINT NOT NULL,
  customer_email VARCHAR(100),
  customer_first_name VARCHAR(50),
  customer_middle_name VARCHAR(50),
  customer_last_name VARCHAR(50),
  customer_phone_number VARCHAR(15),
  account_id INT,
  company_id INT NOT NULL,
  customer_id INT NOT NULL
);

CREATE TABLE page
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL,
  slug VARCHAR(25) NOT NULL,
  title VARCHAR(50) NOT NULL,
  content TEXT,
  data TEXT, -- todo: change to JSON after finding a sensible way of handling this with JPA
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  last_modified TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  parent_id INT,
  page_type INT NOT NULL,
  company_id INT NOT NULL
);

CREATE TABLE page_revision
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL,
  slug VARCHAR(25) NOT NULL,
  title VARCHAR(50) NOT NULL,
  content TEXT,
  data TEXT, -- todo: change to JSON after finding a sensible way of handling this with JPA
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  last_modified TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  page_id INT NOT NULL,
  account_id INT NOT NULL
);

CREATE TABLE text_message
(
  id SERIAL PRIMARY KEY NOT NULL,
  text VARCHAR(160) NOT NULL,
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  number_of_recipients INT NOT NULL,
  status INT NOT NULL DEFAULT 1,
  sender_company_id INT NOT NULL,
  sender_account_id INT NOT NULL
);

CREATE TABLE text_message_customer
(
  text_message_id INT NOT NULL,
  customer_id INT NOT NULL,
  uuid VARCHAR(36) NOT NULL,
  status_update TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
  status SMALLINT,
  PRIMARY KEY (text_message_id, customer_id)
);

CREATE TABLE industry
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(100) COLLATE "C" NOT NULL
);

CREATE TABLE review
(
  id SERIAL PRIMARY KEY NOT NULL,
  uuid VARCHAR(36) NOT NULL,
  rating SMALLINT NOT NULL,
  text VARCHAR NOT NULL,
  company_id INT NOT NULL,
  account_id INT,
  customer_id INT,
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  title VARCHAR(50)
);

CREATE TABLE review_comment
(
  id SERIAL PRIMARY KEY NOT NULL,
  text TEXT NOT NULL,
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  review_id INT NOT NULL,
  account_id INT NOT NULL,
  company_id INT
);

CREATE TABLE role
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL
);

CREATE TABLE service
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(50) COLLATE "C" NOT NULL,
  default_description VARCHAR NOT NULL,
  slug VARCHAR(50) NOT NULL
);

CREATE TABLE service_industry
(
  service_id INT NOT NULL,
  industry_id INT NOT NULL,
  PRIMARY KEY (service_id, industry_id)
);

CREATE TABLE subdomain
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL,
  is_active SMALLINT NOT NULL,
  time_added TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  company_id INT NOT NULL
);

CREATE TABLE upvote
(
  id SERIAL PRIMARY KEY NOT NULL,
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  account_id INT NOT NULL,
  company_id INT NOT NULL
);

CREATE TABLE userconnection
(
  userid VARCHAR(255) NOT NULL,
  providerid VARCHAR(255) NOT NULL,
  provideruserid VARCHAR(255) NOT NULL,
  rank INT NOT NULL,
  displayname VARCHAR(255),
  profileurl VARCHAR(512),
  imageurl VARCHAR(512),
  accesstoken VARCHAR(512) NOT NULL,
  secret VARCHAR(255),
  refreshtoken VARCHAR(255),
  expiretime BIGINT,
  PRIMARY KEY (userid, providerid, provideruserid)
);

CREATE TABLE vote
(
  id SERIAL PRIMARY KEY NOT NULL,
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  account_id INT NOT NULL,
  review_id INT NOT NULL,
  is_upvote SMALLINT NOT NULL
);

CREATE TABLE participant
(
  id SERIAL PRIMARY KEY NOT NULL,
  account_id INT,
  company_id INT,
  thread_id INT NOT NULL,
  last_activity TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  FOREIGN KEY (account_id) REFERENCES account (id) ON UPDATE CASCADE,
  FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE CASCADE,
  FOREIGN KEY (thread_id) REFERENCES thread (id) ON UPDATE CASCADE
);

CREATE TABLE thread
(
  id SERIAL PRIMARY KEY NOT NULL,
  subject VARCHAR(150) NOT NULL,
  last_activity TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc') NOT NULL,
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  sender_participant_id INT,
  receiver_participant_id INT,
  FOREIGN KEY (sender_participant_id) REFERENCES participant (id) ON UPDATE CASCADE,
  FOREIGN KEY (receiver_participant_id) REFERENCES participant (id) ON UPDATE CASCADE
);

CREATE TABLE message
(
  id SERIAL PRIMARY KEY NOT NULL,
  text VARCHAR NOT NULL,
  ip_address VARCHAR(50),
  thread_id INT NOT NULL,
  sender_id INT NOT NULL,
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  FOREIGN KEY (sender_id) REFERENCES participant (id) ON UPDATE CASCADE,
  FOREIGN KEY (thread_id) REFERENCES thread (id) ON UPDATE CASCADE
);

CREATE TABLE request_status
(
  id SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(50) NOT NULL
);

CREATE TABLE subdomain_request
(
  id SERIAL PRIMARY KEY NOT NULL,
  requested_subdomain VARCHAR(50) NOT NULL,
  created TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  company_id INT NOT NULL,
  requester_account_id INT NOT NULL,
  request_status_id INT NOT NULL
);

CREATE TABLE call
(
  id SERIAL PRIMARY KEY NOT NULL,
  from_number VARCHAR(15), -- The length allows for most foreign numbers (00[country-code]...)
  to_number VARCHAR(11) NOT NULL,
  start_time TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  end_time TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  connect_time TIMESTAMP WITHOUT TIME ZONE DEFAULT (now() at time zone 'utc'),
  unique_id VARCHAR(25),
  ip_address VARCHAR(50),
  receiver_id INT NOT NULL,
  customer_id INT
);



ALTER TABLE account ADD CONSTRAINT account_email_unique UNIQUE (email);
ALTER TABLE account ADD CONSTRAINT account_uuid_unique UNIQUE (uuid);
CREATE INDEX account_facebook_profile_id_index ON account (facebook_profile_id);
CREATE INDEX account_created_index ON account (created);
CREATE UNIQUE INDEX account_access_token ON account (access_token);

CREATE INDEX activation_request_token_index ON activation_request (token);
CREATE UNIQUE INDEX activation_request_token_account_id_unique ON activation_request (token, account_id);
ALTER TABLE activation_request ADD FOREIGN KEY (account_id) REFERENCES account (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE account_company ADD FOREIGN KEY (account_id) REFERENCES account (id) ON UPDATE CASCADE;
ALTER TABLE account_company ADD FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE CASCADE;

ALTER TABLE account_role ADD FOREIGN KEY (account_id) REFERENCES account (id) ON UPDATE CASCADE;
ALTER TABLE account_role ADD FOREIGN KEY (role_id) REFERENCES role (id) ON UPDATE CASCADE;

ALTER TABLE acknowledgement ADD CONSTRAINT acknowledgement_name_unique UNIQUE (name);

ALTER TABLE acknowledgement_industry ADD FOREIGN KEY (acknowledgement_id) REFERENCES acknowledgement (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE acknowledgement_industry ADD FOREIGN KEY (industry_id) REFERENCES industry (id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE INDEX company_ci_number_index ON company (ci_number);
CREATE INDEX company_email_index ON company (email);
CREATE INDEX company_created_index ON company (created);
CREATE INDEX company_last_modified_index ON company (last_modified);
CREATE INDEX company_name_index ON company (name);
CREATE INDEX company_postal_code_index ON company (postal_code);
CREATE INDEX company_id_city_name_index ON company (id, city_name);
CREATE UNIQUE INDEX company_call_tracking_number_unique ON company (call_tracking_number);
CREATE UNIQUE INDEX company_access_token ON company (access_token);
ALTER TABLE company ADD CONSTRAINT company_pnr_unique UNIQUE (pnr);

CREATE INDEX company_meta_id_index ON company (meta_id);
ALTER TABLE company ADD FOREIGN KEY (meta_id) REFERENCES company_meta (id) ON UPDATE CASCADE;

ALTER TABLE company_acknowledgement ADD FOREIGN KEY (acknowledgement_id) REFERENCES acknowledgement (id) ON UPDATE CASCADE;
ALTER TABLE company_acknowledgement ADD FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE CASCADE;

CREATE INDEX company_industry_industry_id_company_id_index ON company_industry (industry_id, company_id);
ALTER TABLE company_industry ADD FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE CASCADE;
ALTER TABLE company_industry ADD FOREIGN KEY (industry_id) REFERENCES industry (id) ON UPDATE CASCADE;

CREATE INDEX account_favorite_account_id_company_id_index ON account_favorite (account_id, company_id);
ALTER TABLE account_favorite ADD FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE CASCADE;
ALTER TABLE account_favorite ADD FOREIGN KEY (account_id) REFERENCES account (id) ON UPDATE CASCADE;

ALTER TABLE company_service ADD FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE CASCADE;
ALTER TABLE company_service ADD FOREIGN KEY (service_id) REFERENCES service (id) ON UPDATE CASCADE;

ALTER TABLE company_keyword ADD FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE CASCADE;
ALTER TABLE company_keyword ADD FOREIGN KEY (keyword_id) REFERENCES keyword (id) ON UPDATE CASCADE;

ALTER TABLE booking ADD FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE CASCADE;
ALTER TABLE booking ADD FOREIGN KEY (account_id) REFERENCES account (id) ON UPDATE CASCADE;
ALTER TABLE booking ADD FOREIGN KEY (customer_id) REFERENCES customer (id) ON UPDATE CASCADE;

-- todo: fix so that the below unique constraint handles NULLs correctly for parent_id
CREATE UNIQUE INDEX page_slug_company_id_parent_id_unique ON page (slug, company_id, parent_id);
CREATE INDEX page_company_id ON page (company_id);
CREATE INDEX page_slug ON page (slug);
ALTER TABLE page ADD FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE CASCADE;
ALTER TABLE page ADD FOREIGN KEY (parent_id) REFERENCES page (id) ON UPDATE CASCADE;

CREATE INDEX page_revision_page_id ON page_revision (page_id);
CREATE INDEX page_revision_account_id ON page_revision (account_id);
ALTER TABLE page_revision ADD FOREIGN KEY (page_id) REFERENCES page (id) ON UPDATE CASCADE;
ALTER TABLE page_revision ADD FOREIGN KEY (account_id) REFERENCES account (id) ON UPDATE CASCADE;

CREATE INDEX text_message_created_index ON text_message (created);
CREATE INDEX text_message_status_index ON text_message (status);
CREATE INDEX text_message_sender_company_id_index ON text_message (sender_company_id);
CREATE INDEX text_message_sender_account_id_index ON text_message (sender_account_id);
ALTER TABLE text_message ADD FOREIGN KEY (sender_company_id) REFERENCES company (id) ON UPDATE CASCADE;
ALTER TABLE text_message ADD FOREIGN KEY (sender_account_id) REFERENCES account (id) ON UPDATE CASCADE;

ALTER TABLE text_message_customer ADD FOREIGN KEY (text_message_id) REFERENCES text_message (id) ON UPDATE CASCADE;
ALTER TABLE text_message_customer ADD FOREIGN KEY (customer_id) REFERENCES customer (id) ON UPDATE CASCADE;
CREATE INDEX text_message_status_update_index ON text_message_customer (status_update);
CREATE UNIQUE INDEX text_message_uuid_unique ON text_message_customer (uuid);

CREATE UNIQUE INDEX customer_uuid_unique ON customer (uuid);
CREATE INDEX customer_phone_number_index ON customer (phone_number);
CREATE INDEX customer_type_index ON customer (type);
ALTER TABLE customer ADD CONSTRAINT unique_phone_number_company_id UNIQUE (phone_number, company_id);
ALTER TABLE customer ADD CONSTRAINT unique_email_company_id UNIQUE (email, company_id);
ALTER TABLE customer ADD FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE CASCADE;

ALTER TABLE industry ADD CONSTRAINT industry_name_unique UNIQUE (name);
CREATE INDEX industry_name_index ON industry (LOWER(name));

CREATE UNIQUE INDEX review_uuid_unique ON review (uuid);
CREATE INDEX review_account_id_index ON review (account_id);
CREATE INDEX review_company_id_index ON review (company_id);
CREATE INDEX review_customer_id_index ON review (customer_id);
CREATE INDEX review_created_index ON review (created);
ALTER TABLE review ADD FOREIGN KEY (account_id) REFERENCES account (id) ON UPDATE CASCADE;
ALTER TABLE review ADD FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE CASCADE;
ALTER TABLE review ADD FOREIGN KEY (customer_id) REFERENCES customer (id) ON UPDATE CASCADE;

CREATE INDEX review_comment_account_id_index ON review_comment (account_id);
CREATE INDEX review_comment_company_id_index ON review_comment (company_id);
CREATE INDEX review_comment_review_id_index ON review_comment (review_id);
CREATE INDEX review_comment_created_index ON review_comment (created);
ALTER TABLE review_comment ADD FOREIGN KEY (account_id) REFERENCES account (id) ON UPDATE CASCADE;
ALTER TABLE review_comment ADD FOREIGN KEY (review_id) REFERENCES review (id) ON UPDATE CASCADE;
ALTER TABLE review_comment ADD FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE CASCADE;

ALTER TABLE role ADD CONSTRAINT role_name_unique UNIQUE (name);

ALTER TABLE service ADD CONSTRAINT service_name_unique UNIQUE (name);
ALTER TABLE service ADD CONSTRAINT service_slug_unique UNIQUE (slug);

ALTER TABLE service_industry ADD FOREIGN KEY (industry_id) REFERENCES industry (id) ON UPDATE CASCADE;
ALTER TABLE service_industry ADD FOREIGN KEY (service_id) REFERENCES service (id) ON UPDATE CASCADE;

ALTER TABLE subdomain ADD FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE CASCADE;
CREATE UNIQUE INDEX subdomain_name_unique ON subdomain (name);
CREATE INDEX subdomain_company_id_index ON subdomain (company_id);

ALTER TABLE upvote ADD FOREIGN KEY (account_id) REFERENCES account (id) ON UPDATE CASCADE;
ALTER TABLE upvote ADD FOREIGN KEY (company_id) REFERENCES company (id) ON UPDATE CASCADE;
CREATE UNIQUE INDEX upvote_account_id_company_id_unique_index ON upvote (account_id, company_id);
CREATE INDEX upvote_account_id_index ON upvote (account_id);
CREATE INDEX upvote_company_id_index ON upvote (company_id);

CREATE UNIQUE INDEX "UserConnectionRank" ON userconnection (userid, providerid, rank);

ALTER TABLE vote ADD FOREIGN KEY (account_id) REFERENCES account (id) ON UPDATE CASCADE;
ALTER TABLE vote ADD FOREIGN KEY (review_id) REFERENCES review (id) ON UPDATE CASCADE;
CREATE INDEX vote_account_id_index ON vote (account_id);
CREATE INDEX vote_review_id_index ON vote (review_id);

CREATE INDEX participant_account_id_index ON participant (account_id);
CREATE INDEX participant_company_id_index ON participant (company_id);
CREATE INDEX participant_thread_id_index ON participant (thread_id);

CREATE INDEX thread_created_index ON thread (created);
CREATE INDEX thread_last_activity_index ON thread (last_activity);
CREATE INDEX thread_sender_participant_id_index ON thread (sender_participant_id);
CREATE INDEX thread_receiver_participant_id_index ON thread (receiver_participant_id);

CREATE INDEX message_sender_id_index ON message (sender_id);
CREATE INDEX message_thread_id_index ON message (thread_id);

ALTER TABLE request_status ADD CONSTRAINT unique_name UNIQUE (name);

ALTER TABLE call ADD FOREIGN KEY (receiver_id) REFERENCES company (id) on update cascade on delete restrict;
CREATE INDEX call_receiver_id_index ON call (receiver_id);
CREATE INDEX call_from_number_index ON call (from_number);
CREATE INDEX call_to_number_index ON call (to_number);
CREATE INDEX call_end_time_index ON call (end_time);
CREATE INDEX call_customer_id_index ON call (customer_id);

-- Subdomain request
CREATE INDEX subdomain_request_created_index ON subdomain_request (created);
CREATE INDEX subdomain_request_company_id_index ON subdomain_request (company_id);
CREATE INDEX subdomain_request_requestor_account_id_index ON subdomain_request (requester_account_id);
CREATE INDEX subdomain_request_request_status_id_index ON subdomain_request (request_status_id);
ALTER TABLE subdomain_request ADD FOREIGN KEY (company_id) REFERENCES company (id) on update cascade on delete restrict;
ALTER TABLE subdomain_request ADD FOREIGN KEY (requester_account_id) REFERENCES account (id) on update cascade on delete restrict;
ALTER TABLE subdomain_request ADD FOREIGN KEY (request_status_id) REFERENCES request_status (id) on update cascade on delete restrict;
CREATE UNIQUE INDEX subdomain_request_request_company_unique ON subdomain_request (requested_subdomain, company_id);

ALTER SEQUENCE company_id_seq RESTART WITH 1000000;

-- Insert data (improve: move to separate script)
INSERT INTO role (name) VALUES ('ROLE_USER'), ('ROLE_ADMIN'), ('ROLE_DEVELOPER');
INSERT INTO request_status (name) VALUES ('Pending'), ('Approved'), ('Declined');
INSERT INTO account (id, uuid, email, first_name, password, is_enabled) VALUES (99999999, '4a30cebe-dfe7-492a-85b0-f08495c2d615', 'info@better.dk', 'Better', '$2a$12$wCF73wF7pIcjZ1DUnP1gfOfX3el/kMXgwa1P5Dusn18aE7XivXQ3S', 1);
INSERT INTO account_role (account_id, role_id) VALUES (99999999, 1), (99999999, 2);

COMMIT;